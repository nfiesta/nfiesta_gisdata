--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DROP FUNCTION @extschema@.fn_get_interval_points_app(integer, integer)

CREATE OR REPLACE FUNCTION @extschema@.fn_get_interval_points_app
(
	_config_collection	integer,
	_interval_points	integer
)
RETURNS TABLE
(
	step4interval		integer,
	gid_start		integer,
	gid_end			integer,
	interval_points		integer
)
AS
$BODY$
DECLARE
	_total_points		integer;
	_total_points_plot	integer;
	_count_intervals	integer;
BEGIN
	-----------------------------------------------------------------------
	IF _config_collection IS NULL
	THEN
		RAISE EXCEPTION 'Error 01: fn_get_interval_points_app: The input argument _config_collection must not be NULL!';
	END IF;

	IF _interval_points IS NULL OR _interval_points < 0
	THEN
		RAISE EXCEPTION 'Error 02: fn_get_interval_points_app: The input argument _interval_points must not be NULL or a negative value!';
	END IF;
	-----------------------------------------------------------------------
	-- finding total_points for _config_collection [ref_id_layer_points] 
	SELECT total_points FROM @extschema@.t_config_collection WHERE id = _config_collection
	INTO _total_points;
	-----------------------------------------------------------------------
	IF _total_points IS NULL
	THEN
		RAISE EXCEPTION 'Error 03: fn_get_configs4aux_data_app: In configuration for point layer [ID = %] is not filled value total_points!',_config_collection;
	END IF;
	
	IF _total_points = 0
	THEN
		RAISE EXCEPTION 'Error 04: fn_get_interval_points_app: In configuration for point layer [ID = %] must not by filled value 0 in column total_points!',_config_collection;
	END IF;
	-----------------------------------------------------------------------
	-- determining the number of imported points for the point layer from
	-- the configuration 
	SELECT count(*) FROM @extschema@.f_p_plot WHERE config_collection = _config_collection
	INTO _total_points_plot;
	-----------------------------------------------------------------------
	IF _total_points_plot IS NULL OR _total_points_plot = 0
	THEN
		RAISE EXCEPTION 'Error 05: fn_get_interval_points_app: For configuration of layer of points [ID = %] are not imported data in table f_p_plot!',_config_collection;
	END IF;
	-----------------------------------------------------------------------
	IF _total_points_plot != _total_points
	THEN
		RAISE EXCEPTION 'Error 06: fn_get_interval_points_app: For the configuration of the point layer [ID = %], the number of points [%] does not match the number in the total_points field with the number of imported points [%] in the table f_p_plot!',_config_collection,_total_points,_total_points_plot;
	END IF;
	-----------------------------------------------------------------------
	IF _total_points < _interval_points
	THEN
		RAISE EXCEPTION 'Error 07: fn_get_interval_points_app: The specified number of points in the input argument _interval_points [%] is greater than the number of points [%] that the required point layer has [Configuration ID = %] for intersection!',_interval_points,_total_points,_config_collection;
	END IF;
	-----------------------------------------------------------------------
	IF _interval_points = 0
	THEN
		RETURN QUERY EXECUTE
		'
		SELECT
			1 AS step4interval,
			min(gid) AS gid_start,
			max(gid) AS gid_end,
			$1 AS interval_points
		FROM
			@extschema@.f_p_plot WHERE config_collection = $2 
		'
		USING _total_points, _config_collection;
	ELSE
		_count_intervals := ceiling((_total_points)::numeric/(_interval_points)::numeric);
	
		RETURN QUERY EXECUTE
		'
		WITH
		w1 as 	(
			SELECT
				gid,
				ROW_NUMBER () OVER (ORDER BY gid)
			FROM
				@extschema@.f_p_plot WHERE config_collection = $1
			),
		w2 as 	(
			SELECT
				i as i_interval,
				((i * $2) - $2 + 1) AS i_start,
				(i * $2) AS i_end
			FROM
				generate_series(1, $3) AS i
			),
		w3 as 	(
			SELECT
				w1.*,
				w2.*
			FROM
				w1 INNER JOIN w2
			ON
				w1.row_number >= w2.i_start
			AND
				w1.row_number <= w2.i_end
			)
		SELECT
			(i_interval)::integer AS step4interval,
			(min(gid))::integer AS gid_start,
			(max(gid))::integer AS gid_end,
			(count(gid))::integer AS interval_points
		FROM
			w3
		GROUP
			BY i_interval
		ORDER
			BY i_interval
		'
		USING _config_collection, _interval_points, _count_intervals;
	END IF;	
END;
$BODY$
LANGUAGE plpgsql VOLATILE;

ALTER FUNCTION @extschema@.fn_get_interval_points_app(integer,integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_interval_points_app(integer,integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_interval_points_app(integer,integer) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_interval_points_app(integer,integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_interval_points_app(integer,integer) IS
'The function returns an interval list for the point layer according to the set interval interval_points specifying the number of points.';