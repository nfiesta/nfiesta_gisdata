--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- Function: @extschema@.fn_cm_ext_config_function_after_insert_app()

-- DROP FUNCTION @extschema@.fn_cm_ext_config_function_after_insert_app();

CREATE OR REPLACE FUNCTION @extschema@.fn_cm_ext_config_function_after_insert_app()
  RETURNS trigger AS
$BODY$
BEGIN

	if new.active = false
	then
		raise exception 'fn_cm_ext_config_function_after_insert_app: New value of active must not by false!';
	end if;

	if	(
		select count(*) > 1
		from @extschema@.cm_ext_config_function
		where config_function = new.config_function
		and active = true
		)
	then
		raise exception 'fn_cm_ext_config_function_after_insert_app: For config_function = % are 2 or more records where active is true!',new.config_function;
	end if;

	RETURN NEW;
	
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
  
ALTER FUNCTION @extschema@.fn_cm_ext_config_function_after_insert_app() OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_cm_ext_config_function_after_insert_app() TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_cm_ext_config_function_after_insert_app() TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_cm_ext_config_function_after_insert_app() TO public;

COMMENT ON FUNCTION @extschema@.fn_cm_ext_config_function_after_insert_app() IS 'Function checks that for each config_function argument must be only one record with value active = true.';