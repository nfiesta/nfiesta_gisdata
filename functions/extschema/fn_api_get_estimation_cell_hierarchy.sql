--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
-- fn_api_get_estimation_cell_hierarchy
---------------------------------------------------------------------------------------------------
create or replace function @extschema@.fn_api_get_estimation_cell_hierarchy()
returns table
(
	id integer,
	estimation_cell_id integer,
	estimation_cell_collection_id integer,
	estimation_cell_label_cs varchar,
	estimation_cell_description_cs text,
	estimation_cell_label_en varchar,
	estimation_cell_description_en text,
	superior_estimation_cell_id integer
)
as
$body$
begin
		if	(
			select count(*) > 0
			from @extschema@.cm_estimation_cell_collection
			where estimation_cell_collection_lowest in (select cecc.id from @extschema@.c_estimation_cell_collection as cecc where cecc.use4estimates = true)
			)
		then
			raise exception 'Error 01: fn_api_get_estimation_cell_hierarchy: In table c_estimation_cell_collection is not set value false for the some of lowest estimation cell collection!';
		end if;

		return query
		with
		w_cell as materialized	(
								select
										cmec.estimation_cell,
										cmec.estimation_cell_collection
								from
										@extschema@.cm_estimation_cell as cmec
								where
										cmec.estimation_cell_collection in	(
																			select cecc.id from @extschema@.c_estimation_cell_collection as cecc
																			where cecc.use4estimates = true
																			)										
								)
		select
				(row_number() over(order by t1.estimation_cell))::integer as id,
				t1.estimation_cell as estimation_cell_id,
				w_cell.estimation_cell_collection as estimation_cell_collection_id,
				t2.label as estimation_cell_label_cs,
				t2.description as estimation_cell_description_cs,
				t2.label_en as estimation_cell_label_en,
				t2.description_en as estimation_cell_description_en,
				coalesce(t1.estimation_cell_superior,0) as superior_estimation_cell_id
		from
				@extschema@.t_estimation_cell_hierarchy as t1
				inner join w_cell on t1.estimation_cell = w_cell.estimation_cell
				inner join @extschema@.c_estimation_cell as t2 on w_cell.estimation_cell = t2.id;
end;
$body$
language plpgsql volatile;

ALTER FUNCTION @extschema@.fn_api_get_estimation_cell_hierarchy() OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_api_get_estimation_cell_hierarchy() TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_api_get_estimation_cell_hierarchy() TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_api_get_estimation_cell_hierarchy() TO public;

COMMENT ON FUNCTION @extschema@.fn_api_get_estimation_cell_hierarchy() IS
'Funkce vraci tabulku hierarchii cell z tabulky t_estimation_cell_hierarchy. Ve vystupu jsou cely, ktere maji v tabulce c_estimation_cell_collection nastaveno use4estimates = TRUE.';