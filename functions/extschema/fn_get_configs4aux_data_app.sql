--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DROP FUNCTION @extschema@.fn_get_configs4aux_data_app(integer, integer, integer, boolean)

CREATE OR REPLACE FUNCTION @extschema@.fn_get_configs4aux_data_app
(
	_config_collection	integer,
	_gui_version		integer,
	_interval_points	integer DEFAULT 0,
	_recount		boolean DEFAULT FALSE
)
RETURNS TABLE
(
	step			integer,
	config_collection	integer,
	config			integer,
	intersects		boolean,
	step4interval		integer,
	gid_start		integer,
	gid_end			integer	
)
AS
$BODY$
DECLARE
	_ref_id_layer_points		integer;
	_ref_id_total			integer;
	_ref_id_total_categories	integer[];
	_ref_id_total_config_query	integer[];
	_config_function		integer;
	_aggregated			boolean;
	_ext_version_current		integer;
	_ext_version_current_label	text;
	_step4interval_res		integer[];
	_gid_start_res			integer[];
	_gid_end_res			integer[];
	_interval_points_res		integer[];
	_intersects			boolean;
	_config_id_reference		integer;
	_config_id_base			integer;
	_config_collection_base		integer;
	_check				integer;
	_check_exists_ii		boolean;
	_res_step_ii			integer[];
	_res_config_collection_ii	integer[];
	_res_config_ii			integer[];
	_res_intersects_ii		boolean[];
	_res_config_query_ii		integer[];
	_res_step4interval_ii		integer[];
	_res_check_exists_ii		boolean[];
	_res_step			integer[];
	_res_config_collection		integer[];
	_res_config			integer[];
	_res_intersects			boolean[];
	_res_config_query		integer[];
	_res_step4interval		integer[];
	_res_check_exists		boolean[];	
	_q				text;
	_check_1			integer;
	_check_2			integer;
	_check_3			integer;

	-- NEW --
	_ext_version_valid_from				integer;
	_ext_version_valid_until			integer;
	_ext_version_valid_from_label			character varying;
	_ext_version_valid_until_label			character varying;
BEGIN
	-------------------------------------------------------------------------------------------
	IF _config_collection IS NULL
	THEN
		RAISE EXCEPTION 'Error 01: fn_get_configs4aux_data_app: The input argument _config_collection must not by NULL!';
	END IF;

	IF _gui_version IS NULL
	THEN
		RAISE EXCEPTION 'Error 02: fn_get_configs4aux_data_app: The input argument _gui_version must not by NULL!';
	END IF;

	IF _recount IS NULL
	THEN
		RAISE EXCEPTION 'Error 03: fn_get_configs4aux_data_app: The input argument _recount must not by NULL!';
	END IF;

	IF _interval_points IS NULL OR _interval_points < 0
	THEN
		RAISE EXCEPTION 'Error 04: fn_get_configs4aux_data_app: The input argument _interval_points must not by NULL or a negative value!';
	END IF;
	-------------------------------------------------------------------------------------------
	-- finding reference IDs for the point layer and total configuration
	SELECT
		tcc.ref_id_layer_points,
		tcc.ref_id_total
	FROM
		@extschema@.t_config_collection AS tcc
	WHERE
		tcc.id = _config_collection
	INTO
		_ref_id_layer_points,
		_ref_id_total;
	-------------------------------------------------------------------------------------------
	IF _ref_id_layer_points IS NULL
	THEN
		RAISE EXCEPTION 'Error 05: fn_get_configs4aux_data_app: For input argument _config_collection = % not be in configuration found a reference to point layer configuration!',_config_collection;
	END IF;
	-------------------------------------------------------------------------------------------
	IF _ref_id_total IS NULL
	THEN
		RAISE EXCEPTION 'Error 06: fn_get_configs4aux_data_app: For input argument _config_collection = % not be in configuration found a reference to configuration of total!',_config_collection;
	END IF;
	-------------------------------------------------------------------------------------------
	-- found CATEGORY and config_query for TOTAL REFERENCE
	SELECT
		array_agg(tc.id ORDER BY tc.id) AS id,
		array_agg(tc.config_query ORDER BY tc.id) AS config_query
	FROM
		@extschema@.t_config AS tc
	WHERE
		tc.config_collection = _ref_id_total
	INTO
		_ref_id_total_categories,
		_ref_id_total_config_query;
	-------------------------------------------------------------------------------------------
	IF _ref_id_total_categories IS NULL
	THEN
		RAISE EXCEPTION 'Error 07: fn_get_configs4aux_data_app: Pro argument _ref_id_total = % [ID konfigurace uhrnu] nenalezena v konfiguraci [v tabulce t_config] zadna kategorie teto konfigurace!',_ref_id_total;
	END IF;
	-------------------------------------------------------------------------------------------
	IF _ref_id_total_config_query IS NULL
	THEN
		RAISE EXCEPTION 'Error 08: fn_get_configs4aux_data_app: Pro argument _ref_id_total = % [ID konfigurace uhrnu] nenalezena v konfiguraci [v tabulce t_config] zadna hodnota config_query u kategorii teto konfigurace!',_ref_id_total;
	END IF;
	-------------------------------------------------------------------------------------------
	-- detected config_function and aggregated for _ref_id_total
	SELECT
		config_function,
		aggregated
	FROM
		@extschema@.t_config_collection
	WHERE
		id = _ref_id_total
	INTO
		_config_function,
		_aggregated;
	-------------------------------------------------------------------------------------------
	IF _config_function IS NULL
	THEN
		RAISE EXCEPTION 'Error 09: fn_get_configs4aux_data_app: Pro argument _ref_id_total = % [ID konfigurace uhrnu] nenalezena hodnota _config_function u teto konfigurace!',_ref_id_total;
	END IF;
	-------------------------------------------------------------------------------------------
	IF _aggregated IS NULL
	THEN
		RAISE EXCEPTION 'Error 10: fn_get_configs4aux_data_app: Pro argument _ref_id_total = % [ID konfigurace uhrnu] nenalezena hodnota _aggregated u teto konfigurace!',_ref_id_total;
	END IF;
	-------------------------------------------------------------------------------------------
	-- find the latest version of the extension for the input version of the GUI application
	select max(ext_version) FROM @extschema@.cm_ext_gui_version
	WHERE gui_version = _gui_version
	INTO _ext_version_current;

	IF _ext_version_current IS NULL
	THEN
		RAISE EXCEPTION 'Error 11: fn_get_configs4aux_data_app: Pro vstupni argument _gui_version = % nenalezena verze extenze nfiesta_gisdata v mapovaci tabulce cm_ext_gui_version!',_gui_version;
	END IF;

	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_current
	INTO _ext_version_current_label;

	IF _ext_version_current_label IS NULL
	THEN
		RAISE EXCEPTION 'Error 12: fn_get_configs4aux_data_app: Pro argument _ext_version_current = % nenalezena verze extenze v ciselniku c_ext_version!',_ext_version_current;
	END IF;
	-------------------------------------------------------------------------------------------
	-- NEW --
	IF NOT(_config_function = ANY(array[100,200,300,400]))
	THEN
		RAISE EXCEPTION 'Error 13: fn_get_configs4aux_data_app: Interni promenna _config_function = % musi byt hodnota 100, 200, 300 nebo 400!',_config_function;
	END IF;

	IF _config_function = ANY(array[100,200,400])
	THEN
		IF _config_function = 400 THEN _config_function := 200; ELSE _config_function := _config_function; END IF;

		-- zjisteni ext_version_valid_from a ext_version_valid_until
		SELECT cmcf.ext_version_valid_from, cmcf.ext_version_valid_until
		FROM @extschema@.cm_ext_config_function AS cmcf
		WHERE cmcf.active = TRUE
		AND cmcf.config_function = _config_function
		INTO
			_ext_version_valid_from,
			_ext_version_valid_until;		
	ELSE
		-- varianta VxR => _config_function = 300

		WITH
		w1 AS	(
			SELECT cmcf.ext_version_valid_from, cmcf.ext_version_valid_until
			FROM @extschema@.cm_ext_config_function AS cmcf
			WHERE cmcf.active = TRUE
			AND cmcf.config_function = 100
			),
		w2 AS	(
			SELECT cmcf.ext_version_valid_from, cmcf.ext_version_valid_until
			FROM @extschema@.cm_ext_config_function AS cmcf
			WHERE cmcf.active = TRUE
			AND cmcf.config_function = 200
			),
		w3 AS	(
			SELECT * FROM w1 UNION ALL
			SELECT * FROM w2
			)
		SELECT
			max(ext_version_valid_from),
			max(ext_version_valid_until)
		FROM
			w3
		INTO
			_ext_version_valid_from,
			_ext_version_valid_until;
	END IF;	
	
	IF (_ext_version_valid_from IS NULL OR _ext_version_valid_until IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 14: fn_get_configs4aux_data_app: Pro interni promennou _config_function = % nenalezeno ext_version_valid_from nebo ext_version_valid_until v tabulce cm_ext_config_function!',_config_function;
	END IF;

	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_valid_from  INTO _ext_version_valid_from_label;
	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_valid_until INTO _ext_version_valid_until_label;
	-------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------
	-- proces naplneni promennych pro VLAKNA
	-------------------------------------------------------------------------------------------
	-- volani funkce pro ziskani casti pro VLAKNA [v pripade, ze uzivatel nepozaduje vypocet
	-- na vice vlaken, vstupni promenna _interval_points je hodnota 0, pak funkce
	-- fn_get_interval_points_app vrati jeden radek, kde gid_start_res je jedno prvkove pole
	-- a obsahuje min(gid) z bodove vrstvy, u end je to analogicky stejne a jde pak o max]
	-- poznamka: ve funkci je kontrola, ze hodnota total_points ulozena v konfiguraci odpovida realite,
	-- pokud tomu tak je, pak v pripade, ze je _interval_points hodnota 0, funkce vrati hodnotu
	-- _interval_points rovnu hodnote _total_points pro vstupni _ref_id_layer_points
	WITH
	w AS	(
		SELECT * FROM @extschema@.fn_get_interval_points_app(_ref_id_layer_points,_interval_points)
		)
	SELECT
		array_agg(w.step4interval ORDER BY w.step4interval) AS step4interval_res,
		array_agg(w.gid_start ORDER BY w.step4interval) AS gid_start_res,
		array_agg(w.gid_end ORDER BY w.step4interval) AS gid_end_res,
		array_agg(w.interval_points ORDER BY w.step4interval) AS interval_points_res
	FROM
		w
	INTO
		_step4interval_res,
		_gid_start_res,
		_gid_end_res,
		_interval_points_res;
	-----------------------------------------
	IF _step4interval_res IS NULL
	THEN
		RAISE EXCEPTION 'Error 15: fn_get_configs4aux_data_app: Nenaplnena interni promenna _step4interval_res!';
	END IF;
	-----------------------------------------
	IF _gid_start_res IS NULL
	THEN
		RAISE EXCEPTION 'Error 16: fn_get_configs4aux_data_app: Nenaplnena interni promenna _gid_start_res!';
	END IF;
	-----------------------------------------
	IF _gid_end_res IS NULL
	THEN
		RAISE EXCEPTION 'Error 17: fn_get_configs4aux_data_app: Nenaplnena interni promenna _gid_end_res!';
	END IF;
	-----------------------------------------
	IF _interval_points_res IS NULL
	THEN
		RAISE EXCEPTION 'Error 18: fn_get_configs4aux_data_app: Nenaplnena interni promenna _interval_points_res!';
	END IF;	
	-----------------------------------------
	IF	(
		SELECT count(t.*) > 0
		FROM (SELECT unnest(_step4interval_res) AS step4interval_res) AS t
		WHERE t.step4interval_res IS NULL
		)
	THEN
		RAISE EXCEPTION 'Error 19: fn_get_configs4aux_data_app: Interni promenna _step4interval_res obsahuje hodnotu NULL!';
	END IF;
	-----------------------------------------
	IF	(
		SELECT count(t.*) > 0
		FROM (SELECT unnest(_gid_start_res) AS gid_start_res) AS t
		WHERE t.gid_start_res IS NULL
		)
	THEN
		RAISE EXCEPTION 'Error 20: fn_get_configs4aux_data_app: Interni promenna _gid_start_res obsahuje hodnotu NULL!';
	END IF;
	-----------------------------------------
	IF	(
		SELECT count(t.*) > 0
		FROM (SELECT unnest(_gid_end_res) AS gid_end_res) AS t
		WHERE t.gid_end_res IS NULL
		)
	THEN
		RAISE EXCEPTION 'Error 21: fn_get_configs4aux_data_app: Interni promenna _gid_end_res obsahuje hodnotu NULL!';
	END IF;
	-----------------------------------------
	IF	(
		SELECT count(t.*) > 0
		FROM (SELECT unnest(_interval_points_res) AS interval_points_res) AS t
		WHERE t.interval_points_res IS NULL
		)
	THEN
		RAISE EXCEPTION 'Error 22: fn_get_configs4aux_data_app: Interni promenna _interval_points_res obsahuje hodnotu NULL!';
	END IF;	
	-------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------


	-------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------
	-- CYKLUS po jednotlivych konfiguracnich KATEGORIICH
	FOR i IN 1..array_length(_ref_id_total_categories,1)
	LOOP
		-----------------------------------------------------------------------------------
		IF _ref_id_total_config_query[i] IS NULL
		THEN
			RAISE EXCEPTION 'Error 23: fn_get_configs4aux_data_app: Pro argument _ref_id_total = % [ID konfigurace uhrnu] neni u nektere jeji kategorie vyplnena hodnota config_query!',_ref_id_total;
		END IF;
		-----------------------------------------------------------------------------------
		-- proces naplneni promenne _intersects
		IF	(
			_config_function = ANY(array[100,200])	AND
			_ref_id_total_config_query[i] = 100 AND
			_aggregated = FALSE
			)
		THEN
			_intersects := TRUE;
		ELSE
			_intersects := FALSE;
		END IF;
		-----------------------------------------------------------------------------------
		-- proces zjisteni _config_id_base a _config_collection_base
		IF _ref_id_total_config_query[i] = 500 -- REFERENCE
		THEN
			-- config_id_base
			SELECT tc.categories::integer
			FROM @extschema@.t_config AS tc
			WHERE tc.id = _ref_id_total_categories[i]
			INTO _config_id_reference;

			IF _config_id_reference IS NULL
			THEN
				RAISE EXCEPTION 'Error 24: fn_get_configs4aux_data_app: U argumentu _ref_id_total = % [ID konfigurace uhrnu] je jeho i-ta kategorie [_ref_id_total_config_query[i] = %] config_query 500 [reference] a v konfiguraci [v tabulce t_config] nema ve sloupci categories nakonfigurovanou referenci!',_ref_id_total,_ref_id_total_config_query[i];
			END IF;

			_config_id_base := _config_id_reference;

			-- config_collection_base
			SELECT tcc.id FROM @extschema@.t_config_collection AS tcc
			WHERE tcc.ref_id_total = (SELECT tc.config_collection FROM @extschema@.t_config AS tc WHERE tc.id = _config_id_base)
			AND tcc.ref_id_layer_points = _ref_id_layer_points
			INTO _config_collection_base;
		ELSE
			_config_id_base := _ref_id_total_categories[i];
			_config_collection_base := _config_collection;
		END IF;
		-----------------------------------------------------------------------------------
		-- 1. KONTROLA EXISTENCE v DB
		-----------------------------------------------------------------------------------
		IF _recount = FALSE
		
		THEN	-- kontrola existence v db => vetev pro VYPOCET bodu

			-- CYKLUS po jednotlivych INTERVALECH
			FOR ii IN 1..array_length(_step4interval_res,1)
			LOOP
				-- => kontrola, ze dane _config_collection, dane _config a pro verzi danou povolenym intervalem VERZI
				-- JE nebo NENI v DB vypocitano [poznamka: verze u vsech bodu bude prakticky vzdy stejna !!!]
				WITH
				w1 AS	(
					SELECT DISTINCT tad.gid FROM @extschema@.t_auxiliary_data AS tad
					WHERE tad.config_collection = _config_collection_base
					AND tad.config = _config_id_base
					AND tad.ext_version >= _ext_version_valid_from
					AND tad.ext_version <= _ext_version_valid_until
					AND tad.gid >= _gid_start_res[ii]
					AND tad.gid <= _gid_end_res[ii]
					)
				SELECT count(*) FROM w1
				INTO _check;

				-- varianty:
				-- 1. body pro i-ty interval uz v DB    existuji a pocet bodu se    shoduje s promennou _interval_points => tento zaznam do vypoctu NEPUJDE
				-- 2. body pro i-ty interval uz v DB    existuji a pocet bodu se ne-shoduje s promennou _interval_points => tento zaznam do vypoctu   PUJDE
				-- 3. body pro i-ty interval    v DB ne-existuji => tento zaznam do vypoctu PUJDE

				IF _check > 0
				THEN
					IF _check = _interval_points_res[ii]
					THEN
						_check_exists_ii := TRUE; -- interval nepujde do vypisu
					ELSE
						_check_exists_ii := FALSE; -- interval pujde do vypisu
					END IF;
				ELSE
					IF _ref_id_total_config_query[i] = 500	-- REFERENCE
					THEN
						RAISE EXCEPTION 'Error 25: fn_get_configs4aux_data_app: U argumentu _ref_id_total = % [ID konfigurace uhrnu] je jeho kategorie _ref_id_total_config_query[i] = % reference, pro kterou ale neni doposud proveden vypocet kategorie z neagregovane konfigurace uhrhu!',_ref_id_total,_ref_id_total_config_query[i];
					ELSE
						_check_exists_ii := FALSE; -- interval pujde do vypisu
					END IF;
				END IF;

				-- naplneni promenych do RETURN QUERY v cyklu ii
				IF ii = 1
				THEN
					_res_step_ii := array[i];
					_res_config_collection_ii := array[_config_collection];
					_res_config_ii := array[_ref_id_total_categories[i]];
					_res_intersects_ii := array[_intersects];
					_res_config_query_ii := array[_ref_id_total_config_query[i]];
					_res_step4interval_ii := array[ii];
					_res_check_exists_ii := array[_check_exists_ii];
				ELSE
					_res_step_ii := _res_step_ii || array[i];
					_res_config_collection_ii := _res_config_collection_ii || array[_config_collection];
					_res_config_ii := _res_config_ii || array[_ref_id_total_categories[i]];
					_res_intersects_ii := _res_intersects_ii || array[_intersects];
					_res_config_query_ii := _res_config_query_ii || array[_ref_id_total_config_query[i]];
					_res_step4interval_ii := _res_step4interval_ii || array[ii];
					_res_check_exists_ii := _res_check_exists_ii || array[_check_exists_ii];
				END IF;				
			END LOOP;
		ELSE
			-- kontrola existence v db => vetev pro PREPOCET bodu

			-- 1. prepocet   JE MOZNY pokud v db existuji vsechny kategorie dane konfigurace [na intervalech a na verzi extenze NEZALEZI]

				-- => pocet bodu i-teho config v ii_tem intervalu musi odpovidat poctu bodu ii-teho intervalu
				-- => musi se brat gid pro max_ext_version a pro max_est_date
				
			-- 2. prepocet NENI MOZNY pokud v db existuji vsechny kategorie dane konfigurace u vsech intervalu [na verzi extenze ZALEZI, respektive verze spada do povoleneho intervalu VERZI]


			-- CYKLUS po jednotlivych INTERVALECH
			FOR ii IN 1..array_length(_step4interval_res,1)
			LOOP
				-- => kontrola add 1.
				WITH
				w1 AS	(
					SELECT
					tad.*,
					max(tad.est_date) OVER (PARTITION BY tad.gid, tad.config_collection, tad.config) as max_est_date
					FROM @extschema@.t_auxiliary_data AS tad
					WHERE tad.config_collection = _config_collection_base
					AND tad.config = _config_id_base
					AND tad.gid >= _gid_start_res[ii]
					AND tad.gid <= _gid_end_res[ii]
					),
				w2 AS	(
					SELECT w1.* FROM w1 WHERE w1.est_date = w1.max_est_date
					)
				SELECT count(*) FROM w2
				INTO _check_1;

				IF _check_1 > 0
				THEN
					IF _check_1 = _interval_points_res[ii]
						-- konrola add 1. povoluje prepocet
						-- => v intervalu jsou vsechny body [na verzi extenzi nezalezi]
						-- => dale to pokracuje kontrolou, kde uz na verzi extenze zalezi
					THEN
						-- => kontrola add 2.
						WITH
						w1 AS	(
							SELECT
							tad.*,
							max(tad.est_date) OVER (PARTITION BY tad.gid, tad.config_collection, tad.config) as max_est_date
							FROM @extschema@.t_auxiliary_data AS tad
							WHERE tad.config_collection = _config_collection_base
							AND tad.config = _config_id_base
							AND tad.ext_version >= _ext_version_valid_from -- na verzi extenze ZALEZI
							AND tad.ext_version <= _ext_version_valid_until
							AND tad.gid >= _gid_start_res[ii]
							AND tad.gid <= _gid_end_res[ii]
							),
						w2 AS	(
							SELECT w1.* FROM w1 WHERE w1.est_date = w1.max_est_date
							)							
						SELECT count(*) FROM w2
						INTO _check_2;

						IF _check_2 = _interval_points_res[ii]
						THEN
							_check_exists_ii := TRUE; -- interval obsahuje pozadovany pocet bodu ve verzi spadajici do povoleneho intevalu VERZI => interval neni kandidatem pro PREPOCET
						ELSE
							_check_exists_ii := FALSE; -- tento interval je kandidatem pro prepocet
						END IF;						
					ELSE
						RAISE EXCEPTION 'Error 26: fn_get_configs4aux_data_app: Prepocet NENI mozny [ID vstupni KONFIGURACE bodove vrstvy a uhrnu: _config_collection = %; ID konfigurace UHRNU: _config_collection = %; ID kategorie UHRNU: _config = %] => pro prepocet nejsou doposud vypocitany vsechny body intervalu urceneho podle gid <%;%>! Poznamka: na verzi extenze nezalezi.',_config_collection,_config_collection_base,_config_id_base,_gid_start_res[ii],_gid_end_res[ii]; -- Prepocet NENI mozny => pro prepocet nejsou vypocitany vsechny body v ii-tem intervalu ikdyz na verzi extenze nezalezi
					END IF;
				ELSE
					IF _ref_id_total_config_query[i] = 500	-- REFERENCE
					THEN
						RAISE EXCEPTION 'Error 27: fn_get_configs4aux_data_app: Prepocet NENI mozny [ID vstupni KONFIGURACE bodove vrstvy a uhrnu: _config_collection = %; ID konfigurace UHRNU: _config_collection = %; ID referencni kategorie UHRNU: _config = %] => pro prepocet nejsou doposud vypocitany zadne body intervalu urceneho podle gid <%;%>! Poznamka: na verzi extenze nezalezi.',_config_collection,_config_collection_base,_config_id_base,_gid_start_res[ii],_gid_end_res[ii]; -- Prepocet NENI MOZNY => pro prepocet nejsou vypocitany zadne body v ii-tem intervalu
					ELSE
						RAISE EXCEPTION 'Error 28: fn_get_configs4aux_data_app: Prepocet NENI mozny [ID vstupni KONFIGURACE bodove vrstvy a uhrnu: _config_collection = %; ID konfigurace UHRNU: _config_collection = %; ID kategorie UHRNU: _config = %] => pro prepocet nejsou doposud vypocitany zadne body intervalu urceneho podle gid <%;%>! Poznamka: na verzi extenze nezalezi.',_config_collection,_config_collection_base,_config_id_base,_gid_start_res[ii],_gid_end_res[ii]; -- Prepocet NENI MOZNY => pro prepocet nejsou vypocitany zadne body v ii-tem intervalu
					END IF;
				END IF;

				-- naplneni promenych do RETURN QUERY v cyklu ii
				IF ii = 1
				THEN
					_res_step_ii := array[i];
					_res_config_collection_ii := array[_config_collection];
					_res_config_ii := array[_ref_id_total_categories[i]];
					_res_intersects_ii := array[_intersects];
					_res_config_query_ii := array[_ref_id_total_config_query[i]];
					_res_step4interval_ii := array[ii];
					_res_check_exists_ii := array[_check_exists_ii];
				ELSE
					_res_step_ii := _res_step_ii || array[i];
					_res_config_collection_ii := _res_config_collection_ii || array[_config_collection];
					_res_config_ii := _res_config_ii || array[_ref_id_total_categories[i]];
					_res_intersects_ii := _res_intersects_ii || array[_intersects];
					_res_config_query_ii := _res_config_query_ii || array[_ref_id_total_config_query[i]];
					_res_step4interval_ii := _res_step4interval_ii || array[ii];
					_res_check_exists_ii := _res_check_exists_ii || array[_check_exists_ii];
				END IF;				
			END LOOP;
		END IF;
		-----------------------------------------------------------------------------------
		-----------------------------------------------------------------------------------

		-----------------------------------------------------------------------------------
		-----------------------------------------------------------------------------------
		-- naplneni promenych do RETURN QUERY v cyklu i
		IF i = 1
		THEN
			_res_step := _res_step_ii;
			_res_config_collection := _res_config_collection_ii;
			_res_config := _res_config_ii;
			_res_intersects := _res_intersects_ii;
			_res_config_query := _res_config_query_ii;
			_res_step4interval := _res_step4interval_ii;
			_res_check_exists := _res_check_exists_ii;
		ELSE
			_res_step := _res_step || _res_step_ii;
			_res_config_collection := _res_config_collection || _res_config_collection_ii;
			_res_config := _res_config || _res_config_ii;
			_res_intersects := _res_intersects || _res_intersects_ii;
			_res_config_query := _res_config_query || _res_config_query_ii;
			_res_step4interval := _res_step4interval || _res_step4interval_ii;
			_res_check_exists := _res_check_exists || _res_check_exists_ii;
		END IF;
									
	END LOOP;

	-------------------------------------------------------------------------------------------
	-- kontrola pro RECOUNT = TRUE
	IF _recount = TRUE
	THEN
		IF	(
			SELECT count(t.res_check_exists_recount) = 0
			FROM (SELECT unnest(_res_check_exists) AS res_check_exists_recount) AS t
			WHERE t.res_check_exists_recount = FALSE
			)
		THEN
			RAISE EXCEPTION 'Error 29: fn_get_configs4aux_data_app: Prepocet NENI mozny [ID vstupni KONFIGURACE bodove vrstvy a uhrnu: _config_collection = %; ID konfigurace UHRNU: _config_collection = %; ID kategorie UHRNU: _config = %] => vsechny body jsou ve verzi od verze % do %! Poznamka: na verzi extenze zalezi.',_config_collection,_config_collection_base,_config_id_base,_ext_version_valid_from_label,_ext_version_valid_until_label; -- Prepocet NENI MOZNY => vsechny body jsou v aktualni verzi
		END IF;
	END IF;
	-------------------------------------------------------------------------------------------
					
	-------------------------------------------------------------------------------------------
	-- sestaveni vysledneho _q pro RETURN QUERY
	-------------------------------------------------------------------------------------------
	_q :=	'
		WITH
		w1 AS	(
			SELECT
				unnest($1) AS step,
				unnest($2) AS config_collection,
				unnest($3) AS config,
				unnest($4) AS intersects,
				unnest($5) AS config_query,
				unnest($6) AS step4interval,
				unnest($7) AS check_exists
			),
		w2 AS	(
			SELECT
				unnest($8) AS step4interval_res,
				unnest($9) AS gid_start_res,
				unnest($10) AS gid_end_res
			),
		w3 AS	(
			SELECT
				w1.step,
				w1.config_collection,
				w1.config,
				w1.intersects,
				w1.step4interval
			FROM
				w1
			WHERE
				w1.config_query != 500 AND w1.check_exists = FALSE
			)
		SELECT
			w3.step,
			w3.config_collection,
			w3.config,
			w3.intersects,
			w3.step4interval,
			w2.gid_start_res,
			w2.gid_end_res
		FROM
			w3 INNER JOIN w2
		ON
			w3.step4interval = w2.step4interval_res
		ORDER
			BY w3.step, w3.config, w3.step4interval, w2.gid_start_res
		';
	-------------------------------------------------------------------------------------------		
	-------------------------------------------------------------------------------------------
	-- !!! pozor, protinaji se jen tyto konfigurace: (_config_query = 100 a _config_function = ANY(array[100,200]) AND _aggregated = false)
	-------------------------------------------------------------------------------------------
	--------------------------------------------------------------------------------------------
	-- kontrola ze _q vraci alespon nejakou konfiguraci pro protinani
	EXECUTE 'WITH w_configs AS ('||_q||') SELECT (count(*))::integer FROM w_configs'
	USING
		_res_step,			-- $1
		_res_config_collection,		-- $2
		_res_config,			-- $3
		_res_intersects,		-- $4
		_res_config_query,		-- $5
		_res_step4interval,		-- $6
		_res_check_exists,		-- $7
		_step4interval_res,		-- $8
		_gid_start_res,			-- $9
		_gid_end_res			-- $10
	INTO
		_check_3;
	-----------------------------------------------------------------------------------
	IF _recount = FALSE AND (_check_3 = 0 OR _check_3 IS NULL)
	THEN
		RAISE EXCEPTION 'Error 30: fn_get_configs4aux_data_app: Pro bodove protinani nenalezeny zadne konfigurace pro protinani. Vse jiz bylo vyprotinano!';
	END IF;
	--------------------------------------------------------------------------------------------
	RETURN QUERY EXECUTE ''||_q||''
	USING
		_res_step,			-- $1
		_res_config_collection,		-- $2
		_res_config,			-- $3
		_res_intersects,		-- $4
		_res_config_query,		-- $5
		_res_step4interval,		-- $6
		_res_check_exists,		-- $7
		_step4interval_res,		-- $8
		_gid_start_res,			-- $9
		_gid_end_res;			-- $10
	--------------------------------------------------------------------------------------------
END;
$BODY$
LANGUAGE plpgsql VOLATILE;

ALTER FUNCTION @extschema@.fn_get_configs4aux_data_app(integer,integer,integer,boolean) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_configs4aux_data_app(integer,integer,integer,boolean) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_configs4aux_data_app(integer,integer,integer,boolean) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_configs4aux_data_app(integer,integer,integer,boolean) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_configs4aux_data_app(integer,integer,integer,boolean) IS
'Funkce vraci seznam konfiguraci pro funkci fn_get_aux_data_app.';