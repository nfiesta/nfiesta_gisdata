--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DROP FUNCTION @extschema@.fn_get_reclass_app(raster,integer,text,integer)

CREATE OR REPLACE FUNCTION @extschema@.fn_get_reclass_app(_rast raster, _band integer, _pixel_type text, _reclass_value integer)
RETURNS raster AS
$$
DECLARE
	_max_pixel_range_value	integer;
	_reclass_value_minus	integer;
	_reclass_value_plus	integer;
	_reclass_text		text;
BEGIN
	-------------------------------------------------------------------------------------------
	IF _rast IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_reclass_app: Vstupni argument _rast nesmi byt NULL!';
	END IF;

	IF _band IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_reclass_app: Vstupni argument _band nesmi byt NULL!';
	END IF;

	IF _pixel_type IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_get_reclass_app: Vstupni argument _pixel_type nesmi byt NULL!';
	END IF;

	IF _reclass_value IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_get_reclass_app: Vstupni argument _reclass_value nesmi byt NULL!';
	END IF;

	IF _reclass_value = 0
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_get_reclass_app: Hodnota _reclass_value nesmi byt 0!';
	END IF;
	-------------------------------------------------------------------------------------------
	-- nastaveni _range podle typu pixelu
	CASE
	WHEN _pixel_type = '2BUI'	THEN _max_pixel_range_value := 4;
	WHEN _pixel_type = '4BUI'	THEN _max_pixel_range_value := 16;
	WHEN _pixel_type = '8BUI'	THEN _max_pixel_range_value := 255;
	WHEN _pixel_type = '16BUI'	THEN _max_pixel_range_value := 65536;
	ELSE
		RAISE EXCEPTION 'Chyba 06: fn_get_reclass_app: Neznama hodnota _pixel_type pro nastaveni _max_pixel_range_value!';
	END CASE;
	-------------------------------------------------------------------------------------------
	IF _reclass_value >= _max_pixel_range_value
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_get_reclass_app: Hodnota _reclass_value nesmi byt vetsi nebo rovna hodnote _max_pixel_range_value!';
	END IF;
	-------------------------------------------------------------------------------------------
	-- proces sestaveni reklasifikacniho stringu pro funci ST_Reclass
	IF _reclass_value = 1
	THEN
		_reclass_text := concat('[0-0]:0,[1-1]:1,[2-',(_max_pixel_range_value-1),']:0');

	ELSE
		_reclass_value_minus := _reclass_value - 1;
		_reclass_value_plus  := _reclass_value + 1;

		_reclass_text := concat('[0-',_reclass_value_minus,']:0,[',_reclass_value,'-',_reclass_value,']:1,[',_reclass_value_plus,'-',(_max_pixel_range_value-1),']:0');
	END IF;
	-------------------------------------------------------------------------------------------
	RETURN (ST_Reclass(_rast, _band, _reclass_text, _pixel_type, _max_pixel_range_value));
	-------------------------------------------------------------------------------------------
END;
$$
LANGUAGE plpgsql VOLATILE
COST 100;

ALTER FUNCTION @extschema@.fn_get_reclass_app(raster,integer,text,integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_reclass_app(raster,integer,text,integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_reclass_app(raster,integer,text,integer) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_reclass_app(raster,integer,text,integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_reclass_app(raster,integer,text,integer) IS
'Funkce vraci reklasifikovany raster v zavislosti na zadanem bandu, typu pixelu a hodnoty pro reklasifikaci.';
