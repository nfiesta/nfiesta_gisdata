--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
-- fn_api_get_estimation_cell_hierarchy_stage
---------------------------------------------------------------------------------------------------
create or replace function @extschema@.fn_api_get_estimation_cell_hierarchy_stage(_config_collection integer)
returns table
(
	id integer,
	estimation_cell_id integer,
	stage boolean
)
as
$body$
begin
        if _config_collection is null
        then
                raise exception 'Error 01: fn_api_get_estimation_cell_hierarchy_stage: Input argument _config_collection must not be NULL!';
        end if;

        if	(
                select count(*) > 0
                from @extschema@.cm_estimation_cell_collection
                where estimation_cell_collection_lowest in (select cecc.id from @extschema@.c_estimation_cell_collection as cecc where cecc.use4estimates = true)
                )
        then
                raise exception 'Error 02: fn_api_get_estimation_cell_hierarchy_stage: In table c_estimation_cell_collection is not set value false for the some of lowest estimation cell collection!';
        end if;

        return query
        with
        w_cell as materialized	(
                                select
                                        cmec.estimation_cell,
                                        cmec.estimation_cell_collection
                                from
                                        @extschema@.cm_estimation_cell as cmec
                                where
                                        cmec.estimation_cell_collection in	(
                                                                            select cecc.id from @extschema@.c_estimation_cell_collection as cecc
                                                                            where cecc.use4estimates = true
                                                                            )										
                                )
        ,w1 as	(
                select
                        (row_number() over(order by t1.estimation_cell))::integer as id,
                        t1.estimation_cell as estimation_cell_id,
                        (select count(*) from @extschema@.t_config as tc where tc.config_collection = _config_collection) as count_config
                from
                        @extschema@.t_estimation_cell_hierarchy as t1
                        inner join w_cell on t1.estimation_cell = w_cell.estimation_cell
                )
        ,w2 as	(
                select
                        w1.*,
                        t2.config_id
                from
                        w1, (select tc.id as config_id from @extschema@.t_config as tc where tc.config_collection = _config_collection) as t2
                )
        ,w3 as	(
                select
                        w2.*,
                        case when t1.id is not null then true else false end as stage
                from
                        w2
                        left join @extschema@.t_aux_total as t1
                        on w2.estimation_cell_id = t1.estimation_cell and w2.config_id = t1.config
                )
        ,w4 as	(
                select w3.estimation_cell_id, count(*) as count_stage_true from w3 where w3.stage = true
                group by w3.estimation_cell_id
                )
        ,w5 as	(
                select
                        w1.id,
                        w1.estimation_cell_id,
                        case
                            when w4.count_stage_true is null then null::boolean
                            else
                                case
                                    when w4.count_stage_true = w1.count_config
                                    then true else false
                                end
                        end
                            as stage
                from
                        w1
                        left join w4 on w1.estimation_cell_id = w4.estimation_cell_id
                )
        select
                w5.id,
                w5.estimation_cell_id,
                w5.stage
        from
                w5 order by w5.id;
end;
$body$
language plpgsql volatile;

ALTER FUNCTION @extschema@.fn_api_get_estimation_cell_hierarchy_stage(integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_api_get_estimation_cell_hierarchy_stage(integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_api_get_estimation_cell_hierarchy_stage(integer) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_api_get_estimation_cell_hierarchy_stage(integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_api_get_estimation_cell_hierarchy_stage(integer) IS
'Funkce vraci tabulku hierarchii cell z tabulky t_estimation_cell_hierarchy s identifikaci stage [null = pro celu nebolo dopusud nic pocitano, true = cela je vypocitana, false = cela je rozpracovana]. Ve vystupu jsou cely, ktere maji v tabulce c_estimation_cell_collection nastaveno use4estimates = TRUE.';