--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DROP FUNCTION @extschema@.fn_api_make_aux_total(integer, integer, integer, integer, boolean);

create or replace function @extschema@.fn_api_make_aux_total
(
    _config_id          integer,
    _estimation_cell    integer,
    _gid                integer,
    _gui_version        integer,
    _recount			boolean default false
)
returns void as
$BODY$
declare
begin
    if _config_id is null
    then
        raise exception 'Error 01: fn_api_make_aux_total: Input argument _config_id must not be NULL!';
    end if;

    if _estimation_cell is null
    then
        raise exception 'Error 02: fn_api_make_aux_total: Input argument _estimation_cell must not be NULL!';
    end if;

    if _gui_version is null
    then
        raise exception 'Error 03: fn_api_make_aux_total: Input argument _gui_version must not be NULL!';
    end if;
    ---------------------------------------------
    if _gid is not null
    then
        if _gid != 0
        then
            PERFORM @extschema@.fn_get_aux_total_app(_config_id,_estimation_cell,_gid,_gui_version);
        else
            PERFORM @extschema@.fn_get_aux_total4estimation_cell_app(_config_id,_estimation_cell,_gid,_gui_version,_recount);
        end if;
    else
        PERFORM @extschema@.fn_get_aux_total_app(_config_id,_estimation_cell,_gid,_gui_version);
    end if;
    ---------------------------------------------
end;
$BODY$
language plpgsql VOLATILE;

alter function @extschema@.fn_api_make_aux_total(integer,integer,integer,integer,boolean) OWNER TO adm_nfiesta_gisdata;
grant execute on function @extschema@.fn_api_make_aux_total(integer,integer,integer,integer,boolean) TO adm_nfiesta_gisdata;
grant execute on function @extschema@.fn_api_make_aux_total(integer,integer,integer,integer,boolean) TO app_nfiesta_gisdata;
grant execute on function @extschema@.fn_api_make_aux_total(integer,integer,integer,integer,boolean) TO public;

comment on function @extschema@.fn_api_make_aux_total(integer,integer,integer,integer,boolean) IS
'The function on a base of input arguments calls either a function fn_get_aux_total_app or function fn_get_aux_total4estimation_cell_app.';