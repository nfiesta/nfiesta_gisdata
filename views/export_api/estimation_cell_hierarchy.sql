--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DDL
create or replace view export_api.estimation_cell_hierarchy as
select
	cec1.label as cell,
	cec2.label as cell_superior
from
	gisdata.t_estimation_cell_hierarchy as tech
	inner join gisdata.c_estimation_cell as cec1 on tech.estimation_cell = cec1.id
	inner join gisdata.c_estimation_cell as cec2 on tech.estimation_cell_superior = cec2.id
where
	tech.estimation_cell_superior is not null
;

-- authorization
ALTER TABLE export_api.estimation_cell_hierarchy OWNER TO adm_nfiesta_gisdata;
GRANT ALL ON TABLE export_api.estimation_cell_hierarchy TO adm_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.estimation_cell_hierarchy TO app_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.estimation_cell_hierarchy TO public;

-- documentation
COMMENT ON VIEW export_api.estimation_cell_hierarchy IS 'View providing hierarchy of estimation cells to nfi_esta ETL process.';

