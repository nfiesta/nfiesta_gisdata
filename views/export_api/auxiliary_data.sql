--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DDL
CREATE OR REPLACE VIEW export_api.auxiliary_data AS
with
w_plots as 		(
			select
				country,
				strata_set,
				stratum,
				panel,
				cluster,
				plot
			from
				@extschema@.f_p_plot
			)
,w_config_collection as (
			-- a selection of calculated config_collection from table t_auxiliary_data
			-- and addition of attributes from table t_config, where config_query is the most important
			-- attribute, which in case of value 500 it tells it is only a reference 
			-- [note reference data are not duplicated in table t_auxiliary_data]
						select
							a.*,
							b.id as id_config,
							b.config_collection,
							b.config_query,
							b.label,
							b.categories
						from
								(
								select
									id,
									label,
									ref_id_layer_points,
									ref_id_total
								from
									@extschema@.t_config_collection
								where
									id in	(select distinct config_collection from @extschema@.t_auxiliary_data)
								) as a
						inner
						join	@extschema@.t_config as b
						
						on		a.ref_id_total = b.config_collection
						)
,w_no_ref as 	(
				-- selection of records from CTE w_config_collection, which are not a reference [config_query != 500]
				select w_config_collection.* from w_config_collection
				where w_config_collection.config_query is distinct from 500
				)
,w_ref as 		(
				-- selection of records from CTE w_config_collection, which are a reference [config_query = 500]
				select w_config_collection.* from w_config_collection
				where w_config_collection.config_query = 500
				)
,w_data_no_ref as 	(
					-- selection of data from table t_auxiliary_data for records from CTE w_no_ref,
					-- can be done right through the attribute config_collection = id from CTE
					-- w_no_ref and config = id_config from CTE w_no_ref
					select
						t_auxiliary_data.id,
						t_auxiliary_data.config_collection as config_collection_res,
						t_auxiliary_data.config as config_res,
						t_auxiliary_data.ident_point as pid,
						t_auxiliary_data.value,
						t_auxiliary_data.ext_version,
						t_auxiliary_data.gui_version
					from @extschema@.t_auxiliary_data
					inner join w_no_ref
					on t_auxiliary_data.config_collection = w_no_ref.id
					and t_auxiliary_data.config = w_no_ref.id_config
					)
,w_ref_attribute as 	(
						-- preparation of records with attributes for data selection from
						-- table t_auxiliary_data which area only references
						select
							w_ref_subselect.*,
							tcc_subselect.id as config_collection4auxiliary_data
						from
								(
								select
									w_ref.*,
									t_config.id as categories_integer,
									t_config.config_collection as config_colletion4cf600
								from
										w_ref
								inner
								join	@extschema@.t_config
								
								on		w_ref.categories = t_config.id::character varying 
								) as
									w_ref_subselect 
						inner
						join	(select * from @extschema@.t_config_collection where config_function = 600) as tcc_subselect
						
						on w_ref_subselect.config_colletion4cf600 = tcc_subselect.ref_id_total						
						)						
,w_data_ref as 		(
					-- selection of data from table t_auxiliary_data for records from CTE w_ref
					-- the selection is done through CTE w_ref_attribute, where join keywords are
					-- config_collection4auxiliary_data and categories_integer
					select
						t_auxiliary_data.id,
						w_ref_attribute.id as config_collection_res,
						w_ref_attribute.id_config as config_res,
						t_auxiliary_data.ident_point as pid,
						t_auxiliary_data.value,
						t_auxiliary_data.ext_version,
						t_auxiliary_data.gui_version						
					from
							@extschema@.t_auxiliary_data
					inner
					join	w_ref_attribute
					
					on t_auxiliary_data.config_collection = w_ref_attribute.config_collection4auxiliary_data
					and t_auxiliary_data.config = w_ref_attribute.categories_integer
					)
,w_no_ref_max as 	(
					-- from CTE w_data_nor_ref and from CTE w_data_ref a unique key must be gained,
					-- resp for unique combination of config_collection,config,ident_point,ext_version
					-- must be chosen a record for the most actual EXT_VERSION
					select config_collection_res, config_res, pid, max(ext_version) as max_ext_version
					from w_data_no_ref group by config_collection_res, config_res, pid
					)
,w_ref_max as 		(
					select config_collection_res, config_res, pid, max(ext_version) as max_ext_version
					from w_data_ref group by config_collection_res, config_res, pid
					)
,w_data_no_ref_res as 	(
						select
							w_data_no_ref.config_collection_res as config_collection,
							w_data_no_ref.config_res as config,
							w_data_no_ref.pid,
							w_data_no_ref.value
						from w_data_no_ref inner join w_no_ref_max
						on w_data_no_ref.config_collection_res = w_no_ref_max.config_collection_res
						and w_data_no_ref.config_res = w_no_ref_max.config_res
						and w_data_no_ref.pid = w_no_ref_max.pid
						and w_data_no_ref.ext_version = w_no_ref_max.max_ext_version
						)
,w_data_ref_res as 		(
						select
							w_data_ref.config_collection_res as config_collection,
							w_data_ref.config_res as config,
							w_data_ref.pid,
							w_data_ref.value
						from w_data_ref inner join w_ref_max
						on w_data_ref.config_collection_res = w_ref_max.config_collection_res
						and w_data_ref.config_res = w_ref_max.config_res
						and w_data_ref.pid = w_ref_max.pid
						and w_data_ref.ext_version = w_ref_max.max_ext_version
						)
,w_data_res as 			(
						select * from w_data_no_ref_res	union all
						select * from w_data_ref_res
						)
select
		w_plots.country,
		w_plots.strata_set,
		w_plots.stratum,
		w_plots.panel,
		w_plots.cluster,
		w_plots.plot,
		--w_data_res.config_collection,
		t_config_collection.auxiliary_variable,
		--w_data_res.config,
		t_config.auxiliary_variable_category,
		w_data_res.value,
		t_config.label as comment
from
		w_data_res
inner join	w_plots					on w_data_res.pid::character varying = w_plots.plot
left join 	@extschema@.t_config			on w_data_res.config = t_config.id
left join	@extschema@.t_config_collection		on t_config.config_collection = t_config_collection.id
UNION ALL
SELECT
	country,
	strata_set,
	stratum,
	panel,
	cluster,
	plot,
	0 AS auxiliary_variable,
	0 AS auxialiry_variable_category,
	1.0 AS value,
	'intercept'::text AS comment
FROM
	w_plots
ORDER BY auxiliary_variable_category, plot
;

-- authorization
ALTER TABLE export_api.auxiliary_data OWNER TO adm_nfiesta_gisdata;
GRANT ALL ON TABLE export_api.auxiliary_data TO adm_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.auxiliary_data TO app_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.auxiliary_data TO public;

-- documentation
COMMENT ON VIEW export_api.auxiliary_data IS 'View with the auxiliary plot data.';

COMMENT ON COLUMN export_api.auxiliary_data.country IS 'Identifier of the country. Foreign key to c_country.';
COMMENT ON COLUMN export_api.auxiliary_data.strata_set IS 'Character identifier of strata set.';
COMMENT ON COLUMN export_api.auxiliary_data.stratum IS 'Character identifier of stratum.';
COMMENT ON COLUMN export_api.auxiliary_data.panel IS 'Character identifier of panel.';
COMMENT ON COLUMN export_api.auxiliary_data.cluster IS 'Average of relative plot sampling weights per cluster. Is used to compute pix and pixy.';
COMMENT ON COLUMN export_api.auxiliary_data.plot IS 'Identifier of the plot within cluster.';
COMMENT ON COLUMN export_api.auxiliary_data.auxiliary_variable IS 'Foreign key to c_auxiliary_variable.';
COMMENT ON COLUMN export_api.auxiliary_data.auxiliary_variable_category IS 'Foreign key to c_auxiliary_variable_category.';
COMMENT ON COLUMN export_api.auxiliary_data.value IS 'Value of the auxiliary plot data.';
COMMENT ON COLUMN export_api.auxiliary_data.comment IS 'Comment.';

