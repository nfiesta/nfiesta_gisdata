--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------



---------------------------------------------------------------------------------------------------;
-- update of column use4estimates => value false must be set only for the lowest collections
---------------------------------------------------------------------------------------------------;
update @extschema@.c_estimation_cell_collection set use4estimates = true where id = id;
update @extschema@.c_estimation_cell_collection set use4estimates = false where id in (select distinct estimation_cell_collection_lowest from @extschema@.cm_estimation_cell_collection);
---------------------------------------------------------------------------------------------------;



---------------------------------------------------------------------------------------------------;
-- CM_ESTIMATION_CELL --
---------------------------------------------------------------------------------------------------;
CREATE TABLE @extschema@.cm_estimation_cell
	(
	id							serial NOT NULL,
	estimation_cell				integer NOT NULL,
	estimation_cell_collection	integer,
	CONSTRAINT pkey__cm_estimation_cell PRIMARY KEY (id),
	CONSTRAINT fkey__cm_estimation_cell__estimation_cell FOREIGN KEY (estimation_cell) REFERENCES @extschema@.c_estimation_cell (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT fkey__cm_estimation_cell__estimation_cell_collection FOREIGN KEY (estimation_cell_collection) REFERENCES @extschema@.c_estimation_cell_collection (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
	);

	ALTER TABLE @extschema@.cm_estimation_cell OWNER TO adm_nfiesta_gisdata;
	GRANT ALL ON TABLE @extschema@.cm_estimation_cell TO adm_nfiesta_gisdata;
	GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE @extschema@.cm_estimation_cell TO app_nfiesta_gisdata;
	GRANT SELECT ON TABLE @extschema@.cm_estimation_cell TO public;

	GRANT SELECT ON SEQUENCE @extschema@.cm_estimation_cell_id_seq TO public;

	CREATE INDEX fki__cm_estimation_cell__estimation_cell ON @extschema@.cm_estimation_cell USING btree (estimation_cell);
	CREATE INDEX fki__cm_estimation_cell__estimation_cell_collection ON @extschema@.cm_estimation_cell USING btree (estimation_cell_collection);

	COMMENT ON TABLE  @extschema@.cm_estimation_cell IS 'Maping table between c_estimation_cell and c_estimation_cell_collection table.';
	COMMENT ON COLUMN @extschema@.cm_estimation_cell.id IS 'Primary key, identifier of the record.';
	COMMENT ON COLUMN @extschema@.cm_estimation_cell.estimation_cell IS 'Foreign key to id of c_estimation_cell.';
	COMMENT ON COLUMN @extschema@.cm_estimation_cell.estimation_cell_collection IS 'Foreign key to id of c_estimation_cell_collection.';
	COMMENT ON CONSTRAINT pkey__cm_estimation_cell ON @extschema@.cm_estimation_cell IS 'Primary key.';
	COMMENT ON CONSTRAINT fkey__cm_estimation_cell__estimation_cell ON @extschema@.cm_estimation_cell IS 'Foreign key to table c_estimation_cell.';
	COMMENT ON CONSTRAINT fkey__cm_estimation_cell__estimation_cell_collection ON @extschema@.cm_estimation_cell IS 'Foreign key to table c_estimation_cell_collection.';
	COMMENT ON INDEX @extschema@.fki__cm_estimation_cell__estimation_cell IS 'BTree index on foreign key fkey__cm_estimation_cell__estimation_cell.';
	COMMENT ON INDEX @extschema@.fki__cm_estimation_cell__estimation_cell_collection IS 'BTree index on foreign key fkey__cm_estimation_cell__estimation_cell_collection.';

	ALTER TABLE @extschema@.cm_estimation_cell ADD CONSTRAINT ukey__cm_estimation_cell UNIQUE(estimation_cell, estimation_cell_collection);
	COMMENT ON CONSTRAINT ukey__cm_estimation_cell ON @extschema@.cm_estimation_cell IS 'Unique identificator combination of estimation cell and estimation cell collection.';
---------------------------------------------------------------------------------------------------;



---------------------------------------------------------------------------------------------------;
-- inserting of dynamic datas into cm_estimation_cell
---------------------------------------------------------------------------------------------------;
with
w1 as	(
		select estimation_cell_collection, count(*) as count_estimation_cell
		from @extschema@.c_estimation_cell where estimation_cell_collection in
		(select id from @extschema@.c_estimation_cell_collection where use4estimates = false)
		group by estimation_cell_collection
		)
,w2 as	(
		select id as estimation_cell, estimation_cell_collection
		from @extschema@.c_estimation_cell
		where estimation_cell_collection in
			(
			select w1.estimation_cell_collection from w1
			where w1.count_estimation_cell = (select max(w1.count_estimation_cell) from w1)
			union all
			select id from @extschema@.c_estimation_cell_collection where use4estimates = true
			)
		)
,w3 as	(
		select
			t2.id as estimation_cell,
			t1.estimation_cell_collection
		from
			(
			select * from @extschema@.c_estimation_cell
			where estimation_cell_collection in
				(
				select w1.estimation_cell_collection from w1
				where w1.count_estimation_cell != (select max(w1.count_estimation_cell) from w1)
				)
			) as t1
			inner
			join	(
					select * from @extschema@.c_estimation_cell
					where estimation_cell_collection in
						(
						select w1.estimation_cell_collection from w1
						where w1.count_estimation_cell = (select max(w1.count_estimation_cell) from w1)
						)
					) as t2
			on t1.label = t2.label
		)
,w4 as	(
		select estimation_cell, estimation_cell_collection from w2 union all
		select estimation_cell, estimation_cell_collection from w3
		)
insert into @extschema@.cm_estimation_cell(estimation_cell, estimation_cell_collection)
select estimation_cell, estimation_cell_collection from w4;
---------------------------------------------------------------------------------------------------;



---------------------------------------------------------------------------------------------------;
-- T_ESTIMATION_CELL_HIERARCHY --
---------------------------------------------------------------------------------------------------;
CREATE TABLE @extschema@.t_estimation_cell_hierarchy
	(
	id							serial NOT NULL,
	estimation_cell				integer NOT NULL,
	estimation_cell_superior	integer,
	CONSTRAINT pkey__t_estimation_cell_hierarchy PRIMARY KEY (id),
	CONSTRAINT fkey__t_estimation_cell_hierarchy__estimation_cell_1 FOREIGN KEY (estimation_cell) REFERENCES @extschema@.c_estimation_cell (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT fkey__t_estimation_cell_hierarchy__estimation_cell_2 FOREIGN KEY (estimation_cell_superior) REFERENCES @extschema@.c_estimation_cell (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
	);

	ALTER TABLE @extschema@.t_estimation_cell_hierarchy OWNER TO adm_nfiesta_gisdata;
	GRANT ALL ON TABLE @extschema@.t_estimation_cell_hierarchy TO adm_nfiesta_gisdata;
	GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE @extschema@.t_estimation_cell_hierarchy TO app_nfiesta_gisdata;
	GRANT SELECT ON TABLE @extschema@.t_estimation_cell_hierarchy TO public;

	GRANT SELECT ON SEQUENCE @extschema@.t_estimation_cell_hierarchy_id_seq TO public;

	CREATE INDEX fki__t_estimation_cell_hierarchy__estimation_cell_1 ON @extschema@.t_estimation_cell_hierarchy USING btree (estimation_cell);
	CREATE INDEX fki__t_estimation_cell_hierarchy__estimation_cell_2 ON @extschema@.t_estimation_cell_hierarchy USING btree (estimation_cell_superior);

	COMMENT ON TABLE  @extschema@.t_estimation_cell_hierarchy IS 'Maping table for table c_estimation_cell. Storing hierarchy of cells.';
	COMMENT ON COLUMN @extschema@.t_estimation_cell_hierarchy.id IS 'Primary key, identifier of the record.';
	COMMENT ON COLUMN @extschema@.t_estimation_cell_hierarchy.estimation_cell IS 'Foreign key to id of c_estimation_cell.';
	COMMENT ON COLUMN @extschema@.t_estimation_cell_hierarchy.estimation_cell_superior IS 'Foreign key to id of c_estimation_cell.';
	COMMENT ON CONSTRAINT pkey__t_estimation_cell_hierarchy ON @extschema@.t_estimation_cell_hierarchy IS 'Primary key.';
	COMMENT ON CONSTRAINT fkey__t_estimation_cell_hierarchy__estimation_cell_1 ON @extschema@.t_estimation_cell_hierarchy IS 'Foreign key to table c_estimation_cell.';
	COMMENT ON CONSTRAINT fkey__t_estimation_cell_hierarchy__estimation_cell_2 ON @extschema@.t_estimation_cell_hierarchy IS 'Foreign key to table c_estimation_cell.';
	COMMENT ON INDEX @extschema@.fki__t_estimation_cell_hierarchy__estimation_cell_1 IS 'BTree index on foreign key fkey__t_estimation_cell_hierarchy__estimation_cell_1.';
	COMMENT ON INDEX @extschema@.fki__t_estimation_cell_hierarchy__estimation_cell_2 IS 'BTree index on foreign key fkey__t_estimation_cell_hierarchy__estimation_cell_2.';

	ALTER TABLE @extschema@.t_estimation_cell_hierarchy ADD CONSTRAINT ukey__t_estimation_cell_hierarchy UNIQUE(estimation_cell, estimation_cell_superior);
	COMMENT ON CONSTRAINT ukey__t_estimation_cell_hierarchy ON @extschema@.t_estimation_cell_hierarchy IS 'Unique identificator combination of estimation cell and estimation cell superior.';
---------------------------------------------------------------------------------------------------;



---------------------------------------------------------------------------------------------------;
-- inserting of dynamic datas into t_estimation_cell_hierarchy
---------------------------------------------------------------------------------------------------;
with
w1 as	(
		select estimation_cell_collection, count(*) as count_estimation_cell
		from @extschema@.c_estimation_cell where estimation_cell_collection in
		(select id from @extschema@.c_estimation_cell_collection where use4estimates = false)
		group by estimation_cell_collection
		)
,w2 as	(
		select id as estimation_cell, estimation_cell_collection
		from @extschema@.c_estimation_cell
		where estimation_cell_collection in
			(
			select w1.estimation_cell_collection from w1
			where w1.count_estimation_cell = (select max(w1.count_estimation_cell) from w1)
			union all
			select id from @extschema@.c_estimation_cell_collection where use4estimates = true
			)
		)
,w3 as	(
		select
				w2.estimation_cell,
				t1.gid as gid_f_a_cell,
				t2.cell,
				t2.cell_sup,
				t3.estimation_cell as cell4insert,
				t4.estimation_cell as cell_superior4insert
		from
				w2
				inner join @extschema@.f_a_cell as t1 on w2.estimation_cell = t1.estimation_cell
				inner join @extschema@.cm_f_a_cell as t2 on t1.gid = t2.cell
				inner join @extschema@.f_a_cell as t3 on t2.cell = t3.gid
				left join  @extschema@.f_a_cell as t4 on t2.cell_sup = t4.gid
		)
,w4 as	(
		select id as estimation_cell, estimation_cell_collection
		from @extschema@.c_estimation_cell
		where estimation_cell_collection in
			(
			select w1.estimation_cell_collection from w1
			where w1.count_estimation_cell != (select max(w1.count_estimation_cell) from w1)
			)
		)
,w5 as	(
		select
				w4.estimation_cell,
				t1.gid as gid_f_a_cell,
				t2.cell,
				t2.cell_sup,
				t3.estimation_cell as estimation_cell_nuts_or_biogeoregions,
				t4.estimation_cell as cell_superior4insert,
				t5.label as estimation_cell_nuts_or_biogeoregions_label,
				t6.id as cell4insert
		from
				w4
				inner join @extschema@.f_a_cell as t1 on w4.estimation_cell = t1.estimation_cell
				inner join @extschema@.cm_f_a_cell as t2 on t1.gid = t2.cell
				inner join @extschema@.f_a_cell as t3 on t2.cell = t3.gid
				left join  @extschema@.f_a_cell as t4 on t2.cell_sup = t4.gid
				inner join @extschema@.c_estimation_cell as t5 on t3.estimation_cell = t5.id
				inner join	(
							select * from @extschema@.c_estimation_cell
							where estimation_cell_collection =	(
																select w1.estimation_cell_collection from w1
																where w1.count_estimation_cell = (select max(w1.count_estimation_cell) from w1)
																)
							) as t6 on t5.label = t6.label
		)
,w6 as	(
		select w3.cell4insert, w3.cell_superior4insert from w3 union all
		select w5.cell4insert, w5.cell_superior4insert from w5
		)
insert into @extschema@.t_estimation_cell_hierarchy(estimation_cell,estimation_cell_superior)
select cell4insert, cell_superior4insert from w6;
---------------------------------------------------------------------------------------------------;



DROP VIEW export_api.estimation_cell_hierarchy;
DROP VIEW export_api.plot_cell_associations;



---------------------------------------------------------------------------------------------------;
-- drop table cm_f_a_cell
---------------------------------------------------------------------------------------------------;
ALTER TABLE @extschema@.cm_f_a_cell DROP CONSTRAINT fkey__cm_f_a_cell__f_a_cell_1;
ALTER TABLE @extschema@.cm_f_a_cell DROP CONSTRAINT fkey__cm_f_a_cell__f_a_cell_2;
DROP TABLE @extschema@.cm_f_a_cell;
---------------------------------------------------------------------------------------------------;



---------------------------------------------------------------------------------------------------;
-- deleting dynamic datas from t_aux_total table
---------------------------------------------------------------------------------------------------;
with
w1 as	(
		select estimation_cell_collection, count(*) as count_estimation_cell
		from @extschema@.c_estimation_cell where estimation_cell_collection in
		(select id from @extschema@.c_estimation_cell_collection where use4estimates = false)
		group by estimation_cell_collection
		)
,w2 as	(
		select gid from @extschema@.f_a_cell
		where estimation_cell in
			(
			select id from @extschema@.c_estimation_cell
			where estimation_cell_collection in
				(
				select w1.estimation_cell_collection from w1
				where w1.count_estimation_cell != (select max(w1.count_estimation_cell) from w1)
				)
			)
		)
delete from @extschema@.t_aux_total where cell in (select w2.gid from w2);

select setval('gisdata.seq__t_aux_total_id',(select max(id) from @extschema@.t_aux_total));
---------------------------------------------------------------------------------------------------;



---------------------------------------------------------------------------------------------------;
-- deleting dynamic datas from f_a_cell table
---------------------------------------------------------------------------------------------------;
with
w1 as	(
		select estimation_cell_collection, count(*) as count_estimation_cell
		from @extschema@.c_estimation_cell where estimation_cell_collection in
		(select id from @extschema@.c_estimation_cell_collection where use4estimates = false)
		group by estimation_cell_collection
		)
delete from @extschema@.f_a_cell
where estimation_cell in
	(
	select id from @extschema@.c_estimation_cell
	where estimation_cell_collection in
		(
		select w1.estimation_cell_collection from w1
		where w1.count_estimation_cell != (select max(w1.count_estimation_cell) from w1)
		)
	);

select setval('gisdata.f_a_cell_gid_seq',(select max(gid) from @extschema@.f_a_cell));
---------------------------------------------------------------------------------------------------;



---------------------------------------------------------------------------------------------------;
-- deleting dynamic datas from c_estimation_cell table
---------------------------------------------------------------------------------------------------;
with
w1 as	(
		select estimation_cell_collection, count(*) as count_estimation_cell
		from @extschema@.c_estimation_cell where estimation_cell_collection in
		(select id from @extschema@.c_estimation_cell_collection where use4estimates = false)
		group by estimation_cell_collection
		)
delete from @extschema@.c_estimation_cell
where estimation_cell_collection in
	(
	select w1.estimation_cell_collection from w1
	where w1.count_estimation_cell != (select max(w1.count_estimation_cell) from w1)
	);
---------------------------------------------------------------------------------------------------;




-- <function name="fn_check_cell_sup_app" schema="extschema" src="functions/extschema/fn_check_cell_sup_app.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DROP FUNCTION @extschema@.fn_check_cell_sup_app();

CREATE OR REPLACE FUNCTION @extschema@.fn_check_cell_sup_app()
RETURNS void AS
$BODY$
DECLARE
	_ec_highest	integer[];
	_check		integer;
BEGIN
	---------------------------------------------------
	SELECT array_agg(t.estimation_cell_collection_highest ORDER BY t.estimation_cell_collection_highest)
	FROM (SELECT DISTINCT estimation_cell_collection_highest FROM @extschema@.cm_estimation_cell_collection) AS t
	INTO _ec_highest;
	---------------------------------------------------
	FOR i IN 1..array_length(_ec_highest,1)
	LOOP
		WITH
		w1 AS	(
			select estimation_cell from @extschema@.cm_estimation_cell
			where estimation_cell_collection = _ec_highest[i]
			),
		w2 AS	(
			select estimation_cell from @extschema@.t_estimation_cell_hierarchy
			where estimation_cell_superior is null
			and estimation_cell in (select w1.estimation_cell from w1)
			),
		w3 AS	(
			SELECT estimation_cell FROM w1 except
			SELECT estimation_cell FROM w2
			),
		w4 AS	(
			SELECT estimation_cell FROM w2 except
			SELECT estimation_cell FROM w1
			)
		SELECT count(t.*) FROM (SELECT * FROM w3 UNION ALL SELECT * FROM w4) AS t
		INTO _check;

		IF _check > 0
		THEN
			RAISE EXCEPTION 'Error 01: fn_check_cell_sup_app: For some of estimation_cell_collection = % miss null record in table t_estimation_cell_hierarchy in column estimation_cell_superior!',_ec_highest[i];
		END IF;
			
	END LOOP;
	---------------------------------------------------
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
  
ALTER FUNCTION @extschema@.fn_check_cell_sup_app() OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_check_cell_sup_app() TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_check_cell_sup_app() TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_check_cell_sup_app() TO public;

COMMENT ON FUNCTION @extschema@.fn_check_cell_sup_app() IS
'Function checks that for all the heighest estimation cell collections are null records in table t_estimation_cell_hierarchy in column estimation_cell_superior';
-- </function>



-- <function name="fn_get_lowest_gids_app" schema="extschema" src="functions/extschema/fn_get_lowest_gids_app.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- fn_get_lowest_gids_app
---------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION @extschema@.fn_get_lowest_gids_app
(
	_appropriate_gids	integer[]	-- vstupem je(jsou) gid(gidy) nejake urovne [gid odpovida gid z f_a_cell] a hledaji se gidy az te nejnizsi urovne, ktere tvori zadany vstupni gid 
)
RETURNS integer[] AS
$BODY$
DECLARE
	_res_1	integer[];
	_res	integer[];
BEGIN
	IF _appropriate_gids IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_lowest_gids_app: Vstupni argument _appropriate_gids nesmi byt NULL !';
	END IF;

	select array_agg(fac1.gid) from @extschema@.f_a_cell as fac1
	where fac1.estimation_cell in
		(
		select tech.estimation_cell from @extschema@.t_estimation_cell_hierarchy as tech
		where tech.estimation_cell_superior in	(
									select distinct fac.estimation_cell
									from @extschema@.f_a_cell as fac
									where fac.gid in (select unnest(_appropriate_gids))
									)
		)
	into _res_1;

	IF _res_1 IS NULL
	THEN
		_res := _appropriate_gids;
	ELSE
		_res := @extschema@.fn_get_lowest_gids_app(_res_1);
	END IF;

	RETURN _res;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;

ALTER FUNCTION @extschema@.fn_get_lowest_gids_app(integer[]) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_lowest_gids_app(integer[]) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_lowest_gids_app(integer[]) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_lowest_gids_app(integer[]) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_lowest_gids_app(integer[]) IS
'Funkce vraci gidy nejnizsi urovne, a to pro zadany gid (zadany seznam gidu). Vracene gidy jsou cizim klicem na pole gid do tabulky f_a_cell.';
-- </function>



-- <function name="fn_get_gids4under_estimation_cells_app" schema="extschema" src="functions/extschema/fn_get_gids4under_estimation_cells_app.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DROP FUNCTION @extschema@.fn_get_gids4under_estimation_cells_app(integer[],integer[]);

CREATE OR REPLACE FUNCTION @extschema@.fn_get_gids4under_estimation_cells_app
(
	_gids4check	integer[],
	_gids		integer[]
)
RETURNS integer[] AS
$BODY$
DECLARE
	_cell_4check		integer[];
	_count_check		integer;
	_gids4next_step		integer[];
	_res_gids		integer[];
	_gids4check_original	integer[];
	_gids4check_except	integer[];
begin
	-- pro vstupni _gids se zjisti jejich podrizene gidy
	select array_agg(fac1.gid) from @extschema@.f_a_cell as fac1
	where fac1.estimation_cell in
		(
		select tech.estimation_cell from @extschema@.t_estimation_cell_hierarchy as tech
		where tech.estimation_cell_superior in	(
									select distinct fac.estimation_cell
									from @extschema@.f_a_cell as fac
									where fac.gid in (select unnest(_gids))
									)
		)
	into _cell_4check;

	WITH
	w1 AS	(SELECT unnest(_gids4check) AS gids4check),
	w2 AS	(SELECT unnest(_gids) AS gids)
	SELECT count(w1.*) FROM w1
	WHERE w1.gids4check IN (SELECT w2.gids FROM w2)
	INTO _count_check;

	-- pokud se prichozi gidy z postupne prichozich estimation_cell
	-- stale nevyskytuji v seznamu gidu ZSJ, pak se tato funkce vola
	-- opakovane, s tim, ze seznam prichozich gidu se rozsiruje o seznam
	-- gidu podrizenych, tedy jde o spojeni vstupniho _gids a interniho
	-- seznamu _cell_4check

	-- pokud je promenna _count_check > 0, pak to znamena, ze hledani
	-- podrizenych gidu je ukonceno a do vystupu se vraci seznam gidu
	-- kdy se ze seznamu vstupnich _gids exceptem odstrani seznam gidu
	-- _gids4check

	-- vyjimka zde ale nastava u gidu [ZSJ], ktera sama osobne netvori
	-- celou jeji estimation_cell

	IF _count_check = 0
	THEN
		_gids4next_step := _gids || _cell_4check;
		_res_gids := @extschema@.fn_get_gids4under_estimation_cells_app(_gids4check,_gids4next_step);
	ELSE	
		_gids4check_original := _gids4check;
		_gids4check_except := _gids4check;
		
		-- pokud se v seznamu _gids4check vyskytuji gidy ZSJ, ktere sami o sobe
		-- netvori celou estimation_cell, se pak ze seznamu _gids4check musi odstranit,
		-- aby se tyto gidy ze seznamu _gids pak neodstranili

		FOR i IN 1..array_length(_gids4check,1)
		LOOP
			IF	(
				SELECT count(fac2.gid) > 1
				FROM @extschema@.f_a_cell AS fac2
				WHERE fac2.estimation_cell =
					(
					SELECT fac1.estimation_cell
					FROM @extschema@.f_a_cell AS fac1
					WHERE fac1.gid = _gids4check[i]
					)
				)
			THEN
				-- gid se ze seznamu _gids4check_original musi odstranit
				_gids4check_except := array_remove(_gids4check_except,_gids4check[i]);				
			ELSE
				-- gid se v seznamu _gids4check_original ponecha
				_gids4check_except := _gids4check_except;
			END IF;
		END LOOP;
	
		WITH
		w1 AS	(SELECT unnest(_gids || _gids4check_original) AS gids),
		w2 AS	(SELECT unnest(_gids4check_except) AS gids),
		w3 AS	(
			SELECT w1.gids FROM w1 except
			SELECT w2.gids FROM w2
			)
		SELECT array_agg(w3.gids) FROM w3
		INTO _res_gids;
	END IF;

	RETURN _res_gids;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;

ALTER FUNCTION @extschema@.fn_get_gids4under_estimation_cells_app(integer[],integer[]) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_gids4under_estimation_cells_app(integer[],integer[]) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_gids4under_estimation_cells_app(integer[],integer[]) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_gids4under_estimation_cells_app(integer[],integer[]) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_gids4under_estimation_cells_app(integer[],integer[]) IS
'Funkce pro zadane gidy (druhy vstupni argument funkce _gids), ktere tvori estimation_cell, vraci gidy za vsechny podrizene estimation_cell. Vracene gidy jsou cizim klicem na pole gid do tabulky f_a_cell.';
-- </function>



-- <function name="fn_api_get_estimation_cell_hierarchy" schema="extschema" src="functions/extschema/fn_api_get_estimation_cell_hierarchy.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
-- fn_api_get_estimation_cell_hierarchy
---------------------------------------------------------------------------------------------------
create or replace function @extschema@.fn_api_get_estimation_cell_hierarchy()
returns table
(
	id integer,
	estimation_cell_id integer,
	estimation_cell_collection_id integer,
	estimation_cell_label_cs varchar,
	estimation_cell_description_cs text,
	estimation_cell_label_en varchar,
	estimation_cell_description_en text,
	superior_estimation_cell_id integer
)
as
$body$
begin
		if	(
			select count(*) > 0
			from @extschema@.cm_estimation_cell_collection
			where estimation_cell_collection_lowest in (select cecc.id from @extschema@.c_estimation_cell_collection as cecc where cecc.use4estimates = true)
			)
		then
			raise exception 'Error 01: fn_api_get_estimation_cell_hierarchy: In table c_estimation_cell_collection is not set value false for the some of lowest estimation cell collection!';
		end if;

		return query
		with
		w_cell as materialized	(
								select
										cmec.estimation_cell,
										cmec.estimation_cell_collection
								from
										@extschema@.cm_estimation_cell as cmec
								where
										cmec.estimation_cell_collection in	(
																			select cecc.id from @extschema@.c_estimation_cell_collection as cecc
																			where cecc.use4estimates = true
																			)										
								)
		select
				(row_number() over(order by t1.estimation_cell))::integer as id,
				t1.estimation_cell as estimation_cell_id,
				w_cell.estimation_cell_collection as estimation_cell_collection_id,
				t2.label as estimation_cell_label_cs,
				t2.description as estimation_cell_description_cs,
				t2.label_en as estimation_cell_label_en,
				t2.description_en as estimation_cell_description_en,
				coalesce(t1.estimation_cell_superior,0) as superior_estimation_cell_id
		from
				@extschema@.t_estimation_cell_hierarchy as t1
				inner join w_cell on t1.estimation_cell = w_cell.estimation_cell
				inner join @extschema@.c_estimation_cell as t2 on w_cell.estimation_cell = t2.id;
end;
$body$
language plpgsql volatile;

ALTER FUNCTION @extschema@.fn_api_get_estimation_cell_hierarchy() OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_api_get_estimation_cell_hierarchy() TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_api_get_estimation_cell_hierarchy() TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_api_get_estimation_cell_hierarchy() TO public;

COMMENT ON FUNCTION @extschema@.fn_api_get_estimation_cell_hierarchy() IS
'Funkce vraci tabulku hierarchii cell z tabulky t_estimation_cell_hierarchy. Ve vystupu jsou cely, ktere maji v tabulce c_estimation_cell_collection nastaveno use4estimates = TRUE.';
-- </function>



DROP FUNCTION @extschema@.fn_get_gids4aux_total_app(integer,integer[],integer,boolean);
-- <function name="fn_get_gids4aux_total_app" schema="extschema" src="functions/extschema/fn_get_gids4aux_total_app.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DROP FUNCTION @extschema@.fn_get_gids4aux_total_app(integer,integer[],integer,boolean);

CREATE OR REPLACE FUNCTION @extschema@.fn_get_gids4aux_total_app
(
	_config_id			integer,
	_estimation_cell		integer[],
	_gui_version			integer,
	_recount			boolean DEFAULT FALSE
)
RETURNS TABLE
(
	step				integer,
	config_id			integer,
	estimation_cell		integer,
	gid integer,
	estimation_cell_label varchar,
	estimation_cell_description text,
	estimation_cell_label_en varchar,
	estimation_cell_description_en text
) AS
$BODY$
DECLARE
	-- spolecne promenne:
	_config_query					integer;
	_config_collection				integer;
	_configs					integer[];
	
	_ext_version_current				integer;
	_q						text;
	_complete					character varying;
	_categories					character varying;
	_config_ids_text				text;
	_config_ids_length				integer;
	_config_ids					integer[];
	_config_ids4check				integer[];
	_check_avc4categories				integer;
	_config_id_reference				integer;
	_config_id_base					integer;
	_check						integer;
			
	-- promenne pro vypocet:
	_gids4estimation_cell_i				integer[];
	_gids4estimation_cell_pocet			integer;
	_pocet						integer[];
	_max_pocet					integer;
	_gids4estimation_cell				integer[];
	_pocet_gids4estimation_cell			integer;
	_doplnek					integer[];
	_res_estimation_cell				integer[];
	_res_gids					integer[];
	_res_estimation_cell_summarization		integer[];
	_summarization					boolean[];
	
	-- promenne pro prepocet:
	_gids4estimation_cell_i_prepocet		integer[];
	_gids4estimation_cell_pocet_prepocet		integer;
	_pocet_prepocet					integer[];
	_max_pocet_prepocet				integer;
	_gids4estimation_cell_prepocet			integer[];
	_pocet_gids4estimation_cell_prepocet		integer;
	_doplnek_prepocet				integer[];
	_res_estimation_cell_prepocet			integer[][];
	_res_gids_prepocet				integer[][];
	_res_estimation_cell_prepocet_simple		integer[];
	_res_gids_prepocet_simple			integer[];
	_res_estimation_cell_prepocet_simple_original	integer[];
	_ecc4prepocet					integer;
	_eccl4prepocet					integer;
	_gids4estimation_cell_i_prepocet_mezistupen	integer[];
	_gids4estimation_cell_prepocet_mezistupen	integer[];
	_gids_mezistupen_with_vstupni_estimation_cell	integer[];
	_estimation_cells_mezistupen_i			integer[];
	_res_estimation_cell_prepocet_4_summarization	integer[];
	_res_gids_prepocet_4_recount			integer[];
	_last_ext_version_from_table			character varying;
	_last_ext_version_from_db			character varying;
	_res_estimation_cell_prepocet_4_recount		integer[];
	_res_estimation_cell_prepocet_4_check		integer[];

	_ext_version_current_label			character varying;

	_config_query_ii				integer;
	_config_id_reference_ii				integer;
	_config_id_base_ii				integer;
	_check_ii_1					integer;
	_check_ii_2					integer;
	_check_ii_boolean_1				integer[];
	_check_ii_boolean_2				integer[];
	_check_ii					integer;
	_check_ii_boolean				integer[];

	_res_gids_count_count				integer;
	_res_estimation_cell_4_with_i			integer[];
	_res_estimation_cell_4_with			integer[][];

	-- NEW --
	_config_function					integer;
	_ext_version_valid_from				integer;
	_ext_version_valid_until			integer;
	_ext_version_valid_from_label			character varying;
	_ext_version_valid_until_label			character varying;

	_check_count_estimation_cell_w5		integer;
BEGIN
	--------------------------------------------------------------------------------------------
	IF _config_id IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_gids4aux_total_app: Vstupni argument _config_id nesmi byt NULL!';
	END IF;

	IF _estimation_cell IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_gids4aux_total_app: Vstupni argument _estimation_cell nesmi byt NULL!';
	END IF;

	IF _gui_version IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_get_gids4aux_total_app: Vstupni argument _gui_version nesmi byt NULL!';
	END IF;
	--------------------------------------------------------------------------------------------
	-- kontrola vyplnenosti null zaznamu pro vsechny nejvyssi urovne estimation_cell_collection 
	-- v tabulce t_estimation_cell_hierarchy u sloupce cell_sup
	PERFORM @extschema@.fn_check_cell_sup_app();
	--------------------------------------------------------------------------------------------
	-- zjisteni konfiguracnich promennych z tabulky t_config pro vstupni _config_id
	SELECT
		tc.config_query,
		tc.config_collection
	FROM
		@extschema@.t_config AS tc
	WHERE
		tc.id = _config_id
	INTO
		_config_query,
		_config_collection;
	--------------------------------------------------------------------------------------------
	IF _config_query IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_get_gids4aux_total_app: Pro vstupni argument _config_id = % nenalezeno config_query v tabulce t_config!',_config_id;
	END IF;

	IF _config_collection IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_get_gids4aux_total_app: Pro vstupni argument _config_id = % nenalezeno config_collection v tabulce t_config!',_config_id;
	END IF;
	--------------------------------------------------------------------------------------------
	-- NEW --
	-- zjisteni config_function pro zjistene _config_collection
	SELECT tcc.config_function FROM @extschema@.t_config_collection AS tcc
	WHERE tcc.id = _config_collection
	INTO _config_function;

	IF _config_function IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_get_gids4aux_total_app: Pro interni promennou _config_collection = % nenalezeno config_function v tabulce t_config_collection!',_config_collection;
	END IF;

	IF NOT(_config_function = ANY(array[100,200,300,400]))
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_get_gids4aux_total_app: Interni promenna _config_function = % musi byt hodnota 100, 200, 300 nebo 400!',_config_function;
	END IF;

	-- zjisteni ext_version_valid_from a ext_version_valid_until
	SELECT cmcf.ext_version_valid_from, cmcf.ext_version_valid_until
	FROM @extschema@.cm_ext_config_function AS cmcf
	WHERE cmcf.active = TRUE
	AND cmcf.config_function = _config_function
	INTO
		_ext_version_valid_from,
		_ext_version_valid_until;

	IF (_ext_version_valid_from IS NULL OR _ext_version_valid_until IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 08: fn_get_gids4aux_total_app: Pro interni promennou _config_function = % nenalezeno ext_version_valid_from nebo ext_version_valid_until v tabulce cm_ext_config_function!',_config_function;
	END IF;

	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_valid_from  INTO _ext_version_valid_from_label;
	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_valid_until INTO _ext_version_valid_until_label;
	--------------------------------------------------------------------------------------------
	-- zjisteni vsech config_id pro _config_collection
	SELECT array_agg(tc.id ORDER BY tc.id) FROM @extschema@.t_config AS tc
	WHERE tc.config_collection = _config_collection
	INTO _configs;
	--------------------------------------------------------------------------------------------
	-- nalezeni nejaktualnejsi verze extenze pro vstupni verzi GUI aplikace
	select max(ext_version) FROM @extschema@.cm_ext_gui_version
	WHERE gui_version = _gui_version
	INTO _ext_version_current;

	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_current
	INTO _ext_version_current_label;
	--------------------------------------------------------------------------------------------
	-- NEW --
	-- kontrola, ze nejaktualnejsi verze extenze je v rozmezi aktivniho intervalu verzi pro danou variantu uhrnu, ktera je indentifikovana pomoci hodnoty config_function !!!
	IF NOT((_ext_version_current >= _ext_version_valid_from) AND (_ext_version_current <= _ext_version_valid_until))
	THEN
		RAISE EXCEPTION 'Chyba 09: fn_get_gids4aux_total_app: Aktualni verze extenze _ext_version_current [%] = % neni v rozmezi aktivniho intervalu verzi pro danou variantu uhrnu !',_ext_version_current_label,_ext_version_current;
	END IF;	
	--------------------------------------------------------------------------------------------
	IF _recount = TRUE -- prepocet
	THEN	
		raise exception 'Error: fn_get_gids4aux_total_app: The variant "RECOUNT" of this function is not changed for the new DB structure (e.g. new table cm_estimation_cell, new table t_estimation_cell_hierarchy...)!';
		-----------------------------------------------------------------------------------
		-- KONTROLA EXISTENCE v DB => kontrola prepocet bud povoli nebo ne <= kontrola probiha na urovni CELE KONFIGURACE !!!

		-- 1. [na verzi extenze NE-ZALEZI]
		--	A: prepocet ANO => pro danou celu existuji v DB vsechny kategorie dane konfigurace
		--	B: prepocet NE	=> pro danou celu nektera kategorie z dane konfigurace v DB schazi => RESIT klasickym VYPOCTEM !!!

		-- 2. [na verzi extenze ZALEZI]
		--	A: prepocet NE	=> pro danou celu jsou hodnoty jednotlivych kategorie v extenzi, ktera odpovida (spada) do povoleneho intervalu VERZI => zde prepocet postrada smysl
		--	B: prepocet ANO	=> pro danou celu je hodnota u nektere kategorie v extenzi, ktera NE-SPADA do povoleneho intervalu VERZI, pozn. prepocet bude, respektive by mel probihat
		--			   jen u tech kategorii, jejichz extenze nespadaji do intervalu verzi

		-----------------------------------------------------------------------------------
		FOR i IN 1..array_length(_estimation_cell,1) -- cyklus pres cely
		LOOP
			FOR ii IN 1..array_length(_configs,1) -- cyklus pres jednotlive kategorie dane konfigurace
			LOOP
				SELECT tc.config_query FROM @extschema@.t_config AS tc
				WHERE tc.id = _configs[ii]
				INTO _config_query_ii;
						
				IF _config_query_ii = 500 -- REFERENCE
				THEN
					SELECT tc.categories::integer
					FROM @extschema@.t_config AS tc
					WHERE tc.id = _configs[ii]
					INTO _config_id_reference_ii;

					IF _config_id_reference_ii IS NULL
					THEN
						RAISE EXCEPTION 'Chyba 10: fn_get_gids4aux_total_app: Kontrolovane _config_id = % je reference [config_query = 500] a v konfiguraci [v tabulce t_config] nema ve sloupci categories nakonfigurovanou referenci!',_configs[ii];
					END IF;

					_config_id_base_ii := _config_id_reference_ii;
				ELSE
					_config_id_base_ii := _configs[ii]; -- vstupnim config_id je bud konfigurace ze zakladni skupiny [100,300,400] nebo konfigurace z agregacni skupiny ci navazujici kolekce [200,300,400]
				END IF;

				-----------------------------------------------
				-- kontrola 1. => [na verzi extenze NE-ZALEZI]
				WITH
				w1 AS	(
					SELECT tat.* FROM @extschema@.t_aux_total AS tat
					WHERE tat.estimation_cell = _estimation_cell[i]
					AND tat.config = _config_id_base_ii
					)
				SELECT count(*) FROM w1	-- pokud je pocet vetsi jak 0 tak zaznam(y) exisuje(i)
				INTO _check_ii_1;

				IF _check_ii_1 > 0
				THEN
					IF ii = 1
					THEN
						_check_ii_boolean_1 := array[0];
					ELSE
						_check_ii_boolean_1 := _check_ii_boolean_1 || array[0];
					END IF;
				ELSE
					IF ii = 1
					THEN
						_check_ii_boolean_1 := array[1];
					ELSE
						_check_ii_boolean_1 := _check_ii_boolean_1 || array[1];
					END IF;
				END IF;
				-----------------------------------------------
				-- kontrola 2. => [na verzi extenze ZALEZI]
				WITH
				w1 AS	(
					SELECT tat.* FROM @extschema@.t_aux_total AS tat
					WHERE tat.estimation_cell = _estimation_cell[i]
					AND tat.config = _config_id_base_ii
					AND tat.ext_version >= _ext_version_valid_from
					AND tat.ext_version <= _ext_version_valid_until
					)
				SELECT count(*) FROM w1
				INTO _check_ii_2;

				IF _check_ii_2 > 0 -- pokud je pocet vetsi jak 0 tak zaznam(y) exisuje(i)
				THEN
					IF ii = 1
					THEN
						_check_ii_boolean_2 := array[0];
					ELSE
						_check_ii_boolean_2 := _check_ii_boolean_2 || array[0];
					END IF;
				ELSE
					IF ii = 1
					THEN
						_check_ii_boolean_2 := array[1];
					ELSE
						_check_ii_boolean_2 := _check_ii_boolean_2 || array[1];
					END IF;
				END IF;
				-----------------------------------------------
			END LOOP;
			
			-- vyhodnoceni vysledku kontroly 1
			IF	(
				SELECT sum(t.check_ii_boolean_1) > 0
				FROM (SELECT unnest(_check_ii_boolean_1) AS check_ii_boolean_1) AS t
				)
			THEN
				RAISE EXCEPTION 'Chyba 11: fn_get_gids4aux_total_app: Prepocet neni mozny => pro zadanou estimation_cell = % doposud neexistuje v tabulce t_aux_total hodnota aux_total pro nekterou z config_id = %. Resit klasickym vypoctem!',_estimation_cell[i],_configs;
			END IF;	

			-- vyhodnoceni vysledku kontroly 2
			IF	(
				SELECT sum(t.check_ii_boolean_2) = 0
				FROM (SELECT unnest(_check_ii_boolean_2) AS check_ii_boolean_2) AS t
				)
			THEN
				RAISE EXCEPTION 'Chyba 12: fn_get_gids4aux_total_app: Prepocet neni mozny => pro zadanou estimation_cell = % jiz v tabulce t_aux_total existuji hodnoty aux_total pro vsechny kategorie =  %, ktere jsou ve verzi, jez odpovida nektere verzi od % do verze %!',_estimation_cell[i],_configs,_ext_version_valid_from_label,_ext_version_valid_until_label;

				-- tato podminka defakto ze vstupniho argumentu _estimation_cell (muze obsahovat i ZSJ) donuti uzivatele tam mit jen cely, ktere jsou opravdu povoleny pro prepocet !!!
				-- navic prepocet je konstruovan tak, ze pri prepoctu se prepocitaji i vsechny cely podrizene, a to jen ty podrizenene cely, ktere uz jsou v DB vypocitany a nespadaji do povoleneho intervalu VERZI
			END IF;
			
		END LOOP;
		-----------------------------------------------------------------------------------
		-----------------------------------------------------------------------------------
		IF _config_query = ANY(array[100,200,300,400]) -- => protinani, soucet a doplnky
		THEN
			-- proces ziskani:
			-- pole poli estimation_cell [jde jen o vstupni estimation_cell (muze jit i o estimation_cell, ktere jsou ZSJ)] <= _res_estimation_cell_prepocet
			-- pole poli ZSJ [jde o gidy, ktere tvori vstupni estimation_cell (gidy se zde mohou opakovat)] <= _res_gids_prepocet
			-----------------------------------------------------
			-----------------------------------------------------
			-----------------------------------------------------
			-----------------------------------------------------
			-- cyklus pro ziskani POCTu u PREPOCTU
			FOR i IN 1..array_length(_estimation_cell,1)
			LOOP
				-- zjisteni gidu z tabulky f_a_cell pro i-tou _estimation_cell
				SELECT array_agg(fac.gid ORDER BY fac.gid) FROM @extschema@.f_a_cell AS fac
				WHERE fac.estimation_cell = _estimation_cell[i]
				INTO _gids4estimation_cell_i_prepocet;

				IF _gids4estimation_cell_i_prepocet IS NULL
				THEN
					RAISE EXCEPTION 'Chyba 13: fn_get_gids4aux_total_app: Pro _estimation_cell[i] = % nenalezeny zadne zaznamy v tabulce f_a_cell!',_estimation_cell[i];
				END IF;

				_gids4estimation_cell_pocet_prepocet := array_length(@extschema@.fn_get_lowest_gids_app(_gids4estimation_cell_i_prepocet),1);

				IF i = 1
				THEN
					_pocet_prepocet := array[_gids4estimation_cell_pocet_prepocet];
				ELSE
					_pocet_prepocet := _pocet_prepocet || array[_gids4estimation_cell_pocet_prepocet];
				END IF;
				
			END LOOP;
			-----------------------------------------------------
			-- zjisteni nejdelsiho pole _gids4estimation_cell_pocet_prepocet
			SELECT max(t.pocet) FROM (SELECT unnest(_pocet_prepocet) as pocet) AS t
			INTO _max_pocet_prepocet;
			-----------------------------------------------------
			-- cyklus ziskani GIDu nejnizsi urovne pro vstupni _estimation_cell[]
			FOR i IN 1..array_length(_estimation_cell,1)
			LOOP
				-- zjisteni gidu z tabulky f_a_cell pro i-tou _estimation_cell
				SELECT array_agg(fac.gid ORDER BY fac.gid) FROM @extschema@.f_a_cell AS fac
				WHERE fac.estimation_cell = _estimation_cell[i]
				INTO _gids4estimation_cell_i_prepocet;

				-- volani funkce fn_get_lowest_gids
				-- funkce by mela vratit seznam gidu nejnizsi urovne
				_gids4estimation_cell_prepocet := @extschema@.fn_get_lowest_gids_app(_gids4estimation_cell_i_prepocet);

				-- zjisteni poctu prvku(gidu) v poli _gids4estimation_cell_prepocet
				_pocet_gids4estimation_cell_prepocet := array_length(_gids4estimation_cell_prepocet,1);

				-- vytvoreni pole s nulama o poctu _max_pocet - _pocet_gids4estimation_cell_prepocet
				SELECT array_agg(t.gid_gs) FROM (SELECT 0 AS gid_gs FROM generate_series(1,(_max_pocet_prepocet - _pocet_gids4estimation_cell_prepocet))) AS t
				INTO _doplnek_prepocet;

				-- proces doplneni 0 tak aby pole _gids4estimation_cell melo delku _max_pocet
				_gids4estimation_cell_prepocet := _gids4estimation_cell_prepocet || _doplnek_prepocet;

				IF i = 1
				THEN
					_res_estimation_cell_prepocet := array[array[_estimation_cell[i]]];	-- do pole poli se ulozi pozadovane vstupni estimation_cell pro prepocet
					_res_gids_prepocet := array[_gids4estimation_cell_prepocet];		-- do pole poli se ulozi pozadovane gidy nejnizsi urovne pro prepocet
				ELSE
					_res_estimation_cell_prepocet := _res_estimation_cell_prepocet || array[array[_estimation_cell[i]]];
					_res_gids_prepocet := _res_gids_prepocet || array[_gids4estimation_cell_prepocet];
				END IF;

			END LOOP;
			-----------------------------------------------------
			-- vysvetleni internich argumentu:
			-- _res_estimation_cell_prepocet	=> jde o pole poli s estimation_cell ze vstupu uzivatele urcenych pro prepocet [muze obsahovat i zakladni stavebni jednotku]
								-- pokud to doslo az sem, tak se v poli nemuze vyskytovat estimation_cell nebo defakto ZSJ, ktera je ve verzi, ktere spada
								-- do povoleneho intervalu VERZI (pozn. v intervalu je vzdy zahrnuta i aktualni systemova verze extenze)
			-- _res_gids_prepocet			=> jde o pole poli gidu zakladnich stavebnich jednotek pro vstupni estimation_cell urcenych pro prepocet, [mozny vyskyt DUPLICIT]
								-- pokud to doslo az sem, tak se zde mohou vyskytovat ZSJ, ktere uz jsou v databazi ve verzi, ktera spada do povoleneho intervalu
								-- VERZI => tyto gidy neni nutno prepocitavat, prepocitaji se jen gidy verze, ktere NE-SPADAJI do povoleneho intervalu VERZI
			-----------------------------------------------------
			-----------------------------------------------------
			-----------------------------------------------------
			-----------------------------------------------------
	

			-----------------------------------------------------
			-- CONFIG_ID_BASE --
			-----------------------------------------------------
			IF _config_query = 500 -- REFERENCE
			THEN
				SELECT tc.categories::integer
				FROM @extschema@.t_config AS tc
				WHERE tc.id = _config_id
				INTO _config_id_reference;

				IF _config_id_reference IS NULL
				THEN
					RAISE EXCEPTION 'Chyba 14: fn_get_gids4aux_total_app: Vstupni _config_id = % je reference [config_query = 500] a v konfiguraci [v tabulce t_config] nema nakonfigurovanou referenci!',_config_id;
				END IF;

				_config_id_base := _config_id_reference;
			ELSE
				_config_id_base := _config_id; -- vstupnim config_id je bud konfigurace ze zakladni skupiny [100,300,400] nebo konfigurace z agregacni skupiny ci navazujici kolekce [200,300,400]
			END IF;
			-----------------------------------------------------
			-----------------------------------------------------

		
			-----------------------------------------------------
			-- proces kontroly, ze vstupni ESTIMATION_CELL uz
			-- byly alespon jednou v minulosti vypocitany, tzn.
			-- ze na verzi ext_version nezalezi
			-- => kontrola probiha na urovni KATEGORIE dane konfigurace
			-----------------------------------------------------
			-- prevedeni _res_estimation_cell_prepocet na simple pole
			SELECT array_agg(t.recp) FROM (SELECT unnest(_res_estimation_cell_prepocet) AS recp) AS t
			INTO _res_estimation_cell_prepocet_simple;

			-- kontrola pro _res_estimation_cell_prepocet_simple
			FOR i IN 1..array_length(_res_estimation_cell_prepocet_simple,1)
			LOOP
				IF	(
					SELECT count(tat.*) = 0
					FROM @extschema@.t_aux_total AS tat
					WHERE tat.config = _config_id_base
					AND tat.estimation_cell = _res_estimation_cell_prepocet_simple[i]
					-- zde ext_version nehraje roli
					)
				THEN
					RAISE EXCEPTION 'Chyba 15: fn_get_gids4aux_total_app: Pro config_id = % a estimation_cell = % doposud neexistuje zaznam v tabulce t_aux_total. Prepocet tedy neni mozny! Nutno dopocitat klasickym vypoctem!',_config_id_base,_res_estimation_cell_prepocet_simple[i];
				END IF;
			END LOOP;
			-----------------------------------------------------
			-----------------------------------------------------


			-----------------------------------------------------
			-- proces kontroly, ze ZSJ, ktere tvori jednotlive
			-- vstupni estimation_cell, uz byly alespon jednou
			-- v minulosti vypocitany, verze extenze zde nehraje
			-- roli		
			-- => kontrola probiha na urovni KATEGORIE dane konfigurace
			-----------------------------------------------------
			-- proces prevedeni _res_gids_prepocet na simple pole
			-- a provedeni distinctu gidu ZSJ
			WITH
			w AS	(
				SELECT DISTINCT t.rgp
				FROM (SELECT unnest(_res_gids_prepocet) AS rgp) AS t
				WHERE t.rgp IS DISTINCT FROM 0
				)
			SELECT array_agg(w.rgp) FROM w
			INTO _res_gids_prepocet_simple;

			-- samotna kontrola pro _res_gids_prepocet_simple
			FOR i IN 1..array_length(_res_gids_prepocet_simple,1)
			LOOP
				-- jde-li o CQ 100, pak kontrola musi probihat
				-- pres gidy, respektive pres sloupec cell
				-- v tabulce t_aux_total
				IF _config_query = 100
				THEN
					IF	(
						SELECT count(tat.*) = 0
						FROM @extschema@.t_aux_total AS tat
						WHERE tat.config = _config_id_base
						AND tat.cell = _res_gids_prepocet_simple[i]
						-- zde ext_version nehraje roli,
						-- poznamka: ZSJ, ktere svou verzi spadaji do povoleneho intervalu VERZI, budou nize v kodu
						-- ze seznamu vyjmuty a do samotneho prepoctu nepujdou !!!
						)
					THEN
						RAISE EXCEPTION 'Chyba 16: fn_get_gids4aux_total_app: Pro config_id = % a cell = % doposud neexistuje zaznam v tabulce t_aux_total. Prepocet tedy neni mozny! Nutno dopocitat klasickym vypoctem!',_config_id_base,_res_gids_prepocet_simple[i];
					END IF;
				END IF;

				-- jde-li o CQ 200,300,400, pak kontrola musi probihat
				-- rovnez pres gid, respektive pres sloupec
				-- cell v tabulce t_aux_total, ale je zde jeste vyjimka
				IF _config_query = ANY(array[200,300,400])
				THEN
					-- pokud je i-ty gid v _res_gids_prepocet_simple ZSJ, ktera je tvorena 2 a vice
					-- geometriemi, pak se nize uvedena kontrola provadet nemusi, a to proto, ze
					-- pro takove ZSJ se pro CQ 200,300,400 do databaze vubec neukladaji
					IF	(
						SELECT count(fac2.gid) = 1
						FROM @extschema@.f_a_cell AS fac2
						WHERE fac2.estimation_cell =
									(
									SELECT fac1.estimation_cell
									FROM @extschema@.f_a_cell AS fac1
									WHERE fac1.gid = _res_gids_prepocet_simple[i]
									)
						)
					THEN
						IF	(
							SELECT count(tat.*) = 0
							FROM @extschema@.t_aux_total AS tat
							WHERE tat.config = _config_id_base
							AND tat.cell = _res_gids_prepocet_simple[i]	
							-- zde ext_version nehraje roli
							-- poznamka: ZSJ, ktere svou verzi spadaji do povolenoho intervalu VERZI, budou nize v kodu
							-- ze seznamu vyjmuty a do samotneho prepoctu nepujdou !!!
							)
						THEN
							RAISE EXCEPTION 'Chyba 17: fn_get_gids4aux_total_app: Pro config_id = % a cell = % doposud neexistuje zaznam v tabulce t_aux_total. Prepocet tedy neni mozny! Nutno dopocitat klasickym vypoctem!',_config_id_base,_res_gids_prepocet_simple[i];
						END IF;
					END IF;
				END IF;
			END LOOP;
			-----------------------------------------------------
			-----------------------------------------------------


			-----------------------------------------------------
			-- zachovani _res_estimation_cell_prepocet_simple
			_res_estimation_cell_prepocet_simple_original := _res_estimation_cell_prepocet_simple;
			-----------------------------------------------------


			-----------------------------------------------------
			-- proces doplneni interni promenne
			-- _res_estimation_cell_prepocet_simple
			-- podrizenymi estimation_cell
			-----------------------------------------------------
			FOR i in 1..array_length(_estimation_cell,1)
			LOOP
				SELECT cec.estimation_cell_collection
				FROM @extschema@.c_estimation_cell AS cec
				WHERE cec.id = _estimation_cell[i]
				INTO _ecc4prepocet;
				
				SELECT cmecc.estimation_cell_collection_lowest
				FROM @extschema@.cm_estimation_cell_collection AS cmecc
				WHERE cmecc.estimation_cell_collection = _ecc4prepocet
				INTO _eccl4prepocet;

				IF _ecc4prepocet = _eccl4prepocet
				THEN
					-- pro prepocet uz se nemusi zjistovat podrizene estimation_cell
					_res_estimation_cell_prepocet_simple := _res_estimation_cell_prepocet_simple; -- datovy typ integer[]
				ELSE
					-- pro prepocet se musi zjistit podrizene estimation_cell
					
					-- zjisteni vsech zakladnich gidu tvorici i-tou estimation_cell
					SELECT array_agg(fac.gid ORDER BY fac.gid) FROM @extschema@.f_a_cell AS fac
					WHERE fac.estimation_cell = _estimation_cell[i]
					INTO _gids4estimation_cell_i_prepocet_mezistupen;

					_gids4estimation_cell_prepocet_mezistupen := @extschema@.fn_get_lowest_gids_app(_gids4estimation_cell_i_prepocet_mezistupen);	

					_gids_mezistupen_with_vstupni_estimation_cell := @extschema@.fn_get_gids4under_estimation_cells_app(_gids4estimation_cell_prepocet_mezistupen,_gids4estimation_cell_i_prepocet_mezistupen);

					SELECT array_agg(fac.estimation_cell) FROM @extschema@.f_a_cell AS fac
					WHERE fac.gid IN (SELECT unnest(_gids_mezistupen_with_vstupni_estimation_cell))
					AND fac.estimation_cell IS DISTINCT FROM _estimation_cell[i]
					INTO _estimation_cells_mezistupen_i;

					IF _estimation_cells_mezistupen_i IS NULL
					THEN
						-- RAISE EXCEPTION 'Chyba 11: fn_get_gids4aux_total_app: Interni promenna _estimation_cells_mezistupen_i nesmi byt nikdy NULL!';
						-- tato vyjimka byla zde puvodne chybne, protoze pokud je na vstupu napr. okres, pak mezistupen pro estimation_cell mezi okresem
						-- a KU uz vlastne neni, toto nastava i mezi OPLO a PLO, takze potom to bude takto:
						_res_estimation_cell_prepocet_simple := _res_estimation_cell_prepocet_simple;

						-- chybejici PLO pro OPLO se zde ale musi doplnit => veresit ve funkci fn_get_gids4under_estimation_cells
					ELSE
						_res_estimation_cell_prepocet_simple := _res_estimation_cell_prepocet_simple || _estimation_cells_mezistupen_i;
					END IF;			
				END IF;	
			END LOOP;
			-----------------------------------------------------
			-----------------------------------------------------

			-----------------------------------------------------
			-- distinct hodnot estimation_cell v promenne
			-- _res_estimation_cell_prepocet_simple
			WITH
			w AS	(SELECT distinct t.ecp FROM (SELECT unnest(_res_estimation_cell_prepocet_simple) AS ecp) AS t)
			SELECT array_agg(w.ecp order by w.ecp) FROM w
			INTO _res_estimation_cell_prepocet_simple;

			-- _res_estimation_cell_prepocet_simple [seznam vsech moznych estimation_cell pro prepocet], obsahuje:
			-- v pripade NUTS => vstupni estimation_cell a muzou to byt i ZSJ 
			-- v pripade OPLO => vstupni estimation_cell, rozdrobene ZSJ aplikace neposle, ale napr. pro OPLO se vrati podrizeni PLOcka

			-- takze jde o seznam => VSTUPNI ESTIMATION_CELL + jejich vsechny mozne [ALL] PODRIZENE ESTIMATION_CELL
			-----------------------------------------------------
			

			-----------------------------------------------------
			-- propojeni _res_estimation_cell_prepocet_simple
			-- s databazi a zjisteni estimation_cell, ktere v
			-- databazi jeste pro dane config a povoleny interval
			-- VERZI nejou vypocitany
			-----------------------------------------------------
			WITH
			w1 AS	(
				SELECT DISTINCT tat.estimation_cell
				FROM @extschema@.t_aux_total AS tat
				WHERE tat.config = _config_id_base
				AND tat.cell IS NULL			-- cell se ukladaji jen pro ZSJ, takze toto zajisti ze ZSJ do INTO nepujdou
				AND tat.estimation_cell IN
					(SELECT unnest(_res_estimation_cell_prepocet_simple))
				--AND tat.ext_version < _ext_version_current
				AND tat.ext_version <= _ext_version_valid_until -- defakto valid_until se vzdy bude shodovat s _ext_version_current
				),
			w2 AS	(
				SELECT DISTINCT tat.estimation_cell
				FROM @extschema@.t_aux_total AS tat
				WHERE tat.config = _config_id_base
				AND tat.cell IS NULL
				AND tat.estimation_cell IN
					(SELECT unnest(_res_estimation_cell_prepocet_simple))
				AND tat.ext_version >= _ext_version_valid_from
				AND tat.ext_version <= _ext_version_valid_until
									-- tato podminka je zde proto, ze uzivatel mohl pred tim PREPOCET ZASTAVIT,
									-- tzn. ze jen cast estimation_cell se vypocitala, respektive prepocitala
									-- v ext_version_current, takze pak do prepoctu znovu potrebuji znat jen ty
									-- estimation_cell, ktere jeste nespadaji do povoleneho intervalu VERZI
				),
			w3 AS	(
				SELECT w1.estimation_cell FROM w1 EXCEPT
				SELECT w2.estimation_cell FROM w2
				)
			SELECT array_agg(w3.estimation_cell) FROM w3
			INTO _res_estimation_cell_prepocet_4_summarization;	-- THIS is A -- CQ 100,200,300,400 a NUTS1-4 nebo OPLO,PLO

			-- jde o seznam POZADOVANYCH ESTIMATION_CELL pro SUMARIZACI u PREPOCTU

			-- u CQ 100	a	NUTS1-4		=> to je v poradku
			-- u CQ 100	a	OPLO,PLO 	=> to je v poradku

			-- u CQ 300	a	NUTS1-4		=> to je v poradku <= CQ 100 uz musi existovat
			-- u CQ 300 	a	OPLO,PLO	=> to je v poradku <= CQ 100 uz musi existovat

			-- tzn. pro kontrolu proveditelnosti CQ 300 musi do cyklu jit _res_estimation_cell_prepocet_4_summarization
			-----------------------------------------------------
			-----------------------------------------------------


			--raise notice '_res_estimation_cell_prepocet_4_summarization:%',_res_estimation_cell_prepocet_4_summarization;
			--raise exception 'STOP';


			-----------------------------------------------------
			-----------------------------------------------------
			-----------------------------------------------------
			-- propojeni seznamu ZSJ s databazi, v pride NUTS1-4
			-- jde o KU, v pripade OPLO,PLO jde o rozdrobene casti
			-- [ze seznamu se musi vyjmout ZSJ, ktere uz jsou ve
			-- verzi spadajici do povoleneho intervalu VERZI

			-- u CQ 100 se ZSJ ukladaji jak pro NUTS1-4 tak pro
			-- OPLO a PLO => takze zde se musi pracovat se
			-- sloupcem cell v tabulce t_aux_total, protoze pro
			-- ZSJ u OPLO,PLO se estimation_cell neuklada

			-- u CQ 200,300,400 a pro NUTS1-4 se ZSJ do t_aux_total
			-- ukladaji => zde se muze pracovat bud se sloupcem cell
			-- nebo se sloupcem estimation_cell
			
			-- u CQ 200,300,400 a pro OPLO,PLO se ZSJ do t_aux_total
			-- NE-ukladaji (proc, jde totiz o ZSJ, ktera sama netvori
			-- celou estimation_cell, jejich aux_total hodnoty se
			-- neukladaji, ukladaji se az jejich sumarizace), zde
			-- se musi pracovat ze sloupce estimation_cell
			-----------------------------------------------------
			IF _config_query = 100
			THEN
				WITH
				w1 AS	(SELECT unnest(_res_gids_prepocet_simple) AS gid),	-- _res_gids_prepocet_simple [ALL KU nebo ALL rozdrobene casti PLO tvorici vstupni estimation_cell pro prepocet]
				w2 AS	(
					SELECT DISTINCT tat.cell
					FROM @extschema@.t_aux_total AS tat
					WHERE tat.config = _config_id_base
					AND tat.cell IN (SELECT w1.gid FROM w1)
					AND tat.ext_version >= _ext_version_valid_from
					AND tat.ext_version <= _ext_version_valid_until
					),
				w3 AS	(
					SELECT
						w1.gid,
						w2.cell
					FROM
						w1 LEFT JOIN w2 ON w1.gid = w2.cell
					)
				SELECT
					array_agg(w3.gid) FROM w3 WHERE w3.cell IS NULL
				INTO
					_res_gids_prepocet_4_recount;	-- THIS is B -- CQ 100 a NUTS1-4 nebo OPLO,PLO
			END IF;

			-- seznam ZSJ [KU nebo rozdrobene casti PLO] POZADOVANE
			-- pro prepocet [jde o gidy, ktere jsou starsi verze nez
			-- je povoleny interval VERZI
			-----------------------------------------------------
			-----------------------------------------------------

			
			-----------------------------------------------------
			-----------------------------------------------------
			IF _config_query = ANY(array[200,300,400])
			THEN			
				WITH
				w1 AS	(SELECT unnest(_res_gids_prepocet_simple) AS gid),	-- _res_gids_prepocet_simple [ALL KU nebo ALL rozdrobene casti PLO tvorici vstupni estimation_cell pro prepocet] 
				w2 AS	(-- ALL estimation_cell --				-- vyber estimation_cell z f_a_cell na zaklade gidu (ZSJ)
					SELECT DISTINCT fac.estimation_cell			-- provedeni distinctu estimation_cell
					FROM @extschema@.f_a_cell AS fac
					WHERE fac.gid IN (SELECT w1.gid FROM w1)
					),
					-- vyber (propojeni s DB) estimation_cell co je v DB, zde nejprve nezalezi na verzi ext_version
				w3 AS	(-- DB estimation_cell --
					SELECT DISTINCT tat1.estimation_cell
					FROM @extschema@.t_aux_total AS tat1
					WHERE tat1.config = _config_id_base
					AND tat1.estimation_cell IN (SELECT w2.estimation_cell FROM w2)
					),
				w4 AS	(-- DB estimation_cell + povoleny interval VERZI
					SELECT DISTINCT tat2.estimation_cell
					FROM @extschema@.t_aux_total AS tat2
					WHERE tat2.config = _config_id_base
					AND tat2.estimation_cell IN (SELECT w3.estimation_cell FROM w3)
					AND tat2.ext_version >= _ext_version_valid_from
					AND tat2.ext_version <= _ext_version_valid_until
					),
				w5 AS	(
					SELECT
						w3.estimation_cell AS estimation_cell_w3,
						w4.estimation_cell AS estimation_cell_w4
					FROM
						w3 LEFT JOIN w4 ON w3.estimation_cell = w4.estimation_cell
					)
				SELECT
					array_agg(w5.estimation_cell_w3) FROM w5 WHERE w5.estimation_cell_w4 IS NULL
				INTO
					_res_estimation_cell_prepocet_4_recount; -- THIS is C -- CQ 200,300,400 a NUTS1-4 nebo OPLO,PLO
			END IF;

			-- seznam ZSJ [KU nebo cele PLO] kodovanych pres estimation_cell,
			-- ktere jsou POZADOVANY pro prepocet [jde o ZSJ, ktere jsou
			-- starsi verze nez je aktualni systemova verze extenze
			----------------------------------------------------
			----------------------------------------------------
		END IF;
		-----------------------------------------------------
		-----------------------------------------------------
		

		-------------------------------------------
		--raise notice '_res_estimation_cell_prepocet_4_summarization:%',_res_estimation_cell_prepocet_4_summarization;
		--raise notice '_res_gids_prepocet_4_recount:%',_res_gids_prepocet_4_recount;
		--raise notice '_res_estimation_cell_prepocet_4_recount:%',_res_estimation_cell_prepocet_4_recount;
		--raise exception 'STOP';
		-------------------------------------------

		IF _config_query = 100	-- vetev pro zakladni protinani [ZSJ] a sumarizaci
		THEN
			_q :=
				'
				WITH
				---------------------------------------------------------
				w1 AS	(SELECT unnest($8) AS gid),	-- THIS is B
				w2 AS	(
					SELECT
						1 AS step,
						$3 AS config,
						fac.estimation_cell,
						w1.gid
					FROM
						w1
					LEFT
					JOIN	@extschema@.f_a_cell AS fac ON w1.gid = fac.gid -- toto doplni estimation_cell [v aplikaci by se mohlo spojit LABEL a GID]
					),
				---------------------------------------------------------
				w3 AS	(SELECT unnest($9) AS estimation_cell), -- THIS is A
				w4 AS	(
					SELECT
						2 AS step,
						$3 AS config,
						w3.estimation_cell,
						0::integer AS gid
					FROM
						w3
					ORDER
						BY w3.estimation_cell
					),
				w5 AS	(
					SELECT
						w2.step,
						w2.config,
						w2.estimation_cell,
						w2.gid
					FROM
						w2
					ORDER
						BY w2.step, w2.config, w2.gid, w2.estimation_cell
					)
				---------------------------------------------------------
				,w6 as	(
						SELECT * FROM w5 UNION ALL
						SELECT * FROM w4
						)
				select
						w6.*,
						cec.label as estimation_cell_label,
						cec.description as estimation_cell_description,
						cec.label_en as estimation_cell_label_en,
						cec.description_en as estimation_cell_description_en
				from
						w6
						inner join @extschema@.c_estimation_cell as cec
						on w6.estimation_cell = cec.id				
				';
			
		ELSE
			-- _config_query = ANY(array[200,300,400,500]) -- vetev pro soucet a doplnky

			-- spojeni _res_estimation_cell_prepocet_4_summarization a _estimation_cell a udelan distinct
			-- proc? vstupni estimation_cell muze obsahovat i estimation_cell ZSJ
			-- _res_estimation_cell_prepocet_4_summarization neobsahuje estimation_cell ZSJ

			-- pridani jeste spojeni _res_estimation_cell_prepocet_4_recount
		
			WITH
			w1 AS	(SELECT unnest(_res_estimation_cell_prepocet_4_summarization) AS estimation_cell), 	-- seznam estimation_cell pro sumarizaci, odpovida co je v DB
			w2 AS	(SELECT unnest(_estimation_cell) AS estimation_cell),					-- vstupni estimation_cell, mohou obsahovat i ZSJ
			w3 AS	(SELECT unnest(_res_estimation_cell_prepocet_4_recount) AS estimation_cell),		-- estimation_cell jako ZSJ, odpovida tomu co je v DB
			w4 AS	(
				SELECT w1.estimation_cell FROM w1
				UNION					-- union provede defakto distinct
				SELECT w2.estimation_cell FROM w2
				UNION
				SELECT w3.estimation_cell FROM w3
				)
			SELECT array_agg(w4.estimation_cell) FROM w4
			INTO _res_estimation_cell_prepocet_4_check;

			-------------------------------------------------------
			-- raise notice '_res_estimation_cell_prepocet_4_check:%',_res_estimation_cell_prepocet_4_check;			
			-------------------------------------------------------

			-- KONTROLA prepocitatelnosti pro CONFIG_QUERY:
			-- 200,300,400	=> jsou v t_aux_total prepocitany data zakladu a odpovidaji verzi spadajici do povoleneho intervalu VERZI
			-- 500		=> je v t_aux_total reference odpovidajici verzi spadajici do povoleneho intervalu VERZI => zde probihaji jen kontroly => vystupem je bud vyjimka nebo 0 radku
		
			-- zjisteni potrebnych promennych z konfigurace pro vetev soucet nebo doplnky
			SELECT
				tc.complete,
				tc.categories
			FROM
				@extschema@.t_config AS tc WHERE tc.id = _config_id
			INTO
				_complete,
				_categories;

			-- raise notice '_complete: %',_complete;
			-- raise notice '_categories: %',_categories;

			-- spojeni konfiguracnich idecek _complete/_categories
			IF _complete IS NULL
			THEN
				_config_ids_text := concat('array[',_categories,']');
			ELSE
				_config_ids_text := concat('array[',_complete,',',_categories,']');
			END IF;

			-- raise notice '_config_ids_text: %',_config_ids_text;

			-- zjisteni poctu idecek tvorici _complete/_categories
			EXECUTE 'SELECT array_length('||_config_ids_text||',1)'
			INTO _config_ids_length;

			-- raise notice '_config_ids_length: %',_config_ids_length;

			-- zjisteni existujicich konfiguracnich idecek pro (_complete/_categories)
			EXECUTE 'SELECT array_agg(tc.id ORDER BY tc.id) FROM @extschema@.t_config AS tc WHERE tc.id in (select unnest('||_config_ids_text||'))'
			INTO _config_ids;

			-- raise notice '_config_ids: %',_config_ids;

			-- splneni podminky stejneho poctu idecek
			IF _config_ids_length = array_length(_config_ids,1)
			THEN
				-- kontrola zda i obsah je stejny
				EXECUTE 'SELECT array_agg(t.ids ORDER BY t.ids) FROM (SELECT unnest('||_config_ids_text||') AS ids) AS t'
				INTO _config_ids4check;

				IF _config_ids4check = _config_ids
				THEN
					-- cyklus kontroly, zda pro soucty/doplnky a referenci jsou vypocitany vsechny potrebne
					-- kategorie a odpovidaji verzim spadajici do povoleneho intervalu VERZI
					-- pozn. zde si uz muzu sahat do tabulky t_aux_total, protoze pro tento typ vypoctu
					-- uz musi existovat zakladni (protinaci) aux_total hodnoty, respektive musely pro
					-- complete nebo categories probehnout _config_query 100 !!!

					--raise notice '_res_estimation_cell_prepocet_4_check: %',_res_estimation_cell_prepocet_4_check;
					
					FOR i IN 1..array_length(_res_estimation_cell_prepocet_4_check,1)
					LOOP
						FOR y IN 1..array_length(_config_ids,1)
						LOOP
							WITH
							w1 AS	(
								SELECT
								tat.*,
								max(tat.est_date) OVER (PARTITION BY tat.estimation_cell, tat.config) as max_est_date
								FROM @extschema@.t_aux_total AS tat
								WHERE tat.config = _config_ids[y]
								AND tat.estimation_cell = _res_estimation_cell_prepocet_4_check[i]
								AND tat.ext_version >= _ext_version_valid_from
								AND tat.ext_version <= _ext_version_valid_until
								)
							,w2 AS	(
								SELECT w1.* FROM w1 WHERE w1.est_date = w1.max_est_date
								)
							SELECT count(*) FROM w2
							INTO _check_avc4categories;
						

							IF _check_avc4categories != 1
							THEN		
								IF _config_query = 500
								THEN
									RAISE EXCEPTION 'Chyba 18: fn_get_gids4aux_total_app: Vstupni _config_id = % je reference na _config_id = % a pro estimation_cell = % neexistuje v tabulce t_aux_total vypocitana hodnota aux_total ve verzi od % do %!',_config_id_base,_config_ids[y],_res_estimation_cell_prepocet_4_check[i],_ext_version_valid_from_label,_ext_version_valid_until_label;
								ELSE
									RAISE EXCEPTION 'Chyba 19: fn_get_gids4aux_total_app: Vstupni _config_id = % je soucet nebo doplnek a pro konfiguraci [z complete/categories] = % a pro estimation_cell = % neexistuje v tabulce t_aux_total vypocitana hodnota aux_total ve verzi do % do %!',_config_id_base,_config_ids[y],_res_estimation_cell_prepocet_4_check[i],_ext_version_valid_from_label,_ext_version_valid_until_label;
								END IF;
							END IF;
						END LOOP;
					END LOOP;
				ELSE
					RAISE EXCEPTION 'Chyba 20: fn_get_gids4aux_total_app: Pro nakonfigurovane complete = % nebo pro nektere nakonfigurovane categories = % neexistuje konfigurace v tabulce t_config!',_complete,_categories;
				END IF;
			ELSE
				RAISE EXCEPTION 'Chyba 21: fn_get_gids4aux_total_app: Pro nakonfigurovane complete = % nebo pro nektere nakonfigurovane categories = % neexistuje konfigurace v t_config!',_complete,_categories;
			END IF;

			IF _config_query = 500 -- u reference probihaji jen kontroly a do vystupu se nic nevraci
			THEN
				_q :=	'
					WITH
					w1 AS	(
						SELECT
							1 AS step,
							$3 AS config_id,
							NULL::integer AS estimation_cell,
							NULL::integer AS gid
						)
					,w2 as	(
							SELECT
								w1.step,
								w1.config_id,
								w1.estimation_cell,
								w1.gid
							FROM
								w1 WHERE w1.step = 0
							)
					select
							w2.*,
							cec.label as estimation_cell_label,
							cec.description as estimation_cell_description,
							cec.label_en as estimation_cell_label_en,
							cec.description_en as estimation_cell_description_en
					from
							w2
							inner join @extschema@.c_estimation_cell as cec
							on w2.estimation_cell = cec.id
					';
			ELSE	-- vetev pro config_query 200,300,400 => zde bude promenna _res_estimation_cell_prepocet_4_check, ta obsahuje jak pripadne ZSJ,
				-- tak estimation_cell ze vstupu + estimation_cell podrizene
				_q :=
					'
					WITH
					w1 AS	(SELECT unnest($10) AS estimation_cell),
					w2 AS	(
						SELECT
							1 AS step,
							$3 AS config,
							w1.estimation_cell,
							NULL::integer AS gid
						FROM
							w1
						)
					,w3 as	(
							SELECT
								w2.step,
								w2.config,
								w2.estimation_cell,
								w2.gid
							FROM
								w2
							ORDER
								BY w2.step, w2.config, w2.estimation_cell
							)
					select
							w3.*,
							cec.label as estimation_cell_label,
							cec.description as estimation_cell_description,
							cec.label_en as estimation_cell_label_en,
							cec.description_en as estimation_cell_description_en
					from
							w3
							inner join @extschema@.c_estimation_cell as cec
							on w3.estimation_cell = cec.id
					';
			END IF;	
		END IF;		
	ELSE
		-- vetev pro VYPOCET

		--------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------
		-- Tento proces zatim neimplementuju, podle me neni nutny, tim ze to uzivateli pada na
		-- chybe 19, tak podle me spatne zadal geograficke urovne pro vypocet.
		-- proces odstraneni tech vstupnich ZSJ (ktere tvori estimation_cell jen jednou geometrii)
		-- ze vstupni promenne _estimation_cell, pokud _estimation_cell obsahuje jeji vyssi
		-- geografickou jednotku
		--------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------
		-- KONTROLA EXISTENCE v DB --

		-- zde se v databazi kontroluje pritomnost zaznamu, a to
		-- pro i-tou estimation_cell a pro vsechny config_id dane
		-- "konfigurace" => pokud tato podminka je splnena, pak
		-- nastane vyjimka => uzivatel toto uz v aplikaci zadat nesmi

		-- tato kontrola je zde z duvodu, kdy uzivatel zastavi vypocet,
		-- verze extenze je zde nutna a verze extenze pro jednotlive kategorie konfigurace
		-- musi odpovidat (spadat) do povoleneho intervalu VERZI
		
		FOR i IN 1..array_length(_estimation_cell,1)
		LOOP
			FOR ii IN 1..array_length(_configs,1)
			LOOP
				SELECT tc.config_query FROM @extschema@.t_config AS tc
				WHERE tc.id = _configs[ii]
				INTO _config_query_ii;
				
				IF _config_query_ii = 500 -- REFERENCE
				THEN
					SELECT tc.categories::integer
					FROM @extschema@.t_config AS tc
					WHERE tc.id = _configs[ii]
					INTO _config_id_reference_ii;

					IF _config_id_reference_ii IS NULL
					THEN
						RAISE EXCEPTION 'Chyba 22: fn_get_gids4aux_total_app: Kontrolovane _config_id = % je reference [config_query = 500] a v konfiguraci [v tabulce t_config] nema ve sloupci categories nakonfigurovanou referenci!',_configs[ii];
					END IF;

					_config_id_base_ii := _config_id_reference_ii;
				ELSE
					_config_id_base_ii := _configs[ii]; -- vstupnim config_id je bud konfigurace ze zakladni skupiny [100,300,400] nebo konfigurace z agregacni skupiny ci navazujici kolekce [200,300,400]
				END IF;

				WITH
				w1 AS	(
					SELECT tat.* FROM @extschema@.t_aux_total AS tat
					WHERE tat.estimation_cell = _estimation_cell[i]
					AND tat.config = _config_id_base_ii
					--AND tat.ext_version = _ext_version_current
					AND tat.ext_version >= _ext_version_valid_from AND tat.ext_version <= _ext_version_valid_until	-- NEW --
					)
				SELECT count(*) FROM w1	-- pokud je pocet vetsi jak 0 tak zaznam(y) exisuje(i)
				INTO _check_ii;

				IF _check_ii > 0
				THEN
					IF ii = 1
					THEN
						_check_ii_boolean := array[0];
					ELSE
						_check_ii_boolean := _check_ii_boolean || array[0];
					END IF;
				ELSE
					IF ii = 1
					THEN
						_check_ii_boolean := array[1];
					ELSE
						_check_ii_boolean := _check_ii_boolean || array[1];
					END IF;
				END IF;

			END LOOP;

			IF	(
				SELECT sum(t.check_ii_boolean) = 0
				FROM (SELECT unnest(_check_ii_boolean) AS check_ii_boolean) AS t
				)
			THEN
				IF _config_query = 500
				THEN
					RAISE NOTICE 'NOTICE: fn_get_gids4aux_total_app: Vstupni kategorie _config_id = % z konfigurace	config_collection = % je REFERENCE a pro zadanou i_tou estimation_cell = % jiz v tabulce t_aux_total existuji hodnoty aux_total pro vsechny kategorie dane konfigurace ve verzi %. Tzn. ze nektera z predchozich kategorii dane konfigurace	nebyla referenci a jejim vypoctem doslo k vypoctu vsech kategorii dane konfigurace u i-te estimation_cell, coz je v poradku a neni duvod k vyjimce. Vstupni seznam pocitanych estimation_cell: %.',_config_id,_config_collection,_estimation_cell[i],_ext_version_current_label,_estimation_cell;
				ELSE
					RAISE EXCEPTION 'Chyba 23: fn_get_gids4aux_total_app: Vypocet neni mozny => pro zadanou estimation_cell = % jiz v tabulce t_aux_total existuji hodnoty aux_total pro vsechny kategorie dane konfigurace (config_collection = %) ve verzi %! Vstupni seznam estimation_cell: %.',_estimation_cell[i],_config_collection,_ext_version_current_label,_estimation_cell;
				END IF;
			END IF;

		END LOOP;
		--------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------
		-- CONFIG_ID_BASE
		IF _config_query = 500 -- REFERENCE
		THEN
			SELECT tc.categories::integer
			FROM @extschema@.t_config AS tc
			WHERE tc.id = _config_id
			INTO _config_id_reference;

			IF _config_id_reference IS NULL
			THEN
				RAISE EXCEPTION 'Chyba 24: fn_get_gids4aux_total_app: Vstupni _config_id = % je reference [config_query = 500] a v konfiguraci [v tabulce t_config] nema nakonfigurovanou referenci!',_config_id;
			END IF;

			_config_id_base := _config_id_reference;
		ELSE
			_config_id_base := _config_id; -- vstupnim config_id je bud konfigurace ze zakladni skupiny [100,300,400] nebo konfigurace z agregacni skupiny ci navazujici kolekce [200,300,400]
		END IF;
		--------------------------------------------------------------------------------------------
		-- pokud jde o REFERENCI, pak kontrola, ze _config_id_base pro povoleny interval VERZI
		-- a i_tou estimation_cell uz v DB existuje, resp. musi existovat, jinak vyjimka !!!
		IF _config_query = 500 -- reference
		THEN
			FOR i IN 1..array_length(_estimation_cell,1)
			LOOP
				IF	(
					SELECT count(tat.*) = 0
					FROM @extschema@.t_aux_total AS tat
					WHERE tat.config = _config_id_base
					AND tat.estimation_cell = _estimation_cell[i]
					--AND tat.ext_version = _ext_version_current
					AND tat.ext_version >= _ext_version_valid_from AND tat.ext_version <= _ext_version_valid_until	-- NEW --
					)
				THEN
					RAISE EXCEPTION 'Chyba 25: fn_get_gids4aux_total_app: Vstupni _config_id = % je reference [config_query = 500, referencni id = %], jde o kategorii z konfigurace = %, a pro tuto referenci a estimation_cell = % nejsou doposud vypocitana data v nektere z verzi od % do % verze extenze nfiesta_gisdata!',_config_id,_config_id_base,_config_collection,_estimation_cell[i],_ext_version_valid_from_label,_ext_version_valid_until_label;
				END IF;
			END LOOP;			
		END IF;
		--------------------------------------------------------------------------------------------
		-- KONTROLA proveditelnosti pro "VSTUPNI" CONFIG_QUERY [200,300,400]:
		-- 200,300,400	=> jsou v t_aux_total vypocitany data zakladu v current ext_version
		IF _config_query = ANY(array[200,300,400]) -- vetev pro soucet a doplnky
		THEN
			-- zjisteni potrebnych promennych z konfigurace pro vetev soucet nebo doplnky
			SELECT
				tc.complete,
				tc.categories
			FROM
				@extschema@.t_config AS tc WHERE tc.id = _config_id_base
			INTO
				_complete,
				_categories;

			-- raise notice '_complete: %',_complete;
			-- raise notice '_categories: %',_categories;

			IF _complete IS NULL AND _categories IS NULL
			THEN
				RAISE EXCEPTION 'Chyba 26: fn_get_gids4aux_total_app: Vstupni _config_id = % [jde o kategorii z konfigurace %] je soucet nebo doplnek, a pro jeho konfiguraci nejsou spravne vyplnena pole _complete a _categories!',_config_id,_config_collection;
			END IF;

			-- spojeni konfiguracnich idecek _complete/_categories
			IF _complete IS NULL
			THEN
				_config_ids_text := concat('array[',_categories,']');
			ELSE
				_config_ids_text := concat('array[',_complete,',',_categories,']');
			END IF;

			-- raise notice '_config_ids_text: %',_config_ids_text;

			-- zjisteni poctu idecek tvorici _complete/_categories
			EXECUTE 'SELECT array_length('||_config_ids_text||',1)'
			INTO _config_ids_length;

			-- raise notice '_config_ids_length: %',_config_ids_length;

			-- zjisteni existujicich konfiguracnich idecek pro (_complete/_categories)
			EXECUTE 'SELECT array_agg(tc.id ORDER BY tc.id) FROM @extschema@.t_config AS tc WHERE tc.id in (select unnest('||_config_ids_text||'))'
			INTO _config_ids;

			-- raise notice '_config_ids: %',_config_ids;

			-- splneni podminky stejneho poctu idecek
			IF _config_ids_length = array_length(_config_ids,1)
			THEN
				-- kontrola zda i obsah je stejny
				EXECUTE 'SELECT array_agg(t.ids ORDER BY t.ids) FROM (SELECT unnest('||_config_ids_text||') AS ids) AS t'
				INTO _config_ids4check;

				IF _config_ids4check = _config_ids
				THEN
					-- cyklus kontroly, zda pro souctet nebo doplnek jsou vypocitany vsechny potrebne
					-- kategorie a odpovidaji verzi spadajici do povoleneho intervalu VERZI
					-- pozn. zde si uz muzu sahat do tabulky t_aux_total, protoze pro tento typ vypoctu
					-- uz musi existovat zakladni (protinaci) aux_total hodnoty, respektive musely pro
					-- complete nebo categories probehnout _config_query 100 !!!

					-- raise notice '_estimation_cell: %',_estimation_cell;
					
					FOR i IN 1..array_length(_estimation_cell,1)
					LOOP
						FOR y IN 1..array_length(_config_ids,1)
						LOOP
							WITH
							w1 AS	(
								SELECT
								tat.*,
								max(tat.est_date) OVER (PARTITION BY tat.estimation_cell, tat.config) as max_est_date
								FROM @extschema@.t_aux_total AS tat
								WHERE tat.config = _config_ids[y]
								AND tat.estimation_cell = _estimation_cell[i]
								AND tat.ext_version >= _ext_version_valid_from and tat.ext_version <= _ext_version_valid_until								
								)
							,w2 AS	(
								SELECT w1.* FROM w1 WHERE w1.est_date = w1.max_est_date
								)
							SELECT count(*) FROM w2
							INTO _check_avc4categories;

							IF _check_avc4categories != 1
							THEN		
								RAISE EXCEPTION 'Chyba 27: fn_get_gids4aux_total_app: Vstupni _config_id = % [kategorie z konfigurace %] je soucet nebo doplnek a pro konfiguraci [z complete/categories] = % a pro estimation_cell = % neexistuje v tabulce t_aux_total zadna vypocitana hodnota aux_total ve verzi od % do %!',_config_id_base,_config_collection,_config_ids[y],_estimation_cell[i],_ext_version_valid_from_label,_ext_version_valid_until_label;
							END IF;
						END LOOP;
					END LOOP;
				ELSE
					RAISE EXCEPTION 'Chyba 28: fn_get_gids4aux_total_app: Pro nakonfigurovane complete = % nebo pro nektere nakonfigurovane categories = % neexistuje konfigurace v t_config!',_complete,_categories;
				END IF;
			ELSE
				RAISE EXCEPTION 'Chyba 29: fn_get_gids4aux_total_app: Pro nakonfigurovane complete = % nebo pro nektere nakonfigurovane categories = % neexistuje konfigurace v t_config!',_complete,_categories;
			END IF;
		END IF;
		--------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------

		IF _config_query = ANY(array[100,200,300,400])
		THEN
			-- zde je implementovan proces ziskani zakladnich stavebnich jednotek (ZSJ)
			---------------------------------------------------------------------------
				
			-- cyklus pro ziskani POCTu
			FOR i IN 1..array_length(_estimation_cell,1)
			LOOP
				-- zjisteni gidu z tabulky f_a_cell pro i-tou _estimation_cell
				-- pozn. nektera estimation_cell muze byt v tabulce f_a_cell tvorena vice geometriemi
				SELECT array_agg(fac.gid ORDER BY fac.gid) FROM @extschema@.f_a_cell AS fac
				WHERE fac.estimation_cell = _estimation_cell[i]
				INTO _gids4estimation_cell_i;

				IF _gids4estimation_cell_i IS NULL
				THEN
					RAISE EXCEPTION 'Chyba 30: fn_get_gids4aux_total_app: Pro _estimation_cell[i] = % nenalezeny zadne zaznamy v tabulce f_a_cell!',_estimation_cell[i];
				END IF;

				-- volani funkce fn_get_lowest_gids [funkce si saha do tabulky t_estimation_cell_hierarchy]
				-- funkce by mela vratit seznam zakladnich stavebnich jednotek (gidu), ktere tvori zadany
				-- seznam vyssich gidu
				-- v tomto cyklu ale nejprve pozaduju pocet gidu
				_gids4estimation_cell_pocet := array_length(@extschema@.fn_get_lowest_gids_app(_gids4estimation_cell_i),1);

				IF i = 1
				THEN
					_pocet := array[_gids4estimation_cell_pocet];
				ELSE
					_pocet := _pocet || array[_gids4estimation_cell_pocet];
				END IF;
				
			END LOOP;

			-- zjisteni nejdelsiho pole _gids4estimation_cell
			SELECT max(t.pocet) FROM (SELECT unnest(_pocet) as pocet) AS t
			INTO _max_pocet;

			-- cyklus ziskani GIDu
			FOR i IN 1..array_length(_estimation_cell,1)
			LOOP
				-- zjisteni gidu z tabulky f_a_cell pro i-tou _estimation_cell
				-- pozn. nektera estimation_cell muze byt v tabulce f_a_cell tvorena vice geometriemi
				-- proto se nize dela pole poli
				SELECT array_agg(fac.gid ORDER BY fac.gid) FROM @extschema@.f_a_cell AS fac
				WHERE fac.estimation_cell = _estimation_cell[i]
				INTO _gids4estimation_cell_i;

				-- volani funkce fn_get_lowest_gids
				-- funkce by mela vratit seznam gidu nejnizsi urovne, ktere tvori zadany seznam vyssich gidu
				_gids4estimation_cell := @extschema@.fn_get_lowest_gids_app(_gids4estimation_cell_i);

				-- zjisteni poctu prvku(gidu) v poli _gids4estimation_cell
				_pocet_gids4estimation_cell := array_length(_gids4estimation_cell,1);

				-- vytvoreni pole s nulama o poctu _max_pocet - _pocet_gids4estimation_cell
				SELECT array_agg(t.gid_gs) FROM (SELECT 0 AS gid_gs FROM generate_series(1,(_max_pocet - _pocet_gids4estimation_cell))) AS t
				INTO _doplnek;

				-- proces doplneni 0 tak aby pole _gids4estimation_cell melo delku _max_pocet
				_gids4estimation_cell := _gids4estimation_cell || _doplnek;

				IF i = 1
				THEN
					_res_estimation_cell := array[array[_estimation_cell[i]]];
					_res_gids := array[_gids4estimation_cell];
				ELSE
					_res_estimation_cell := _res_estimation_cell || array[array[_estimation_cell[i]]];
					_res_gids := _res_gids || array[_gids4estimation_cell];
				END IF;

			END LOOP;
		END IF;

		--------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------

		-- note:
			-- _res_estimation_cell => pole poli vstupnich estimation_cell co si uzivatel zvolil [muze
			-- jit o estimation_cell z ruznych geografickych urovni a muzou se zde vyskytovat estimation_cell jako ZSJ]
			-- _res_gids => seznam zakladnich stavebnich jednotek (gidu) pro zadane estimation_cell [pozor gidy se zde mohou duplikovat]

		--------------------------------------------------------------------------------------------
		--raise notice '_res_estimation_cell: %',_res_estimation_cell;
		--raise notice '_res_gids: %',_res_gids;
		--------------------------------------------------------------------------------------------
		--raise notice '_unnest_text:%',_unnest_text;
		--------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------
		-- sestaveni vysledneho textu pro _config_query
		IF _config_query = 100	-- vetev pro zakladni (protinani)
		THEN
			-- kontrola ze pocet prvku v poli poli _res_estimation_cell se rovna poctu prvku v poli poli _res_gids
			IF (array_length(_res_estimation_cell,1) != array_length(_res_gids,1))
			THEN
				RAISE EXCEPTION 'Chyba 31: fn_get_gids4aux_total_app: Pocet prvku v interni promenne _res_estimation_cell neodpovida poctu prvku v interni promenne _res_gids!';
			END IF;

			-- zjisteni poctu prvku v jednotlivych prvcich u _res_gids [u multi array je vsude stejny pocet]
			SELECT array_length(_res_gids,2)
			INTO _res_gids_count_count;		

			-- proces sestaveni _res_estimation_cell_4_with do return query
			FOR i IN 1..array_length(_res_estimation_cell,1)
			LOOP
				WITH
				w AS	(
					SELECT
						unnest(_res_estimation_cell[i:i]) as estimation_cell,
						generate_series(1,_res_gids_count_count)
					)
				SELECT array_agg(w.estimation_cell) FROM w
				INTO _res_estimation_cell_4_with_i;

				IF i = 1
				THEN
					_res_estimation_cell_4_with := array[_res_estimation_cell_4_with_i];
				ELSE
					_res_estimation_cell_4_with := _res_estimation_cell_4_with || array[_res_estimation_cell_4_with_i];
				END IF;
			END LOOP;
			--------------------------------------------------------------------------------------------
			-- proces zjisteni k jednotlivym _res_estimation_cell techto informaci:
			-- je-li estimation_cell zakladni stavebni jednotkou nebo neni
			-- je-li estimation_cell zakladni stavebni jednotkou a je nutna jeste jeji sumarizace,

			-- 1. step => check
			WITH
			w1 AS	(
					SELECT unnest(_res_estimation_cell) AS estimation_cell
					)
			,w2 as	(
					select
							w1.estimation_cell,
							t1.estimation_cell_collection,
							t2.estimation_cell_collection_lowest
					from
							w1
							inner join @extschema@.cm_estimation_cell as t1 on w1.estimation_cell = t1.estimation_cell
							inner join @extschema@.cm_estimation_cell_collection as t2 on t1.estimation_cell_collection = t2.estimation_cell_collection
					)
			,w3 as	(
					SELECT
						w2.*,
						CASE
						WHEN w2.estimation_cell_collection = w2.estimation_cell_collection_lowest
						THEN TRUE ELSE FALSE
						END AS estimation_cell_w4 -- if estimation_cell_w4 is true then the estimation_cell is the lowest geometry
					FROM
						w2
					)
			,w4 as	(
					select distinct w3.estimation_cell, w3.estimation_cell_w4 from w3
					)
			,w5 as	(
					select w4.estimation_cell, count(*) as pocet from w4 group by w4.estimation_cell
					)
			select count(*) from w5 where w5.pocet > 1
			into _check_count_estimation_cell_w5;
		
			if _check_count_estimation_cell_w5 > 0
			then
				RAISE EXCEPTION 'Chyba 32: fn_get_gids4aux_total_app: For some estimation cell (for some of values in _res_estimation_cell = %) is different identification if estimation cell is or not the lowest geometry!',_res_estimation_cell;
			end if;
			---------------------------
			-- 2 step => get internal values _res_estimation_cell_summarization and _summarization
			WITH
			w1 AS	(
					SELECT unnest(_res_estimation_cell) AS estimation_cell
					)
			,w2 as	(
					select
							w1.estimation_cell,
							t1.estimation_cell_collection,
							t2.estimation_cell_collection_lowest
					from
							w1
							inner join @extschema@.cm_estimation_cell as t1 on w1.estimation_cell = t1.estimation_cell
							inner join @extschema@.cm_estimation_cell_collection as t2 on t1.estimation_cell_collection = t2.estimation_cell_collection
					)
			,w3 as	(
					SELECT
						w2.*,
						CASE
						WHEN w2.estimation_cell_collection = w2.estimation_cell_collection_lowest
						THEN TRUE ELSE FALSE
						END AS estimation_cell_w4 -- if estimation_cell_w4 is true then the estimation_cell is the lowest geometry
					FROM
						w2
					)
			,w4 as	(
					select distinct w3.estimation_cell, w3.estimation_cell_w4 from w3
					)
			,w5 AS	(
					select
							fac.estimation_cell,
							count(fac.gid) as count_gids
					from
							@extschema@.f_a_cell AS fac
					where
							fac.estimation_cell in (SELECT w1.estimation_cell FROM w1)
					group
							by fac.estimation_cell
					)
			,w6 AS	(
					SELECT
							w4.*,
							w5.count_gids
					FROM
							w4 LEFT JOIN w5 ON w4.estimation_cell = w5.estimation_cell
					)				
			,w7 AS	(
					SELECT
							w6.*,
							CASE
								WHEN w6.estimation_cell_w4 = FALSE THEN true -- if true => estimation cell must be sumarizationed
								ELSE
									CASE
									WHEN w6.count_gids > 1 THEN true -- if true => the estimation cell (the lowest geometry) must be sumarizationed, becouse is composed with more geometries
									ELSE FALSE
									END
							END AS summarization
					FROM
						w6
					)
			SELECT
					array_agg(w7.estimation_cell ORDER BY w7.estimation_cell),
					array_agg(w7.summarization ORDER BY w7.estimation_cell)
			FROM
					w7
			INTO
				_res_estimation_cell_summarization,
				_summarization;
			--------------------------------------------------------------------------------------------
			_q := 	'
				WITH
				---------------------------------------------------------------------------
				---------------------------------------------------------------------------
				-- tato cast je plny (i s duplicitama) seznam gidu zakladnich stavebnich jednotek,
				-- ktery tvori jednotlive pozadovane _estimation_cell[]
				w1 AS	(
					SELECT
						$11 AS estimation_cell,	-- jde o pole poli => identifikace estimation_cell ze vstupu teto funkce [muze obsahovat estimation_cell i ZSJ]
						$2 AS gid		-- jde o pole poli => seznam gidu (ZSJ) tvorici danou estimation_cell
					),
				w2 AS 	(
					SELECT
						unnest(w1.estimation_cell) AS estimation_cell,
						unnest(w1.gid) AS gid
					FROM
						w1
					),
				w3 AS	(SELECT * FROM w2 WHERE gid IS DISTINCT FROM 0),	-- zde je roz-unnestovan with w1 a odstraneny radky, kde ve sloupci GID je hodnota 0
				---------------------------------------------------------------------------
				-- pripojeni informace summarization k estimation_cell
				w4 AS	(
					SELECT
						unnest($5) AS estimation_cell_summarization,
						unnest($6) AS summarization
					),
				w5 AS	(
					SELECT
						w3.estimation_cell,
						w3.gid,
						w4.summarization
					FROM
						w3 LEFT JOIN w4 ON w3.estimation_cell = w4.estimation_cell_summarization
					),
				---------------------------------------------------------------------------
				-- tato cast je vyber zakladnich stavebnich jednotek (gidu), ktere jsou
				-- doposud vypocitany v t_aux_total a odpovidaji verzi, ktera spada do povolenoho intervalu VERZI
				-- Ty, ktere neodpovidaji se budou pocitat znovu !!!
				w9a AS 	(
					SELECT
					tat.*,
					max(tat.est_date) OVER (PARTITION BY tat.cell, tat.config) as max_est_date
					FROM @extschema@.t_aux_total AS tat
					WHERE tat.config = $3
					AND tat.cell IS NOT NULL					-- ve sloupci cell se ukladaji jen gidy nejnizsi urovne z f_a_cell 
					AND tat.cell IN (SELECT DISTINCT gid FROM w3)
					AND tat.ext_version >= $12
					AND tat.ext_version <= $13		
					),
				w9 AS	(
					SELECT w9a.* FROM w9a WHERE w9a.est_date = w9a.max_est_date				
					),
				---------------------------------------------------------------------------
				-- tato cast je pripojeni withu w9 k withu w5
				w10 AS	(
					SELECT
						w5.estimation_cell,
						w5.gid,			-- gid nejnizsi urovne, ktery tvori danou estimation_cell
						w5.summarization,			
						w9.cell			-- gid nejsizsi urovne, ktery je doposud vypocitan v t_aux_total
					FROM
						w5
					LEFT
					JOIN	w9	ON w5.gid = w9.cell
					),
				---------------------------------------------------------------------------
				-- tato cast rika, zda gid nejnizsi urovne je pro danou estimation_cell[i] jiz vypocitan nebo nikoliv
				-- pro danou estimation_cell => gidy s calculated vsude TRUE		=> vsechny pozadovane gidy uz jsou v DB vypocitany
				-- pro danou estimation_cell => gidy s calculated vsude FALSE		=> zadny z pozadovanych gidu jeste neni v DB vypocitan
				-- pro danou estimation_cell => gidy s calculated TRUE nebo FALSE	=> z pozadovanych gidu uz nektere v DB vypocitany jsou a nektere ne
				w11 AS	(
					SELECT
						$3 AS config_id,
						w10.estimation_cell,			-- vstupni hodnota i-te estimation_cell
						w10.gid,				-- gid nejnizsi urovne tvorici danou i-tou estimation_cell
						w10.summarization,

						CASE
						WHEN w10.cell IS NOT NULL THEN TRUE	-- toto plati je-li gid nejnizsi urovne pro danou i-tou estimation_cell je jiz vypocitan
						ELSE FALSE				-- toto plati je-li gid nejnizsi urovne pro danou i-tou estimation_cell neni jeste vypocitan
						END AS calculated
					FROM
						w10
					),
				---------------------------------------------------------------------------
				---------------------------------------------------------------------------
				-- vyber zakladnich stavebnich jednotek pro vypocet
				w12 AS	(SELECT DISTINCT gid FROM w11 WHERE calculated = FALSE ORDER BY gid),
				w13 AS	(
					SELECT
						1 AS step,
						$3 AS config_id,
						fac.estimation_cell,
						w12.gid
					FROM
						w12
					---------------------------------------------------------------------
					LEFT JOIN	@extschema@.f_a_cell AS fac ON w12.gid = fac.gid	-- toto doplnuje zpatky estimation_cell [v aplikaci by se mohlo spojit LABEL a GID]
					---------------------------------------------------------------------
					ORDER
						BY w12.gid
					),
				---------------------------------------------------------------------------
				---------------------------------------------------------------------------
				-- ted reseni SUMARIZACE => bude se provadet vzdy
				---------------------------------------------------------------------------
				w14 AS	(SELECT DISTINCT w11.estimation_cell FROM w11 WHERE summarization = TRUE),
				w15a AS	( -- napojeni na DB
					SELECT
						tat.*,
						max(tat.est_date) OVER (PARTITION BY tat.estimation_cell, tat.config) as max_est_date
					FROM
						@extschema@.t_aux_total AS tat
					WHERE
						tat.config = $3
					AND
						tat.cell IS NULL
					AND
						tat.estimation_cell IN (SELECT w14.estimation_cell FROM w14)
					AND
						tat.ext_version >= $12
					AND
						tat.ext_version <= $13
					),
				w15 AS	(
					select w15a.* FROM w15a WHERE w15a.est_date = w15a.max_est_date				
					),
				w16 AS	(
					SELECT
						w14.estimation_cell,
						CASE
						WHEN w15.estimation_cell IS NOT NULL THEN TRUE	-- estimation_cell, ktera uz je v DB sumarizovana a jeji verze odpovida (spada) do povoleneho intervalu VERZI
						ELSE FALSE					-- estimation_cell, kterou je nutno do-sumarizovat
						END AS calculated
					FROM
						w14
					LEFT
					JOIN	w15	ON w14.estimation_cell = w15.estimation_cell
					),
				w17 AS	(
					SELECT
						2 AS step,
						$3 AS config_id,
						w16.estimation_cell,
						0::integer AS gid
					FROM
						w16
					WHERE
						w16.calculated = FALSE
					
					ORDER
						BY w16.estimation_cell
					)
				---------------------------------------------------------------------------
				---------------------------------------------------------------------------
				,w18 as	(
						SELECT * FROM w13	-- => VZDY NUTNY DOPLNEK
						UNION ALL
						SELECT * FROM w17	-- => VZDY NUTNA SUMMARIZACE
						)
				select
						w18.*,
						cec.label as estimation_cell_label,
						cec.description as estimation_cell_description,
						cec.label_en as estimation_cell_label_en,
						cec.description_en as estimation_cell_description_en
				from
						w18
						inner join @extschema@.c_estimation_cell as cec
						on w18.estimation_cell = cec.id
				---------------------------------------------------------------------------			
				';
		ELSE
			IF _config_query = 500
			THEN
				_q :=	'
					WITH
					w1 AS	(
						SELECT
							1 AS step,
							$3 AS config_id,
							NULL::integer AS estimation_cell,
							NULL::integer AS gid
						)
					,w2 as	(
							SELECT
								w1.step,
								w1.config_id,
								w1.estimation_cell,
								w1.gid
							FROM
								w1 WHERE w1.step = 0
							)
					select
							w2.*,
							cec.label as estimation_cell_label,
							cec.description as estimation_cell_description,
							cec.label_en as estimation_cell_label_en,
							cec.description_en as estimation_cell_description_en
					from
							w2
							inner join @extschema@.c_estimation_cell as cec
							on w2.estimation_cell = cec.id
					';
			ELSE
				-- vetev pro config_query [200,300,400] => soucet a doplnky

				_q :=	'
					WITH
					w1 AS	(SELECT unnest($2) AS gids),	-- jde o seznam gidu (ZSJ), ktere tvori vstupni estimation_cell [vyskyt duplicitnich gidu]
					w2 AS	(SELECT DISTINCT gids FROM w1),	-- z-unikatneni gidu (ZSJ)
					w3 AS	(				-- vyber zaznamu z f_a_cell pro unikatni gidy (ZSJ)
						SELECT
							fac.gid,
							fac.estimation_cell
						FROM
							@extschema@.f_a_cell AS fac
						WHERE
							fac.gid IN (SELECT gids FROM w2)
						),
					w4 AS	(				-- zjisteni poctu gidu tvorici estimation_cell
						SELECT
							w3.estimation_cell,
							count(w3.gid) AS pocet
						FROM
							w3 GROUP BY w3.estimation_cell
						),
					w5 AS	(				-- vyber ZSJ, ktere tvori estimation_cell jen jednou geometrii
						SELECT w4.estimation_cell FROM w4
						WHERE w4.pocet = 1
						),
					w6 AS	(SELECT unnest($4) AS estimation_cell),	-- roz-unnestovani vstupnich estimation_cell
					w7 AS	(					-- odstraneni vstupnich estimation_cell z w5.estimation_cell
						SELECT w5.estimation_cell FROM w5 EXCEPT
						SELECT w6.estimation_cell FROM w6	-- vysledkem jsou estimation_cell (ZSJ) pro DOPLNENI
						),
					w8a AS	(				-- zjisteni gidu (ZSJ) pres estimation_cell co je opravdu v DB pro danou ext_version_current
						SELECT				-- u (ZSJ) je estimation_cell vzdy vyplneno pokud je estimation_cell tvoreno jednou geometrii
							tat.*,
							max(tat.est_date) OVER (PARTITION BY tat.estimation_cell, tat.config) as max_est_date
						FROM
							@extschema@.t_aux_total AS tat
						WHERE
							config = $3
						AND
							tat.ext_version >= $12
						AND
							tat.ext_version <= $13
						AND
							tat.estimation_cell IN (SELECT w7.estimation_cell FROM w7)
						),
					w8 AS	(
						SELECT w8a.* FROM w8a WHERE w8a.est_date = w8a.max_est_date
						),						
					w9 AS	(
						SELECT
							$3 AS config_id,
							w7.estimation_cell,
						
							CASE
							WHEN w8.estimation_cell IS NOT NULL THEN TRUE	-- toto plati je-li estimation_cell nejnizsi urovne pro danou ext_version_current => jiz vypocitano
							ELSE FALSE					-- toto plati je-li estimation_cell nejnizsi urovne pro danou ext_version_current => neni jeste vypocitano
							END AS calculated
						FROM
							w7 LEFT JOIN w8 ON w7.estimation_cell = w8.estimation_cell					
						),
					w10 AS	(				-- 1. cast do UNIONu
						SELECT
							1 AS step,
							w9.config_id,
							w9.estimation_cell,
							NULL::integer AS gid
						FROM
							w9 WHERE w9.calculated = FALSE
						ORDER
							BY w9.estimation_cell
						),
					-------------------------------------------------
					w11 AS	(
						SELECT
							2 AS step,
							$3 AS config_id,
							$4 AS estimation_cell,
							NULL::integer AS gid
						),
					w12 AS	(	
						SELECT
							w11.step,
							w11.config_id,
							unnest(w11.estimation_cell) AS estimation_cell,
							w11.gid
						FROM
							w11
						),
					w13a AS	( -- napojeni na DB
						SELECT
							tat.*,
							max(tat.est_date) OVER (PARTITION BY tat.estimation_cell, tat.config) as max_est_date
						FROM
							@extschema@.t_aux_total AS tat
						WHERE
							tat.config = $3
						AND
							tat.cell IS NULL
						AND
							tat.estimation_cell IN (SELECT w12.estimation_cell FROM w12)
						AND
							tat.ext_version >= $12
						AND
							tat.ext_version <= $13
						),
					w13 AS	(
						SELECT w13a.* FROM w13a WHERE w13a.est_date = w13a.max_est_date
						),
					w14 AS	(
						SELECT
							w12.step,
							w12.config_id,
							w12.estimation_cell,
							w12.gid,
							CASE
							WHEN w13.estimation_cell IS NOT NULL THEN TRUE	-- estimation_cell, ktera uz je v DB sumarizovana ve verzi spadajici do povoleneho intervalu VERZI
							ELSE FALSE					-- estimation_cell, kterou je nutno do-sumarizovat
							END AS calculated
						FROM
							w12
						LEFT
						JOIN	w13	ON w12.estimation_cell = w13.estimation_cell
						),						
					w15 AS	(				-- 2. cast do UNIONu
						SELECT
							w14.step,
							w14.config_id,
							w14.estimation_cell,
							w14.gid
						FROM
							w14
						WHERE
							w14.calculated = FALSE
						ORDER
							BY w14.estimation_cell
						)
					-------------------------------------------------------
					,w16 as	(
							SELECT w10.* FROM w10 UNION ALL
							SELECT w15.* FROM w15
							)
					select
							w16.*,
							cec.label as estimation_cell_label,
							cec.description as estimation_cell_description,
							cec.label_en as estimation_cell_label_en,
							cec.description_en as estimation_cell_description_en
					from
							w16
							inner join @extschema@.c_estimation_cell as cec
							on w16.estimation_cell = cec.id
					';
			END IF;
		END IF;
	END IF;
	--------------------------------------------------------------------------------------------
	RETURN QUERY EXECUTE ''||_q||''
	USING
		_res_estimation_cell,				-- $1
		_res_gids,					-- $2
		_config_id,					-- $3
		_estimation_cell,				-- $4
		_res_estimation_cell_summarization,		-- $5
		_summarization,					-- $6
		_ext_version_current,				-- $7
		_res_gids_prepocet_4_recount,			-- $8
		_res_estimation_cell_prepocet_4_summarization,	-- $9
		_res_estimation_cell_prepocet_4_check,		-- $10
		_res_estimation_cell_4_with,			-- $11
		_ext_version_valid_from,			-- $12
		_ext_version_valid_until;			-- $13
	--------------------------------------------------------------------------------------------	
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


ALTER FUNCTION @extschema@.fn_get_gids4aux_total_app(integer,integer[],integer,boolean) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_gids4aux_total_app(integer,integer[],integer,boolean) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_gids4aux_total_app(integer,integer[],integer,boolean) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_gids4aux_total_app(integer,integer[],integer,boolean) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_gids4aux_total_app(integer,integer[],integer,boolean) IS
'Funkce vraci seznam vypocetnich udaju pro funkci fn_get_aux_total_app.';
-- </function>



DROP FUNCTION @extschema@.fn_get_aux_total_app(integer, integer, integer);
-- <function name="fn_get_aux_total_app" schema="extschema" src="functions/extschema/fn_get_aux_total_app.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- Function: @extschema@.fn_get_aux_total_app(integer, integer, integer, integer)

-- DROP FUNCTION @extschema@.fn_get_aux_total_app(integer, integer, integer, integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_get_aux_total_app(
    IN _config_id integer,
    IN _estimation_cell integer,
    IN _gid integer,
	IN _gui_version integer)
returns void as
  --RETURNS TABLE(estimation_cell integer, config integer, aux_total double precision, cell integer, ext_version integer) AS
$BODY$
DECLARE
	_config_collection			integer;
	_config_query				integer;
	_config_function			integer;
	_estimation_cell_exit			integer;
	_function_aux_total			text;
	_categories				character varying;
	_categories_sum				double precision;
	_res_aux_total				double precision;
	_estimation_cell_area			double precision;	
	_complete				character varying;
	_complete_sum				double precision;
	_aux_total				double precision;
	_estimation_cell_collection		integer;
	_estimation_cell_collection_lowest	integer;
	_estimation_cell_result			integer;
	_check_pocet				integer;
	_gid_result				integer;	
	_q					text;

	_ext_version_label_system		text;
	_ext_version_current			integer;

	-- NEW --
	_ext_version_valid_from			integer;
	_ext_version_valid_until		integer;
	_ext_version_valid_from_label		character varying;
	_ext_version_valid_until_label		character varying;

	_unit double precision;

	_check_estimation_cell_lowest_count		integer;
	_check_estimation_cell_lowest			boolean;
BEGIN
	--------------------------------------------------------------------------------------------
	IF _config_id IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_aux_total_app: Vstupni argument _config_id nesmi byt NULL!';
	END IF;	
	
	IF _estimation_cell IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_aux_total_app: Vstupni argument _estimation_cell nesmi byt NULL!';
	END IF;
	--------------------------------------------------------------------------------------------
	-- proces ziskani aktualni extenze nfiesta_gisdata
	SELECT extversion FROM pg_extension WHERE extname = 'nfiesta_gisdata'
	INTO _ext_version_label_system;

	IF _ext_version_label_system IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_get_aux_total_app: V systemove tabulce pg_extension nenalezena zadna verze pro extenzi nfiesta_gisdata!';
	END IF;

	SELECT id FROM @extschema@.c_ext_version
	WHERE label = _ext_version_label_system
	INTO _ext_version_current;
	
	IF _ext_version_current IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_get_aux_total_app: V ciselniku c_ext_version nenalezen zaznam odpovidajici systemove verzi % extenze nfiesta_gisdata!',_ext_version_label_system;
	END IF;	
	--------------------------------------------------------------------------------------------
	-- proces ziskani potrebnych hodnot z konfigurace
	SELECT
		t.config_collection,
		t.config_query
	FROM
		@extschema@.t_config AS t
	WHERE
		id = _config_id
	INTO
		_config_collection,
		_config_query;
	--------------------------------------------------------------------------------------------
	IF _config_collection IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_get_aux_total_app: Argument _config_collection nesmi byt NULL!';
	END IF;	

	IF _config_query IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_get_aux_total_app: Argument _config_query nesmi byt NULL!';
	END IF;
	--------------------------------------------------------------------------------------------
	IF _config_query = 500
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_get_aux_total_app: Vypocet nelze provest pro config_query = %!',_config_query;
	END IF;
	--------------------------------------------------------------------------------------------
	IF _gid IS NOT NULL AND _config_query = ANY(array[200,300,400])
	THEN
		RAISE EXCEPTION 'Chyba 08: fn_get_aux_total_app: Vstupni argument _gid musi byt NULL!';
	END IF;
	--------------------------------------------------------------------------------------------
	-- proces ziskani potrebnych hodnot z kolekce
	SELECT tcc.config_function, tcc.unit FROM @extschema@.t_config_collection AS tcc
	WHERE tcc.id = _config_collection
	INTO _config_function, _unit;
	--------------------------------------------------------------------------------------------
	IF _config_function IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 09: fn_get_aux_total_app: Argument _config_function nesmi byt NULL!';
	END IF;	
	--------------------------------------------------------------------------------------------
	if _unit is null
	then
		_unit := 0.0::double precision;
	else
		_unit := _unit;
	end if;
	--------------------------------------------------------------------------------------------
	-- NEW --
	IF NOT(_config_function = ANY(array[100,200,300,400]))
	THEN
		RAISE EXCEPTION 'Chyba 10: fn_get_aux_total_app: Interni promenna _config_function = % musi byt hodnota 100, 200, 300 nebo 400!',_config_function;
	END IF;

	-- zjisteni ext_version_valid_from a ext_version_valid_until
	SELECT cmcf.ext_version_valid_from, cmcf.ext_version_valid_until
	FROM @extschema@.cm_ext_config_function AS cmcf
	WHERE cmcf.active = TRUE
	AND cmcf.config_function = _config_function
	INTO
		_ext_version_valid_from,
		_ext_version_valid_until;

	IF (_ext_version_valid_from IS NULL OR _ext_version_valid_until IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 11: fn_get_aux_total_app: Pro interni promennou _config_function = % nenalezeno ext_version_valid_from nebo ext_version_valid_until v tabulce cm_ext_config_function!',_config_function;
	END IF;

	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_valid_from  INTO _ext_version_valid_from_label;
	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_valid_until INTO _ext_version_valid_until_label;
	--------------------------------------------------------------------------------------------
	-- kontrola zdali pro zadanou konfiguraci a zadanou _estimation_cell
	-- uz je ci neni hodnota aux_total vypocitana a jestli neni nahodou duplicitni
	-- => toto by mel hlidat bud trigger nebo check-constraint v tabulce t_aux_total
	-- => nebo je implementovano jiz ve funkci fn_get_gids4aux_total
	--------------------------------------------------------------------------------------------	
	--------------------------------------------------------------------------------------------
	-- rozhodovaci proces co funkce fn_get_aux_total_app bude delat:
	CASE
		WHEN (_config_query = 100) -- zakladni with [vypocet z GIS vrstvy]
		THEN
			-- _gid zde nesmi byt hodnota 0 => to je pro identifikaci pro sumarizaci
			-- _calculated zde muze byt jen FALSE => JIRKOVA aplikace tuto funkci bude poustet jen na gidy, 
			-- ktere nejsou doposud vypocitany, nebo pokud se jedna o prepocet, a provede insert do t_aux_total
			-- pokud se    jedna o gid zakladu, pak do vystupu musi jit estimation_cell zakladu
			-- pokud se ne-jedna o gid zakladu, pak do vystupu pujde estimation_cell co je zde na vstupu

			IF _gid > 0
			THEN
				-- zjisteni estimation_cell do vystupu teto funkce
				SELECT fac.estimation_cell FROM @extschema@.f_a_cell AS fac WHERE fac.gid = _gid
				INTO _estimation_cell_exit;

				IF _estimation_cell_exit IS NULL
				THEN
					RAISE EXCEPTION 'Chyba 12: fn_get_aux_total_app: Pro gid = % nenalezeno estimation_cell v tabulce f_a_cell!',_gid;
				END IF;
				
				-- povolen vypocet aux_total hodnoty pro vstupni gid [zakladni protinani]
				CASE
					WHEN (_config_function = 100)	-- vector
					THEN
						_function_aux_total := 'fn_get_aux_total_vector_app';

					WHEN (_config_function = 200)	-- raster
					THEN
						_function_aux_total := 'fn_get_aux_total_raster_app';

					WHEN (_config_function = 300)	-- vector_comb
					THEN
						_function_aux_total := 'fn_get_aux_total_vector_comb_app';

					WHEN (_config_function = 400)	-- raster_comb
					THEN
						_function_aux_total := 'fn_get_aux_total_raster_comb_app';
					ELSE
						RAISE EXCEPTION 'Chyba 13: fn_get_aux_total_app: Neznama hodnota parametru _config_function [%].',_config_function;
				END CASE;
			ELSE
				RAISE EXCEPTION 'Chyba 14: fn_get_aux_total_app: Nepovolena kombinace vstupnich argumentu [_config_id = %, _estimation_cell = %, _gid = %] pro vypocet aux_total hodnoty pro config_query 100!',_config_id,_estimation_cell,_gid;
			END IF;

		WHEN (_config_query = ANY(array[200,300,400]))
		THEN
			-- 200 [soucet existujicich kategorii]
			-- 300 [doplnek do rozlohy vypocetni bunky]
			-- 400 [doplnek do rozlohy existujici kategorie]
			
			_function_aux_total := NULL::text;

			IF _gid IS NULL
			THEN
				-- povolen vypocet aux_total hodnoty pro soucet/doplnky

				_estimation_cell_exit := _estimation_cell;

				--raise notice '_estimation_cell_exit:%',_estimation_cell_exit;

				-- 1. zde se nejpreve z konfigurace zjisti _categories
				SELECT tc.categories FROM @extschema@.t_config AS tc WHERE tc.id = _config_id
				INTO _categories;

				--raise notice '_categories:%',_categories;

				-- 2. potom se pro _categories provede kontrola zda v tabulce t_aux_total vubec _categories existuji
				-- => toto uz je implementovano ve funkci fn_get_gids4aux_total

				-- 3. zde se provede suma za _categories pro danou _estimation_cell, ale tim ze povoluje interval VERZI
				--    musi se suma provest pro zaznam s max ext_version a nejspise i s max est_date
				EXECUTE '
				WITH
				w1 AS	(
					SELECT
					t.*,
					max(t.est_date) OVER (PARTITION BY t.estimation_cell, t.config) as max_est_date
					FROM @extschema@.t_aux_total AS t
					WHERE t.estimation_cell = $1
					AND t.config IN (SELECT unnest(array['||_categories||']))
					AND t.ext_version >= $2
					AND t.ext_version <= $3
					),
				w2 AS	(
					SELECT w1.* FROM w1 WHERE w1.est_date = w1.max_est_date			
					)
				SELECT sum(w2.aux_total) FROM w2
				'
				USING _estimation_cell, _ext_version_valid_from, _ext_version_valid_until
				INTO _categories_sum;

				--raise notice '_categories_sum: %',_categories_sum;

				-- 4. proces ziskani _res_aux_total pro jednotlive varianty _config_query 200,300,400
				CASE
				WHEN _config_query = 200	-- soucet
				THEN
					_res_aux_total := _categories_sum;
					
				WHEN _config_query = 300	-- doplnek do rozlohy vypocetni bunky [doplnek pro danou estimation_cell]
				THEN
					-- zjisteni plochy pro danou estimation_cell z tabulky f_a_cell
					SELECT sum(ST_Area(fac.geom))*_unit FROM @extschema@.f_a_cell AS fac
					WHERE fac.estimation_cell = _estimation_cell
					INTO _estimation_cell_area;

					IF _estimation_cell_area IS NULL
					THEN
						RAISE EXCEPTION 'Chyba 15: fn_get_aux_total_app: Pro zadanou _estimation_cell = % nezjistena plocha z tabulky f_a_cell!',_estimation_cell;
					END IF;

					_res_aux_total := _estimation_cell_area - _categories_sum;

				WHEN _config_query = 400
				THEN
					-- 1. zde se nejpreve z konfigurace zjisti complete
					SELECT tc.complete FROM @extschema@.t_config AS tc WHERE tc.id = _config_id
					INTO _complete;

					-- 2. potom se pro _complete provede kontrola zda v tabulce t_aux_total vubec existuje
					-- => toto uz je implementovano ve funkci fn_get_gids4aux_total

					-- 3. zjisteni sumy za _complete;
					EXECUTE '
					WITH
					w1 AS	(
						SELECT
						t.*,
						max(t.est_date) OVER (PARTITION BY t.estimation_cell, t.config) as max_est_date
						FROM @extschema@.t_aux_total AS t
						WHERE t.estimation_cell = $1
						AND t.config IN (SELECT unnest(array['||_complete||']))
						AND t.ext_version >= $2
						AND t.ext_version <= $3
						),
					w2 AS	(
						SELECT w1.* FROM w1 WHERE w1.est_date = w1.max_est_date				
						)
					SELECT sum(w2.aux_total) FROM w2
					'
					USING _estimation_cell, _ext_version_valid_from, _ext_version_valid_until
					INTO _complete_sum;

					_res_aux_total := _complete_sum - _categories_sum;	
				ELSE
					RAISE EXCEPTION 'Chyba 16: fn_get_aux_total_app: Pro _config_query = % doposud v tele funkce neprovedena implemetace procesu ziskani hodnoty _res_aux_total!',_config_query;
				END CASE;

			ELSE
				RAISE EXCEPTION 'Chyba 17: fn_get_aux_total_app: Nepovolena kombinace vstupnich argumentu [_config_id = %, _estimation_cell = %, _gid = %, _calculated = %] pro vypocet aux_total hodnoty pro config_query 200,300 nebo 400!',_config_id,_estimation_cell,_gid,_calculated;
			END IF;
		ELSE
			RAISE EXCEPTION 'Chyba 18: fn_get_aux_total_app: Neznama hodnota parametru _config_query [%].',_config_query;
	END CASE;
	--------------------------------------------------------------------------------------------
	IF _function_aux_total IS NOT NULL
	THEN
		EXECUTE 'SELECT @extschema@.'||_function_aux_total||'($1,$2)'
		USING _config_id, _gid
		INTO _aux_total;
	ELSE
		_aux_total := _res_aux_total;
	END IF;
	--------------------------------------------------------------------------------------------
	--------------------------------------------------------------------------------------------
	-- zjisteni do jake (jakych) estimation cell collection patri zadana vstupni estimation_cell
	-- a zjisteni jestli vstupni estimation_cell patri nebo nepatri do nejnizsi (lowest) kolekce
	with
	w1 as	(
			select cmec.* from @extschema@.cm_estimation_cell as cmec where cmec.estimation_cell = _estimation_cell	-- input is integer		
			)
	,w2 as	(
			select
					w1.*,
					cecc.estimation_cell_collection_lowest
			from
					w1
					inner join @extschema@.cm_estimation_cell_collection as cecc on w1.estimation_cell_collection = cecc.estimation_cell_collection
			)
	,w3 as	(
			select
					w2.*,
					case
						when w2.estimation_cell_collection = w2.estimation_cell_collection_lowest
						then true else false
					end as estimation_cell_check -- if estimation_cell_check is true then the estimation_cell is the lowest geometry
			from
					w2
			)	
	,w4 as	(
			select distinct w3.estimation_cell, w3.estimation_cell_check from w3
			)
	,w5 as	(
			select w4.estimation_cell, count(*) as pocet from w4 group by w4.estimation_cell
			)
	select count(*) from w5 where w5.pocet > 1
	into _check_estimation_cell_lowest_count;

	if _check_estimation_cell_lowest_count > 0
	then
		RAISE EXCEPTION 'Chyba 19: fn_get_aux_total_app: For input estimation cell = % is different identification if estimation cell is or not the lowest geometry!',_estimation_cell;
	end if;

	with
	w1 as	(
			select cmec.* from @extschema@.cm_estimation_cell as cmec where cmec.estimation_cell = _estimation_cell -- input is integer
			)
	,w2 as	(
			select
					w1.*,
					cecc.estimation_cell_collection_lowest
			from
					w1
					inner join @extschema@.cm_estimation_cell_collection as cecc on w1.estimation_cell_collection = cecc.estimation_cell_collection
			)
	,w3 as	(
			select
					w2.*,
					case
						when w2.estimation_cell_collection = w2.estimation_cell_collection_lowest
						then true else false
					end as estimation_cell_check -- if estimation_cell_check is true then the estimation_cell is the lowest geometry
			from
					w2
			)
	select distinct w3.estimation_cell_check from w3
	into _check_estimation_cell_lowest; -- if true => then the input estimation cell belongs into the lowest collection
	--------------------------------------------------------------------------------------------
	IF _config_query = 100
	THEN
		IF _check_estimation_cell_lowest = false -- VSTUPNI estimation_cell NENI zaklad [pozn. gidy pro protinani jsou vzdy zaklad]
		THEN
			-- ale pokud funkce fn_get_gids4aux_total_app vratila gid pro danou vstupni estimation_cell => tzn. ze zakladni gid jeste neni v t_aux_total
			-- a musel se jiz drive v kodu pro gid dohledat estimation_cell
			-- napr. chci TREBIC a zakladni gidy pro tuto estimation_cell nejsou jeste ulozeny v t_aux_total
			-- vystup za NUTS1-4 a rajonizace => pujde pres step 2 a sumarizaci

			IF _gid > 0
			THEN		
				_estimation_cell_result := _estimation_cell_exit;
			ELSE
				RAISE EXCEPTION 'Chyba 19: fn_get_aux_total_app: Jde-li o config_query = 100, pak gid nesmi byt nikdy NULL nebo hodnota 0!';
			END IF;
		ELSE
			-- VSTUPNI estimation_cell JE zaklad
			
			-- zjisteni poctu geometrii, ktere tvori vstupni estimation_cell
			SELECT count(fac.gid) FROM @extschema@.f_a_cell AS fac WHERE fac.estimation_cell = _estimation_cell
			INTO _check_pocet;

			IF _check_pocet IS NULL
			THEN
				RAISE EXCEPTION 'Chyba 20: fn_get_aux_total_app: Pro estimation_cell = % nenalezeny geometrie v tabulce f_a_cell!',_estimation_cell;
			END IF;

			IF _check_pocet = 1 -- vstupni estimation_cell uz neni nijak geometricky rozdrobena v f_a_cell
			THEN
				_estimation_cell_result := _estimation_cell_exit;
			ELSE
				_estimation_cell_result := NULL::integer;
			END IF;
		END IF;

		_gid_result := _gid;
	ELSE
		IF _config_query = ANY(array[200,300,400])
		THEN
			_estimation_cell_result := _estimation_cell_exit;
			_gid_result := NULL::integer;
		ELSE
			RAISE EXCEPTION 'Chyba 21: fn_get_aux_total_app: Neznama hodnota config_query = %!',_config_query;
		END IF;
	END IF;
	--------------------------------------------------------------------------------------------
	--_q := 'SELECT $1,$2,$3,$4,$5';
	--------------------------------------------------------------------------------------------
	--RETURN QUERY EXECUTE ''||_q||'' USING _estimation_cell_result,_config_id,_aux_total,_gid_result,_ext_version_current;
	--------------------------------------------------------------------------------------------
	insert into @extschema@.t_aux_total(config,estimation_cell,cell,aux_total,ext_version,gui_version) values
	(_config_id, _estimation_cell_result, _gid_result, _aux_total, _ext_version_current, _gui_version);

END;
$BODY$
  LANGUAGE plpgsql VOLATILE;

ALTER FUNCTION @extschema@.fn_get_aux_total_app(integer,integer,integer,integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_app(integer,integer,integer,integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_app(integer,integer,integer,integer) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_app(integer,integer,integer,integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_aux_total_app(integer,integer,integer,integer) IS
'Funkce uklada vypocitanou hodnotu aux_total pro zadanou konfiguraci (config_id) a celu (estimation_cell) nebo gid, a dalsi metadata do tabulky t_aux_total.';
-- </function>



DROP function @extschema@.fn_get_aux_total4estimation_cell_app (integer,integer,integer,boolean);
-- <function name="fn_get_aux_total4estimation_cell_app" schema="extschema" src="functions/extschema/fn_get_aux_total4estimation_cell_app.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DROP function @extschema@.fn_get_aux_total4estimation_cell_app (integer,integer,integer,integer,boolean)

CREATE OR REPLACE FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app
	(
	_config_id			integer,
	_estimation_cell	integer,
	_gid				integer,
	_gui_version		integer,
	_recount			boolean DEFAULT FALSE
	)
returns void as
--RETURNS TABLE
	--(
        --estimation_cell			integer,
        --config				integer, 
        --aux_total			double precision,
        --cell				integer,
        --ext_version			integer
    --)
$BODY$
DECLARE
	_estimation_cell_collection		integer;
	_estimation_cell_collection_lowest	integer;
	_check_estimation_cell			boolean;
	_check_pocet				integer;
	_gids4estimation_cell			integer[];
	_gids4estimation_cell_lowest		integer[];
	_gids_in_t_aux_total_check		integer;
	_aux_total				double precision;
	_q					text;

	_ext_version_label_system		text;
	_ext_version_current			integer;

	-- NEW --
	_config_function				integer;
	_ext_version_valid_from				integer;
	_ext_version_valid_until			integer;
	_ext_version_valid_from_label			character varying;
	_ext_version_valid_until_label			character varying;

	_check_estimation_cell_lowest_count		integer;
	_check_estimation_cell_lowest			boolean;	
BEGIN
	-------------------------------------------------------------------------------------------
	IF _config_id IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_aux_total4estimation_cell_app: Vstupni argument _config_id nesmi byt NULL!';
	END IF;

	IF _estimation_cell IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_aux_total4estimation_cell_app: Vstupni argument _estimation_cell nesmi byt NULL!';
	END IF;

	IF _gid IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_get_aux_total4estimation_cell_app: Vstupni argument _gid nesmi byt NULL!';
	END IF;

	IF _gid != 0
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_get_aux_total4estimation_cell_app: Vstupni argument _gid musi byt hodnota 0!';
	END IF;

	IF _recount IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_get_aux_total4estimation_cell_app: Vstupni argument _recount nesmi byt NULL!';
	END IF;	
	-------------------------------------------------------------------------------------------
	--------------------------------------------------------------------------------------------
	-- proces ziskani aktualni systemove extenze nfiesta_gisdata
	
	SELECT extversion FROM pg_extension WHERE extname = 'nfiesta_gisdata'
	INTO _ext_version_label_system;

	IF _ext_version_label_system IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_get_aux_total4estimation_cell_app: V systemove tabulce pg_extension nenalezena zadna verze pro extenzi nfiesta_gisdata!';
	END IF;

	SELECT id FROM @extschema@.c_ext_version
	WHERE label = _ext_version_label_system
	INTO _ext_version_current;
	
	IF _ext_version_current IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_get_aux_total4estimation_cell_app: V ciselniku c_ext_version nenalezen zaznam odpovidajici systemove verzi % extenze nfiesta_gisdata!',_ext_version_label_system;
	END IF;		
	-------------------------------------------------------------------------------------------
	-- NEW --
	-- zjisteni config_function pro zjistene _config_collection potazmo pro vstupni _config_id
	SELECT tcc.config_function FROM @extschema@.t_config_collection AS tcc
	WHERE tcc.id = (SELECT config_collection FROM @extschema@.t_config WHERE id = _config_id)
	INTO _config_function;

	IF _config_function IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 08: fn_get_aux_total4estimation_cell_app: Pro vstupni argument _config_id = % nenalezeno config_function v tabulce t_config_collection!',_config_id;
	END IF;

	IF NOT(_config_function = ANY(array[100,200,300,400]))
	THEN
		RAISE EXCEPTION 'Chyba 09: fn_get_aux_total4estimation_cell_app: Interni promenna _config_function = % musi byt hodnota 100, 200, 300 nebo 400!',_config_function;
	END IF;

	-- zjisteni ext_version_valid_from a ext_version_valid_until
	SELECT cmcf.ext_version_valid_from, cmcf.ext_version_valid_until
	FROM @extschema@.cm_ext_config_function AS cmcf
	WHERE cmcf.active = TRUE
	AND cmcf.config_function = _config_function
	INTO
		_ext_version_valid_from,
		_ext_version_valid_until;

	IF (_ext_version_valid_from IS NULL OR _ext_version_valid_until IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 10: fn_get_gids4aux_total_app: Pro interni promennou _config_function = % nenalezeno ext_version_valid_from nebo ext_version_valid_until v tabulce cm_ext_config_function!',_config_function;
	END IF;

	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_valid_from  INTO _ext_version_valid_from_label;
	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_valid_until INTO _ext_version_valid_until_label;
	-------------------------------------------------------------------------------------------
	-- kontrola, ze estimation_cell:
	-- 1. nesmi byt zakladni stavebni jednotka [ZSJ] => vyjimka plati jen pro viz. bod 2
	-- 2. muze to byt ZSJ ale pocet geometrii, ktery tvori onu ZSJ musi byt vice nez 1
	--------------------------------------------------------------------------------------------
	with
	w1 as	(
			select cmec.* from @extschema@.cm_estimation_cell as cmec where cmec.estimation_cell = _estimation_cell	-- input is integer		
			)
	,w2 as	(
			select
					w1.*,
					cecc.estimation_cell_collection_lowest
			from
					w1
					inner join @extschema@.cm_estimation_cell_collection as cecc on w1.estimation_cell_collection = cecc.estimation_cell_collection
			)
	,w3 as	(
			select
					w2.*,
					case
						when w2.estimation_cell_collection = w2.estimation_cell_collection_lowest
						then true else false
					end as estimation_cell_check -- if estimation_cell_check is true then the estimation_cell is the lowest geometry
			from
					w2
			)	
	,w4 as	(
			select distinct w3.estimation_cell, w3.estimation_cell_check from w3
			)
	,w5 as	(
			select w4.estimation_cell, count(*) as pocet from w4 group by w4.estimation_cell
			)
	select count(*) from w5 where w5.pocet > 1
	into _check_estimation_cell_lowest_count;

	if _check_estimation_cell_lowest_count > 0
	then
		RAISE EXCEPTION 'Chyba 11: fn_get_aux_total_app: For input estimation cell = % is different identification if estimation cell is or not the lowest geometry!',_estimation_cell;
	end if;

	with
	w1 as	(
			select cmec.* from @extschema@.cm_estimation_cell as cmec where cmec.estimation_cell = _estimation_cell -- input is integer
			)
	,w2 as	(
			select
					w1.*,
					cecc.estimation_cell_collection_lowest
			from
					w1
					inner join @extschema@.cm_estimation_cell_collection as cecc on w1.estimation_cell_collection = cecc.estimation_cell_collection
			)
	,w3 as	(
			select
					w2.*,
					case
						when w2.estimation_cell_collection = w2.estimation_cell_collection_lowest
						then true else false
					end as estimation_cell_check -- if estimation_cell_check is true then the estimation_cell is the lowest geometry
			from
					w2
			)
	select distinct w3.estimation_cell_check from w3
	into _check_estimation_cell_lowest; -- if true => then the input estimation cell belongs into the lowest collection
	--------------------------------------------------------------------------------------------
	IF _check_estimation_cell_lowest = false
	THEN
		-- vstupni estimation_cell neni ZSJ => splnena 1. podminka kontroly
		_check_estimation_cell := TRUE;
	ELSE
		-- vstupni estimation_cell je ZSJ
		
		-- zjisteni poctu geometrii, ktere tvori vstupni estimation_cell, ktera je ZSJ
		SELECT count(fac.gid) FROM @extschema@.f_a_cell AS fac
		WHERE fac.estimation_cell = _estimation_cell
		INTO _check_pocet;

		IF _check_pocet IS NULL
		THEN
			RAISE EXCEPTION 'Chyba 12: fn_get_aux_total4estimation_cell_app: Pro estimation_cell = % nenalezena zadna geometrie v tabulce f_a_cell!',_estimation_cell;
		END IF;

		IF _check_pocet = 1 -- vstupni estimation_cell (ZSJ) uz neni nijak geometricky rozdrobena v f_a_cell na mensi casti
		THEN
			_check_estimation_cell := FALSE;
		ELSE
			_check_estimation_cell := TRUE;
		END IF;
	END IF;
	--------------------------------------------------------------------------------------------
	IF _check_estimation_cell = FALSE
	THEN
		RAISE EXCEPTION 'Chyba 13: fn_get_aux_total4estimation_cell_app: Zadanou estimation_cell = % neni mozno sumarizovat. Jedna se totiz o nejnizsi geografickou uroven, ktera neni geometricky rozdrobena na mensi casti!',_estimation_cell;
	END IF;
	--------------------------------------------------------------------------------------------
	-- kontrola zda jiz pro zadanou estimation_cell, config_id a verzi spadajici do povoleneho
	-- intervalu VERZI jiz existuje hodnota aux_total v tabulce t_aux_total
	-- kontrola se provadi pri VYPOCTU i PREPOCTU [puvodne zde bylo jen pri VYPOCTU]
	-- jelikoz je u vstupnich argumentu ponechan _recount, pak IF jsem upravil nasledovne
	IF _recount = ANY(array[TRUE,FALSE])
	THEN
		IF	(
			SELECT count(tat.*) > 0
			FROM @extschema@.t_aux_total AS tat
			WHERE tat.config = _config_id
			AND tat.estimation_cell = _estimation_cell
			AND tat.ext_version = _ext_version_valid_from
			AND tat.ext_version = _ext_version_valid_until
			)
		THEN
			RAISE EXCEPTION 'Chyba 14: fn_get_aux_total4estimation_cell_app: Pro estimation_cell = %, config_id = % a verzi od % do % jiz existuje hodnota aux_total v tabulce t_aux_total. Sumarizace neni mozna!',_estimation_cell,_config_id,_ext_version_valid_from_label,_ext_version_valid_until_label;
		END IF;
	END IF;
	-------------------------------------------------------------------------------------------
	-- zjisteni gidu (ZSJ) z tabulky f_a_cell, ktere tvori zadanou _estimation_cell
	SELECT array_agg(fac.gid ORDER BY fac.gid) FROM @extschema@.f_a_cell AS fac
	WHERE fac.estimation_cell = _estimation_cell
	INTO _gids4estimation_cell;

	IF _gids4estimation_cell IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 15: fn_get_aux_total4estimation_cell_app: Pro estimation_cell = % nenalezeny zadne zaznamy v tabulce f_a_cell!',_estimation_cell;
	END IF;

	-- volani funkce fn_get_lowest_gids [funkce si saha do tabulky t_estimation_cell_hierarchy]
	-- funkce by mela vratit seznam gidu nejnizsi urovne (ZSJ), ktere tvori zadany seznam vyssich gidu
	_gids4estimation_cell_lowest := @extschema@.fn_get_lowest_gids_app(_gids4estimation_cell);
	-------------------------------------------------------------------------------------------
	-- pro jistotu provedeni DISTINCTU pro gidy
	SELECT array_agg(t2.gids ORDER BY t2.gids) FROM
	(SELECT DISTINCT t1.gids FROM (SELECT unnest(_gids4estimation_cell_lowest) AS gids) as t1) AS t2
	INTO _gids4estimation_cell_lowest;
	-------------------------------------------------------------------------------------------
	-- kontrola zda v tabulce t_aux_total jsou pro sumarizaci vsechny gidy nejnizsi urovne
	-- pro config_id a pro verzi spadajici do povoleneho intervalu VERZI
	WITH
	w1 AS	(SELECT unnest(_gids4estimation_cell_lowest) AS gids),
	w2 AS	(
		SELECT
			DISTINCT tat.config, tat.cell
		FROM
			@extschema@.t_aux_total AS tat
		WHERE
			tat.config = _config_id
		AND
			tat.cell IN (SELECT gids FROM w1)
		AND
			tat.ext_version >= _ext_version_valid_from
		AND
			tat.ext_version <= _ext_version_valid_until
		),
	w3 AS	(
		SELECT
			w1.gids,
			w2.cell
		FROM
			w1 LEFT JOIN w2
		ON
			w1.gids = w2.cell
		)
	SELECT
		count(*) FROM w3 WHERE w3.cell IS NULL
	INTO
		_gids_in_t_aux_total_check;
	-------------------------------------------------------------------------------------------
	IF _gids_in_t_aux_total_check > 0
	THEN
		RAISE EXCEPTION 'Chyba 16: fn_get_aux_total4estimation_cell_app: Pro estimation_cell = %, config_id = % a verzi od % do % neni v tabulce t_aux_total kompletni seznam hodnot aux_total pro sumarizaci!',_estimation_cell,_config_id,_ext_version_valid_from_label,_ext_version_valid_until_label;
	END IF;
	-------------------------------------------------------------------------------------------
	-- vypocet hodnoty aux_total pro danou estimation_cell
	WITH
	w1 AS	(SELECT unnest(_gids4estimation_cell_lowest) AS gids),
	w2 AS	(
		SELECT
			tat.*,
			max(tat.est_date) OVER (PARTITION BY tat.cell, tat.config) as max_est_date
		FROM
			@extschema@.t_aux_total AS tat
		WHERE
			tat.config = _config_id
		AND
			tat.cell IN (SELECT gids FROM w1)
		AND
			tat.ext_version >= _ext_version_valid_from
		AND
			tat.ext_version <= _ext_version_valid_until			
		),
	w3 AS	(
		SELECT w2.* FROM w2 WHERE w2.est_date = w2.max_est_date		
		)
	SELECT
		sum(w3.aux_total) AS aux_total
	FROM
		w3
	INTO
		_aux_total;
	-------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------
	--_q := 'SELECT $1,$2,$3,NULL::integer,$4';
	--------------------------------------------------------------------------------------------
	--RETURN QUERY EXECUTE ''||_q||'' USING _estimation_cell,_config_id,_aux_total,_ext_version_current;
	--------------------------------------------------------------------------------------------
	insert into @extschema@.t_aux_total(config,estimation_cell,cell,aux_total,ext_version,gui_version) values
	(_config_id, _estimation_cell, null::integer, _aux_total, _ext_version_current, _gui_version);
	--------------------------------------------------------------------------------------------
END ;
$BODY$
LANGUAGE plpgsql VOLATILE;

ALTER FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app(integer,integer,integer,integer,boolean) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app(integer,integer,integer,integer,boolean) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app(integer,integer,integer,integer,boolean) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app(integer,integer,integer,integer,boolean) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app(integer,integer,integer,integer,boolean) IS
'Funkce uklada hodnotu aux_total a patricna metadata do tabulky t_aux_total pro zadanou estimation_cell a config_id.';
-- </function>



-- <function name="fn_api_get_estimation_cell_hierarchy_stage" schema="extschema" src="functions/extschema/fn_api_get_estimation_cell_hierarchy_stage.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
-- fn_api_get_estimation_cell_hierarchy_stage
---------------------------------------------------------------------------------------------------
create or replace function @extschema@.fn_api_get_estimation_cell_hierarchy_stage(_config_collection integer)
returns table
(
	id integer,
	estimation_cell_id integer,
	stage boolean
)
as
$body$
begin
        if _config_collection is null
        then
                raise exception 'Error 01: fn_api_get_estimation_cell_hierarchy_stage: Input argument _config_collection must not be NULL!';
        end if;

        if	(
                select count(*) > 0
                from @extschema@.cm_estimation_cell_collection
                where estimation_cell_collection_lowest in (select cecc.id from @extschema@.c_estimation_cell_collection as cecc where cecc.use4estimates = true)
                )
        then
                raise exception 'Error 02: fn_api_get_estimation_cell_hierarchy_stage: In table c_estimation_cell_collection is not set value false for the some of lowest estimation cell collection!';
        end if;

        return query
        with
        w_cell as materialized	(
                                select
                                        cmec.estimation_cell,
                                        cmec.estimation_cell_collection
                                from
                                        @extschema@.cm_estimation_cell as cmec
                                where
                                        cmec.estimation_cell_collection in	(
                                                                            select cecc.id from @extschema@.c_estimation_cell_collection as cecc
                                                                            where cecc.use4estimates = true
                                                                            )										
                                )
        ,w1 as	(
                select
                        (row_number() over(order by t1.estimation_cell))::integer as id,
                        t1.estimation_cell as estimation_cell_id,
                        (select count(*) from @extschema@.t_config as tc where tc.config_collection = _config_collection) as count_config
                from
                        @extschema@.t_estimation_cell_hierarchy as t1
                        inner join w_cell on t1.estimation_cell = w_cell.estimation_cell
                )
        ,w2 as	(
                select
                        w1.*,
                        t2.config_id
                from
                        w1, (select tc.id as config_id from @extschema@.t_config as tc where tc.config_collection = _config_collection) as t2
                )
        ,w3 as	(
                select
                        w2.*,
                        case when t1.id is not null then true else false end as stage
                from
                        w2
                        left join @extschema@.t_aux_total as t1
                        on w2.estimation_cell_id = t1.estimation_cell and w2.config_id = t1.config
                )
        ,w4 as	(
                select w3.estimation_cell_id, count(*) as count_stage_true from w3 where w3.stage = true
                group by w3.estimation_cell_id
                )
        ,w5 as	(
                select
                        w1.id,
                        w1.estimation_cell_id,
                        case
                            when w4.count_stage_true is null then null::boolean
                            else
                                case
                                    when w4.count_stage_true = w1.count_config
                                    then true else false
                                end
                        end
                            as stage
                from
                        w1
                        left join w4 on w1.estimation_cell_id = w4.estimation_cell_id
                )
        select
                w5.id,
                w5.estimation_cell_id,
                w5.stage
        from
                w5 order by w5.id;
end;
$body$
language plpgsql volatile;

ALTER FUNCTION @extschema@.fn_api_get_estimation_cell_hierarchy_stage(integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_api_get_estimation_cell_hierarchy_stage(integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_api_get_estimation_cell_hierarchy_stage(integer) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_api_get_estimation_cell_hierarchy_stage(integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_api_get_estimation_cell_hierarchy_stage(integer) IS
'Funkce vraci tabulku hierarchii cell z tabulky t_estimation_cell_hierarchy s identifikaci stage [null = pro celu nebolo dopusud nic pocitano, true = cela je vypocitana, false = cela je rozpracovana]. Ve vystupu jsou cely, ktere maji v tabulce c_estimation_cell_collection nastaveno use4estimates = TRUE.';
-- </function>



-- <function name="fn_api_make_aux_total" schema="extschema" src="functions/extschema/fn_api_make_aux_total.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DROP FUNCTION @extschema@.fn_api_make_aux_total(integer, integer, integer, integer, boolean);

create or replace function @extschema@.fn_api_make_aux_total
(
    _config_id          integer,
    _estimation_cell    integer,
    _gid                integer,
    _gui_version        integer,
    _recount			boolean default false
)
returns void as
$BODY$
declare
begin
    if _config_id is null
    then
        raise exception 'Error 01: fn_api_make_aux_total: Input argument _config_id must not be NULL!';
    end if;

    if _estimation_cell is null
    then
        raise exception 'Error 02: fn_api_make_aux_total: Input argument _estimation_cell must not be NULL!';
    end if;

    if _gui_version is null
    then
        raise exception 'Error 03: fn_api_make_aux_total: Input argument _gui_version must not be NULL!';
    end if;
    ---------------------------------------------
    if _gid is not null
    then
        if _gid != 0
        then
            PERFORM @extschema@.fn_get_aux_total_app(_config_id,_estimation_cell,_gid,_gui_version);
        else
            PERFORM @extschema@.fn_get_aux_total4estimation_cell_app(_config_id,_estimation_cell,_gid,_gui_version,_recount);
        end if;
    else
        PERFORM @extschema@.fn_get_aux_total_app(_config_id,_estimation_cell,_gid,_gui_version);
    end if;
    ---------------------------------------------
end;
$BODY$
language plpgsql VOLATILE;

alter function @extschema@.fn_api_make_aux_total(integer,integer,integer,integer,boolean) OWNER TO adm_nfiesta_gisdata;
grant execute on function @extschema@.fn_api_make_aux_total(integer,integer,integer,integer,boolean) TO adm_nfiesta_gisdata;
grant execute on function @extschema@.fn_api_make_aux_total(integer,integer,integer,integer,boolean) TO app_nfiesta_gisdata;
grant execute on function @extschema@.fn_api_make_aux_total(integer,integer,integer,integer,boolean) TO public;

comment on function @extschema@.fn_api_make_aux_total(integer,integer,integer,integer,boolean) IS
'The function on a base of input arguments calls either a function fn_get_aux_total_app or function fn_get_aux_total4estimation_cell_app.';
-- </function>



-- <function name="estimation_cell_hierarchy" schema="extschema" src="views/export_api/estimation_cell_hierarchy.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DDL
create or replace view export_api.estimation_cell_hierarchy as
select
	cec1.label as cell,
	cec2.label as cell_superior
from
	@extschema@.t_estimation_cell_hierarchy as tech
	inner join @extschema@.c_estimation_cell as cec1 on tech.estimation_cell = cec1.id
	inner join @extschema@.c_estimation_cell as cec2 on tech.estimation_cell_superior = cec2.id
where
	tech.estimation_cell_superior is not null
;

-- authorization
ALTER TABLE export_api.estimation_cell_hierarchy OWNER TO adm_nfiesta_gisdata;
GRANT ALL ON TABLE export_api.estimation_cell_hierarchy TO adm_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.estimation_cell_hierarchy TO app_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.estimation_cell_hierarchy TO public;

-- documentation
COMMENT ON VIEW export_api.estimation_cell_hierarchy IS 'View providing hierarchy of estimation cells to nfi_esta ETL process.';
-- </view>



---------------------------------------------------------------------------------------------------;
-- delete column estimation_cell_collection from c_estimation_cell table
---------------------------------------------------------------------------------------------------;
alter table @extschema@.c_estimation_cell drop constraint fkey__c_estimation_cell__c_estimation_cell_collection;
alter table @extschema@.c_estimation_cell drop column estimation_cell_collection;
---------------------------------------------------------------------------------------------------;



---------------------------------------------------------------------------------------------------;
-- adding new columns in t_config_collection table
---------------------------------------------------------------------------------------------------;
alter table @extschema@.t_config_collection add column label_en varchar;
alter table @extschema@.t_config_collection add column description_en text;
alter table @extschema@.t_config_collection add column edit_user name DEFAULT CURRENT_USER COLLATE "C";
comment on column @extschema@.t_config_collection.label_en is 'Configuration group description.';
comment on column @extschema@.t_config_collection.description_en is 'Detailed description of the configuration group.';
comment on column @extschema@.t_config_collection.edit_user is 'The user who configured the group.';
update @extschema@.t_config_collection set label_en = label where id = id;
update @extschema@.t_config_collection set description_en = description where id = id;
update @extschema@.t_config_collection set edit_user = CURRENT_USER COLLATE "C" where id = id;
alter table @extschema@.t_config_collection alter column label_en set not null;
alter table @extschema@.t_config_collection alter column description_en set not null;
alter table @extschema@.t_config_collection alter column edit_user set not null;
---------------------------------------------------------------------------------------------------;



---------------------------------------------------------------------------------------------------;
-- adding new columns in t_config table
---------------------------------------------------------------------------------------------------;
alter table @extschema@.t_config add column label_en varchar;
alter table @extschema@.t_config add column description_en text;
alter table @extschema@.t_config add column edit_user name DEFAULT CURRENT_USER COLLATE "C";
comment on column @extschema@.t_config.label_en is 'Category description from the configuration group.';
comment on column @extschema@.t_config.description_en is 'Detailed description of the category from the configuration group.';
comment on column @extschema@.t_config.edit_user is 'The user who configured the category of group.';
update @extschema@.t_config set label_en = label where id = id;
update @extschema@.t_config set description_en = description where id = id;
update @extschema@.t_config set edit_user = CURRENT_USER COLLATE "C" where id = id;
alter table @extschema@.t_config alter column label_en set not null;
alter table @extschema@.t_config alter column description_en set not null;
alter table @extschema@.t_config alter column edit_user set not null;
---------------------------------------------------------------------------------------------------;



---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;
insert into @extschema@.c_ext_version(id, label, description, label_en, description_en) values
(2000,'3.1.0','Verze 3.1.0 - extenze nfiesta_gisdata pro pomocná data. Úprava datové struktury popisujíci hierarchie výpočetních cell.','3.1.0','Version 3.1.0 - nfiesta_gisdata extension for auxiliary data. Update data structure discribing hierarchies of estimation cells.');

update @extschema@.cm_ext_config_function set ext_version_valid_until = 2000 where active = true;

insert into @extschema@.c_gui_version(id, label, description, label_en, description_en) values
(900,'4.1.0','Verze 4.1.0 - GUI aplikace pro pomocná data.','4.1.0','Version 4.1.0 - GUI application for auxiliary data.');

insert into @extschema@.cm_ext_gui_version(ext_version, gui_version) values
(2000,900);
---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;
