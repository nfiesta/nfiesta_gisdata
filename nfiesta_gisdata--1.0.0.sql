--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------;
--CREATE SCHEMA @extschema@;
ALTER SCHEMA @extschema@ OWNER TO adm_nfiesta_gisdata;
GRANT USAGE ON SCHEMA @extschema@ TO app_nfiesta_gisdata;
---------------------------------------------------------------------------------------------------;

---------------------------------------------------------------------------------------------------;
-- TRIGGER FUNCTIONS --
---------------------------------------------------------------------------------------------------;
-- <function name="fn_aux_total_before_insert_app" schema="extschema" src="functions/extschema/fn_aux_total_before_insert_app.sql">
-- Function: @extschema@.fn_aux_total_before_insert_app()

-- DROP FUNCTION @extschema@.fn_aux_total_before_insert_app();

CREATE OR REPLACE FUNCTION @extschema@.fn_aux_total_before_insert_app()
  RETURNS trigger AS
$BODY$
	DECLARE
		_ext_version_label_system	text;
		_ext_version_id_system		integer;

		_ext_version_label		character varying;
	BEGIN
		-------------------------------------------------------------------------
		-- cast SYSTEMOVA --
		-------------------------------------------------------------------------
		SELECT extversion FROM pg_extension
		WHERE extname = 'nfiesta_gisdata'
		INTO _ext_version_label_system;
		
		-- docasne naplneni promenne [extenze jeste v db neexistuje]
		--_ext_version_label_system := '1.0.0'::text;
	
		IF _ext_version_label_system IS NULL
		THEN
			RAISE EXCEPTION 'Chyba 01: fn_aux_total_before_insert_app: V systemove tabulce pg_extension nenalezena zadna verze extenze nfiesta_gisdata!';
		END IF;
	
		SELECT id FROM @extschema@.c_ext_version
		WHERE label = _ext_version_label_system
		INTO _ext_version_id_system;
	
		IF _ext_version_id_system IS NULL
		THEN
			RAISE EXCEPTION 'Chyba 02: fn_aux_total_before_insert_app: V ciselniku c_ext_version nenalezen zaznam odpovidajici systemove verzi % extenze nfiesta_gisdata!',_ext_version_label_system;
		END IF;
		-------------------------------------------------------------------------
		-- cast INSERTOVANA
		-------------------------------------------------------------------------
		SELECT label FROM @extschema@.c_ext_version WHERE id = NEW.ext_version
		INTO _ext_version_label;

		IF _ext_version_label IS NULL
		THEN
			RAISE EXCEPTION 'Chyba 03: fn_aux_total_before_insert_app: V ciselniku c_ext_version nenalezen zaznam odpovidajici insertovane verzi % extenze nfiesta_gisdata!',_ext_version_label;
		END IF;
		-------------------------------------------------------------------------
		-- cast KONTROLY
		-------------------------------------------------------------------------
		IF NEW.ext_version > _ext_version_id_system
		THEN
			RAISE EXCEPTION 'Chyba 04: fn_aux_total_before_insert_app: Insertovana verze extenze (ext_version = %) nesmi byt novejsi, nez je prave aktualni systemova verze (%) extenze nfiesta_gisdata!',_ext_version_label,_ext_version_label_system;
		END IF;
		-------------------------------------------------------------------------
		RETURN NEW;
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION @extschema@.fn_aux_total_before_insert_app() OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_aux_total_before_insert_app() TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_aux_total_before_insert_app() TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_aux_total_before_insert_app() TO public;

COMMENT ON FUNCTION @extschema@.fn_aux_total_before_insert_app() IS
'Funkce provádí kontrolu importovaného záznamu verze extenze (ext_version). Spouští se triggerem before insert tabulky t_aux_total.';

-- </function>
---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- TABLES --
---------------------------------------------------------------------------------------------------;
-- Table: r_s2_ftype_smooth

CREATE TABLE @extschema@.r_s2_ftype_smooth
 (rid serial NOT NULL,
  rast raster NOT NULL,
  CONSTRAINT pkey__r_s2_ftype_smooth__rid PRIMARY KEY (rid), -- Jednoznačný identifikátor, primární klíč tabulky r_s2_ftype_smooth.
  CONSTRAINT enforce_height_rast CHECK (st_height(rast) = 10), -- Ukládaná rastrová data mají každý tile vysoký 10 pixelů.
  CONSTRAINT enforce_max_extent_rast CHECK (st_envelope(rast) @ '010300002065140000010000000500000000000000903A2CC100000000C0EB32C100000000903A2CC10000000080132CC100000000A0F019C10000000080132CC100000000A0F019C100000000C0EB32C100000000903A2CC100000000C0EB32C1'::geometry), -- Maximální rozsah ukládaných dat.
  CONSTRAINT enforce_nodata_values_rast CHECK (_raster_constraint_nodata_values(rast) = '{255,255}'::numeric[]), -- No Data jsou vyjádřena hodnotou 255
  CONSTRAINT enforce_num_bands_rast CHECK (st_numbands(rast) = 2), -- Ukládaná rastrová data  mají 2 bandy (vrstvy), první band obsahuje jehličnaté dřeviny a druhý listnaté.
  CONSTRAINT enforce_out_db_rast CHECK (_raster_constraint_out_db(rast) = '{f,f}'::boolean[]),
  CONSTRAINT enforce_pixel_types_rast CHECK (_raster_constraint_pixel_types(rast) = '{8BUI,8BUI}'::text[]), -- Ukládaná rastrová data mají pixely datového typu 8BUI.
  CONSTRAINT enforce_scalex_rast CHECK (st_scalex(rast) = 10::double precision), -- Ukládaná rastrová data ftype_smooth mají pixel velikosti 10 m ve směru osy x.
  CONSTRAINT enforce_scaley_rast CHECK (st_scaley(rast) = '-10'::integer::double precision), -- Ukládaná rastrová data ftype_smooth mají pixel velikosti -10 m ve směru osy y.
  CONSTRAINT enforce_srid_rast CHECK (st_srid(rast) = 5221), -- Ukládaná rastrová data ftype_smooth musí být SRID 5221.
  CONSTRAINT enforce_width_rast CHECK (st_width(rast) = 10) -- Ukládaná rastrová data ftype_smooth mají každý tile široký 10 pixelů.
);

COMMENT ON TABLE @extschema@.r_s2_ftype_smooth IS 'Rastr jehličnatých-listnatých porostů vznikl řízenou klasifikací družicových snímků ESA Sentinel-2 provedenou na základě spektrální odezvy listnatých a jehličnatých dřevin v průběhu fenologických fází vegetace s využitím trénovacích dat z fotogrammetrické interpretace bodů NIL v tzv. gridovém poli (síť NIL 500 x 500m). Jedná se o shlazený rastr o dvou vrstvách, z nichž první vrstva obsahuje hodnoty pro jehličnaté dřeviny a druhá má uloženy hodnoty pro listnaté dřeviny. Hodnoty pixelů v jednotlivých tilech pro jehličnaté dřeviny ukazují plošné zastoupení jehličnatých dřevin v rámci kernelu 9x9 pixelů. Výsledná hodnota je v procentech. Stejně jsou ukládány i hodnoty ve druhé vrstvě listnatých dřevin. Obě pásma jsou si tedy inverzní, pokud jejich součet dává dohromady 100. Pokud ne, zbytek do 100 reprezentuje plošné zastoupení kategorie pozemku Ostatní. Pokud kernel zasahuje mimo oblast rasteru, je okno kernelu ořezáno na okraj rasteru. Hodnoty nodata z původního rasteru r_s2_ftype jsou považovány za 0, tedy za kat. pozemku Ostatní. Velikost tilu je 10x10 pixelů.';
COMMENT ON COLUMN @extschema@.r_s2_ftype_smooth.rid IS 'primární klíč, databázový identifikátor';
COMMENT ON COLUMN @extschema@.r_s2_ftype_smooth.rast IS 'prostorový datový typ obsahující informace viz. komentar tabulky';

COMMENT ON CONSTRAINT pkey__r_s2_ftype_smooth__rid ON @extschema@.r_s2_ftype_smooth IS 'Jednoznačný identifikátor, primární klíč tabulky r_s2_ftype_smooth.';
COMMENT ON CONSTRAINT enforce_height_rast ON @extschema@.r_s2_ftype_smooth IS 'Ukládaná rastrová data mají každý tile vysoký 10 pixelů.';
COMMENT ON CONSTRAINT enforce_max_extent_rast ON @extschema@.r_s2_ftype_smooth IS 'Maximální rozsah ukládaných dat.';
COMMENT ON CONSTRAINT enforce_nodata_values_rast ON @extschema@.r_s2_ftype_smooth IS 'No Data jsou vyjádřena hodnotou 255';
COMMENT ON CONSTRAINT enforce_num_bands_rast ON @extschema@.r_s2_ftype_smooth IS 'Ukládaná rastrová data mají 2 bandy (vrstvy), první band obsahuje jehličnaté dřeviny a druhý listnaté.';
COMMENT ON CONSTRAINT enforce_pixel_types_rast ON @extschema@.r_s2_ftype_smooth IS 'Ukládaná rastrová data mají pixely datového typu 8BUI.';
COMMENT ON CONSTRAINT enforce_scalex_rast ON @extschema@.r_s2_ftype_smooth IS 'Ukládaná rastrová data ftype mají pixel velikosti 10 m ve směru osy x.';
COMMENT ON CONSTRAINT enforce_scaley_rast ON @extschema@.r_s2_ftype_smooth IS 'Ukládaná rastrová data ftype mají pixel velikosti -10 m ve směru osy y.';
COMMENT ON CONSTRAINT enforce_srid_rast ON @extschema@.r_s2_ftype_smooth IS 'Ukládaná rastrová data ftype musí být SRID 5221.';
COMMENT ON CONSTRAINT enforce_width_rast ON @extschema@.r_s2_ftype_smooth IS 'Ukládaná rastrová data mají každý tile široký 10 pixelů.';

CREATE INDEX r_s2_ftype_smooth_st_convexhull_idx
  ON @extschema@.r_s2_ftype_smooth
  USING gist
  (st_convexhull(rast));

COMMENT ON INDEX @extschema@.r_s2_ftype_smooth_st_convexhull_idx
  IS 'Prostorový index tabulky r_s2_ftype_smooth na poli rast, vytvořený příkazem USING GIST (ST_ConvexHull(rast)).';

ALTER TABLE @extschema@.r_s2_ftype_smooth OWNER TO adm_nfiesta_gisdata;
GRANT ALL ON TABLE @extschema@.r_s2_ftype_smooth TO adm_nfiesta_gisdata;
GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE @extschema@.r_s2_ftype_smooth TO app_nfiesta_gisdata;
GRANT SELECT ON TABLE @extschema@.r_s2_ftype_smooth TO public;
---------------------------------------------------------------------------------------------------;
-- Table: r_ndsm_smooth

-- DROP TABLE @extschema@.r_ndsm_smooth;

CREATE TABLE @extschema@.r_ndsm_smooth
(
  rid serial NOT NULL, -- Primární klíč, databázový identifikátor.
  syear smallint NOT NULL, -- Rok snímkování.
  map_section_smo50 integer NOT NULL, -- Identifikátor mapového listu SMO-50.
  rast raster NOT NULL, -- Prostorový datový typ obsahující informace o poloze a hodnotě jednotlivých pixelů.
  CONSTRAINT pkey__r_ndsm_smooth PRIMARY KEY (rid), -- Jednoznačný identifikátor, primární klíč tabulky r_ndsm_smooth.
  CONSTRAINT enforce_height_rast CHECK (st_height(rast) = 10), -- Ukládaná rastrová data nDSM_smooth mají každý tile vysoký 10 pixelů.
  CONSTRAINT enforce_num_bands_rast CHECK (st_numbands(rast) = 1), -- Ukládaná rastrová data nDSM_smooth musí mít 1 band (vrstvu).
  CONSTRAINT enforce_pixel_types_rast CHECK (_raster_constraint_pixel_types(rast) = '{8BUI}'::text[]), -- Ukládaná rastrová data nDSM_smooth mají pixely datového typu 8BUI.
  CONSTRAINT enforce_scalex_rast CHECK (st_scalex(rast)::numeric(16,10) = 5::numeric(16,10)), -- Ukládaná rastrová data nDSM_smooth mají pixel velikosti 5 m ve směru osy x.
  CONSTRAINT enforce_scaley_rast CHECK (st_scaley(rast)::numeric(16,10) = '-5'::integer::numeric(16,10)), -- Ukládaná rastrová data nDSM_smooth mají pixel velikosti -5 m ve směru osy y.
  CONSTRAINT enforce_srid_rast CHECK (st_srid(rast) = 5221), -- Ukládaná rastrová data nDSM_smooth musí být SRID 5221.
  CONSTRAINT enforce_width_rast CHECK (st_width(rast) = 10) -- Ukládaná rastrová data nDSM_smooth mají každý tile široký 10 pixelů.
);

COMMENT ON TABLE @extschema@.r_ndsm_smooth IS 'Rastrová data nDSM_smooth (normalizovaný digitální shlazený model povrchu) celé ČR v rozlišení 5m/pixel. Velikost tilu je 10x10 pixelů. Tily, ve kterých byly pouze pixely s hodnotou 0, jsou odstraněny.';
COMMENT ON COLUMN @extschema@.r_ndsm_smooth.rid IS 'Primární klíč, databázový identifikátor.';
COMMENT ON COLUMN @extschema@.r_ndsm_smooth.syear IS 'Rok snímkování.';
COMMENT ON COLUMN @extschema@.r_ndsm_smooth.map_section_smo50 IS 'Identifikátor mapového listu SMO-50.';
COMMENT ON COLUMN @extschema@.r_ndsm_smooth.rast IS 'Prostorový datový typ obsahující informace o poloze a hodnotě jednotlivých pixelů.';

COMMENT ON CONSTRAINT pkey__r_ndsm_smooth ON @extschema@.r_ndsm_smooth IS 'Jednoznačný identifikátor, primární klíč tabulky r_ndsm_smooth.';
COMMENT ON CONSTRAINT enforce_height_rast ON @extschema@.r_ndsm_smooth IS 'Ukládaná rastrová data nDSM_smooth mají každý tile vysoký 10 pixelů.';
COMMENT ON CONSTRAINT enforce_num_bands_rast ON @extschema@.r_ndsm_smooth IS 'Ukládaná rastrová data nDSM_smooth musí mít 1 band (vrstvu).';
COMMENT ON CONSTRAINT enforce_pixel_types_rast ON @extschema@.r_ndsm_smooth IS 'Ukládaná rastrová data nDSM_smooth mají pixely datového typu 8BUI.';
COMMENT ON CONSTRAINT enforce_scalex_rast ON @extschema@.r_ndsm_smooth IS 'Ukládaná rastrová data nDSM_smooth mají pixel velikosti 5 m ve směru osy x.';
COMMENT ON CONSTRAINT enforce_scaley_rast ON @extschema@.r_ndsm_smooth IS 'Ukládaná rastrová data nDSM_smooth mají pixel velikosti -5 m ve směru osy y.';
COMMENT ON CONSTRAINT enforce_srid_rast ON @extschema@.r_ndsm_smooth IS 'Ukládaná rastrová data nDSM_smooth musí být SRID 5221.';
COMMENT ON CONSTRAINT enforce_width_rast ON @extschema@.r_ndsm_smooth IS 'Ukládaná rastrová data nDSM_smooth mají každý tile široký 10 pixelů.';

-- Index: @extschema@.fki_fkey__r_ndsm_smooth__map_section_smo50

-- DROP INDEX @extschema@.fki_fkey__r_ndsm_smooth__map_section_smo50;

CREATE INDEX fki_fkey__r_ndsm_smooth__map_section_smo50
  ON @extschema@.r_ndsm_smooth
  USING btree
  (map_section_smo50);
COMMENT ON INDEX @extschema@.fki_fkey__r_ndsm_smooth__map_section_smo50
  IS 'BTREE index tabulky r_ndsm_smooth na poli map_section_smo50.';

-- Index: @extschema@.idx__r_ndsm_smooth__syear

-- DROP INDEX @extschema@.idx__r_ndsm_smooth__syear;

CREATE INDEX idx__r_ndsm_smooth__syear
  ON @extschema@.r_ndsm_smooth
  USING btree
  (syear);
COMMENT ON INDEX @extschema@.idx__r_ndsm_smooth__syear
  IS 'BTREE index tabulky r_ndsm_smooth na poli syear.';

-- Index: @extschema@.spidx__r_ndsm_smooth__rast

-- DROP INDEX @extschema@.spidx__r_ndsm_smooth__rast;

CREATE INDEX spidx__r_ndsm_smooth__rast
  ON @extschema@.r_ndsm_smooth
  USING gist
  (st_convexhull(rast));
COMMENT ON INDEX @extschema@.spidx__r_ndsm_smooth__rast
  IS 'Prostorový index tabulky r_ndsm_smooth na poli rast, vytvořený příkazem USING GIST (ST_ConvexHull(rast)).';

ALTER TABLE @extschema@.r_ndsm_smooth OWNER TO adm_nfiesta_gisdata;
GRANT ALL ON TABLE @extschema@.r_ndsm_smooth TO adm_nfiesta_gisdata;
GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE @extschema@.r_ndsm_smooth TO app_nfiesta_gisdata;
GRANT SELECT ON TABLE @extschema@.r_ndsm_smooth TO public;
---------------------------------------------------------------------------------------------------;
-- Table: @extschema@.r_s2_species_smooth

-- DROP TABLE @extschema@.r_s2_species_smooth;

CREATE TABLE @extschema@.r_s2_species_smooth
 (rid serial NOT NULL,
  rast raster NOT NULL,
  CONSTRAINT pkey__r_s2_species_smooth__rid PRIMARY KEY (rid), -- Jednoznačný identifikátor, primární klíč tabulky r_s2_species_smooth.
  CONSTRAINT enforce_height_rast CHECK (st_height(rast) = 10), -- Ukládaná rastrová data mají každý tile vysoký 10 pixelů.
  CONSTRAINT enforce_max_extent_rast CHECK (st_envelope(rast) @ '010300002065140000010000000500000000000000903A2CC100000000C0EB32C100000000903A2CC10000000080132CC100000000A0F019C10000000080132CC100000000A0F019C100000000C0EB32C100000000903A2CC100000000C0EB32C1'::geometry), -- Maximální rozsah ukládaných dat.
  CONSTRAINT enforce_nodata_values_rast CHECK (_raster_constraint_nodata_values(rast) = '{255,255,255,255,255,255,255,255,255}'::numeric[]), -- No Data jsou vyjádřena hodnotou 255
  CONSTRAINT enforce_num_bands_rast CHECK (st_numbands(rast) = 9), -- Ukládaná rastrová data  mají 9 bandu (vrstev), první band obsahuje smrk, druhý buk, třetí borovici, čtvrtý listnaté ostatní, pátý duby, šestý těžbu, sedmý směs-nejisté, osmý kulturu a devátý kleč.
  CONSTRAINT enforce_out_db_rast CHECK (_raster_constraint_out_db(rast) = '{f,f,f,f,f,f,f,f,f}'::boolean[]),
  CONSTRAINT enforce_pixel_types_rast CHECK (_raster_constraint_pixel_types(rast) = '{8BUI,8BUI,8BUI,8BUI,8BUI,8BUI,8BUI,8BUI,8BUI}'::text[]), -- Ukládaná rastrová data mají pixely datového typu 8BUI.
  CONSTRAINT enforce_scalex_rast CHECK (st_scalex(rast) = 10::double precision), -- Ukládaná rastrová data species_smooth mají pixel velikosti 10 m ve směru osy x.
  CONSTRAINT enforce_scaley_rast CHECK (st_scaley(rast) = '-10'::integer::double precision), -- Ukládaná rastrová data species_smooth mají pixel velikosti -10 m ve směru osy y.
  CONSTRAINT enforce_srid_rast CHECK (st_srid(rast) = 5221), -- Ukládaná rastrová data species_smooth musí být SRID 5221.
  CONSTRAINT enforce_width_rast CHECK (st_width(rast) = 10) -- Ukládaná rastrová data species_smooth mají každý tile široký 10 pixelů.
);

COMMENT ON TABLE @extschema@.r_s2_species_smooth
  IS 'Rastr dřevin vznikl řízenou klasifikací družicových snímků ESA Sentinel-2 provedenou na základě spektrální odezvy dřevin v průběhu fenologických fází vegetace s využitím trénovacích dat nasbíraných během pozemního šetření NIL. 
Výběrem ploch NIL s výskytem lesních dřevin s dominantním nebo majoritním zastoupením bylo možné provést zatřídění pixelů dat Sentinel dle hlavních hospodářských dřevin, tj. SM, BO, BK, DB a na ostatní listnaté a ostatní jehličnaté dřeviny. 
Tematická přesnost pro hlavní dřeviny vychází: SM 95%, BK 74%, DB 73%, a BO 71%. Velikost tilu je 10x10 pixelů. 
Rastr obsahuje devět pásem - 1 - smrk, 2 - buk, 3 - borovice, 4 - ostatní listnaté, 5 - duby, 6 - těžba, 7 - směs-nejisté, 8 - kultura, 9 - kleč.
Hodnoty pixelů v jednotlivých tilech pro smrk dřeviny ukazují plošné zastoupení smrkuv rámci kernelu 9x9 pixelů. Výsledná hodnota je v procentech. Stejně jsou ukládány i hodnoty 
v dalších vrtsvách. Součet všech pásem dává dohromady 100. Pokud ne, zbytek do 100 reprezentuje plošné zastoupení kategorie pozemku Ostatní. 
Pokud kernel zasahuje mimo oblast rasteru, je okno kernelu ořezáno na okraj rasteru. Hodnoty nodata z původního rasteru r_s2_species jsou považovány za 0, tedy za kat. pozemku Ostatní. ';
COMMENT ON COLUMN @extschema@.r_s2_species_smooth.rid IS 'primární klíč, databázový identifikátor';
COMMENT ON COLUMN @extschema@.r_s2_species_smooth.rast IS 'prostorový datový typ obsahující informace viz. komentar tabulky';

COMMENT ON CONSTRAINT pkey__r_s2_species_smooth__rid ON @extschema@.r_s2_species_smooth IS 'Jednoznačný identifikátor, primární klíč tabulky r_s2_species_smooth.';
COMMENT ON CONSTRAINT enforce_height_rast ON @extschema@.r_s2_species_smooth IS 'Ukládaná rastrová data mají každý tile vysoký 10 pixelů.';
COMMENT ON CONSTRAINT enforce_max_extent_rast ON @extschema@.r_s2_species_smooth IS 'Maximální rozsah ukládaných dat.';
COMMENT ON CONSTRAINT enforce_num_bands_rast ON @extschema@.r_s2_species_smooth IS 'Ukládaná rastrová data  musí mít 2 bandy (vrstvy).';
COMMENT ON CONSTRAINT enforce_pixel_types_rast ON @extschema@.r_s2_species_smooth IS 'Ukládaná rastrová data mají pixely datového typu 8BUI.';
COMMENT ON CONSTRAINT enforce_scalex_rast ON @extschema@.r_s2_species_smooth IS 'Ukládaná rastrová data species mají pixel velikosti 10 m ve směru osy x.';
COMMENT ON CONSTRAINT enforce_scaley_rast ON @extschema@.r_s2_species_smooth IS 'Ukládaná rastrová data species mají pixel velikosti -10 m ve směru osy y.';
COMMENT ON CONSTRAINT enforce_srid_rast ON @extschema@.r_s2_species_smooth IS 'Ukládaná rastrová data species musí být SRID 5221.';
COMMENT ON CONSTRAINT enforce_width_rast ON @extschema@.r_s2_species_smooth IS 'Ukládaná rastrová data mají každý tile široký 10 pixelů.';

-- Index: @extschema@.r_s2_species_smooth_st_convexhull_idx

-- DROP INDEX @extschema@.r_s2_species_smooth_st_convexhull_idx;

CREATE INDEX r_s2_species_smooth_st_convexhull_idx
  ON @extschema@.r_s2_species_smooth
  USING gist
  (st_convexhull(rast));

ALTER TABLE @extschema@.r_s2_species_smooth OWNER TO adm_nfiesta_gisdata;
GRANT ALL ON TABLE @extschema@.r_s2_species_smooth TO adm_nfiesta_gisdata;
GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE @extschema@.r_s2_species_smooth TO app_nfiesta_gisdata;
GRANT SELECT ON TABLE @extschema@.r_s2_species_smooth TO public;

COMMENT ON CONSTRAINT enforce_num_bands_rast ON @extschema@.r_s2_species_smooth IS 'Ukládaná rastrová data  musí mít 9 bandů (vrstev).';
COMMENT ON CONSTRAINT enforce_pixel_types_rast ON @extschema@.r_s2_species_smooth IS 'Ukládaná rastrová data ve všech bandech mají pixely datového typu 8BUI.';
COMMENT ON CONSTRAINT enforce_nodata_values_rast ON @extschema@.r_s2_species_smooth IS 'Ukládaná rastrová data ve všech bandech mají hodnotu nodata = 255.';
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
--	c_s2_ftype_bands
---------------------------------------------------------------------------------------------------;
--	SELECT * FROM @extschema@.c_s2_ftype_bands;

	DROP TABLE IF EXISTS @extschema@.c_s2_ftype_bands CASCADE;

	CREATE TABLE @extschema@.c_s2_ftype_bands
	(
		id 		integer 			NOT NULL,
		label 		character varying(100) 		NOT NULL,
		description 	text,
		CONSTRAINT pkey__c_s2_ftype_bands		PRIMARY KEY (id)
	);

	COMMENT ON TABLE  @extschema@.c_s2_ftype_bands						IS 'Číselník pásem v rasteru r_s2_ftype';
	COMMENT ON COLUMN @extschema@.c_s2_ftype_bands.id					IS 'Kód kategorie';
	COMMENT ON COLUMN @extschema@.c_s2_ftype_bands.label					IS 'Popis kategorie';
	COMMENT ON COLUMN @extschema@.c_s2_ftype_bands.description 				IS 'Podrobná definice kategorie';
	COMMENT ON CONSTRAINT pkey__c_s2_ftype_bands
		ON @extschema@.c_s2_ftype_bands 						IS 'Jednoznačný identifikátor, primární klíč tabulky číselníku';

	ALTER TABLE @extschema@.c_s2_ftype_bands						OWNER TO adm_nfiesta_gisdata;
	GRANT ALL ON TABLE @extschema@.c_s2_ftype_bands						TO adm_nfiesta_gisdata;
	GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE @extschema@.c_s2_ftype_bands	TO app_nfiesta_gisdata;
	GRANT SELECT ON TABLE @extschema@.c_s2_ftype_bands					TO public;

	INSERT INTO @extschema@.c_s2_ftype_bands (id, label, description)
	VALUES
		( '1', 'typ porostu', 	'typ porostu');
---------------------------------------------------------------------------------------------------;
--	c_s2_ftype_category
---------------------------------------------------------------------------------------------------;
--	SELECT * FROM @extschema@.c_s2_ftype_category;

	DROP TABLE IF EXISTS @extschema@.c_s2_ftype_category CASCADE;

	CREATE TABLE @extschema@.c_s2_ftype_category
	(
		id 		integer 			NOT NULL,
		label 		character varying(100) 		NOT NULL,
		description 	text,
		CONSTRAINT pkey__c_s2_ftype_category		PRIMARY KEY (id)
	);

	COMMENT ON TABLE  @extschema@.c_s2_ftype_category					IS 'Číselník kategorií v rasteru r_s2_ftype';
	COMMENT ON COLUMN @extschema@.c_s2_ftype_category.id					IS 'Kód kategorie';
	COMMENT ON COLUMN @extschema@.c_s2_ftype_category.label					IS 'Popis kategorie';
	COMMENT ON COLUMN @extschema@.c_s2_ftype_category.description 				IS 'Podrobná definice kategorie';
	COMMENT ON CONSTRAINT pkey__c_s2_ftype_category
		ON @extschema@.c_s2_ftype_category 						IS 'Jednoznačný identifikátor, primární klíč tabulky číselníku';

	ALTER TABLE @extschema@.c_s2_ftype_category						OWNER TO adm_nfiesta_gisdata;
	GRANT ALL ON TABLE @extschema@.c_s2_ftype_category					TO adm_nfiesta_gisdata;
	GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE @extschema@.c_s2_ftype_category	TO app_nfiesta_gisdata;
	GRANT SELECT ON TABLE @extschema@.c_s2_ftype_category					TO public;

	INSERT INTO @extschema@.c_s2_ftype_category (id, label, description)
	VALUES
		( '1', 'jehličnaté', 	'jehličnaté'),
		( '2', 'listnaté', 	'listnaté'),
		( '3', 'těžba', 	'těžba'),
		( '4', 'kultura', 	'kultura');
---------------------------------------------------------------------------------------------------;
--	c_s2_ftype_smooth_bands
---------------------------------------------------------------------------------------------------;
--	SELECT * FROM @extschema@.c_s2_ftype_smooth_bands;

	DROP TABLE IF EXISTS @extschema@.c_s2_ftype_smooth_bands CASCADE;

	CREATE TABLE @extschema@.c_s2_ftype_smooth_bands
	(
		id 		integer 			NOT NULL,
		label 		character varying(100) 		NOT NULL,
		description 	text,
		CONSTRAINT pkey__c_s2_ftype_smooth_bands	PRIMARY KEY (id)
	);

	COMMENT ON TABLE  @extschema@.c_s2_ftype_smooth_bands						IS 'Číselník pásem v rasteru r_s2_ftype_smooth';
	COMMENT ON COLUMN @extschema@.c_s2_ftype_smooth_bands.id					IS 'Kód kategorie';
	COMMENT ON COLUMN @extschema@.c_s2_ftype_smooth_bands.label					IS 'Popis kategorie';
	COMMENT ON COLUMN @extschema@.c_s2_ftype_smooth_bands.description 				IS 'Podrobná definice kategorie';
	COMMENT ON CONSTRAINT pkey__c_s2_ftype_smooth_bands
		ON @extschema@.c_s2_ftype_smooth_bands							IS 'Jednoznačný identifikátor, primární klíč tabulky číselníku';

	ALTER TABLE @extschema@.c_s2_ftype_smooth_bands							OWNER TO adm_nfiesta_gisdata;
	GRANT ALL ON TABLE @extschema@.c_s2_ftype_smooth_bands						TO adm_nfiesta_gisdata;
	GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE @extschema@.c_s2_ftype_smooth_bands	TO app_nfiesta_gisdata;
	GRANT SELECT ON TABLE @extschema@.c_s2_ftype_smooth_bands					TO public;

	INSERT INTO @extschema@.c_s2_ftype_smooth_bands (id, label, description)
	VALUES
		( '1', 'jehličnaté', 	'jehličnaté'),
		( '2', 'listnaté', 	'listnaté');
---------------------------------------------------------------------------------------------------;
--	c_s2_ftype_smooth_category
---------------------------------------------------------------------------------------------------;
--	číselník kategorií v rasteru r_s2_ftype_smooth
--	není potřeba, hodnoty rasteru představuji počet buněk v okolí, které při shlazovaní měly
--	danou dřevinu
---------------------------------------------------------------------------------------------------;
--	c_s2_species_bands
---------------------------------------------------------------------------------------------------;
--	SELECT * FROM @extschema@.c_s2_species_bands;

	DROP TABLE IF EXISTS @extschema@.c_s2_species_bands CASCADE;

	CREATE TABLE @extschema@.c_s2_species_bands
	(
		id 		integer 			NOT NULL,
		label 		character varying(100) 		NOT NULL,
		description 	text,
		CONSTRAINT pkey__c_s2_species_bands		PRIMARY KEY (id)
	);

	COMMENT ON TABLE  @extschema@.c_s2_species_bands					IS 'Číselník pásem v rasteru r_s2_species';
	COMMENT ON COLUMN @extschema@.c_s2_species_bands.id					IS 'Kód kategorie';
	COMMENT ON COLUMN @extschema@.c_s2_species_bands.label					IS 'Popis kategorie';
	COMMENT ON COLUMN @extschema@.c_s2_species_bands.description 				IS 'Podrobná definice kategorie';
	COMMENT ON CONSTRAINT pkey__c_s2_species_bands
		ON @extschema@.c_s2_species_bands						IS 'Jednoznačný identifikátor, primární klíč tabulky číselníku';

	ALTER TABLE @extschema@.c_s2_species_bands						OWNER TO adm_nfiesta_gisdata;
	GRANT ALL ON TABLE @extschema@.c_s2_species_bands					TO adm_nfiesta_gisdata;
	GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE @extschema@.c_s2_species_bands	TO app_nfiesta_gisdata;
	GRANT SELECT ON TABLE @extschema@.c_s2_species_bands					TO public;

	INSERT INTO @extschema@.c_s2_species_bands (id, label, description)
	VALUES
		( '1', 'dřevina', 'dřevina');
---------------------------------------------------------------------------------------------------;
--	c_s2_species_category
---------------------------------------------------------------------------------------------------;
--	SELECT * FROM @extschema@.c_s2_species_category;

	DROP TABLE IF EXISTS @extschema@.c_s2_species_category CASCADE;

	CREATE TABLE @extschema@.c_s2_species_category
	(
		id 		integer 			NOT NULL,
		label 		character varying(100) 		NOT NULL,
		description 	text,
		CONSTRAINT pkey__c_s2_species_category		PRIMARY KEY (id)
	);

	COMMENT ON TABLE  @extschema@.c_s2_species_category					IS 'Číselník kategorií v rasteru r_s2_species';
	COMMENT ON COLUMN @extschema@.c_s2_species_category.id					IS 'Kód kategorie';
	COMMENT ON COLUMN @extschema@.c_s2_species_category.label				IS 'Popis kategorie';
	COMMENT ON COLUMN @extschema@.c_s2_species_category.description 			IS 'Podrobná definice kategorie';
	COMMENT ON CONSTRAINT pkey__c_s2_species_category
		ON @extschema@.c_s2_species_category						IS 'Jednoznačný identifikátor, primární klíč tabulky číselníku';

	ALTER TABLE @extschema@.c_s2_species_category							OWNER TO adm_nfiesta_gisdata;
	GRANT ALL ON TABLE @extschema@.c_s2_species_category						TO adm_nfiesta_gisdata;
	GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE @extschema@.c_s2_species_category	TO app_nfiesta_gisdata;
	GRANT SELECT ON TABLE @extschema@.c_s2_species_category						TO public;

	INSERT INTO @extschema@.c_s2_species_category (id, label, description)
	VALUES
		( '1', 'smrk',			'smrk'),
		( '2', 'buk',			'buk'),
		( '3', 'borovice',		'borovice'),
		( '4', 'ostatní listnaté',	'ostatní listnaté'),
		( '5', 'duby',			'duby'),
		( '6', 'těžba',			'těžba'),
		( '7', 'směs - nejisté',	'směs - nejisté'),
		( '8', 'kultura',		'kultura'),
		( '9', 'kleč',			'kleč');
---------------------------------------------------------------------------------------------------;
--	c_s2_species_smooth_bands
---------------------------------------------------------------------------------------------------;
--	SELECT * FROM @extschema@.c_s2_species_smooth_bands;

	DROP TABLE IF EXISTS @extschema@.c_s2_species_smooth_bands CASCADE;

	CREATE TABLE @extschema@.c_s2_species_smooth_bands
	(
		id 		integer 			NOT NULL,
		label 		character varying(100) 		NOT NULL,
		description 	text,
		CONSTRAINT pkey__c_s2_species_smooth_bands	PRIMARY KEY (id)
	);

	COMMENT ON TABLE  @extschema@.c_s2_species_smooth_bands						IS 'Číselník pásem v rasteru r_s2_species_smooth';
	COMMENT ON COLUMN @extschema@.c_s2_species_smooth_bands.id					IS 'Kód kategorie';
	COMMENT ON COLUMN @extschema@.c_s2_species_smooth_bands.label					IS 'Popis kategorie';
	COMMENT ON COLUMN @extschema@.c_s2_species_smooth_bands.description 				IS 'Podrobná definice kategorie';
	COMMENT ON CONSTRAINT pkey__c_s2_species_smooth_bands
		ON @extschema@.c_s2_species_smooth_bands						IS 'Jednoznačný identifikátor, primární klíč tabulky číselníku';

	ALTER TABLE @extschema@.c_s2_species_smooth_bands						OWNER TO adm_nfiesta_gisdata;
	GRANT ALL ON TABLE @extschema@.c_s2_species_smooth_bands					TO adm_nfiesta_gisdata;
	GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE @extschema@.c_s2_species_smooth_bands	TO app_nfiesta_gisdata;
	GRANT SELECT ON TABLE @extschema@.c_s2_species_smooth_bands					TO public;

	INSERT INTO @extschema@.c_s2_species_smooth_bands (id, label, description)
	VALUES
		( '1', 'smrk',			'smrk'),
		( '2', 'buk',			'buk'),
		( '3', 'borovice',		'borovice'),
		( '4', 'ostatní listnaté',	'ostatní listnaté'),
		( '5', 'duby',			'duby'),
		( '6', 'těžba',			'těžba'),
		( '7', 'směs - nejisté',	'směs - nejisté'),
		( '8', 'kultura',		'kultura'),
		( '9', 'kleč',			'kleč');
---------------------------------------------------------------------------------------------------;
--	c_s2_species_smooth_category
---------------------------------------------------------------------------------------------------;
--	číselník kategorií v rasteru r_s2_species_smooth
--	není potřeba, hodnoty rasteru představuji počet buněk v okolí, které při shlazovaní měly
--	danou dřevinu
---------------------------------------------------------------------------------------------------;
--	c_lai_decrease_bands
---------------------------------------------------------------------------------------------------;
--	SELECT * FROM @extschema@.c_lai_decrease_bands;

	DROP TABLE IF EXISTS @extschema@.c_lai_decrease_bands CASCADE;

	CREATE TABLE @extschema@.c_lai_decrease_bands
	(
		id 		integer 		NOT NULL,
		label 		character varying(100) 	NOT NULL,
		description	text,
		CONSTRAINT pkey__c_lai_decrease_bands	PRIMARY KEY (id)
	);

	COMMENT ON TABLE  @extschema@.c_lai_decrease_bands					IS 'Číselník pásem v rasteru r_s2_species_smooth';
	COMMENT ON COLUMN @extschema@.c_lai_decrease_bands.id					IS 'Kód kategorie';
	COMMENT ON COLUMN @extschema@.c_lai_decrease_bands.label				IS 'Popis kategorie';
	COMMENT ON COLUMN @extschema@.c_lai_decrease_bands.description 				IS 'Podrobná definice kategorie';
	COMMENT ON CONSTRAINT pkey__c_lai_decrease_bands
		ON @extschema@.c_lai_decrease_bands						IS 'Jednoznačný identifikátor, primární klíč tabulky číselníku';

	ALTER TABLE @extschema@.c_lai_decrease_bands							OWNER TO adm_nfiesta_gisdata;
	GRANT ALL ON TABLE @extschema@.c_lai_decrease_bands						TO adm_nfiesta_gisdata;
	GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE @extschema@.c_lai_decrease_bands	TO app_nfiesta_gisdata;
	GRANT SELECT ON TABLE @extschema@.c_lai_decrease_bands						TO public;

	INSERT INTO @extschema@.c_lai_decrease_bands (id, label, description)
	VALUES
		( '1', 'kategorie změny LAI',	'kategorie změny LAI');
---------------------------------------------------------------------------------------------------;
--	c_lai_decrease_category
---------------------------------------------------------------------------------------------------;
--	SELECT * FROM @extschema@.c_lai_decrease_category;

	DROP TABLE IF EXISTS @extschema@.c_lai_decrease_category CASCADE;

	CREATE TABLE @extschema@.c_lai_decrease_category
	(
		id 		integer 			NOT NULL,
		label 		character varying(100) 		NOT NULL,
		description	text,
		CONSTRAINT pkey__c_lai_decrease_category	PRIMARY KEY (id)
	);

	COMMENT ON TABLE  @extschema@.c_lai_decrease_category						IS 'Číselník kategorií v rasteru r_s2_species_smooth';
	COMMENT ON COLUMN @extschema@.c_lai_decrease_category.id					IS 'Kód kategorie';
	COMMENT ON COLUMN @extschema@.c_lai_decrease_category.label					IS 'Popis kategorie';
	COMMENT ON COLUMN @extschema@.c_lai_decrease_category.description 				IS 'Podrobná definice kategorie';
	COMMENT ON CONSTRAINT pkey__c_lai_decrease_category
	ON @extschema@.c_lai_decrease_category								IS 'Jednoznačný identifikátor, primární klíč tabulky číselníku';

	ALTER TABLE @extschema@.c_lai_decrease_category							OWNER TO adm_nfiesta_gisdata;
	GRANT ALL ON TABLE @extschema@.c_lai_decrease_category						TO adm_nfiesta_gisdata;
	GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE @extschema@.c_lai_decrease_category	TO app_nfiesta_gisdata;
	GRANT SELECT ON TABLE @extschema@.c_lai_decrease_category					TO public;

	INSERT INTO @extschema@.c_lai_decrease_category (id, label, description)
	VALUES
		( '1', 'výrazný přírůst LAI',	'výrazný přírůst LAI mezi roky 2018 - 2016 (indexu listové plochy) = výrazné zlepšení zdravotního stavu = přírůst LAI o více než 1.5'),
		( '2', 'mírný přírůst LAI',	'mírný přírůst LAI mezi roky 2018 - 2016 = mírné zlepšení zdravotního stavu = přírůst LAI o 0.5 až 1.5'),
		( '3', 'setrvalý stav LAI',	'setrvalý stav LAI mezi roky 2016 - 2018 se nezměníl, nebo změna nedosáhla prahové hodnoty +0.5, nebo -0.5'),
		( '4', 'mírné zhoršení LAI',	'mírné zhoršení LAI mezi roky 2016 - 2018 = mírné zhoršení zdravotního stavu pokles LAU o 0.5 až 1.5'),
		( '5', 'výrazný pokles LAI',	'výrazný pokles LAI mezi roky 2016 - 2018 = výrazné zhoršení zdravotniho stavu = pokles o více než 1.5');
---------------------------------------------------------------------------------------------------;
-- c_config_function
---------------------------------------------------------------------------------------------------;
--	SELECT * FROM @extschema@.c_config_function;

	DROP TABLE IF EXISTS @extschema@.c_config_function CASCADE;

	CREATE TABLE @extschema@.c_config_function
	(
		id 		integer 			NOT NULL,
		label 		character varying(100) 		NOT NULL,
		description	text,
		CONSTRAINT pkey__c_config_function		PRIMARY KEY (id)
	);

	COMMENT ON TABLE  @extschema@.c_config_function							IS 'Číselník funkcí pro výpočet úhrnů pomocných proměnných';
	COMMENT ON COLUMN @extschema@.c_config_function.id						IS 'Kód kategorie';
	COMMENT ON COLUMN @extschema@.c_config_function.label						IS 'Popis kategorie';
	COMMENT ON COLUMN @extschema@.c_config_function.description 					IS 'Podrobná definice kategorie';
	COMMENT ON CONSTRAINT pkey__c_config_function
		ON @extschema@.c_config_function							IS 'Jednoznačný identifikátor, primární klíč tabulky číselníku';

	ALTER TABLE @extschema@.c_config_function							OWNER TO adm_nfiesta_gisdata;
	GRANT ALL ON TABLE @extschema@.c_config_function						TO adm_nfiesta_gisdata;
	GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE @extschema@.c_config_function		TO app_nfiesta_gisdata;
	GRANT SELECT ON TABLE @extschema@.c_config_function						TO public;

	INSERT INTO @extschema@.c_config_function (id, label, description)
	VALUES
		( '100', 'Úhrn kategorie z vektorové vrstvy',			NULL),
		( '200', 'Úhrn kategorie z rasterové vrstvy',			NULL),
		( '300', 'Úhrn rasteru v rámci kategorie vektorové vrstvy',	NULL),
		( '400', 'Úhrn součinu dvou rasterů',				NULL);
---------------------------------------------------------------------------------------------------;
-- c_config_query
---------------------------------------------------------------------------------------------------;
--	SELECT * FROM @extschema@.c_config_query;

	DROP TABLE IF EXISTS @extschema@.c_config_query CASCADE;

	CREATE TABLE @extschema@.c_config_query
	(
		id 		integer 			NOT NULL,
		label 		character varying(100) 		NOT NULL,
		description	text,
		CONSTRAINT pkey__c_config_query			PRIMARY KEY (id)
	);

	COMMENT ON TABLE  @extschema@.c_config_query							IS 'Číselník variant výpočtu úhrnů pomocných proměnných';
	COMMENT ON COLUMN @extschema@.c_config_query.id							IS 'Kód kategorie';
	COMMENT ON COLUMN @extschema@.c_config_query.label						IS 'Popis kategorie';
	COMMENT ON COLUMN @extschema@.c_config_query.description 					IS 'Podrobná definice kategorie';
	COMMENT ON CONSTRAINT pkey__c_config_query
		ON @extschema@.c_config_query								IS 'Jednoznačný identifikátor, primární klíč tabulky číselníku';

	ALTER TABLE @extschema@.c_config_query								OWNER TO adm_nfiesta_gisdata;
	GRANT ALL ON TABLE @extschema@.c_config_query							TO adm_nfiesta_gisdata;
	GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE @extschema@.c_config_query		TO app_nfiesta_gisdata;
	GRANT SELECT ON TABLE @extschema@.c_config_query						TO public;

	INSERT INTO @extschema@.c_config_query (id, label, description)
	VALUES
		( 100, 'Výpočet z GIS vrstvy',				NULL),
		( 200, 'Součet existujících kategorií',			NULL),
		( 300, 'Doplněk do rozlohy výpočetní buňky',		NULL),
		( 400, 'Doplněk do rozlohy existující kategorie',	NULL),
		( 500, 'Odkaz na existující konfiguraci',		NULL);
---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- c_estimation_cell_collection
---------------------------------------------------------------------------------------------------;

--	SELECT * FROM @extschema@.c_estimation_cell_collection;

	DROP TABLE IF EXISTS @extschema@.c_estimation_cell_collection;

	CREATE TABLE @extschema@.c_estimation_cell_collection
	(
		id		integer,
		label		character varying(120),
		description	text
	);

--------------------------------------------------------------------------------;
-- dokumentace
--------------------------------------------------------------------------------;

	-- tabulka
	COMMENT ON TABLE @extschema@.c_estimation_cell_collection
		IS 'Číselník skupin výpočetních buněk.';

	-- atributy
	COMMENT ON COLUMN @extschema@.c_estimation_cell_collection.id
		IS 'Identifikační číslo skupiny výpočetních buněk.';
	COMMENT ON COLUMN @extschema@.c_estimation_cell_collection.label
		IS 'Název skupiny výpočetních buněk.';
	COMMENT ON COLUMN @extschema@.c_estimation_cell_collection.description
		IS 'Podrobný popis skupiny výpočetních buněk.';

--------------------------------------------------------------------------------;
-- not null
--------------------------------------------------------------------------------;

	-- id
	ALTER TABLE @extschema@.c_estimation_cell_collection
		ALTER COLUMN id				SET NOT NULL;

	-- label
	ALTER TABLE @extschema@.c_estimation_cell_collection
		ALTER COLUMN label			SET NOT NULL;

	-- description
	ALTER TABLE @extschema@.c_estimation_cell_collection
		ALTER COLUMN description		SET NOT NULL;

--------------------------------------------------------------------------------;
-- primarni klic
--------------------------------------------------------------------------------;

	-- pkey__c_estimation_cell_collection
	ALTER TABLE @extschema@.c_estimation_cell_collection
	DROP CONSTRAINT IF EXISTS pkey__c_estimation_cell_collection;

	ALTER TABLE @extschema@.c_estimation_cell_collection ADD
		CONSTRAINT pkey__c_estimation_cell_collection
		PRIMARY KEY (id)
		NOT DEFERRABLE
		INITIALLY IMMEDIATE;

	COMMENT ON CONSTRAINT pkey__c_estimation_cell_collection
		ON @extschema@.c_estimation_cell_collection
	IS 'Primární klíč.';

--------------------------------------------------------------------------------;
-- vlastnik a opravneni
--------------------------------------------------------------------------------;

	-- tabulka

	ALTER TABLE @extschema@.c_estimation_cell_collection							OWNER TO adm_nfiesta_gisdata;
	GRANT ALL ON TABLE @extschema@.c_estimation_cell_collection						TO adm_nfiesta_gisdata;
	GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE @extschema@.c_estimation_cell_collection	TO app_nfiesta_gisdata;
	GRANT SELECT ON TABLE @extschema@.c_estimation_cell_collection						TO public;

---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- c_estimation_cell
---------------------------------------------------------------------------------------------------;

--	SELECT * FROM @extschema@.c_estimation_cell;

	DROP TABLE IF EXISTS @extschema@.c_estimation_cell;

	CREATE TABLE @extschema@.c_estimation_cell
	(
		id 				integer,
		estimation_cell_collection 	integer,
		label 				character varying(20),
		description 			text
	);

--------------------------------------------------------------------------------;
-- dokumentace
--------------------------------------------------------------------------------;

	-- tabulka
	COMMENT ON TABLE @extschema@.c_estimation_cell
		 IS 'Číselník výpočetních buněk.';

	-- atributy
	COMMENT ON COLUMN @extschema@.c_estimation_cell.id
		 IS 'Identifikační číslo výpočetní buňky, primární klíč.';
	COMMENT ON COLUMN @extschema@.c_estimation_cell.estimation_cell_collection
		IS 'Identifikační číslo skupiny výpočetních buněk, cizí klíč na číselník c_estimation_cell_collection.';
	COMMENT ON COLUMN @extschema@.c_estimation_cell.label
		 IS 'Název výpočetní buňky.';
	COMMENT ON COLUMN @extschema@.c_estimation_cell.description
		IS 'Podrobný popis výpočetní buňky.';

--------------------------------------------------------------------------------;
-- not null
--------------------------------------------------------------------------------;

	-- id
	ALTER TABLE @extschema@.c_estimation_cell
		ALTER COLUMN id					SET NOT NULL;

	-- estimation_cell_collection
	ALTER TABLE @extschema@.c_estimation_cell
		ALTER COLUMN estimation_cell_collection		SET NOT NULL;

	-- label
	ALTER TABLE @extschema@.c_estimation_cell
		ALTER COLUMN label				SET NOT NULL;

	-- description
	ALTER TABLE @extschema@.c_estimation_cell
		ALTER COLUMN description			SET NOT NULL;

--------------------------------------------------------------------------------;
-- primarni klic
--------------------------------------------------------------------------------;

	-- pkey__c_estimation_cell
	ALTER TABLE @extschema@.c_estimation_cell
	DROP CONSTRAINT IF EXISTS pkey__c_estimation_cell;

	ALTER TABLE @extschema@.c_estimation_cell ADD
		CONSTRAINT pkey__c_estimation_cell
		PRIMARY KEY (id)
		NOT DEFERRABLE
		INITIALLY IMMEDIATE;

	COMMENT ON CONSTRAINT pkey__c_estimation_cell
		ON @extschema@.c_estimation_cell
	IS 'Primární klíč.';

--------------------------------------------------------------------------------;
-- cizi klic
--------------------------------------------------------------------------------;

	-- fkey__c_estimation_cell__c_estimation_cell_collection
	ALTER TABLE @extschema@.c_estimation_cell
	DROP CONSTRAINT IF EXISTS fkey__c_estimation_cell__c_estimation_cell_collection;

	ALTER TABLE @extschema@.c_estimation_cell ADD
		CONSTRAINT fkey__c_estimation_cell__c_estimation_cell_collection
		FOREIGN KEY (estimation_cell_collection)
		REFERENCES @extschema@.c_estimation_cell_collection (id)
		MATCH SIMPLE
		ON UPDATE NO ACTION
		ON DELETE NO ACTION
		NOT DEFERRABLE
		INITIALLY IMMEDIATE;

	COMMENT ON CONSTRAINT fkey__c_estimation_cell__c_estimation_cell_collection
		ON @extschema@.c_estimation_cell
	IS 'Cizí klíč na číselník c_estimation_cell_collection.';

--------------------------------------------------------------------------------;
-- vlastnik a opravneni
--------------------------------------------------------------------------------;

	-- tabulka

	ALTER TABLE @extschema@.c_estimation_cell						OWNER TO adm_nfiesta_gisdata;
	GRANT ALL ON TABLE @extschema@.c_estimation_cell					TO adm_nfiesta_gisdata;
	GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE @extschema@.c_estimation_cell	TO app_nfiesta_gisdata;
	GRANT SELECT ON TABLE @extschema@.c_estimation_cell					TO public;

---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;
-- F_A_CELL --
CREATE TABLE @extschema@.f_a_cell
	(
	gid serial NOT NULL,
	geom geometry(MULTIPOLYGON) NOT NULL,
	estimation_cell integer NOT NULL,
	CONSTRAINT pkey__f_a_cell PRIMARY KEY (gid),
	CONSTRAINT fkey__f_a_cell__c_estimation_cell FOREIGN KEY (estimation_cell) REFERENCES @extschema@.c_estimation_cell(id) ON UPDATE CASCADE
	);

	ALTER TABLE @extschema@.f_a_cell						OWNER TO adm_nfiesta_gisdata;
	GRANT ALL ON TABLE @extschema@.f_a_cell						TO adm_nfiesta_gisdata;
	GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE @extschema@.f_a_cell	TO app_nfiesta_gisdata;
	GRANT SELECT ON TABLE @extschema@.f_a_cell					TO public;

	COMMENT ON TABLE @extschema@.f_a_cell IS 'Table of areas, where the estimation is done (cells), contains geometries.';
	COMMENT ON COLUMN @extschema@.f_a_cell.gid IS 'Primary key, identifier of the cell.';
	COMMENT ON COLUMN @extschema@.f_a_cell.geom IS 'Polygon geometry of the cell.';
	COMMENT ON COLUMN @extschema@.f_a_cell.estimation_cell IS 'Estimation cell, foreign key to table c_estimation_cell.';
	COMMENT ON CONSTRAINT pkey__f_a_cell ON @extschema@.f_a_cell IS 'Primary key.';
	COMMENT ON CONSTRAINT fkey__f_a_cell__c_estimation_cell ON @extschema@.f_a_cell IS 'Foreign key to table c_estimation_cell.';

	CREATE INDEX idx_f_a_cell_geom ON @extschema@.f_a_cell USING gist (geom);
	CREATE INDEX fki__f_a_cell__c_estimation_cell ON @extschema@.f_a_cell USING btree (estimation_cell);
	COMMENT ON INDEX @extschema@.idx_f_a_cell_geom IS 'GIST index on column geom.';
	COMMENT ON INDEX @extschema@.fki__f_a_cell__c_estimation_cell IS 'BTree index on foreign key fkey__f_a_cell__c_estimation_cell.';
---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;
-- CM_F_A_CELL --
CREATE TABLE @extschema@.cm_f_a_cell
	(
	id		serial NOT NULL,
	cell		integer NOT NULL,
	cell_sup	integer,
	CONSTRAINT pkey__cm_f_a_cell PRIMARY KEY (id),
	CONSTRAINT fkey__cm_f_a_cell__f_a_cell_1 FOREIGN KEY (cell) REFERENCES @extschema@.f_a_cell (gid) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT fkey__cm_f_a_cell__f_a_cell_2 FOREIGN KEY (cell_sup) REFERENCES @extschema@.f_a_cell (gid) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
	);

	ALTER TABLE @extschema@.cm_f_a_cell							OWNER TO adm_nfiesta_gisdata;
	GRANT ALL ON TABLE @extschema@.cm_f_a_cell						TO adm_nfiesta_gisdata;
	GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE @extschema@.cm_f_a_cell		TO app_nfiesta_gisdata;
	GRANT SELECT ON TABLE @extschema@.cm_f_a_cell						TO public;

	CREATE INDEX fki__cm_f_a_cell__f_a_cell_1 ON @extschema@.cm_f_a_cell USING btree (cell);
	CREATE INDEX fki__cm_f_a_cell__f_a_cell_2 ON @extschema@.cm_f_a_cell USING btree (cell_sup);

	COMMENT ON TABLE  @extschema@.cm_f_a_cell IS 'Maping table for table f_a_cell.';
	COMMENT ON COLUMN @extschema@.cm_f_a_cell.id IS 'Primary key, identifier of the record.';
	COMMENT ON COLUMN @extschema@.cm_f_a_cell.cell IS 'Foreign key to gid of f_a_cell (all or part of target estimate area).';
	COMMENT ON COLUMN @extschema@.cm_f_a_cell.cell_sup IS 'Foreign key to gid of f_a_cell (all or part of target estimate area).';
	COMMENT ON CONSTRAINT pkey__cm_f_a_cell ON @extschema@.cm_f_a_cell IS 'Primary key.';
	COMMENT ON CONSTRAINT fkey__cm_f_a_cell__f_a_cell_1 ON @extschema@.cm_f_a_cell IS 'Foreign key to table f_a_cell.';
	COMMENT ON CONSTRAINT fkey__cm_f_a_cell__f_a_cell_2 ON @extschema@.cm_f_a_cell IS 'Foreign key to table f_a_cell.';
	COMMENT ON INDEX @extschema@.fki__cm_f_a_cell__f_a_cell_1 IS 'BTree index on foreign key fkey__cm_f_a_cell__f_a_cell_1.';
	COMMENT ON INDEX @extschema@.fki__cm_f_a_cell__f_a_cell_2 IS 'BTree index on foreign key fkey__cm_f_a_cell__f_a_cell_2.';
---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;
-- CM_ESTIMATION_CELL_COLLECTION --
CREATE TABLE @extschema@.cm_estimation_cell_collection
	(
	id serial NOT NULL,
	estimation_cell_collection integer NOT NULL,
	estimation_cell_collection_lowest integer NOT NULL,
	CONSTRAINT pkey__cm_estimation_cell_collection PRIMARY KEY (id),
	CONSTRAINT fkey__cm_ecc__c_ecc_1 FOREIGN KEY (estimation_cell_collection) REFERENCES @extschema@.c_estimation_cell_collection (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT fkey__cm_ecc__c_ecc_2 FOREIGN KEY (estimation_cell_collection_lowest) REFERENCES @extschema@.c_estimation_cell_collection (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
	);

	ALTER TABLE @extschema@.cm_estimation_cell_collection							OWNER TO adm_nfiesta_gisdata;
	GRANT ALL ON TABLE @extschema@.cm_estimation_cell_collection						TO adm_nfiesta_gisdata;
	GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE @extschema@.cm_estimation_cell_collection	TO app_nfiesta_gisdata;
	GRANT SELECT ON TABLE @extschema@.cm_estimation_cell_collection						TO public;

	COMMENT ON TABLE @extschema@.cm_estimation_cell_collection IS 'Identification lowest estimation_cell_collecition for each estimation_cell_collection.';
	COMMENT ON COLUMN @extschema@.cm_estimation_cell_collection.id IS 'Primary key, identifier of the record.';
	COMMENT ON COLUMN @extschema@.cm_estimation_cell_collection.estimation_cell_collection IS 'Foreign key to id of c_estimation_cell_collection.';
	COMMENT ON COLUMN @extschema@.cm_estimation_cell_collection.estimation_cell_collection_lowest IS 'Foreign key to id of c_estimation_cell_collection.';
	COMMENT ON CONSTRAINT fkey__cm_ecc__c_ecc_1 ON @extschema@.cm_estimation_cell_collection IS 'Foreign key to table c_estimation_cell_collection.';
	COMMENT ON CONSTRAINT fkey__cm_ecc__c_ecc_2 ON @extschema@.cm_estimation_cell_collection IS 'Foreign key to table c_estimation_cell_collection.';

	CREATE INDEX fki__cm_ecc__c_ecc_1 ON @extschema@.cm_estimation_cell_collection USING btree (estimation_cell_collection);
	CREATE INDEX fki__cm_ecc__c_ecc_2 ON @extschema@.cm_estimation_cell_collection USING btree (estimation_cell_collection_lowest);

	COMMENT ON INDEX @extschema@.fki__cm_ecc__c_ecc_1 IS 'BTree index on foreign key fkey__cm_ecc__c_ecc_1.';
	COMMENT ON INDEX @extschema@.fki__cm_ecc__c_ecc_2 IS 'BTree index on foreign key fkey__cm_ecc__c_ecc_2.';
---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- C_GUI_VERSION --
---------------------------------------------------------------------------------------------------;

--	SELECT * FROM @extschema@.c_gui_version;

	DROP TABLE IF EXISTS @extschema@.c_gui_version;

	CREATE TABLE @extschema@.c_gui_version
	(
		id		integer,
		label		character varying(120),
		description	text
	);

	ALTER TABLE @extschema@.c_gui_version ALTER COLUMN id SET NOT NULL;
	ALTER TABLE @extschema@.c_gui_version ALTER COLUMN label SET NOT NULL;
	ALTER TABLE @extschema@.c_gui_version ALTER COLUMN description SET NOT NULL;

	ALTER TABLE @extschema@.c_gui_version
	DROP CONSTRAINT IF EXISTS pkey__c_gui_version;

	ALTER TABLE @extschema@.c_gui_version ADD
		CONSTRAINT pkey__c_gui_version
		PRIMARY KEY (id)
		NOT DEFERRABLE
		INITIALLY IMMEDIATE;

	COMMENT ON TABLE @extschema@.c_gui_version IS 'Číselník verzí GUI aplikace pro pomocná data.';
	COMMENT ON COLUMN @extschema@.c_gui_version.id IS 'Identifikační číslo verze GUI aplikace.';
	COMMENT ON COLUMN @extschema@.c_gui_version.label IS 'Název verze GUI aplikace.';
	COMMENT ON COLUMN @extschema@.c_gui_version.description IS 'Podrobný popis verze GUI aplikace.';

	COMMENT ON CONSTRAINT pkey__c_gui_version ON @extschema@.c_gui_version IS 'Primární klíč.';

	ALTER TABLE @extschema@.c_gui_version							OWNER TO adm_nfiesta_gisdata;
	GRANT ALL ON TABLE @extschema@.c_gui_version						TO adm_nfiesta_gisdata;
	GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE @extschema@.c_gui_version	TO app_nfiesta_gisdata;
	GRANT SELECT ON TABLE @extschema@.c_gui_version						TO public;

-- staticka data
	INSERT INTO @extschema@.c_gui_version(id, label, description) VALUES
	(100,'1.0.0','Verze 1.0.0 - GUI aplikace pro pomocná data.'),
	(200,'1.0.1','Verze 1.0.1 - GUI aplikace pro pomocná data.'),
	(300,'1.0.2','Verze 1.0.2 - GUI aplikace pro pomocná data.'),
	(400,'1.0.3','Verze 1.0.3 - GUI aplikace pro pomocná data.');
	
---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- C_EXT_VERSION --
---------------------------------------------------------------------------------------------------;

--	SELECT * FROM @extschema@.c_ext_version;

	DROP TABLE IF EXISTS @extschema@.c_ext_version;

	CREATE TABLE @extschema@.c_ext_version
	(
		id		integer,
		label		character varying(120),
		description	text
	);

	ALTER TABLE @extschema@.c_ext_version ALTER COLUMN id SET NOT NULL;
	ALTER TABLE @extschema@.c_ext_version ALTER COLUMN label SET NOT NULL;
	ALTER TABLE @extschema@.c_ext_version ALTER COLUMN description SET NOT NULL;

	ALTER TABLE @extschema@.c_ext_version
	DROP CONSTRAINT IF EXISTS pkey__c_ext_version;

	ALTER TABLE @extschema@.c_ext_version ADD
		CONSTRAINT pkey__c_ext_version
		PRIMARY KEY (id)
		NOT DEFERRABLE
		INITIALLY IMMEDIATE;

	COMMENT ON TABLE @extschema@.c_ext_version IS 'Číselník verzí extenze nfiesta_gisdata pro pomocná data.';
	COMMENT ON COLUMN @extschema@.c_ext_version.id IS 'Identifikační číslo verze extenze nfiesta_gisdata.';
	COMMENT ON COLUMN @extschema@.c_ext_version.label IS 'Název verze extenze nfiesta_gisdata.';
	COMMENT ON COLUMN @extschema@.c_ext_version.description IS 'Podrobný popis verze extenze nfiesta_gisdata.';

	COMMENT ON CONSTRAINT pkey__c_ext_version ON @extschema@.c_ext_version IS 'Primární klíč.';

	ALTER TABLE @extschema@.c_ext_version							OWNER TO adm_nfiesta_gisdata;
	GRANT ALL ON TABLE @extschema@.c_ext_version						TO adm_nfiesta_gisdata;
	GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE @extschema@.c_ext_version	TO app_nfiesta_gisdata;
	GRANT SELECT ON TABLE @extschema@.c_ext_version						TO public;

-- staticka data
	INSERT INTO @extschema@.c_ext_version(id, label, description) VALUES
	(100,'1.0.0','Verze 1.0.0 - extenze nfiesta_gisdata pro pomocná data. Implementována pouze část pro výpočet úhrnů.');
---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- CM_EXT_GUI_VERSION
---------------------------------------------------------------------------------------------------;

--	SELECT * FROM @extschema@.cm_ext_gui_version;

	DROP TABLE IF EXISTS @extschema@.cm_ext_gui_version;
	
	CREATE TABLE @extschema@.cm_ext_gui_version
		(
		id		serial NOT NULL,
		ext_version	integer NOT NULL,
		gui_version	integer NOT NULL,
		CONSTRAINT pkey__cm_ext_gui_version PRIMARY KEY (id),
		CONSTRAINT ukey__cm_ext_gui_version UNIQUE (ext_version, gui_version) NOT DEFERRABLE INITIALLY IMMEDIATE,
		CONSTRAINT fkey__cm_ext_gui_version__c_ext_version FOREIGN KEY (ext_version) REFERENCES @extschema@.c_ext_version(id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE NO ACTION,
		CONSTRAINT fkey__cm_ext_gui_version__c_gui_version FOREIGN KEY (gui_version) REFERENCES @extschema@.c_gui_version(id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE NO ACTION
		);

	CREATE INDEX fki__cm_ext_gui_version__c_ext_version ON @extschema@.cm_ext_gui_version USING btree (ext_version);
	CREATE INDEX fki__cm_ext_gui_version__c_gui_version ON @extschema@.cm_ext_gui_version USING btree (gui_version);

	ALTER TABLE @extschema@.cm_ext_gui_version							OWNER TO adm_nfiesta_gisdata;
	GRANT ALL ON TABLE @extschema@.cm_ext_gui_version						TO adm_nfiesta_gisdata;
	GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE @extschema@.cm_ext_gui_version		TO app_nfiesta_gisdata;
	GRANT SELECT ON TABLE @extschema@.cm_ext_gui_version						TO public;

	COMMENT ON TABLE  @extschema@.cm_ext_gui_version IS 'Maping table between table c_gui_version and c_ext_version.';
	COMMENT ON COLUMN @extschema@.cm_ext_gui_version.id IS 'Primary key, identifier of the record.';
	COMMENT ON COLUMN @extschema@.cm_ext_gui_version.ext_version IS 'Foreign key to id of c_ext_version.';
	COMMENT ON COLUMN @extschema@.cm_ext_gui_version.gui_version IS 'Foreign key to id of c_gui_version.';
	COMMENT ON CONSTRAINT pkey__cm_ext_gui_version ON @extschema@.cm_ext_gui_version IS 'Primary key.';
	COMMENT ON CONSTRAINT ukey__cm_ext_gui_version ON @extschema@.cm_ext_gui_version IS 'Unique key.';
	COMMENT ON CONSTRAINT fkey__cm_ext_gui_version__c_ext_version ON @extschema@.cm_ext_gui_version IS 'Foreign key to table c_ext_version.';
	COMMENT ON CONSTRAINT fkey__cm_ext_gui_version__c_gui_version ON @extschema@.cm_ext_gui_version IS 'Foreign key to table c_gui_version.';
	COMMENT ON INDEX @extschema@.fki__cm_ext_gui_version__c_ext_version IS 'BTree index on foreign key fkey__cm_ext_gui_version__c_ext_version.';
	COMMENT ON INDEX @extschema@.fki__cm_ext_gui_version__c_gui_version IS 'BTree index on foreign key fkey__cm_ext_gui_version__c_gui_version.';

-- staticka data
	INSERT INTO @extschema@.cm_ext_gui_version(ext_version, gui_version) VALUES
	(100,100),
	(100,200),
	(100,300),
	(100,400);
---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
--	t_config_collection
---------------------------------------------------------------------------------------------------;
--	SELECT * FROM @extschema@.t_config_collection ORDER BY id;

	DROP TABLE IF EXISTS @extschema@.t_config_collection CASCADE;

	CREATE TABLE @extschema@.t_config_collection
	(
		id 					integer,
		auxiliary_variable			integer,
		config_function				integer,
		label		 			character varying,
		aggregated				boolean,
		catalog_name				character varying,
		schema_name				character varying,
		table_name				character varying,
		column_ident				character varying,
		column_name				character varying,
		unit					double precision,
		tag					text,
		description				text,
		closed					boolean,
		edit_date				timestamp without time zone
	);

--------------------------------------------------------------------------------;
-- dokumentace
--------------------------------------------------------------------------------;

	-- tabulka
	COMMENT ON TABLE @extschema@.t_config_collection
		IS 'Skupiny konfigurací výpočtu úhrnů pomocných proměnných';

	-- atributy
	COMMENT ON COLUMN @extschema@.t_config_collection.id
		IS 'Identifikační číslo záznamu skupiny konfigurací';
	COMMENT ON COLUMN @extschema@.t_config_collection.auxiliary_variable
		IS 'Pomocná proměnná';
	COMMENT ON COLUMN @extschema@.t_config_collection.config_function
		IS 'Identifikační číslo funkce pro výpočet úhrnů pomocných proměnných';
	COMMENT ON COLUMN @extschema@.t_config_collection.label
		IS 'Popis skupiny konfigurací';
	COMMENT ON COLUMN @extschema@.t_config_collection.aggregated
		IS 'Agregovaná skupina';
	COMMENT ON COLUMN @extschema@.t_config_collection.catalog_name
		IS 'Jméno databáze s GIS vrstvou';
	COMMENT ON COLUMN @extschema@.t_config_collection.schema_name
		IS 'Jméno databázového schéma GIS vrstvy';
	COMMENT ON COLUMN @extschema@.t_config_collection.table_name
		IS 'Jméno databázové tabulky GIS vrstvy';
	COMMENT ON COLUMN @extschema@.t_config_collection.column_ident
		IS 'Jméno databázového sloupce s identifikátorem záznamu';
	COMMENT ON COLUMN @extschema@.t_config_collection.column_name
		IS 'Jméno databázového sloupce s geometrií';
	COMMENT ON COLUMN @extschema@.t_config_collection.unit
		IS 'Koeficient pro převod jednotek';
	COMMENT ON COLUMN @extschema@.t_config_collection.tag
		IS 'Doplňující informace';
	COMMENT ON COLUMN @extschema@.t_config_collection.description
		IS 'Podrobný popis skupiny konfigurací';
	COMMENT ON COLUMN @extschema@.t_config_collection.closed
		IS 'Editace skupiny je ukončena.';
	COMMENT ON COLUMN @extschema@.t_config_collection.edit_date
		IS 'Datum editace.';

--------------------------------------------------------------------------------;
-- not null constraints
--------------------------------------------------------------------------------;

	-- id
	ALTER TABLE @extschema@.t_config_collection
		ALTER COLUMN id						SET NOT NULL;

	-- auxiliary_variable

	-- config_function
	ALTER TABLE @extschema@.t_config_collection
		ALTER COLUMN config_function				SET NOT NULL;

	-- label
	ALTER TABLE @extschema@.t_config_collection
		ALTER COLUMN label					SET NOT NULL;

	-- aggregated
	ALTER TABLE @extschema@.t_config_collection
		ALTER COLUMN aggregated					SET NOT NULL;

	-- catalog_name

	-- schema_name

	-- table_name

	-- column_ident

	-- column_name

	-- unit

	-- tag

	-- description
	ALTER TABLE @extschema@.t_config_collection
		ALTER COLUMN description				SET NOT NULL;

	ALTER TABLE @extschema@.t_config_collection
		ALTER COLUMN closed					SET NOT NULL;
	
	ALTER TABLE @extschema@.t_config_collection
		ALTER COLUMN edit_date					SET NOT NULL;

--------------------------------------------------------------------------------;
-- primarni klic
--------------------------------------------------------------------------------;

	-- pkey__t_config_collection
	ALTER TABLE @extschema@.t_config_collection
	DROP CONSTRAINT IF EXISTS pkey__t_config_collection;

	ALTER TABLE @extschema@.t_config_collection ADD
	CONSTRAINT pkey__t_config_collection
		PRIMARY KEY (id)
		NOT DEFERRABLE
		INITIALLY IMMEDIATE;

	COMMENT ON CONSTRAINT pkey__t_config_collection
		ON @extschema@.t_config_collection
	IS 'Jednoznačný identifikátor, primární klíč tabulky';

--------------------------------------------------------------------------------;
-- cizi klice
--------------------------------------------------------------------------------;

	-- fkey__t_config_collection__c_config_function
	ALTER TABLE @extschema@.t_config_collection
	DROP CONSTRAINT IF EXISTS fkey__t_config_collection__c_config_function;

	ALTER TABLE @extschema@.t_config_collection ADD
	CONSTRAINT fkey__t_config_collection__c_config_function
		FOREIGN KEY (config_function)
		REFERENCES @extschema@.c_config_function(id)
		MATCH SIMPLE
		ON UPDATE CASCADE
		ON DELETE NO ACTION
		NOT DEFERRABLE
		INITIALLY IMMEDIATE;

	COMMENT ON CONSTRAINT fkey__t_config_collection__c_config_function ON @extschema@.t_config_collection
	IS 'Cizí klíč, číselník funkcí pro výpočet úhrnů pomocných proměnných';

--------------------------------------------------------------------------------;
-- indexy
--------------------------------------------------------------------------------;

	-- fki__t_config_collection__c_config_function
	DROP INDEX IF EXISTS @extschema@.fki__t_config_collection__c_config_function;
	CREATE INDEX fki__t_config_collection__c_config_function
		ON @extschema@.t_config_collection
		USING btree(config_function);

	COMMENT ON INDEX @extschema@.fki__t_config_collection__c_config_function
	IS 'Index přes cizí klíč fkey__t_config_collection__c_config_function';

--------------------------------------------------------------------------------;
-- check-constraints
--------------------------------------------------------------------------------;

	-- catalog_name
	ALTER TABLE @extschema@.t_config_collection
	DROP CONSTRAINT IF EXISTS check__t_config_collection__not_null__catalog_name;

	ALTER TABLE @extschema@.t_config_collection ADD
	CONSTRAINT check__t_config_collection__not_null__catalog_name
	CHECK
		(((config_function = 100) AND (NOT aggregated) 	AND (catalog_name IS NOT NULL))	OR
		 ((config_function = 100) AND (aggregated) 	AND (catalog_name IS NULL)) 	OR
		 ((config_function = 200) AND (NOT aggregated) 	AND (catalog_name IS NOT NULL)) OR
		 ((config_function = 200) AND (aggregated) 	AND (catalog_name IS NULL)) 	OR
		 ((config_function = 300) 			AND (catalog_name IS NULL)) 	OR
		 ((config_function = 400)			AND (catalog_name IS NULL)));

	COMMENT ON CONSTRAINT check__t_config_collection__not_null__catalog_name
		ON @extschema@.t_config_collection
	IS 'Vyplněnost a nevyplněnost catalog_name';

	-- schema_name
	ALTER TABLE @extschema@.t_config_collection
	DROP CONSTRAINT IF EXISTS check__t_config_collection__not_null__schema_name;

	ALTER TABLE @extschema@.t_config_collection ADD
	CONSTRAINT check__t_config_collection__not_null__schema_name
	CHECK
		(((config_function = 100) AND (NOT aggregated) 	AND (schema_name IS NOT NULL))	OR
		 ((config_function = 100) AND (aggregated) 	AND (schema_name IS NULL)) 	OR
		 ((config_function = 200) AND (NOT aggregated) 	AND (schema_name IS NOT NULL))  OR
		 ((config_function = 200) AND (aggregated) 	AND (schema_name IS NULL)) 	OR
		 ((config_function = 300) 			AND (schema_name IS NULL)) 	OR
		 ((config_function = 400)			AND (schema_name IS NULL)));

	COMMENT ON CONSTRAINT check__t_config_collection__not_null__schema_name
		ON @extschema@.t_config_collection
	IS 'Vyplněnost a nevyplněnost schema_name';

	-- table_name
	ALTER TABLE @extschema@.t_config_collection
	DROP CONSTRAINT IF EXISTS check__t_config_collection__not_null__table_name;

	ALTER TABLE @extschema@.t_config_collection ADD
	CONSTRAINT check__t_config_collection__not_null__table_name
	CHECK
		(((config_function = 100) AND (NOT aggregated) 	AND (table_name IS NOT NULL))	OR
		 ((config_function = 100) AND (aggregated) 	AND (table_name IS NULL)) 	OR
		 ((config_function = 200) AND (NOT aggregated) 	AND (table_name IS NOT NULL)) 	OR
		 ((config_function = 200) AND (aggregated) 	AND (table_name IS NULL)) 	OR
		 ((config_function = 300) 			AND (table_name IS NULL)) 	OR
		 ((config_function = 400)			AND (table_name IS NULL)));

	COMMENT ON CONSTRAINT check__t_config_collection__not_null__table_name
		ON @extschema@.t_config_collection
	IS 'Vyplněnost a nevyplněnost table_name';

	-- column_name
	ALTER TABLE @extschema@.t_config_collection
	DROP CONSTRAINT IF EXISTS check__t_config_collection__not_null__column_name;

	ALTER TABLE @extschema@.t_config_collection ADD
	CONSTRAINT check__t_config_collection__not_null__column_name
	CHECK
		(((config_function = 100) AND (NOT aggregated) 	AND (column_name IS NOT NULL))	OR
		 ((config_function = 100) AND (aggregated) 	AND (column_name IS NULL)) 	OR
		 ((config_function = 200) AND (NOT aggregated) 	AND (column_name IS NOT NULL)) 	OR
		 ((config_function = 200) AND (aggregated) 	AND (column_name IS NULL)) 	OR
		 ((config_function = 300) 			AND (column_name IS NULL)) 	OR
		 ((config_function = 400)			AND (column_name IS NULL)));

	COMMENT ON CONSTRAINT check__t_config_collection__not_null__column_name
		ON @extschema@.t_config_collection
	IS 'Vyplněnost a nevyplněnost column_name';

	-- unit
	ALTER TABLE @extschema@.t_config_collection
	DROP CONSTRAINT IF EXISTS check__t_config_collection__not_null__unit;

	ALTER TABLE @extschema@.t_config_collection ADD
	CONSTRAINT check__t_config_collection__not_null__unit
	CHECK
		(((config_function = 100) AND (NOT aggregated) 	AND (unit IS NOT NULL))	OR
		 ((config_function = 100) AND (aggregated) 	AND (unit IS NULL)) 	OR
		 ((config_function = 200) AND (NOT aggregated) 	AND (unit IS NOT NULL)) OR
		 ((config_function = 200) AND (aggregated) 	AND (unit IS NULL)) 	OR
		 ((config_function = 300) 			AND (unit IS NULL)) 	OR
		 ((config_function = 400)			AND (unit IS NULL)));

	COMMENT ON CONSTRAINT check__t_config_collection__not_null__unit
		ON @extschema@.t_config_collection
	IS 'Vyplněnost a nevyplněnost unit';

--------------------------------------------------------------------------------;
-- vlastnik a opravneni
--------------------------------------------------------------------------------;

	-- tabulka

	ALTER TABLE @extschema@.t_config_collection							OWNER TO adm_nfiesta_gisdata;
	GRANT ALL ON TABLE @extschema@.t_config_collection						TO adm_nfiesta_gisdata;
	GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE @extschema@.t_config_collection		TO app_nfiesta_gisdata;
	GRANT SELECT ON TABLE @extschema@.t_config_collection						TO public;

---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
--	t_config
---------------------------------------------------------------------------------------------------;

--	SELECT * FROM @extschema@.t_config ORDER BY id;

	DROP TABLE IF EXISTS @extschema@.t_config CASCADE;

	CREATE TABLE @extschema@.t_config
	(
		id 					integer,
		auxiliary_variable_category		integer,
		config_collection			integer,
		config_query				integer,
		label		 			character varying,
		complete				character varying,
		categories				character varying,
		vector					integer,
		raster					integer,
		raster_1				integer,
		band					integer,
		reclass					integer,
		condition				character varying,
		tag					text,
		description				text,
		edit_date				timestamp without time zone
	);

--------------------------------------------------------------------------------;
-- dokumentace
--------------------------------------------------------------------------------;

	-- tabulka
	COMMENT ON TABLE @extschema@.t_config
		IS 'Konfigurace výpočtu úhrnů pomocných proměnných';

	-- atributy
	COMMENT ON COLUMN @extschema@.t_config.id
		IS 'Identifikační číslo záznamu konfigurace';
	COMMENT ON COLUMN @extschema@.t_config.auxiliary_variable_category
		IS 'Kategorie pomocné proměnné';
	COMMENT ON COLUMN @extschema@.t_config.config_collection
		IS 'Identifikační číslo skupiny konfigurací výpočtu úhrnů pomocných proměnných';
	COMMENT ON COLUMN @extschema@.t_config.config_query
		IS 'Způsob výpočtu úhrnu pomocných proměnných';
	COMMENT ON COLUMN @extschema@.t_config.label
		IS 'Popis konfigurace';
	COMMENT ON COLUMN @extschema@.t_config.complete
		IS 'Seznam kategorií pomocných proměnných tvořících celé území pro dopočet';
	COMMENT ON COLUMN @extschema@.t_config.categories
		IS 'Seznam kategorií pomocných proměnných';
	COMMENT ON COLUMN @extschema@.t_config.vector
		IS 'Konfigurace vektorové vrstvy pro výpočet interakcí';
	COMMENT ON COLUMN @extschema@.t_config.raster
		IS 'Konfigurace první rasterové vrstvy pro výpočet interakcí';
	COMMENT ON COLUMN @extschema@.t_config.raster_1
		IS 'Konfigurace druhé rasterové vrstvy pro výpočet interakcí';
	COMMENT ON COLUMN @extschema@.t_config.band
		IS 'Číslo pásma rasteru';
	COMMENT ON COLUMN @extschema@.t_config.reclass
		IS 'Hodnota pro reklasifikaci rasteru';
	COMMENT ON COLUMN @extschema@.t_config.condition
		IS 'Výběrová podmínka z GIS vrstvy';
	COMMENT ON COLUMN @extschema@.t_config.tag
		IS 'Doplňující informace';
	COMMENT ON COLUMN @extschema@.t_config.description
		IS 'Podrobný popis kategorie ze skupiny konfigurací';
	COMMENT ON COLUMN @extschema@.t_config.edit_date
		IS 'Datum editace.';

--------------------------------------------------------------------------------;
-- not null constraints
--------------------------------------------------------------------------------;

	-- id
	ALTER TABLE @extschema@.t_config
		ALTER COLUMN id						SET NOT NULL;

	-- auxiliary_variable_category

	-- config_function
	ALTER TABLE @extschema@.t_config
		ALTER COLUMN config_collection				SET NOT NULL;

	-- config_query
	ALTER TABLE @extschema@.t_config
		ALTER COLUMN config_query				SET NOT NULL;

	-- label
	ALTER TABLE @extschema@.t_config
		ALTER COLUMN label					SET NOT NULL;

	-- complete

	-- categories

	-- vector

	-- raster

	-- raster_1

	-- band

	-- reclass

	-- condition

	-- tag

	-- description
	ALTER TABLE @extschema@.t_config
		ALTER COLUMN description				SET NOT NULL;
		
	ALTER TABLE @extschema@.t_config
		ALTER COLUMN edit_date					SET NOT NULL;

--------------------------------------------------------------------------------;
-- primarni klic
--------------------------------------------------------------------------------;

	-- pkey__t_config
	ALTER TABLE @extschema@.t_config
	DROP CONSTRAINT IF EXISTS pkey__t_config;

	ALTER TABLE @extschema@.t_config ADD
	CONSTRAINT pkey__t_config
		PRIMARY KEY (id)
		NOT DEFERRABLE
		INITIALLY IMMEDIATE;

	COMMENT ON CONSTRAINT pkey__t_config
		ON @extschema@.t_config
	IS 'Jednoznačný identifikátor, primární klíč tabulky';

--------------------------------------------------------------------------------;
-- cizi klice
--------------------------------------------------------------------------------;

	-- fkey__t_config__t_config_collection
	ALTER TABLE @extschema@.t_config
	DROP CONSTRAINT IF EXISTS fkey__t_config__t_config_collection;

	ALTER TABLE @extschema@.t_config ADD
	CONSTRAINT fkey__t_config__t_config_collection
		FOREIGN KEY (config_collection)
		REFERENCES @extschema@.t_config_collection(id)
		MATCH SIMPLE
		ON UPDATE CASCADE
		ON DELETE CASCADE
		NOT DEFERRABLE
		INITIALLY IMMEDIATE;

	COMMENT ON CONSTRAINT fkey__t_config__t_config_collection ON @extschema@.t_config
	IS 'Cizí klíč, tabulka skupin konfigurací výpočtu úhrnů pomocných proměnných';

	-- fkey__t_config__c_config_query
	ALTER TABLE @extschema@.t_config
	DROP CONSTRAINT IF EXISTS fkey__t_config__c_config_query;

	ALTER TABLE @extschema@.t_config ADD
	CONSTRAINT fkey__t_config__c_config_query
		FOREIGN KEY (config_query)
		REFERENCES @extschema@.c_config_query(id)
		MATCH SIMPLE
		ON UPDATE CASCADE
		ON DELETE NO ACTION
		NOT DEFERRABLE
		INITIALLY IMMEDIATE;

	COMMENT ON CONSTRAINT fkey__t_config__c_config_query ON @extschema@.t_config
	IS 'Cizí klíč, číselník variant výpočtu úhrnů pomocných proměnných';

	-- fkey__t_config__vector
	ALTER TABLE @extschema@.t_config
	DROP CONSTRAINT IF EXISTS fkey__t_config__vector;

	ALTER TABLE @extschema@.t_config ADD
	CONSTRAINT fkey__t_config__vector
		FOREIGN KEY (vector)
		REFERENCES @extschema@.t_config(id)
		MATCH SIMPLE
		ON UPDATE NO ACTION
		ON DELETE NO ACTION
		NOT DEFERRABLE
		INITIALLY IMMEDIATE;

	COMMENT ON CONSTRAINT fkey__t_config__vector ON @extschema@.t_config
	IS 'Cizí klíč, identifikační číslo konfigurace pro vektorovou vrstvu';

	-- fkey__t_config__raster
	ALTER TABLE @extschema@.t_config
	DROP CONSTRAINT IF EXISTS fkey__t_config__raster;

	ALTER TABLE @extschema@.t_config ADD
	CONSTRAINT fkey__t_config__raster
		FOREIGN KEY (raster)
		REFERENCES @extschema@.t_config(id)
		MATCH SIMPLE
		ON UPDATE NO ACTION
		ON DELETE NO ACTION
		NOT DEFERRABLE
		INITIALLY IMMEDIATE;

	COMMENT ON CONSTRAINT fkey__t_config__raster ON @extschema@.t_config
	IS 'Cizí klíč, identifikační číslo konfigurace pro prvni rasterovou vrstvu';

	-- fkey__t_config__raster_1
	ALTER TABLE @extschema@.t_config
	DROP CONSTRAINT IF EXISTS fkey__t_config__raster_1;

	ALTER TABLE @extschema@.t_config ADD
	CONSTRAINT fkey__t_config__raster_1
		FOREIGN KEY (raster_1)
		REFERENCES @extschema@.t_config(id)
		MATCH SIMPLE
		ON UPDATE NO ACTION
		ON DELETE NO ACTION
		NOT DEFERRABLE
		INITIALLY IMMEDIATE;

	COMMENT ON CONSTRAINT fkey__t_config__raster_1 ON @extschema@.t_config
	IS 'Cizí klíč, identifikační číslo konfigurace pro druhou rasterovou vrstvu';

--------------------------------------------------------------------------------;
-- indexy
--------------------------------------------------------------------------------;

	-- fki__t_config__t_config_collection
	DROP INDEX IF EXISTS @extschema@.fki__t_config__t_config_collection;
	CREATE INDEX fki__t_config__t_config_collection
		ON @extschema@.t_config
		USING btree(config_collection);

	COMMENT ON INDEX @extschema@.fki__t_config__t_config_collection
	IS 'Index přes cizí klíč fkey__t_config__t_config_collection';

	-- fki__t_config__c_config_query
	DROP INDEX IF EXISTS @extschema@.fki__t_config__c_config_query;
	CREATE INDEX fki__t_config__c_config_query
		ON @extschema@.t_config
		USING btree(config_query);

	COMMENT ON INDEX @extschema@.fki__t_config__c_config_query
	IS 'Index přes cizí klíč fkey__t_config__c_config_query';

	-- fki__t_config__vector
	DROP INDEX IF EXISTS @extschema@.fki__t_config__vector;
	CREATE INDEX fki__t_config__vector
		ON @extschema@.t_config
		USING btree(vector);

	COMMENT ON INDEX @extschema@.fki__t_config__vector
	IS 'Index přes cizí klíč fkey__t_config__vector';

	-- fki__t_config__raster
	DROP INDEX IF EXISTS @extschema@.fki__t_config__raster;
	CREATE INDEX fki__t_config__raster
		ON @extschema@.t_config
		USING btree(raster);

	COMMENT ON INDEX @extschema@.fki__t_config__raster
	IS 'Index přes cizí klíč fkey__t_config__raster';

	-- fki__t_config__raster_1
	DROP INDEX IF EXISTS @extschema@.fki__t_config__raster_1;
	CREATE INDEX fki__t_config__raster_1
		ON @extschema@.t_config
		USING btree(raster_1);

	COMMENT ON INDEX @extschema@.fki__t_config__raster_1
	IS 'Index přes cizí klíč fkey__t_config__raster_1';

--------------------------------------------------------------------------------;
-- check-constraints
--------------------------------------------------------------------------------;

	-- complete
	ALTER TABLE @extschema@.t_config
	DROP CONSTRAINT IF EXISTS check__t_config__not_null__complete;

	ALTER TABLE @extschema@.t_config ADD
	CONSTRAINT check__t_config__not_null__complete
	CHECK
		(((config_query = 100) AND (complete IS NULL))		OR
		 ((config_query = 200) AND (complete IS NULL))		OR
		 ((config_query = 300) AND (complete IS NULL))		OR
		 ((config_query = 400) AND (complete IS NOT NULL)) 	OR
		 ((config_query = 500) AND (complete IS NULL)) );

	COMMENT ON CONSTRAINT check__t_config__not_null__complete
		ON @extschema@.t_config
	IS 'Vyplněnost a nevyplněnost complete';

	-- categories
	ALTER TABLE @extschema@.t_config
	DROP CONSTRAINT IF EXISTS check__t_config__not_null__categories;

	ALTER TABLE @extschema@.t_config ADD
	CONSTRAINT check__t_config__not_null__categories
	CHECK
		(((config_query = 100) AND (categories IS NULL))	OR
		 ((config_query = 200) AND (categories IS NOT NULL))	OR
		 ((config_query = 300) AND (categories IS NOT NULL))	OR
		 ((config_query = 400) AND (categories IS NOT NULL))	OR
		 ((config_query = 500) AND (categories IS NOT NULL)));

	COMMENT ON CONSTRAINT check__t_config__not_null__categories
		ON @extschema@.t_config
	IS 'Vyplněnost a nevyplněnost categories';

--------------------------------------------------------------------------------;
-- vlastnik a opravneni
--------------------------------------------------------------------------------;

	-- tabulka

	ALTER TABLE @extschema@.t_config							OWNER TO adm_nfiesta_gisdata;
	GRANT ALL ON TABLE @extschema@.t_config							TO adm_nfiesta_gisdata;
	GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE @extschema@.t_config		TO app_nfiesta_gisdata;
	GRANT SELECT ON TABLE @extschema@.t_config						TO public;

---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
--	t_aux_total
---------------------------------------------------------------------------------------------------;

--	SELECT * FROM @extschema@.t_aux_total LIMIT 10;

	DROP TABLE IF EXISTS @extschema@.t_aux_total;

	CREATE TABLE @extschema@.t_aux_total
	(
		id 				serial,
		config			        integer,
		estimation_cell 		integer,
		cell				integer,
		aux_total			double precision,
		est_date			timestamp without time zone	DEFAULT now(),
		ext_version			integer,
		gui_version			integer
	);

--------------------------------------------------------------------------------;
-- dokumentace
--------------------------------------------------------------------------------;

	-- tabulka
	COMMENT ON TABLE @extschema@.t_aux_total
		IS 'Tabulka úhrnů pomocných proměnných uvnitř výpočetních buněk.';

	-- atributy
	COMMENT ON COLUMN @extschema@.t_aux_total.id
		IS 'Primární klíč, identifikátor záznamu.';
	COMMENT ON COLUMN @extschema@.t_aux_total.config
		IS 'Cizí klíč na tabulku konfigurací výpočtu úhrnu pomocných proměnných (identifikace pomocné proměnné).';
	COMMENT ON COLUMN @extschema@.t_aux_total.estimation_cell
		IS 'Cizí klíč na číselník výpočetních buněk.';
	COMMENT ON COLUMN @extschema@.t_aux_total.cell
		IS 'Identifikační číslo části výpočetní buňky.';
	COMMENT ON COLUMN @extschema@.t_aux_total.aux_total
		IS 'Úhrn pomocné proměnné.';
	COMMENT ON COLUMN @extschema@.t_aux_total.est_date
		IS 'Datum a čas vypočtu úhrnu pomocné proměnné.';
	COMMENT ON COLUMN @extschema@.t_aux_total.ext_version
		IS 'Identifikator verze extenze nfiesta_gisdata.';
	COMMENT ON COLUMN @extschema@.t_aux_total.gui_version
		IS 'Identifikátor verze GUI aplikace pro výpočet úhrnů pomocných proměnných';		

--------------------------------------------------------------------------------;
-- not null
--------------------------------------------------------------------------------;

	-- id
	ALTER TABLE @extschema@.t_aux_total
		ALTER COLUMN id				SET NOT NULL;

	-- config
	ALTER TABLE @extschema@.t_aux_total
		ALTER COLUMN config			SET NOT NULL;

	-- estimation_cell				--> povoleno NULL

	-- cell						--> povoleno NULL

	-- aux_total
	ALTER TABLE @extschema@.t_aux_total
		ALTER COLUMN aux_total			SET NOT NULL;

	-- est_date
	ALTER TABLE @extschema@.t_aux_total
		ALTER COLUMN est_date			SET NOT NULL;

	-- ext_version
	ALTER TABLE @extschema@.t_aux_total
		ALTER COLUMN ext_version		SET NOT NULL;

	-- ext_version
	ALTER TABLE @extschema@.t_aux_total
		ALTER COLUMN gui_version		SET NOT NULL;
		
--------------------------------------------------------------------------------;
-- primarni klic
--------------------------------------------------------------------------------;

	-- pkey__t_aux_total
	ALTER TABLE @extschema@.t_aux_total
	DROP CONSTRAINT IF EXISTS pkey__t_aux_total;

	ALTER TABLE @extschema@.t_aux_total ADD
		CONSTRAINT pkey__t_aux_total
		PRIMARY KEY (id)
		NOT DEFERRABLE
		INITIALLY IMMEDIATE;

	COMMENT ON CONSTRAINT pkey__t_aux_total
		ON @extschema@.t_aux_total
	IS 'Primární klíč';

--------------------------------------------------------------------------------;
-- sekvence
--------------------------------------------------------------------------------;

	-- seq__t_aux_total_id
	ALTER SEQUENCE @extschema@.t_aux_total_id_seq
	RENAME TO seq__t_aux_total_id;

	ALTER SEQUENCE @extschema@.seq__t_aux_total_id
	MINVALUE 0;

	SELECT setval('@extschema@.seq__t_aux_total_id', 0, true);

	COMMENT ON SEQUENCE @extschema@.seq__t_aux_total_id
	IS 'Sekvence pro generování identifikačního čísla úhrnu pomocné proměnné ve výpočetní buňce.';

--------------------------------------------------------------------------------;
-- unique omezeni
--------------------------------------------------------------------------------;

	-- ukey__t_aux_total__config__estimation_cell
	ALTER TABLE @extschema@.t_aux_total
	DROP CONSTRAINT IF EXISTS ukey__t_aux_total;

	ALTER TABLE @extschema@.t_aux_total ADD
		CONSTRAINT ukey__t_aux_total
		UNIQUE (config, estimation_cell, cell, ext_version)
		NOT DEFERRABLE
		INITIALLY IMMEDIATE;

	COMMENT ON CONSTRAINT ukey__t_aux_total
		ON @extschema@.t_aux_total
	IS 'Jednoznačný identifikátor, pro každou konfiguraci, výpočetní buňku nebo její část a pro každou verzi extenze nfiesta_gisdata může existovat pouze jeden platný záznam úhrnu pomocné proměnné.';

--------------------------------------------------------------------------------;
-- cizi klice
--------------------------------------------------------------------------------;

	-- fkey__t_aux_total__t_config
	ALTER TABLE @extschema@.t_aux_total
	DROP CONSTRAINT IF EXISTS fkey__t_aux_total__t_config;

	ALTER TABLE @extschema@.t_aux_total ADD
		CONSTRAINT fkey__t_aux_total__t_config
		FOREIGN KEY (config)
		REFERENCES @extschema@.t_config(id)
		MATCH SIMPLE
		ON UPDATE NO ACTION
		ON DELETE NO ACTION
		NOT DEFERRABLE
		INITIALLY IMMEDIATE;

	COMMENT ON CONSTRAINT fkey__t_aux_total__t_config
		ON @extschema@.t_aux_total
	IS 'Cizí klíč na tabulku konfigurací výpočtu úhrnu pomocných proměnných.';

	-- fkey__t_aux_total__c_estimation_cell
	ALTER TABLE @extschema@.t_aux_total
	DROP CONSTRAINT IF EXISTS fkey__t_aux_total__c_estimation_cell;

	ALTER TABLE @extschema@.t_aux_total ADD
		CONSTRAINT fkey__t_aux_total__c_estimation_cell
		FOREIGN KEY (estimation_cell)
		REFERENCES @extschema@.c_estimation_cell(id)
		MATCH SIMPLE
		ON UPDATE NO ACTION
		ON DELETE NO ACTION
		NOT DEFERRABLE
		INITIALLY IMMEDIATE;

	COMMENT ON CONSTRAINT fkey__t_aux_total__c_estimation_cell
		ON @extschema@.t_aux_total
	IS  'Cizí klíč na číselník výpočetních buněk.';


	-- fkey__t_aux_total__c_ext_version
	ALTER TABLE @extschema@.t_aux_total
	DROP CONSTRAINT IF EXISTS fkey__t_aux_total__c_ext_version;
	
	ALTER TABLE @extschema@.t_aux_total ADD
		CONSTRAINT fkey__t_aux_total__c_ext_version
		FOREIGN KEY (ext_version)
		REFERENCES @extschema@.c_ext_version (id)
		MATCH SIMPLE
		ON UPDATE NO ACTION
		ON DELETE NO ACTION
		NOT DEFERRABLE
		INITIALLY IMMEDIATE;

	COMMENT ON CONSTRAINT fkey__t_aux_total__c_ext_version
		ON @extschema@.t_aux_total
	IS  'Foreign key to table c_ext_version.';


	-- fkey__t_aux_total__c_gui_version
	ALTER TABLE @extschema@.t_aux_total
	DROP CONSTRAINT IF EXISTS fkey__t_aux_total__c_gui_version;
	
	ALTER TABLE @extschema@.t_aux_total ADD
		CONSTRAINT fkey__t_aux_total__c_gui_version
		FOREIGN KEY (gui_version)
		REFERENCES @extschema@.c_gui_version (id)
		MATCH SIMPLE
		ON UPDATE NO ACTION
		ON DELETE NO ACTION
		NOT DEFERRABLE
		INITIALLY IMMEDIATE;

	COMMENT ON CONSTRAINT fkey__t_aux_total__c_gui_version
		ON @extschema@.t_aux_total
	IS  'Foreign key to table c_gui_version.';


	-- fkey__t_aux_total__f_a_cell
	ALTER TABLE @extschema@.t_aux_total
	DROP CONSTRAINT IF EXISTS fkey__t_aux_total__f_a_cell;

	ALTER TABLE @extschema@.t_aux_total ADD
		CONSTRAINT fkey__t_aux_total__f_a_cell
		FOREIGN KEY (cell)
		REFERENCES @extschema@.f_a_cell (gid)
		MATCH SIMPLE
		ON UPDATE NO ACTION
		ON DELETE NO ACTION
		NOT DEFERRABLE
		INITIALLY IMMEDIATE;

	COMMENT ON CONSTRAINT fkey__t_aux_total__f_a_cell
		ON @extschema@.t_aux_total
	IS 'Foreign key to table f_a_cell.';

--------------------------------------------------------------------------------;
-- indexy
--------------------------------------------------------------------------------;

	-- fki__t_aux_total__c_estimation_cell
	DROP INDEX IF EXISTS @extschema@.fki__t_aux_total__c_estimation_cell;
	CREATE INDEX fki__t_aux_total__c_estimation_cell
		ON @extschema@.t_aux_total
		USING btree (estimation_cell);

	COMMENT ON INDEX @extschema@.fki__t_aux_total__c_estimation_cell
	IS 'Index přes cizí klíč fkey__t_aux_total__c_estimation_cell';


	-- fki__t_aux_total__c_ext_version
	DROP INDEX IF EXISTS @extschema@.fki__t_aux_total__c_ext_version;
	CREATE INDEX fki__t_aux_total__c_ext_version
		ON @extschema@.t_aux_total
		USING btree (ext_version);

	COMMENT ON INDEX @extschema@.fki__t_aux_total__c_ext_version
	IS 'BTree index on foreign key fkey__t_aux_total__c_ext_version.';


	-- fki__t_aux_total__c_gui_version
	DROP INDEX IF EXISTS @extschema@.fki__t_aux_total__c_gui_version;
	CREATE INDEX fki__t_aux_total__c_gui_version
		ON @extschema@.t_aux_total
		USING btree (gui_version);

	COMMENT ON INDEX @extschema@.fki__t_aux_total__c_gui_version
	IS 'BTree index on foreign key fkey__t_aux_total__c_gui_version.';


	-- fki__t_aux_total__f_a_cell
	DROP INDEX IF EXISTS @extschema@.fki__t_aux_total__f_a_cell;
	CREATE INDEX fki__t_aux_total__f_a_cell
		ON @extschema@.t_aux_total
		USING btree (cell);
	  
	COMMENT ON INDEX @extschema@.fki__t_aux_total__f_a_cell
	IS 'Btree index over foreign key fkey__t_aux_total__f_a_cell.';


	-- fki__t_aux_total__t_config
	DROP INDEX IF EXISTS @extschema@.fki__t_aux_total__t_config;
	CREATE INDEX fki__t_aux_total__t_config
		ON @extschema@.t_aux_total
		USING btree (config);

	COMMENT ON INDEX @extschema@.fki__t_aux_total__t_config
	IS 'Index přes cizí klíč fkey__t_aux_total__t_config';

--------------------------------------------------------------------------------;
-- check constraints
--------------------------------------------------------------------------------;

	-- estimation_cell and cell
	ALTER TABLE @extschema@.t_aux_total
	DROP CONSTRAINT IF EXISTS check__t_aux_total__estimation_cell__cell;

	ALTER TABLE @extschema@.t_aux_total ADD
	CONSTRAINT check__t_aux_total__estimation_cell__cell
	CHECK
		(((estimation_cell IS NOT NULL) AND (cell IS NOT NULL))	OR
		((estimation_cell  IS NOT NULL) AND (cell IS     NULL))	OR
		((estimation_cell  IS     NULL) AND (cell IS NOT NULL)));

	COMMENT ON CONSTRAINT check__t_aux_total__estimation_cell__cell
		ON @extschema@.t_aux_total
	IS 'Vyplněnost a nevyplněnost estimation_cell a cell.';

--------------------------------------------------------------------------------;
-- vlastnik a opravneni
--------------------------------------------------------------------------------;

	-- tabulka

	ALTER TABLE @extschema@.t_aux_total							OWNER TO adm_nfiesta_gisdata;
	GRANT ALL ON TABLE @extschema@.t_aux_total						TO adm_nfiesta_gisdata;
	GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE @extschema@.t_aux_total		TO app_nfiesta_gisdata;
	GRANT SELECT ON TABLE @extschema@.t_aux_total						TO public;

	-- sekvence

	ALTER SEQUENCE @extschema@.seq__t_aux_total_id				OWNER TO adm_nfiesta_gisdata;
	GRANT ALL ON SEQUENCE @extschema@.seq__t_aux_total_id			TO adm_nfiesta_gisdata;
	GRANT USAGE, SELECT ON SEQUENCE @extschema@.seq__t_aux_total_id		TO app_nfiesta_gisdata;

--------------------------------------------------------------------------------;
-- trigger trg__t_aux_total__before_insert
--------------------------------------------------------------------------------;

CREATE TRIGGER trg__t_aux_total__before_insert
  BEFORE INSERT
  ON @extschema@.t_aux_total
  FOR EACH ROW
  EXECUTE PROCEDURE @extschema@.fn_aux_total_before_insert_app();
  
COMMENT ON TRIGGER trg__t_aux_total__before_insert ON @extschema@.t_aux_total
IS 'Trigger před vložením nového záznamu spouští funkci fn_aux_total_before_insert(), která provádí kontrolu nově vkládane verze extenze s aktulani systemovou verzi extenze nfiesta_gisdata.';

--------------------------------------------------------------------------------;

---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- FUNCTIONS --
---------------------------------------------------------------------------------------------------;

-- <function name="fn_check_versions_app" schema="extschema" src="functions/extschema/fn_check_versions_app.sql">
---------------------------------------------------------------------------------------------------
-- fn_check_versions_app
---------------------------------------------------------------------------------------------------
-- DROP FUNCTION @extschema@.fn_check_versions_app(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_check_versions_app
(
	_gui_version_id		integer
)
RETURNS boolean AS
$BODY$
DECLARE
	_gui_version_label		character varying;
	_ext_version_label_system	text;
	_ext_version_id_system		integer;
	_ext_version_id_c_max		integer;
	_ext_version_label_c_max	character varying;
	_ext_vesion_id_max_mapping	integer;
BEGIN
	---------------------------------------------------
	IF _gui_version_id IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_check_versions_app: Vstupni argument _gui_version_id nesmi byt NULL!';
	END IF;
	---------------------------------------------------
	-- kontrola existence GUI verze v ciselniku
	-- soucasne i ziskani labelu
	SELECT label FROM @extschema@.c_gui_version
	WHERE id = _gui_version_id
	INTO _gui_version_label;

	IF _gui_version_label IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_check_versions_app: Pro vstupni argument _gui_version_id = % nenalezen zaznam v ciselniku c_gui_version!',_gui_version_id;
	END IF;
	---------------------------------------------------
	-- kontrola existence systemove verze EXTENZE v ciselniku
	SELECT extversion FROM pg_extension
	WHERE extname = 'nfiesta_gisdata'
	INTO _ext_version_label_system;

	-- docasne naplneni promennych
	--_ext_version_label_system := '1.0.0'::text;
	
	IF _ext_version_label_system IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_check_versions_app: V systemove tabulce pg_extension nenalezena zadna verze extenze nfiesta_gisdata!';
	END IF;
	
	SELECT id FROM @extschema@.c_ext_version
	WHERE label = _ext_version_label_system
	INTO _ext_version_id_system;
	
	IF _ext_version_id_system IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_check_versions_app: V ciselniku c_ext_version nenalezen zaznam odpovidajici systemove verzi % extenze nfiesta_gisdata!',_ext_version_label_system;
	END IF;
	---------------------------------------------------
	-- kontrola ze systemova verze extenze je tou
	-- nejaktualnejsi co je v ciselniku
	SELECT max(id) FROM @extschema@.c_ext_version
	INTO _ext_version_id_c_max;

	SELECT label FROM @extschema@.c_ext_version
	WHERE id = _ext_version_id_c_max
	INTO _ext_version_label_c_max;

	IF _ext_version_id_system != _ext_version_id_c_max
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_check_versions_app: Systemova (databazova) verze % extenze nfiesta_gisdata neodpovida nejaktualnejsi verzi % v ciselniku c_ext_version!',_ext_version_label_system, _ext_version_label_c_max;
	END IF;
	---------------------------------------------------
	-- kontrola v mapovaci tabulce, ze pro vstupni GUI verzi
	-- odpovida systemova extenze, ktera by mela byt tou nejakutalnejsi
	SELECT max(ext_version) FROM @extschema@.cm_ext_gui_version
	WHERE gui_version = _gui_version_id
	INTO _ext_vesion_id_max_mapping;

	IF _ext_vesion_id_max_mapping != _ext_version_id_c_max
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_check_versions_app: Verze (%) GUI aplikace neni kompatibilni s verzi (%) extenze nfiesta_gisdata',_gui_version_label,_ext_version_label_c_max;
	END IF;
	---------------------------------------------------
	RETURN TRUE;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;

ALTER FUNCTION @extschema@.fn_check_versions_app(integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_check_versions_app(integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_check_versions_app(integer) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_check_versions_app(integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_check_versions_app(integer) IS
'Funkce, ktera provadi kontrolu kompatibility verze GUI aplikace s verzi extenze nfiesta_gisdata. Funkce vraci TRUE pokud je vse kompatibilni, jinak vyjimky funkce.';
---------------------------------------------------------------------------------------------------
-- </function>


-- <function name="fn_get_aux_total_app" schema="extschema" src="functions/extschema/fn_get_aux_total_app.sql">
-- Function: @extschema@.fn_get_aux_total_app(integer, integer, integer)

-- DROP FUNCTION @extschema@.fn_get_aux_total_app(integer, integer, integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_get_aux_total_app(
    IN _config_id integer,
    IN _estimation_cell integer,
    IN _gid integer)
  RETURNS TABLE(estimation_cell integer, config integer, aux_total double precision, cell integer, ext_version integer) AS
$BODY$
DECLARE
	_config_collection			integer;
	_config_query				integer;
	_config_function			integer;
	_estimation_cell_exit			integer;
	_function_aux_total			text;
	_categories				character varying;
	_categories_sum				double precision;
	_res_aux_total				double precision;
	_estimation_cell_area			double precision;	
	_complete				character varying;
	_complete_sum				double precision;
	_aux_total				double precision;
	_estimation_cell_collection		integer;
	_estimation_cell_collection_lowest	integer;
	_estimation_cell_result			integer;
	_check_pocet				integer;
	_gid_result				integer;	
	_q					text;

	_ext_version_label_system		text;
	_ext_version_current			integer;
BEGIN
	--------------------------------------------------------------------------------------------
	IF _config_id IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_aux_total_app: Vstupni argument _config_id nesmi byt NULL!';
	END IF;	
	
	IF _estimation_cell IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_aux_total_app: Vstupni argument _estimation_cell nesmi byt NULL!';
	END IF;
	--------------------------------------------------------------------------------------------
	-- proces ziskani aktualni extenze nfiesta_gisdata
	SELECT ext_version FROM pg_extension WHERE extname = 'nfiesta_gisdata'
	INTO _ext_version_label_system;

	--_ext_version_label_system := '1.0.0';

	IF _ext_version_label_system IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_get_aux_total_app: V systemove tabulce pg_extension nenalezena zadna verze pro extenzi nfiesta_gisdata!';
	END IF;

	SELECT id FROM @extschema@.c_ext_version
	WHERE label = _ext_version_label_system
	INTO _ext_version_current;
	
	IF _ext_version_current IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_get_aux_total_app: V ciselniku c_ext_version nenalezen zaznam odpovidajici systemove verzi % extenze nfiesta_gisdata!',_ext_version_label_system;
	END IF;	
	--------------------------------------------------------------------------------------------
	-- proces ziskani potrebnych hodnot z konfigurace
	SELECT
		t.config_collection,
		t.config_query
	FROM
		@extschema@.t_config AS t
	WHERE
		id = _config_id
	INTO
		_config_collection,
		_config_query;
	--------------------------------------------------------------------------------------------
	IF _config_collection IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_get_aux_total_app: Argument _config_collection nesmi byt NULL!';
	END IF;	

	IF _config_query IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_get_aux_total_app: Argument _config_query nesmi byt NULL!';
	END IF;
	--------------------------------------------------------------------------------------------
	IF _config_query = 500
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_get_aux_total_app: Vypocet nelze provest pro config_query = %!',_config_query;
	END IF;
	--------------------------------------------------------------------------------------------
	IF _gid IS NOT NULL AND _config_query = ANY(array[200,300,400])
	THEN
		RAISE EXCEPTION 'Chyba 08: fn_get_aux_total_app: Vstupni argument _gid musi byt NULL!';
	END IF;
	--------------------------------------------------------------------------------------------
	-- proces ziskani potrebnych hodnot z kolekce
	SELECT tcc.config_function FROM @extschema@.t_config_collection AS tcc
	WHERE tcc.id = _config_collection
	INTO _config_function;
	--------------------------------------------------------------------------------------------
	IF _config_function IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 09: fn_get_aux_total_app: Argument _config_function nesmi byt NULL!';
	END IF;	
	--------------------------------------------------------------------------------------------
	-- kontrola zdali pro zadanou konfiguraci a zadanou _estimation_cell
	-- uz je ci neni hodnota aux_total vypocitana a jestli neni nahodou duplicitni
	-- => toto by mel hlidat bud trigger nebo check-constraint v tabulce t_aux_total
	-- => nebo je implementovano jiz ve funkci fn_get_gids4aux_total
	--------------------------------------------------------------------------------------------	
	--------------------------------------------------------------------------------------------
	-- rozhodovaci proces co funkce fn_get_aux_total_app bude delat:
	CASE
		WHEN (_config_query = 100) -- zakladni with [vypocet z GIS vrstvy]
		THEN
			-- _gid zde nesmi byt hodnota 0 => to je pro identifikaci pro sumarizaci
			-- _calculated zde muze byt jen FALSE => JIRKOVA aplikace tuto funkci bude poustet jen na gidy, 
			-- ktere nejsou doposud vypocitany, nebo pokud se jedna o prepocet, a provede insert do t_aux_total
			-- pokud se    jedna o gid zakladu, pak do vystupu musi jit estimation_cell zakladu
			-- pokud se ne-jedna o gid zakladu, pak do vystupu pujde estimation_cell co je zde na vstupu

			IF _gid > 0
			THEN
				-- zjisteni estimation_cell do vystupu teto funkce
				SELECT fac.estimation_cell FROM @extschema@.f_a_cell AS fac WHERE fac.gid = _gid
				INTO _estimation_cell_exit;

				IF _estimation_cell_exit IS NULL
				THEN
					RAISE EXCEPTION 'Chyba 10: fn_get_aux_total_app: Pro gid = % nenalezeno estimation_cell v tabulce f_a_cell!',_gid;
				END IF;
				
				-- povolen vypocet aux_total hodnoty pro vstupni gid [zakladni protinani]
				CASE
					WHEN (_config_function = 100)	-- vector
					THEN
						_function_aux_total := 'fn_get_aux_total_vector_app';

					WHEN (_config_function = 200)	-- raster
					THEN
						_function_aux_total := 'fn_get_aux_total_raster_app';

					WHEN (_config_function = 300)	-- vector_comb
					THEN
						_function_aux_total := 'fn_get_aux_total_vector_comb_app';

					WHEN (_config_function = 400)	-- raster_comb
					THEN
						_function_aux_total := 'fn_get_aux_total_raster_comb_app';
					ELSE
						RAISE EXCEPTION 'Chyba 11: fn_get_aux_total_app: Neznama hodnota parametru _config_function [%].',_config_function;
				END CASE;
			ELSE
				RAISE EXCEPTION 'Chyba 12: fn_get_aux_total_app: Nepovolena kombinace vstupnich argumentu [_config_id = %, _estimation_cell = %, _gid = %] pro vypocet aux_total hodnoty pro config_query 100!',_config_id,_estimation_cell,_gid;
			END IF;

		WHEN (_config_query = ANY(array[200,300,400]))
		THEN
			-- 200 [soucet existujicich kategorii]
			-- 300 [doplnek do rozlohy vypocetni bunky]
			-- 400 [doplnek do rozlohy existujici kategorie]
			
			_function_aux_total := NULL::text;

			IF _gid IS NULL
			THEN
				-- povolen vypocet aux_total hodnoty pro soucet/doplnky

				_estimation_cell_exit := _estimation_cell;

				--raise notice '_estimation_cell_exit:%',_estimation_cell_exit;

				-- 1. zde se nejpreve z konfigurace zjisti _categories
				SELECT tc.categories FROM @extschema@.t_config AS tc WHERE tc.id = _config_id
				INTO _categories;

				--raise notice '_categories:%',_categories;

				-- 2. potom se pro _categories provede kontrola zda v tabulce t_aux_total vubec _categories existuji
				-- => toto uz je implementovano ve funkci fn_get_gids4aux_total

				-- 3. zde se provede suma za _categories pro danou _estimation_cell
				EXECUTE '
				SELECT sum(t.aux_total) FROM @extschema@.t_aux_total AS t
				WHERE t.estimation_cell = $1
				AND t.config IN (SELECT unnest(array['||_categories||']))
				AND t.ext_version = $2
				'
				USING _estimation_cell, _ext_version_current
				INTO _categories_sum;

				--raise notice '_categories_sum: %',_categories_sum;

				-- 4. proces ziskani _res_aux_total pro jednotlive varianty _config_query 200,300,400
				CASE
				WHEN _config_query = 200	-- soucet
				THEN
					_res_aux_total := _categories_sum;
					
				WHEN _config_query = 300	-- doplnek do rozlohy vypocetni bunky [doplnek pro danou estimation_cell]
				THEN
					-- zjisteni plochy pro danou estimation_cell z tabulky f_a_cell
					SELECT sum(ST_Area(fac.geom))/10000.0 FROM @extschema@.f_a_cell AS fac
					WHERE fac.estimation_cell = _estimation_cell
					INTO _estimation_cell_area;

					IF _estimation_cell_area IS NULL
					THEN
						RAISE EXCEPTION 'Chyba 13: fn_get_aux_total_app: Pro zadanou _estimation_cell = % nezjistena plocha z tabulky f_a_cell!',_estimation_cell;
					END IF;

					_res_aux_total := _estimation_cell_area - _categories_sum;

				WHEN _config_query = 400
				THEN
					-- 1. zde se nejpreve z konfigurace zjisti complete
					SELECT tc.complete FROM @extschema@.t_config AS tc WHERE tc.id = _config_id
					INTO _complete;

					-- 2. potom se pro _complete provede kontrola zda v tabulce t_aux_total vubec existuje
					-- => toto uz je implementovano ve funkci fn_get_gids4aux_total

					-- 3. zjisteni sumy za _complete;
					EXECUTE '
					SELECT sum(t.aux_total) FROM @extschema@.t_aux_total AS t
					WHERE t.estimation_cell = $1
					AND t.config IN (SELECT unnest(array['||_complete||']))
					AND t.ext_version = $2
					'
					USING _estimation_cell, _ext_version_current
					INTO _complete_sum;

					_res_aux_total := _complete_sum - _categories_sum;	
				ELSE
					RAISE EXCEPTION 'Chyba 14: fn_get_aux_total_app: Pro _config_query = % doposud v tele funkce neprovedena implemetace procesu ziskani hodnoty _res_aux_total!',_config_query;
				END CASE;

			ELSE
				RAISE EXCEPTION 'Chyba 15: fn_get_aux_total_app: Nepovolena kombinace vstupnich argumentu [_config_id = %, _estimation_cell = %, _gid = %, _calculated = %] pro vypocet aux_total hodnoty pro config_query 200,300 nebo 400!',_config_id,_estimation_cell,_gid,_calculated;
			END IF;
		ELSE
			RAISE EXCEPTION 'Chyba 16: fn_get_aux_total_app: Neznama hodnota parametru _config_query [%].',_config_query;
	END CASE;
	--------------------------------------------------------------------------------------------
	IF _function_aux_total IS NOT NULL
	THEN
		EXECUTE 'SELECT @extschema@'||_function_aux_total||'($1,$2)'
		USING _config_id, _gid
		INTO _aux_total;
	ELSE
		_aux_total := _res_aux_total;
	END IF;
	--------------------------------------------------------------------------------------------
	--------------------------------------------------------------------------------------------
	-- zjisteni do jake collection patri zadana vstupni estimation_cell
	SELECT cec.estimation_cell_collection FROM @extschema@.c_estimation_cell  AS cec WHERE cec.id = _estimation_cell
	INTO _estimation_cell_collection;
	--------------------------------------------------------------------------------------------
	-- zjisteni estimation_cell_collection_lowest pro zadanou _estimation_cell_collection
	SELECT cecc.estimation_cell_collection_lowest FROM @extschema@.cm_estimation_cell_collection AS cecc
	WHERE cecc.estimation_cell_collection = _estimation_cell_collection
	INTO _estimation_cell_collection_lowest;
	--------------------------------------------------------------------------------------------
	IF _config_query = 100
	THEN
		IF _estimation_cell_collection != _estimation_cell_collection_lowest	-- VSTUPNI estimation_cell NENI zaklad [pozn. gidy pro protinani jsou vzdy zaklad]
		THEN
			-- ale pokud funkce fn_get_gids4aux_total_app vratila gid pro danou vstupni estimation_cell => tzn. ze zakladni gid jeste neni v t_aux_total
			-- a musel se jiz drive v kodu pro gid dohledat estimation_cell
			-- napr. chci TREBIC a zakladni gidy pro tuto estimation_cell nejsou jeste ulozeny v t_aux_total
			-- vystup za NUTS1-4 a rajonizace => pujde pres step 2 a sumarizaci
			-- vystup za OPLO => pujde pres step 2 a sumarizaci

			IF _gid > 0
			THEN		
				_estimation_cell_result := _estimation_cell_exit;
			ELSE
				RAISE EXCEPTION 'Chyba 17: fn_get_aux_total_app: Jde-li o config_query = 100, pak gid nesmi byt nikdy NULL nebo hodnota 0!';
			END IF;
		ELSE
			-- VSTUPNI estimation_cell JE zakladd
			
			-- zjisteni poctu geometrii, ktere tvori vstupni estimation_cell
			SELECT count(fac.gid) FROM @extschema@.f_a_cell AS fac WHERE fac.estimation_cell = _estimation_cell
			INTO _check_pocet;

			IF _check_pocet IS NULL
			THEN
				RAISE EXCEPTION 'Chyba 18: fn_get_aux_total_app: Pro estimation_cell = % nenalezeny geometrie v tabulce f_a_cell!',_estimation_cell;
			END IF;

			IF _check_pocet = 1 -- vstupni estimation_cell uz neni nijak geometricky rozdrobena v f_a_cell
			THEN
				_estimation_cell_result := _estimation_cell_exit;
			ELSE
				_estimation_cell_result := NULL::integer;
			END IF;
		END IF;

		_gid_result := _gid;
	ELSE
		IF _config_query = ANY(array[200,300,400])
		THEN
			_estimation_cell_result := _estimation_cell_exit;
			_gid_result := NULL::integer;
		ELSE
			RAISE EXCEPTION 'Chyba 19: fn_get_aux_total_app: Neznama hodnota config_query = %!',_config_query;
		END IF;
	END IF;
	--------------------------------------------------------------------------------------------
	_q := 'SELECT $1,$2,$3,$4,$5';
	--------------------------------------------------------------------------------------------
	RETURN QUERY EXECUTE ''||_q||'' USING _estimation_cell_result,_config_id,_aux_total,_gid_result,_ext_version_current;
	--------------------------------------------------------------------------------------------
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;

ALTER FUNCTION @extschema@.fn_get_aux_total_app(integer,integer,integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_app(integer,integer,integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_app(integer,integer,integer) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_app(integer,integer,integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_aux_total_app(integer,integer,integer) IS
'Funkce vraci vypocitanou hodnotu aux_total pro zadanou konfiguraci (config_id) a celu (estimation_cell) nebo gid.';

-- </function>


-- <function name="fn_get_aux_total_raster_app" schema="extschema" src="functions/extschema/fn_get_aux_total_raster_app.sql">
-- DROP FUNCTION @extschema@.fn_get_aux_total_raster_app(integer,integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_get_aux_total_raster_app
( 
	_config_id			integer,
	_gid				integer
)
RETURNS double precision AS
$BODY$
DECLARE
	_config_collection		integer;
	_condition			character varying;
	_band				integer;
	_reclass			integer;
	_schema_name			character varying;
	_table_name			character varying;
	_column_name			character varying;
	_unit				double precision;
	_gid_text			character varying;
	_command			text;
	_res				double precision;
BEGIN
	-------------------------------------------------------------------------------------------
	IF _config_id IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_aux_total_raster_app: Vstupni argument _config_id nesmi byt NULL!';
	END IF;

	IF _gid IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_aux_total_raster_app: Vstupni argument _gid nesmi byt NULL!';
	END IF;
	-------------------------------------------------------------------------------------------
	-- proces ziskani konfiguraci pro vstupni _config_id z t_config
	SELECT
		config_collection,
		condition,
		band,
		reclass
	FROM
		@extschema@.t_config
	WHERE
		id = _config_id
	INTO
		_config_collection,
		_condition,
		_band,
		_reclass;
	-------------------------------------------------------------------------------------------
	-- proces ziskani konfiguraci pro vstupni _config_collection z t_config_collection
	SELECT
		schema_name,
		table_name,
		column_name,
		unit
	FROM
		@extschema@.t_config_collection
	WHERE
		id = _config_collection
	INTO
		_schema_name,
		_table_name,
		_column_name,
		_unit;
	-------------------------------------------------------------------------------------------
	-- prevedeni gidu na character varying
	_gid_text := _gid::character varying;
	-------------------------------------------------------------------------------------------
	-- vytvoreni (sestaveni) vysledneho sql dotazu pro ziskatni hodnoty aux_total
	SELECT @extschema@.fn_sql_aux_total_raster_app(_config_id, _schema_name, _table_name, _column_name, _band, _reclass,  _condition, _unit, _gid_text)
	INTO _command;
	-------------------------------------------------------------------------------------------
	EXECUTE ''||_command||'' INTO _res;
	-------------------------------------------------------------------------------------------
	IF _res < 0.0
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_get_aux_total_raster_app: Pro zadanou hodnotu (_config_id = %) a _gid = % je aux_total ZAPORNA hodnota !', _config_id,_gid;	
	END IF;
	-------------------------------------------------------------------------------------------
	RETURN _res;
	-------------------------------------------------------------------------------------------
END ;
$BODY$
LANGUAGE plpgsql VOLATILE;
	
ALTER FUNCTION @extschema@.fn_get_aux_total_raster_app(integer,integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_raster_app(integer,integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_raster_app(integer,integer) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_raster_app(integer,integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_aux_total_raster_app(integer,integer) IS
'Funkce vraci vypocitanou hodnotu aux_total_raster pro zadanou konfiguraci config_id a zadany gid geometrie z tabulky f_a_cell.';

-- </function>


-- <function name="fn_get_aux_total_raster_comb_app" schema="extschema" src="functions/extschema/fn_get_aux_total_raster_comb_app.sql">
-- DROP FUNCTION @extschema@.fn_get_aux_total_raster_comb_app(integer,integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_get_aux_total_raster_comb_app
( 
	_config_id			integer,
	_gid				integer
)
RETURNS double precision AS
$BODY$
DECLARE
	_config_id_raster		integer;
	_config_id_raster_1		integer;
	_schema_name			character varying;
	_table_name			character varying;
	_column_name			character varying;
	_unit				double precision;
	_schema_name_1			character varying;
	_table_name_1			character varying;
	_column_name_1			character varying;
	_unit_1				double precision;
	_band				integer;
	_reclass			integer;
	_condition			character varying;
	_band_1				integer;
	_reclass_1			integer;
	_condition_1			character varying;
	_gid_text			character varying;
	_command			text;
	_res				double precision;
BEGIN
	-------------------------------------------------------------------------------------------
	IF _config_id IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_aux_total_raster_comb_app: Vstupni argument _config_id nesmi byt NULL!';
	END IF;

	IF _gid IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_aux_total_raster_comb_app: Vstupni argument _gid nesmi byt NULL!';
	END IF;						
	-------------------------------------------------------------------------------------------
	-- proces ziskani konfiguraci pro vstupni _config_id
	SELECT
		raster,
		raster_1
	FROM
		@extschema@.t_config
	WHERE
		id = _config_id
	INTO
		_config_id_raster,
		_config_id_raster_1;
	-------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------
	-- proces ziskani konfiguraci pro raster z tabulky t_config_collection
	SELECT
		schema_name,
		table_name,
		column_name,
		unit
	FROM
		@extschema@.t_config_collection
	WHERE
		id = (SELECT config_collection FROM @extschema@.t_config WHERE id = _config_id_raster)
	INTO
		_schema_name,
		_table_name,
		_column_name,
		_unit;
	-------------------------------------------------------------------------------------------
	-- proces ziskani konfiguraci pro raster_1 z tabulky t_config_collection
	SELECT
		schema_name,
		table_name,
		column_name,
		unit
	FROM
		@extschema@.t_config_collection
	WHERE
		id = (SELECT config_collection FROM @extschema@.t_config WHERE id = _config_id_raster_1)
	INTO
		_schema_name_1,
		_table_name_1,
		_column_name_1,
		_unit_1;
	-------------------------------------------------------------------------------------------
	-- proces ziskani konfiguraci pro raster z tabulky t_config
	SELECT
		band,
		reclass,
		condition
	FROM
		@extschema@.t_config
	WHERE
		id = _config_id_raster
	INTO
		_band,
		_reclass,
		_condition;
	-------------------------------------------------------------------------------------------
	-- proces ziskani konfiguraci pro raster_1 z tabulky t_config
	SELECT
		band,
		reclass,
		condition
	FROM
		@extschema@.t_config
	WHERE
		id = _config_id_raster_1
	INTO
		_band_1,
		_reclass_1,
		_condition_1;
	-------------------------------------------------------------------------------------------
	-- prevedeni gidu na character varying
	_gid_text := _gid::character varying;
	-------------------------------------------------------------------------------------------
	-- vytvoreni (sestaveni) vysledneho sql dotazu pro ziskatni hodnoty aux_total
	SELECT @extschema@.fn_sql_aux_total_raster_comb_app
		(
		_config_id,
		_schema_name, _table_name, _column_name, _band, _reclass, _condition, _unit,
		_schema_name_1, _table_name_1, _column_name_1, _band_1, _reclass_1,  _condition_1, _unit_1,
		_gid_text
		)
	INTO _command;	
	-------------------------------------------------------------------------------------------
	EXECUTE ''||_command||'' INTO _res;
	-------------------------------------------------------------------------------------------
	IF _res < 0.0
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_get_aux_total_raster_comb_app: Pro zadanou hodnotu (_config_id = %) a _gid = % je aux_total ZAPORNA hodnota !', _config_id,_gid;	
	END IF;
	-------------------------------------------------------------------------------------------
	RETURN _res;
	-------------------------------------------------------------------------------------------
END ;
$BODY$
LANGUAGE plpgsql VOLATILE ;
	
ALTER FUNCTION @extschema@.fn_get_aux_total_raster_comb_app(integer,integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_raster_comb_app(integer,integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_raster_comb_app(integer,integer) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_raster_comb_app(integer,integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_aux_total_raster_comb_app(integer,integer) IS
'Funkce vraci vypocitanou hodnotu aux_total_raster_combination pro zadanou konfiguraci (config_id) a zadany gid geometrie z tabulky f_a_cell.';

-- </function>


-- <function name="fn_get_aux_total_vector_app" schema="extschema" src="functions/extschema/fn_get_aux_total_vector_app.sql">
-- DROP FUNCTION @extschema@.fn_get_aux_total_vector_app(integer,integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_get_aux_total_vector_app
(
    _config_id integer,
    _gid integer
)
  RETURNS double precision AS
$BODY$
DECLARE
	_config_collection		integer;
	_condition			character varying;
	_schema_name			character varying;
	_table_name			character varying;
	_column_name			character varying;
	_unit				double precision;
	_gid_text			character varying;
	_command			text;
	_res				double precision;
BEGIN
	-------------------------------------------------------------------------------------------
	IF _config_id IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_aux_total_vector_app: Vstupni argument _config_id nesmi byt NULL!';
	END IF;

	IF _gid IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_aux_total_vector_app: Vstupni argument _gid nesmi byt NULL!';
	END IF;
	-------------------------------------------------------------------------------------------
	-- proces ziskani konfiguraci pro vstupni _config_id
	SELECT
		config_collection,
		condition
	FROM
		@extschema@.t_config
	WHERE
		id = _config_id
	INTO
		_config_collection,
		_condition;
	-------------------------------------------------------------------------------------------
	SELECT
		schema_name,
		table_name,
		column_name,
		unit
	FROM
		@extschema@.t_config_collection
	WHERE
		id = _config_collection
	INTO
		_schema_name,
		_table_name,
		_column_name,
		_unit;
	-------------------------------------------------------------------------------------------
	-- prevedeni gidu na character varying
	_gid_text := _gid::character varying;
	-------------------------------------------------------------------------------------------
	-- vytvoreni (sestaveni) vysledneho sql dotazu pro ziskatni hodnoty aux_total
	SELECT @extschema@.fn_sql_aux_total_vector_app(_config_id, _schema_name, _table_name, _column_name, _condition, _unit, _gid_text)
	INTO _command;
	-------------------------------------------------------------------------------------------
	EXECUTE ''||_command||'' INTO _res;
	-------------------------------------------------------------------------------------------
	IF _res < 0.0
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_get_aux_total_vector_app: Pro zadanou hodnotu (_config_id = %) a _gid = % je aux_total ZAPORNA hodnota !', _config_id,_gid;	
	END IF;
	-------------------------------------------------------------------------------------------	
	RETURN _res;
	-------------------------------------------------------------------------------------------
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;

ALTER FUNCTION @extschema@.fn_get_aux_total_vector_app(integer,integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_vector_app(integer,integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_vector_app(integer,integer) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_vector_app(integer,integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_aux_total_vector_app(integer,integer) IS
'Funkce vraci vypocitanou hodnotu aux_total_vector pro zadanou konfiguraci (config_id) a zadany gidu geometrie z tabulky f_a_cell.';

-- </function>


-- <function name="fn_get_aux_total_vector_comb_app" schema="extschema" src="functions/extschema/fn_get_aux_total_vector_comb_app.sql">
-- DROP FUNCTION @extschema@.fn_get_aux_total_vector_comb_app(integer,integer);
  
CREATE OR REPLACE FUNCTION @extschema@.fn_get_aux_total_vector_comb_app
( 
	_config_id			integer,
	_gid				integer
)
RETURNS double precision AS
$BODY$
DECLARE
	_config_id_vector		integer;
	_config_id_raster		integer;
	_schema_name			character varying;
	_table_name			character varying;
	_column_name			character varying;
	_schema_name_1			character varying;
	_table_name_1			character varying;
	_column_name_1			character varying;
	_unit				double precision;
	_unit_1				double precision;
	_condition			character varying;	
	_condition_1			character varying;
	_band_1				integer;
	_reclass_1			integer;
	_gid_text			character varying;
	_command			text;
	_res				double precision;
BEGIN
	-------------------------------------------------------------------------------------------
	IF _config_id IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_aux_total_vector_comb_app: Vstupni argument _config_id nesmi byt NULL!';
	END IF;

	IF _gid IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_aux_total_vector_comb_app: Vstupni argument _gid nesmi byt NULL!';
	END IF;	
	-------------------------------------------------------------------------------------------
	-- proces ziskani konfiguraci pro vstupni _config_id
	SELECT
		vector,
		raster
	FROM
		@extschema@.t_config
	WHERE
		id = _config_id
	INTO
		_config_id_vector,
		_config_id_raster;
	-------------------------------------------------------------------------------------------
	-- proces ziskani konfiguraci pro vector z t_config_collection pro _config_id_vector
	SELECT
		schema_name,
		table_name,
		column_name,
		unit
	FROM
		@extschema@.t_config_collection
	WHERE
		id = (SELECT config_collection FROM @extschema@.t_config WHERE id = _config_id_vector)
	INTO
		_schema_name,
		_table_name,
		_column_name,
		_unit;
	-------------------------------------------------------------------------------------------
	-- proces ziskani konfiguraci pro raster z t_config_collection pro _config_id_raster
	SELECT
		schema_name,
		table_name,
		column_name,
		unit
	FROM
		@extschema@.t_config_collection
	WHERE
		id = (SELECT config_collection FROM @extschema@.t_config WHERE id = _config_id_raster)
	INTO
		_schema_name_1,
		_table_name_1,
		_column_name_1,
		_unit_1;
	-------------------------------------------------------------------------------------------	
	-- proces ziskani konfiguraci pro vektor z t_config pro _config_id_vector
	SELECT
		condition
	FROM
		@extschema@.t_config
	WHERE
		id = _config_id_vector
	INTO
		_condition;
	-------------------------------------------------------------------------------------------
	SELECT
		condition,
		band,
		reclass
	FROM
		@extschema@.t_config
	WHERE
		id = _config_id_raster
	INTO
		_condition_1,
		_band_1,
		_reclass_1;
	-------------------------------------------------------------------------------------------
	-- prevedeni gidu na character varying
	_gid_text := _gid::character varying;
	-------------------------------------------------------------------------------------------
	-- vytvoreni (sestaveni) vysledneho sql dotazu pro ziskatni hodnoty aux_total
	SELECT @extschema@.fn_sql_aux_total_vector_comb_app
		(
		_config_id,
		_schema_name, _table_name, _column_name, _condition, _unit,
		_schema_name_1, _table_name_1, _column_name_1, _band_1, _reclass_1,  _condition_1, _unit_1,
		_gid_text
		)
	INTO _command;	
	-------------------------------------------------------------------------------------------
	EXECUTE ''||_command||'' INTO _res;
	-------------------------------------------------------------------------------------------
	IF _res < 0.0
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_get_aux_total_vector_comb_app: Pro zadanou hodnotu (_config_id = %) a _gid = % je aux_total ZAPORNA hodnota !', _config_id,_gid;	
	END IF;
	-------------------------------------------------------------------------------------------
	RETURN _res;
	-------------------------------------------------------------------------------------------
END ;
$BODY$
LANGUAGE plpgsql VOLATILE ;
	
ALTER FUNCTION @extschema@.fn_get_aux_total_vector_comb_app(integer,integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_vector_comb_app(integer,integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_vector_comb_app(integer,integer) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_vector_comb_app(integer,integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_aux_total_vector_comb_app(integer,integer) IS
'Funkce vraci vypocitanou hodnotu aux_total_vector_combination pro zadanou konfiguraci (config_id) a zadany gid geometrie z tabulky f_a_cell.';

-- </function>


-- <function name="fn_get_aux_total4estimation_cell_app" schema="extschema" src="functions/extschema/fn_get_aux_total4estimation_cell_app.sql">
-- DROP function @extschema@.fn_get_aux_total4estimation_cell_app (integer,integer,integer,boolean)

CREATE OR REPLACE FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app
	(
	_config_id			integer,
	_estimation_cell		integer,
	_gid				integer,
	_recount			boolean DEFAULT FALSE
	)
RETURNS TABLE
	(
        estimation_cell			integer,
        config				integer, 
        aux_total			double precision,
        cell				integer,
        ext_version			integer
        )
AS
$BODY$
DECLARE
	_estimation_cell_collection		integer;
	_estimation_cell_collection_lowest	integer;
	_check_estimation_cell			boolean;
	_check_pocet				integer;
	_gids4estimation_cell			integer[];
	_gids4estimation_cell_lowest		integer[];
	_gids_in_t_aux_total_check		integer;
	_aux_total				double precision;
	_q					text;

	_ext_version_label_system		text;
	_ext_version_current			integer;
BEGIN
	-------------------------------------------------------------------------------------------
	IF _config_id IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_aux_total4estimation_cell_app: Vstupni argument _config_id nesmi byt NULL!';
	END IF;

	IF _estimation_cell IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_aux_total4estimation_cell_app: Vstupni argument _estimation_cell nesmi byt NULL!';
	END IF;

	IF _gid IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_get_aux_total4estimation_cell_app: Vstupni argument _gid nesmi byt NULL!';
	END IF;

	IF _gid != 0
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_get_aux_total4estimation_cell_app: Vstupni argument _gid musi byt hodnota 0!';
	END IF;

	IF _recount IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_get_aux_total4estimation_cell_app: Vstupni argument _recount nesmi byt NULL!';
	END IF;	
	-------------------------------------------------------------------------------------------
	--------------------------------------------------------------------------------------------
	-- proces ziskani aktualni systemove extenze nfiesta_gisdata
	
	SELECT ext_version FROM pg_extension WHERE extname = 'nfiesta_gisdata'
	INTO _ext_version_label_system;

	--_ext_version_label_system := '1.0.0';

	IF _ext_version_label_system IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_get_aux_total4estimation_cell_app: V systemove tabulce pg_extension nenalezena zadna verze pro extenzi nfiesta_gisdata!';
	END IF;

	SELECT id FROM @extschema@.c_ext_version
	WHERE label = _ext_version_label_system
	INTO _ext_version_current;
	
	IF _ext_version_current IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_get_aux_total4estimation_cell_app: V ciselniku c_ext_version nenalezen zaznam odpovidajici systemove verzi % extenze nfiesta_gisdata!',_ext_version_label_system;
	END IF;		
	-------------------------------------------------------------------------------------------
	-- kontrola, ze estimation_cell:
	-- 1. nesmi byt zakladni stavebni jednotka [ZSJ] => vyjimka plati jen pro viz. bod 2
	-- 2. muze to byt ZSJ ale pocet geometrii, ktery tvori onu ZSJ musi byt vice nez 1
	--------------------------------------------------------------------------------------------
	-- zjisteni do jake estimation_cell_collection patri zadana vstupni estimation_cell
	SELECT cec.estimation_cell_collection FROM @extschema@.c_estimation_cell AS cec WHERE cec.id = _estimation_cell
	INTO _estimation_cell_collection;
	--------------------------------------------------------------------------------------------
	-- zjisteni estimation_cell_collection_lowest pro zjistenou _estimation_cell_collection
	SELECT cecc.estimation_cell_collection_lowest FROM @extschema@.cm_estimation_cell_collection AS cecc
	WHERE cecc.estimation_cell_collection = _estimation_cell_collection
	INTO _estimation_cell_collection_lowest;
	--------------------------------------------------------------------------------------------
	IF _estimation_cell_collection != _estimation_cell_collection_lowest
	THEN
		-- vstupni estimation_cell neni ZSJ => splnena 1. podminka kontroly
		_check_estimation_cell := TRUE;
	ELSE
		-- vstupni estimation_cell je ZSJ
		
		-- zjisteni poctu geometrii, ktere tvori vstupni estimation_cell, ktera je ZSJ
		SELECT count(fac.gid) FROM @extschema@.f_a_cell AS fac
		WHERE fac.estimation_cell = _estimation_cell
		INTO _check_pocet;

		IF _check_pocet IS NULL
		THEN
			RAISE EXCEPTION 'Chyba 08: fn_get_aux_total4estimation_cell_app: Pro estimation_cell = % nenalezena zadna geometrie v tabulce f_a_cell!',_estimation_cell;
		END IF;

		IF _check_pocet = 1 -- vstupni estimation_cell (ZSJ) uz neni nijak geometricky rozdrobena v f_a_cell na mensi casti
		THEN
			_check_estimation_cell := FALSE;
		ELSE
			_check_estimation_cell := TRUE;
		END IF;
	END IF;
	--------------------------------------------------------------------------------------------
	IF _check_estimation_cell = FALSE
	THEN
		RAISE EXCEPTION 'Chyba 09: fn_get_aux_total4estimation_cell_app: Zadanou estimation_cell = % neni mozno sumarizovat. Jedna se totiz o nejnizsi geografickou uroven, ktera neni geometricky rozdrobena na mensi casti!',_estimation_cell;
	END IF;
	--------------------------------------------------------------------------------------------
	-- kontrola zda jiz pro zadanou estimation_cell, config_id a ext_version neni uz vypocitana
	-- hodnota aux_total v tabulce t_aux_total
	-- kontrola se provadi pri VYPOCTU i PREPOCTU [puvodne zde bylo jen pri VYPOCTU]
	-- jelikoz je u vstupnich argumentu ponechan _recount, pak IF jsem upravil nasledovne
	IF _recount = ANY(array[TRUE,FALSE])
	THEN
		IF	(
			SELECT count(tat.*) > 0
			FROM @extschema@.t_aux_total AS tat
			WHERE tat.config = _config_id
			AND tat.estimation_cell = _estimation_cell
			AND tat.ext_version = _ext_version_current
			)
		THEN
			RAISE EXCEPTION 'Chyba 10: fn_get_aux_total4estimation_cell_app: Pro estimation_cell = %, config_id = % a ext_version = % jiz existuje hodnota aux_total v tabulce t_aux_total. Sumarizace neni mozna!',_estimation_cell,_config_id,_ext_version_label_system;
		END IF;
	END IF;
	-------------------------------------------------------------------------------------------
	-- zjisteni gidu (ZSJ) z tabulky f_a_cell, ktere tvori zadanou _estimation_cell
	SELECT array_agg(fac.gid ORDER BY fac.gid) FROM @extschema@.f_a_cell AS fac
	WHERE fac.estimation_cell = _estimation_cell
	INTO _gids4estimation_cell;

	IF _gids4estimation_cell IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 11: fn_get_aux_total4estimation_cell_app: Pro estimation_cell = % nenalezeny zadne zaznamy v tabulce f_a_cell!',_estimation_cell;
	END IF;

	-- volani funkce fn_get_lowest_gids [funkce si saha do mapovaci tabulky cm_f_a_cell]
	-- funkce by mela vratit seznam gidu nejnizsi urovne (ZSJ), ktere tvori zadany seznam vyssich gidu
	_gids4estimation_cell_lowest := @extschema@.fn_get_lowest_gids_app(_gids4estimation_cell);
	-------------------------------------------------------------------------------------------
	-- kontrola zda v tabulce t_aux_total jsou pro sumarizaci vsechny gidy nejnizsi urovne
	-- pro config_id a ext_version
	WITH
	w1 AS	(SELECT unnest(_gids4estimation_cell_lowest) AS gids),
	w2 AS	(
		SELECT
			tat.config,
			tat.cell
		FROM
			@extschema@.t_aux_total AS tat
		WHERE
			tat.config = _config_id
		AND
			tat.cell IN (SELECT gids FROM w1)
		AND
			tat.ext_version = _ext_version_current
		),
	w3 AS	(
		SELECT
			w1.gids,
			w2.cell
		FROM
			w1 LEFT JOIN w2
		ON
			w1.gids = w2.cell
		)
	SELECT
		count(*) FROM w3 WHERE w3.cell IS NULL
	INTO
		_gids_in_t_aux_total_check;
	-------------------------------------------------------------------------------------------
	IF _gids_in_t_aux_total_check > 0
	THEN
		RAISE EXCEPTION 'Chyba 12: fn_get_aux_total4estimation_cell_app: Pro estimation_cell = %, config_id = % a ext_version = % neni v tabulce t_aux_total kompletni seznam hodnot aux_total pro sumarizaci!',_estimation_cell,_config_id,_ext_version_label_system;
	END IF;
	-------------------------------------------------------------------------------------------
	-- vypocet hodnoty aux_total pro danou estimation_cell
	WITH
	w1 AS	(SELECT unnest(_gids4estimation_cell_lowest) AS gids),
	w2 AS	(
		SELECT
			tat.cell,
			tat.aux_total
		FROM
			@extschema@.t_aux_total AS tat
		WHERE
			tat.config = _config_id
		AND
			tat.cell IN (SELECT gids FROM w1)
		AND
			tat.ext_version = _ext_version_current
		)
	SELECT
		sum(w2.aux_total) AS aux_total
	FROM
		w2
	INTO
		_aux_total;
	-------------------------------------------------------------------------------------------
	IF _aux_total < 0.0
	THEN
		RAISE EXCEPTION 'Chyba 13: fn_get_aux_total4estimation_cell_app: Pro zadanou hodnotu (_config_id = %), (_estimation_cell = %) ve verzi (ext_version = %) je aux_total ZAPORNA hodnota !', _config_id,_estimation_cell,_ext_version_label_system;	
	END IF;
	-------------------------------------------------------------------------------------------
	_q := 'SELECT $1,$2,$3,NULL::integer,$4';
	--------------------------------------------------------------------------------------------
	RETURN QUERY EXECUTE ''||_q||'' USING _estimation_cell,_config_id,_aux_total,_ext_version_current;
	--------------------------------------------------------------------------------------------
END ;
$BODY$
LANGUAGE plpgsql VOLATILE;

ALTER FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app(integer,integer,integer,boolean) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app(integer,integer,integer,boolean) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app(integer,integer,integer,boolean) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app(integer,integer,integer,boolean) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app(integer,integer,integer,boolean) IS
'Funkce vraci hodnotu aux_total pro zadanou estimation_cell a config_id.';

-- </function>


-- <function name="fn_get_gids4aux_total_app" schema="extschema" src="functions/extschema/fn_get_gids4aux_total_app.sql">

-- DROP FUNCTION @extschema@.fn_get_gids4aux_total_app(integer,integer[],integer,boolean);

CREATE OR REPLACE FUNCTION @extschema@.fn_get_gids4aux_total_app
(
	_config_id			integer,
	_estimation_cell		integer[],
	_gui_version			integer,
	_recount			boolean DEFAULT FALSE
)
RETURNS TABLE
(
	step				integer,
	config_id			integer,
	estimation_cell			integer,
	gid				integer
) AS
$BODY$
DECLARE
	-- spolecne promenne:
	_config_query					integer;
	_config_collection				integer;
	_configs					integer[];
	
	_ext_version_current				integer;
	_q						text;
	_complete					character varying;
	_categories					character varying;
	_config_ids_text				text;
	_config_ids_length				integer;
	_config_ids					integer[];
	_config_ids4check				integer[];
	_check_avc4categories				integer;
	_config_id_reference				integer;
	_config_id_base					integer;
	_check						integer;
			
	-- promenne pro vypocet:
	_gids4estimation_cell_i				integer[];
	_gids4estimation_cell_pocet			integer;
	_pocet						integer[];
	_max_pocet					integer;
	_gids4estimation_cell				integer[];
	_pocet_gids4estimation_cell			integer;
	_doplnek					integer[];
	_res_estimation_cell				integer[];
	_res_gids					integer[];
	_res_estimation_cell_summarization		integer[];
	_summarization					boolean[];
	
	-- promenne pro prepocet:
	_gids4estimation_cell_i_prepocet		integer[];
	_gids4estimation_cell_pocet_prepocet		integer;
	_pocet_prepocet					integer[];
	_max_pocet_prepocet				integer;
	_gids4estimation_cell_prepocet			integer[];
	_pocet_gids4estimation_cell_prepocet		integer;
	_doplnek_prepocet				integer[];
	_res_estimation_cell_prepocet			integer[][];
	_res_gids_prepocet				integer[][];
	_res_estimation_cell_prepocet_simple		integer[];
	_res_gids_prepocet_simple			integer[];
	_res_estimation_cell_prepocet_simple_original	integer[];
	_ecc4prepocet					integer;
	_eccl4prepocet					integer;
	_gids4estimation_cell_i_prepocet_mezistupen	integer[];
	_gids4estimation_cell_prepocet_mezistupen	integer[];
	_gids_mezistupen_with_vstupni_estimation_cell	integer[];
	_estimation_cells_mezistupen_i			integer[];
	_res_estimation_cell_prepocet_4_summarization	integer[];
	_res_gids_prepocet_4_recount			integer[];
	_last_ext_version_from_table			character varying;
	_last_ext_version_from_db			character varying;
	_res_estimation_cell_prepocet_4_recount		integer[];
	_res_estimation_cell_prepocet_4_check		integer[];

	_ext_version_current_label			character varying;

	_config_query_ii				integer;
	_config_id_reference_ii				integer;
	_config_id_base_ii				integer;
	_check_ii_1					integer;
	_check_ii_2					integer;
	_check_ii_boolean_1				integer[];
	_check_ii_boolean_2				integer[];
	_check_ii					integer;
	_check_ii_boolean				integer[];

	_res_gids_count_count				integer;
	_res_estimation_cell_4_with_i			integer[];
	_res_estimation_cell_4_with			integer[][];	
BEGIN
	--------------------------------------------------------------------------------------------
	IF _config_id IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_gids4aux_total_app: Vstupni argument _config_id nesmi byt NULL!';
	END IF;

	IF _estimation_cell IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_gids4aux_total_app: Vstupni argument _estimation_cell nesmi byt NULL!';
	END IF;

	IF _gui_version IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_get_gids4aux_total_app: Vstupni argument _gui_version nesmi byt NULL!';
	END IF;
	--------------------------------------------------------------------------------------------
	-- zjisteni konfiguracnich promennych z tabulky t_config pro vstupni _config_id
	SELECT
		tc.config_query,
		tc.config_collection
	FROM
		@extschema@.t_config AS tc
	WHERE
		tc.id = _config_id
	INTO
		_config_query,
		_config_collection;
	--------------------------------------------------------------------------------------------
	IF _config_query IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_get_gids4aux_total_app: Pro vstupni argument _config_id = % nenalezeno config_query v tabulce t_config!',_config_id;
	END IF;

	IF _config_collection IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_get_gids4aux_total_app: Pro vstupni argument _config_id = % nenalezeno config_collection v tabulce t_config!',_config_id;
	END IF;
	--------------------------------------------------------------------------------------------
	-- zjisteni vsech config_id pro _config_collection
	SELECT array_agg(tc.id ORDER BY tc.id) FROM @extschema@.t_config AS tc
	WHERE tc.config_collection = _config_collection
	INTO _configs;
	--------------------------------------------------------------------------------------------
	-- nalezeni nejaktualnejsi verze extenze pro vstupni verzi GUI aplikace
	select max(ext_version) FROM @extschema@.cm_ext_gui_version
	WHERE gui_version = _gui_version
	INTO _ext_version_current;

	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_current
	INTO _ext_version_current_label;
	--------------------------------------------------------------------------------------------
	IF _recount = TRUE -- prepocet
	THEN	
		-----------------------------------------------------------------------------------
		-- KONTROLA EXISTENCE v DB
		-- A: prepocet   JE mozny pokud pro danou estimation_cell existuji vsechny kategorie dane konfigurace [na verzi extenze nezalezi]
		-- B: prepocet NENI mozny pokud pro danou estimation_cell existuji vsechny kategorie dane konfigurace [na verzi extenze   zalezi]
		-----------------------------------------------------------------------------------
		FOR i IN 1..array_length(_estimation_cell,1)
		LOOP
			FOR ii IN 1..array_length(_configs,1)
			LOOP
				SELECT tc.config_query FROM @extschema@.t_config AS tc
				WHERE tc.id = _configs[ii]
				INTO _config_query_ii;
						
				IF _config_query_ii = 500 -- REFERENCE
				THEN
					SELECT tc.categories::integer
					FROM @extschema@.t_config AS tc
					WHERE tc.id = _configs[ii]
					INTO _config_id_reference_ii;

					IF _config_id_reference_ii IS NULL
					THEN
						RAISE EXCEPTION 'Chyba 06: fn_get_gids4aux_total_app: Kontrolovane _config_id = % je reference [config_query = 500] a v konfiguraci [v tabulce t_config] nema ve sloupci categories nakonfigurovanou referenci!',_configs[ii];
					END IF;

					_config_id_base_ii := _config_id_reference_ii;
				ELSE
					_config_id_base_ii := _configs[ii]; -- vstupnim config_id je bud konfigurace ze zakladni skupiny [100,300,400] nebo konfigurace z agregacni skupiny ci navazujici kolekce [200,300,400]
				END IF;

				-----------------------------------------------
				-- kontrola A: prepocet JE mozny, pokud pro danou estimation_cell existuji vsechny kategorie dane konfigurace [na verzi extenze nezalezi]
				WITH
				w1 AS	(
					SELECT tat.* FROM @extschema@.t_aux_total AS tat
					WHERE tat.estimation_cell = _estimation_cell[i]
					AND tat.config = _config_id_base_ii
					-- na verzi extenze nezalezi
					)
				SELECT count(*) FROM w1	-- pokud je pocet vetsi jak 0 tak zaznam(y) exisuje(i)
				INTO _check_ii_1;

				IF _check_ii_1 > 0
				THEN
					IF ii = 1
					THEN
						_check_ii_boolean_1 := array[0];
					ELSE
						_check_ii_boolean_1 := _check_ii_boolean_1 || array[0];
					END IF;
				ELSE
					IF ii = 1
					THEN
						_check_ii_boolean_1 := array[1];
					ELSE
						_check_ii_boolean_1 := _check_ii_boolean_1 || array[1];
					END IF;
				END IF;
				-----------------------------------------------
				-- kontrola B: prepocet NENI mozny, pokud pro danou estimation_cell existuji vsechny kategorie dane konfigurace [na verzi extenze zalezi]
				WITH
				w1 AS	(
					SELECT tat.* FROM @extschema@.t_aux_total AS tat
					WHERE tat.estimation_cell = _estimation_cell[i]
					AND tat.config = _config_id_base_ii
					AND tat.ext_version = _ext_version_current	-- na verzi extenze zalezi
					)
				SELECT count(*) FROM w1
				INTO _check_ii_2;

				IF _check_ii_2 > 0 -- pokud je pocet vetsi jak 0 tak zaznam(y) exisuje(i)
				THEN
					IF ii = 1
					THEN
						_check_ii_boolean_2 := array[0];
					ELSE
						_check_ii_boolean_2 := _check_ii_boolean_2 || array[0];
					END IF;
				ELSE
					IF ii = 1
					THEN
						_check_ii_boolean_2 := array[1];
					ELSE
						_check_ii_boolean_2 := _check_ii_boolean_2 || array[1];
					END IF;
				END IF;
				-----------------------------------------------
			END LOOP;

			IF	(
				SELECT sum(t.check_ii_boolean_1) > 0
				FROM (SELECT unnest(_check_ii_boolean_1) AS check_ii_boolean_1) AS t
				)
			THEN
				RAISE EXCEPTION 'Chyba 07: fn_get_gids4aux_total_app: Vypocet neni mozny => pro zadanou estimation_cell = % doposud neexistuje v tabulce t_aux_total hodnota aux_total pro nekterou z config_id =  %!',_estimation_cell[i],_configs;
			END IF;


			IF	(
				SELECT sum(t.check_ii_boolean_2) = 0
				FROM (SELECT unnest(_check_ii_boolean_2) AS check_ii_boolean_2) AS t
				)
			THEN
				RAISE EXCEPTION 'Chyba 08: fn_get_gids4aux_total_app: Vypocet neni mozny => pro zadanou estimation_cell = % jiz v tabulce t_aux_total existuji hodnoty aux_total pro config_id =  % v aktualni verzi extenze = %!',_estimation_cell[i],_configs,_ext_version_current;
			END IF;
			
		END LOOP;
		-----------------------------------------------------------------------------------
		-----------------------------------------------------------------------------------
		IF _config_query = ANY(array[100,200,300,400])
		THEN
			-- proces ziskani:
			-- pole poli estimation_cell [jde jen o vstupni estimation_cell (muze jit i o estimation_cell, ktere jsou ZSJ)] <= _res_estimation_cell_prepocet
			-- pole poli ZSJ [jde o gidy, ktere tvori vstupni estimation_cell (gidy se zde mohou opakovat)] <= _res_gids_prepocet
			-----------------------------------------------------
			-----------------------------------------------------
			-----------------------------------------------------
			-----------------------------------------------------
			-- cyklus pro ziskani POCTu u PREPOCTU
			FOR i IN 1..array_length(_estimation_cell,1)
			LOOP
				-- zjisteni gidu z tabulky f_a_cell pro i-tou _estimation_cell
				SELECT array_agg(fac.gid ORDER BY fac.gid) FROM @extschema@.f_a_cell AS fac
				WHERE fac.estimation_cell = _estimation_cell[i]
				INTO _gids4estimation_cell_i_prepocet;

				IF _gids4estimation_cell_i_prepocet IS NULL
				THEN
					RAISE EXCEPTION 'Chyba 09: fn_get_gids4aux_total_app: Pro _estimation_cell[i] = % nenalezeny zadne zaznamy v tabulce f_a_cell!',_estimation_cell[i];
				END IF;

				_gids4estimation_cell_pocet_prepocet := array_length(@extschema@.fn_get_lowest_gids_app(_gids4estimation_cell_i_prepocet),1);

				IF i = 1
				THEN
					_pocet_prepocet := array[_gids4estimation_cell_pocet_prepocet];
				ELSE
					_pocet_prepocet := _pocet_prepocet || array[_gids4estimation_cell_pocet_prepocet];
				END IF;
				
			END LOOP;
			-----------------------------------------------------
			-- zjisteni nejdelsiho pole _gids4estimation_cell_pocet_prepocet
			SELECT max(t.pocet) FROM (SELECT unnest(_pocet_prepocet) as pocet) AS t
			INTO _max_pocet_prepocet;
			-----------------------------------------------------
			-- cyklus ziskani GIDu nejnizsi urovne pro vstupni _estimation_cell[]
			FOR i IN 1..array_length(_estimation_cell,1)
			LOOP
				-- zjisteni gidu z tabulky f_a_cell pro i-tou _estimation_cell
				SELECT array_agg(fac.gid ORDER BY fac.gid) FROM @extschema@.f_a_cell AS fac
				WHERE fac.estimation_cell = _estimation_cell[i]
				INTO _gids4estimation_cell_i_prepocet;

				-- volani funkce fn_get_lowest_gids
				-- funkce by mela vratit seznam gidu nejnizsi urovne
				_gids4estimation_cell_prepocet := @extschema@.fn_get_lowest_gids_app(_gids4estimation_cell_i_prepocet);

				-- zjisteni poctu prvku(gidu) v poli _gids4estimation_cell_prepocet
				_pocet_gids4estimation_cell_prepocet := array_length(_gids4estimation_cell_prepocet,1);

				-- vytvoreni pole s nulama o poctu _max_pocet - _pocet_gids4estimation_cell_prepocet
				SELECT array_agg(t.gid_gs) FROM (SELECT 0 AS gid_gs FROM generate_series(1,(_max_pocet_prepocet - _pocet_gids4estimation_cell_prepocet))) AS t
				INTO _doplnek_prepocet;

				-- proces doplneni 0 tak aby pole _gids4estimation_cell melo delku _max_pocet
				_gids4estimation_cell_prepocet := _gids4estimation_cell_prepocet || _doplnek_prepocet;

				IF i = 1
				THEN
					_res_estimation_cell_prepocet := array[array[_estimation_cell[i]]];	-- do pole poli se ulozi pozadovane vstupni estimation_cell pro prepocet
					_res_gids_prepocet := array[_gids4estimation_cell_prepocet];		-- do pole poli se ulozi pozadovane gidy nejnizsi urovne pro prepocet
				ELSE
					_res_estimation_cell_prepocet := _res_estimation_cell_prepocet || array[array[_estimation_cell[i]]];
					_res_gids_prepocet := _res_gids_prepocet || array[_gids4estimation_cell_prepocet];
				END IF;

			END LOOP;
			-----------------------------------------------------
			-- po sem je hotovo:
			-- _res_estimation_cell_prepocet	=> jde o pole poli s estimation_cell ze vstupu uzivatele urcenych pro prepocet [muze obsahovat i zakladni stavebni jednotku]
								-- pokud to doslo az sem, tak se v poli nemuze vyskytovat estimation_cell nebo defakto ZSJ, ktera je ve shodne verzi jako
								-- je aktualni systemova verze extenze
			-- _res_gids_prepocet			=> jde o pole poli gidu zakladnich stavebnich jednotek pro vstupni estimation_cell urcenych pro prepocet, [vyskyt DUPLICIT]
								-- pokud to doslo az sem, tak se zde mohou vyskytovat ZSJ, ktere uz jsou v databazi ve shodne verzi jako je aktualni
								-- systemova verze extenze => tyto gidy neni nutno prepocitavat, prepocitaji se gidy nizssi verze nez je aktualni systemova
			-----------------------------------------------------
			-----------------------------------------------------
			-----------------------------------------------------
			-----------------------------------------------------
	

			-----------------------------------------------------
			-- CONFIG_ID_BASE --
			-----------------------------------------------------
			IF _config_query = 500 -- REFERENCE
			THEN
				SELECT tc.categories::integer
				FROM @extschema@.t_config AS tc
				WHERE tc.id = _config_id
				INTO _config_id_reference;

				IF _config_id_reference IS NULL
				THEN
					RAISE EXCEPTION 'Chyba 10: fn_get_gids4aux_total_app: Vstupni _config_id = % je reference [config_query = 500] a v konfiguraci [v tabulce t_config] nema nakonfigurovanou referenci!',_config_id;
				END IF;

				_config_id_base := _config_id_reference;
			ELSE
				_config_id_base := _config_id; -- vstupnim config_id je bud konfigurace ze zakladni skupiny [100,300,400] nebo konfigurace z agregacni skupiny ci navazujici kolekce [200,300,400]
			END IF;
			-----------------------------------------------------
			-----------------------------------------------------

		
			-----------------------------------------------------
			-- proces kontroly, ze vstupni ESTIMATION_CELL uz
			-- byly alespon jednou v minulosti vypocitany, tzn.
			-- ze na verzi ext_version nezalezi
			-----------------------------------------------------
			-- prevedeni _res_estimation_cell_prepocet na simple pole
			SELECT array_agg(t.recp) FROM (SELECT unnest(_res_estimation_cell_prepocet) AS recp) AS t
			INTO _res_estimation_cell_prepocet_simple;

			-- kontrola pro _res_estimation_cell_prepocet_simple
			FOR i IN 1..array_length(_res_estimation_cell_prepocet_simple,1)
			LOOP
				IF	(
					SELECT count(tat.*) = 0
					FROM @extschema@.t_aux_total AS tat
					WHERE tat.config = _config_id_base
					AND tat.estimation_cell = _res_estimation_cell_prepocet_simple[i]
					-- zde ext_version nehraje roli
					)
				THEN
					RAISE EXCEPTION 'Chyba 11: fn_get_gids4aux_total_app: Pro config_id = % a estimation_cell = % doposud neexistuje zaznam v tabulce t_aux_total. Prepocet tedy neni mozny!',_config_id_base,_res_estimation_cell_prepocet_simple[i];
				END IF;
			END LOOP;
			-----------------------------------------------------
			-----------------------------------------------------


			-----------------------------------------------------
			-- proces kontroly, ze ZSJ, ktere tvori jednotlive
			-- vstupni estimation_cell, uz byly alespon jednou
			-- v minulosti vypocitany, verze extenze zde nehraje
			-- roli		
			-----------------------------------------------------
			-- proces prevedeni _res_gids_prepocet na simple pole
			WITH
			w AS	(
				SELECT DISTINCT t.rgp
				FROM (SELECT unnest(_res_gids_prepocet) AS rgp) AS t
				WHERE t.rgp IS DISTINCT FROM 0
				)
			SELECT array_agg(w.rgp) FROM w
			INTO _res_gids_prepocet_simple;

			-- samotna kontrola pro _res_gids_prepocet_simple
			FOR i IN 1..array_length(_res_gids_prepocet_simple,1)
			LOOP
				-- jde-li o CQ 100, pak kontrola musi probihat
				-- pres gidy, respektive pres sloupec cell
				-- v tabulce t_aux_total
				IF _config_query = 100
				THEN
					IF	(
						SELECT count(tat.*) = 0
						FROM @extschema@.t_aux_total AS tat
						WHERE tat.config = _config_id_base
						AND tat.cell = _res_gids_prepocet_simple[i]
						-- zde ext_version nehraje roli,
						-- poznamka: ZSJ, ktere jsou shodne verze jako je aktualni verze extenze, budou nize v kodu
						-- ze seznamu vyjmuty a do samotneho prepoctu nemusi jit znovu !!!
						)
					THEN
						RAISE EXCEPTION 'Chyba 12: fn_get_gids4aux_total_app: Pro config_id = % a cell = % doposud neexistuje zaznam v tabulce t_aux_total. Prepocet tedy neni mozny!',_config_id_base,_res_gids_prepocet_simple[i];
					END IF;
				END IF;

				-- jde-li o CQ 200,300,400, pak kontrola musi probihat
				-- rovnez pres gid, respektive pres sloupec
				-- cell v tabulce t_aux_total, ale je zde jeste vyjimka
				IF _config_query = ANY(array[200,300,400])
				THEN
					-- pokud je i-ty gid v _res_gids_prepocet_simple ZSJ, ktera je tvorena 2 a vice
					-- geometriemi, pak se nize uvedena kontrola provadet nemusi, a to proto, ze
					-- pro takove ZSJ se pro CQ 200,300,400 do databaze vubec neukladaji
					IF	(
						SELECT count(fac2.gid) = 1
						FROM @extschema@.f_a_cell AS fac2
						WHERE fac2.estimation_cell =
									(
									SELECT fac1.estimation_cell
									FROM @extschema@.f_a_cell AS fac1
									WHERE fac1.gid = _res_gids_prepocet_simple[i]
									)
						)
					THEN
						IF	(
							SELECT count(tat.*) = 0
							FROM @extschema@.t_aux_total AS tat
							WHERE tat.config = _config_id_base
							AND tat.cell = _res_gids_prepocet_simple[i]	
							-- zde ext_version nehraje roli
							-- poznamka: ZSJ, ktere jsou shodne verze jako je aktualni verze extenze, budou nize v kodu
							-- ze seznamu vyjmuty a do samotneho prepoctu nemusi jit znovu !!!
							)
						THEN
							RAISE EXCEPTION 'Chyba 13: fn_get_gids4aux_total_app: Pro config_id = % a cell = % doposud neexistuje zaznam v tabulce t_aux_total. Prepocet tedy neni mozny!',_config_id_base,_res_gids_prepocet_simple[i];
						END IF;
					END IF;
				END IF;
			END LOOP;
			-----------------------------------------------------
			-----------------------------------------------------


			-----------------------------------------------------
			-- zachovani _res_estimation_cell_prepocet_simple
			_res_estimation_cell_prepocet_simple_original := _res_estimation_cell_prepocet_simple;
			-----------------------------------------------------


			-----------------------------------------------------
			-- proces doplneni interni promenne
			-- _res_estimation_cell_prepocet_simple
			-- podrizenymi estimation_cell
			-----------------------------------------------------
			FOR i in 1..array_length(_estimation_cell,1)
			LOOP
				SELECT cec.estimation_cell_collection
				FROM @extschema@.c_estimation_cell AS cec
				WHERE cec.id = _estimation_cell[i]
				INTO _ecc4prepocet;
				
				SELECT cmecc.estimation_cell_collection_lowest
				FROM @extschema@.cm_estimation_cell_collection AS cmecc
				WHERE cmecc.estimation_cell_collection = _ecc4prepocet
				INTO _eccl4prepocet;

				IF _ecc4prepocet = _eccl4prepocet
				THEN
					-- pro prepocet uz se nemusi zjistovat podrizene estimation_cell
					_res_estimation_cell_prepocet_simple := _res_estimation_cell_prepocet_simple; -- datovy typ integer[]
				ELSE
					-- pro prepocet se musi zjistit podrizene estimation_cell
					
					-- zjisteni vsech zakladnich gidu tvorici i-tou estimation_cell
					SELECT array_agg(fac.gid ORDER BY fac.gid) FROM @extschema@.f_a_cell AS fac
					WHERE fac.estimation_cell = _estimation_cell[i]
					INTO _gids4estimation_cell_i_prepocet_mezistupen;

					_gids4estimation_cell_prepocet_mezistupen := @extschema@.fn_get_lowest_gids_app(_gids4estimation_cell_i_prepocet_mezistupen);	

					_gids_mezistupen_with_vstupni_estimation_cell := @extschema@.fn_get_gids4under_estimation_cells_app(_gids4estimation_cell_prepocet_mezistupen,_gids4estimation_cell_i_prepocet_mezistupen);

					SELECT array_agg(fac.estimation_cell) FROM @extschema@.f_a_cell AS fac
					WHERE fac.gid IN (SELECT unnest(_gids_mezistupen_with_vstupni_estimation_cell))
					AND fac.estimation_cell IS DISTINCT FROM _estimation_cell[i]
					INTO _estimation_cells_mezistupen_i;

					IF _estimation_cells_mezistupen_i IS NULL
					THEN
						-- RAISE EXCEPTION 'Chyba 11: fn_get_gids4aux_total_app: Interni promenna _estimation_cells_mezistupen_i nesmi byt nikdy NULL!';
						-- tato vyjimka byla zde puvodne chybne, protoze pokud je na vstupu napr. okres, pak mezistupen pro estimation_cell mezi okresem
						-- a KU uz vlastne neni, toto nastava i mezi OPLO a PLO, takze potom to bude takto:
						_res_estimation_cell_prepocet_simple := _res_estimation_cell_prepocet_simple;

						-- chybejici PLO pro OPLO se zde ale musi doplnit => veresit ve funkci fn_get_gids4under_estimation_cells
					ELSE
						_res_estimation_cell_prepocet_simple := _res_estimation_cell_prepocet_simple || _estimation_cells_mezistupen_i;
					END IF;			
				END IF;	
			END LOOP;
			-----------------------------------------------------
			-----------------------------------------------------

			-----------------------------------------------------
			-- distinct hodnot estimation_cell v promenne
			-- _res_estimation_cell_prepocet_simple
			WITH
			w AS	(SELECT distinct t.ecp FROM (SELECT unnest(_res_estimation_cell_prepocet_simple) AS ecp) AS t)
			SELECT array_agg(w.ecp order by w.ecp) FROM w
			INTO _res_estimation_cell_prepocet_simple;

			-- _res_estimation_cell_prepocet_simple [seznam vsech moznych estimation_cell pro prepocet], obsahuje:
			-- v pripade NUTS => vstupni estimation_cell a muzou to byt i ZSJ 
			-- v pripade OPLO => vstupni estimation_cell, rozdrobene ZSJ aplikace neposle, ale napr. pro OPLO se vrati podrizeni PLOcka

			-- takze jde o seznam => VSTUPNI ESTIMATION_CELL + jejich vsechny mozne [ALL] PODRIZENE ESTIMATION_CELL
			-----------------------------------------------------
			

			-----------------------------------------------------
			-- propojeni _res_estimation_cell_prepocet_simple
			-- s databazi a zjisteni estimation_cell, ktere v
			-- databazi jeste pro dane config a ext_version
			-- nejou vypocitany
			-----------------------------------------------------
			WITH
			w1 AS	(
				SELECT DISTINCT tat.estimation_cell
				FROM @extschema@.t_aux_total AS tat
				WHERE tat.config = _config_id_base
				AND tat.cell IS NULL			-- cell se ukladaji jen pro ZSJ, takze toto zajisti ze ZSJ do INTO nepujdou
				AND tat.estimation_cell IN
					(SELECT unnest(_res_estimation_cell_prepocet_simple))
				AND tat.ext_version < _ext_version_current
				),
			w2 AS	(
				SELECT DISTINCT tat.estimation_cell
				FROM @extschema@.t_aux_total AS tat
				WHERE tat.config = _config_id_base
				AND tat.cell IS NULL
				AND tat.estimation_cell IN
					(SELECT unnest(_res_estimation_cell_prepocet_simple))
				AND tat.ext_version = _ext_version_current
									-- tato podminka je zde proto, ze uzivatel mohl pred tim PREPOCET ZASTAVIT,
									-- tzn. ze cast estimation_cell se uz mohlo pro ext_version_current vypocitat,
									-- takze pak do prepoctu musi jit jen estimation_cell, ktere jeste nemaji verzi
									-- _ext_version_current
				),
			w3 AS	(
				SELECT w1.estimation_cell FROM w1 EXCEPT
				SELECT w2.estimation_cell FROM w2
				)
			SELECT array_agg(w3.estimation_cell) FROM w3
			INTO _res_estimation_cell_prepocet_4_summarization;	-- THIS is A -- CQ 100,200,300,400 a NUTS1-4 nebo OPLO,PLO

			-- jde o seznam POZADOVANYCH ESTIMATION_CELL pro SUMARIZACI u PREPOCTU

			-- u CQ 100	a	NUTS1-4		=> to je v poradku
			-- u CQ 100	a	OPLO,PLO 	=> to je v poradku

			-- u CQ 300	a	NUTS1-4		=> to je v poradku <= CQ 100 uz musi existovat
			-- u CQ 300 	a	OPLO,PLO	=> to je v poradku <= CQ 100 uz musi existovat

			-- tzn. pro kontrolu proveditelnosti CQ 300 musi do cyklu jit _res_estimation_cell_prepocet_4_summarization
			-----------------------------------------------------
			-----------------------------------------------------


			--raise notice '_res_estimation_cell_prepocet_4_summarization:%',_res_estimation_cell_prepocet_4_summarization;
			--raise exception 'STOP';


			-----------------------------------------------------
			-----------------------------------------------------
			-----------------------------------------------------
			-- propojeni seznamu ZSJ s databazi, v pride NUTS1-4
			-- jde o KU, v pripade OPLO,PLO jde o rozdrobene casti
			-- [ze seznamu se musi vyjmout ZSJ, ktere uz jsou ve
			-- shodne ext_version jako je aktualni systemova
			-- verze extenze]

			-- u CQ 100 se ZSJ ukladaji jak pro NUTS1-4 tak pro
			-- OPLO a PLO => takze zde se musi pracovat se
			-- sloupcem cell v tabulce t_aux_total, protoze pro
			-- ZSJ u OPLO,PLO se estimation_cell neuklada

			-- u CQ 200,300,400 a pro NUTS1-4 se ZSJ do t_aux_total
			-- ukladaji => zde se muze pracovat bud se sloupcem cell
			-- nebo se sloupcem estimation_cell
			
			-- u CQ 200,300,400 a pro OPLO,PLO se ZSJ do t_aux_total
			-- NE-ukladaji (proc, jde totiz o ZSJ, ktera sama netvori
			-- celou estimation_cell, jejich aux_total hodnoty se
			-- neukladaji, ukladaji se az jejich sumarizace), zde
			-- se musi pracovat ze sloupce estimation_cell
			-----------------------------------------------------
			IF _config_query = 100
			THEN
				WITH
				w1 AS	(SELECT unnest(_res_gids_prepocet_simple) AS gid),	-- _res_gids_prepocet_simple [ALL KU nebo ALL rozdrobene casti PLO tvorici vstupni estimation_cell pro prepocet]
				w2 AS	(
					SELECT tat.cell
					FROM @extschema@.t_aux_total AS tat
					WHERE tat.config = _config_id_base
					AND tat.cell IN (SELECT w1.gid FROM w1)
					AND tat.ext_version = _ext_version_current
					),
				w3 AS	(
					SELECT
						w1.gid,
						w2.cell
					FROM
						w1 LEFT JOIN w2 ON w1.gid = w2.cell
					)
				SELECT
					array_agg(w3.gid) FROM w3 WHERE w3.cell IS NULL
				INTO
					_res_gids_prepocet_4_recount;	-- THIS is B -- CQ 100 a NUTS1-4 nebo OPLO,PLO
			END IF;

			-- seznam ZSJ [KU nebo rozdrobene casti PLO] POZADOVANE
			-- pro prepocet [jde o gidy, ktere jsou starsi verze nez
			-- je aktualni systemova verze extenze
			-----------------------------------------------------
			-----------------------------------------------------

			
			-----------------------------------------------------
			-----------------------------------------------------
			IF _config_query = ANY(array[200,300,400])
			THEN			
				WITH
				w1 AS	(SELECT unnest(_res_gids_prepocet_simple) AS gid),	-- _res_gids_prepocet_simple [ALL KU nebo ALL rozdrobene casti PLO tvorici vstupni estimation_cell pro prepocet] 
				w2 AS	(-- ALL estimation_cell --				-- vyber estimation_cell z f_a_cell na zaklade gidu (ZSJ)
					SELECT DISTINCT fac.estimation_cell			-- provedeni distinctu estimation_cell
					FROM @extschema@.f_a_cell AS fac
					WHERE fac.gid IN (SELECT w1.gid FROM w1)
					),
					-- vyber (propojeni s DB) estimation_cell co je v DB, zde nejprve nezalezi na verzi ext_version
				w3 AS	(-- DB estimation_cell --
					SELECT tat1.estimation_cell
					FROM @extschema@.t_aux_total AS tat1
					WHERE tat1.config = _config_id_base
					AND tat1.estimation_cell IN (SELECT w2.estimation_cell FROM w2)
					),
				w4 AS	(-- DB estimation_cell + ext_version
					SELECT tat2.estimation_cell
					FROM @extschema@.t_aux_total AS tat2
					WHERE tat2.config = _config_id_base
					AND tat2.estimation_cell IN (SELECT w3.estimation_cell FROM w3)
					AND tat2.ext_version = _ext_version_current
					),
				w5 AS	(
					SELECT
						w3.estimation_cell AS estimation_cell_w3,
						w4.estimation_cell AS estimation_cell_w4
					FROM
						w3 LEFT JOIN w4 ON w3.estimation_cell = w4.estimation_cell
					)
				SELECT
					array_agg(w5.estimation_cell_w3) FROM w5 WHERE w5.estimation_cell_w4 IS NULL
				INTO
					_res_estimation_cell_prepocet_4_recount; -- THIS is C -- CQ 200,300,400 a NUTS1-4 nebo OPLO,PLO
			END IF;

			-- seznam ZSJ [KU nebo cele PLO] kodovanych pres estimation_cell,
			-- ktere jsou POZADOVANY pro prepocet [jde o ZSJ, ktere jsou
			-- starsi verze nez je aktualni systemova verze extenze
			----------------------------------------------------
			----------------------------------------------------
		END IF;
		-----------------------------------------------------
		-----------------------------------------------------
		
		-------------------------------------------
		--raise notice '_res_estimation_cell_prepocet_4_summarization:%',_res_estimation_cell_prepocet_4_summarization;
		--raise notice '_res_gids_prepocet_4_recount:%',_res_gids_prepocet_4_recount;
		--raise notice '_res_estimation_cell_prepocet_4_recount:%',_res_estimation_cell_prepocet_4_recount;
		--raise exception 'STOP';
		-------------------------------------------

		IF _config_query = 100	-- vetev pro zakladni protinani [ZSJ] a sumarizaci
		THEN
			_q :=
				'
				WITH
				---------------------------------------------------------
				w1 AS	(SELECT unnest($8) AS gid),	-- THIS is B
				w2 AS	(
					SELECT
						1 AS step,
						$3 AS config,
						fac.estimation_cell,
						w1.gid
					FROM
						w1
					LEFT
					JOIN	@extschema@.f_a_cell AS fac ON w1.gid = fac.gid -- toto doplni estimation_cell [v aplikaci by se mohlo spojit LABEL a GID]
					),
				---------------------------------------------------------
				w3 AS	(SELECT unnest($9) AS estimation_cell), -- THIS is A
				w4 AS	(
					SELECT
						2 AS step,
						$3 AS config,
						w3.estimation_cell,
						0::integer AS gid
					FROM
						w3
					ORDER
						BY w3.estimation_cell
					),
				w5 AS	(
					SELECT
						w2.step,
						w2.config,
						w2.estimation_cell,
						w2.gid
					FROM
						w2
					ORDER
						BY w2.step, w2.config, w2.gid, w2.estimation_cell
					)
				---------------------------------------------------------
				SELECT * FROM w5 UNION ALL
				SELECT * FROM w4					
				';
			
		ELSE
			-- _config_query = ANY(array[200,300,400,500]) -- vetev pro soucet a doplnky

			-- spojeni _res_estimation_cell_prepocet_4_summarization a _estimation_cell a udelan distinct
			-- proc? vstupni estimation_cell muze obsahovat i estimation_cell ZSJ
			-- _res_estimation_cell_prepocet_4_summarization neobsahuje estimation_cell ZSJ

			-- pridani jeste spojeni _res_estimation_cell_prepocet_4_recount
		
			WITH
			w1 AS	(SELECT unnest(_res_estimation_cell_prepocet_4_summarization) AS estimation_cell), 	-- seznam estimation_cell pro sumarizaci, odpovida co je v DB
			w2 AS	(SELECT unnest(_estimation_cell) AS estimation_cell),					-- vstupni estimation_cell, mohou obsahovat i ZSJ
			w3 AS	(SELECT unnest(_res_estimation_cell_prepocet_4_recount) AS estimation_cell),		-- estimation_cell jako ZSJ, odpovida tomu co je v DB
			w4 AS	(
				SELECT w1.estimation_cell FROM w1
				UNION					-- union provede defakto distinct
				SELECT w2.estimation_cell FROM w2
				UNION
				SELECT w3.estimation_cell FROM w3
				)
			SELECT array_agg(w4.estimation_cell) FROM w4
			INTO _res_estimation_cell_prepocet_4_check;

			-------------------------------------------------------
			-- raise notice '_res_estimation_cell_prepocet_4_check:%',_res_estimation_cell_prepocet_4_check;			
			-------------------------------------------------------

			-- KONTROLA prepocitatelnosti pro CONFIG_QUERY:
			-- 200,300,400	=> jsou v t_aux_total prepocitany data zakladu a odpovidaji posledni verzi
			-- 500		=> je v t_aux_total reference odpovidajici posledni verzi => zde probihaji jen kontroly => vystupem je bud vyjimka nebo 0 radku
		
			-- zjisteni potrebnych promennych z konfigurace pro vetev soucet nebo doplnky
			SELECT
				tc.complete,
				tc.categories
			FROM
				@extschema@.t_config AS tc WHERE tc.id = _config_id
			INTO
				_complete,
				_categories;

			-- raise notice '_complete: %',_complete;
			-- raise notice '_categories: %',_categories;

			-- spojeni konfiguracnich idecek _complete/_categories
			IF _complete IS NULL
			THEN
				_config_ids_text := concat('array[',_categories,']');
			ELSE
				_config_ids_text := concat('array[',_complete,',',_categories,']');
			END IF;

			-- raise notice '_config_ids_text: %',_config_ids_text;

			-- zjisteni poctu idecek tvorici _complete/_categories
			EXECUTE 'SELECT array_length('||_config_ids_text||',1)'
			INTO _config_ids_length;

			-- raise notice '_config_ids_length: %',_config_ids_length;

			-- zjisteni existujicich konfiguracnich idecek pro (_complete/_categories)
			EXECUTE 'SELECT array_agg(tc.id ORDER BY tc.id) FROM @extschema@.t_config AS tc WHERE tc.id in (select unnest('||_config_ids_text||'))'
			INTO _config_ids;

			-- raise notice '_config_ids: %',_config_ids;

			-- splneni podminky stejneho poctu idecek
			IF _config_ids_length = array_length(_config_ids,1)
			THEN
				-- kontrola zda i obsah je stejny
				EXECUTE 'SELECT array_agg(t.ids ORDER BY t.ids) FROM (SELECT unnest('||_config_ids_text||') AS ids) AS t'
				INTO _config_ids4check;

				IF _config_ids4check = _config_ids
				THEN
					-- cyklus kontroly, zda pro soucty/doplnky a referenci jsou vypocitany vsechny potrebne
					-- kategorie a odpovidaji posledni ext_version
					-- pozn. zde si uz muzu sahat do tabulky t_aux_total, protoze pro tento typ vypoctu
					-- uz musi existovat zakladni (protinaci) aux_total hodnoty, respektive musely pro
					-- complete nebo categories probehnout _config_query 100 !!!

					--raise notice '_res_estimation_cell_prepocet_4_check: %',_res_estimation_cell_prepocet_4_check;
					
					FOR i IN 1..array_length(_res_estimation_cell_prepocet_4_check,1)
					LOOP
						FOR y IN 1..array_length(_config_ids,1)
						LOOP
							WITH
							w1 AS	(
								SELECT tat.* FROM @extschema@.t_aux_total AS tat
								WHERE tat.config = _config_ids[y]
								AND tat.estimation_cell = _res_estimation_cell_prepocet_4_check[i]
								AND tat.ext_version = _ext_version_current
								)
							SELECT count(*) FROM w1
							INTO _check_avc4categories;

							IF _check_avc4categories != 1
							THEN		
								IF _config_query = 500
								THEN
									RAISE EXCEPTION 'Chyba 14: fn_get_gids4aux_total: Vstupni _config_id = % je reference na _config_id = %	a pro estimation_cell = % neexistuje v tabulce t_aux_total vypocitana hodnota aux_total v nejaktualnejsi verzi ext_version = %!',_config_id_base,_config_ids[y],_res_estimation_cell_prepocet_4_check[i],_ext_version_current_label;
								ELSE
									RAISE EXCEPTION 'Chyba 15: fn_get_gids4aux_total: Vstupni _config_id = % je soucet nebo doplnek	a pro konfiguraci [z complete/categories] = % a pro estimation_cell = % neexistuje v tabulce t_aux_total vypocitana hodnota aux_total v nejaktualnejsi verzi ext_version = %!',_config_id_base,_config_ids[y],_res_estimation_cell_prepocet_4_check[i],_ext_version_current_label;
								END IF;
							END IF;
						END LOOP;
					END LOOP;
				ELSE
					RAISE EXCEPTION 'Chyba 16: fn_get_gids4aux_total: Pro nakonfigurovane complete = % nebo pro nektere nakonfigurovane categories = % neexistuje konfigurace v tabulce t_config!',_complete,_categories;
				END IF;
			ELSE
				RAISE EXCEPTION 'Chyba 17: fn_get_gids4aux_total: Pro nakonfigurovane complete = % nebo pro nektere nakonfigurovane categories = % neexistuje konfigurace v t_config!',_complete,_categories;
			END IF;

			IF _config_query = 500 -- u reference probihaji jen kontroly a do vystupu se nic nevraci
			THEN
				_q :=	'
					WITH
					w1 AS	(
						SELECT
							1 AS step,
							$3 AS config_id,
							NULL::integer AS estimation_cell,
							NULL::integer AS gid
						)
					SELECT
						w1.step,
						w1.config_id,
						w1.estimation_cell,
						w1.gid
					FROM
						w1 WHERE w1.step = 0;
					';
			ELSE	-- vetev pro config_query 200,300,400 => zde bude promenna _res_estimation_cell_prepocet_4_check, ta obsahuje jak pripadne ZSJ,
				-- tak estimation_cell ze vstupu + estimation_cell podrizene
				_q :=
					'
					WITH
					w1 AS	(SELECT unnest($10) AS estimation_cell),
					w2 AS	(
						SELECT
							1 AS step,
							$3 AS config,
							w1.estimation_cell,
							NULL::integer AS gid
						FROM
							w1
						)
					SELECT
						w2.step,
						w2.config,
						w2.estimation_cell,
						w2.gid
					FROM
						w2
					ORDER
						BY w2.step, w2.config, w2.estimation_cell
					';
			END IF;	
		END IF;		
	ELSE
		-- vetev pro VYPOCET

		--------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------
		-- Tento proces zatim neimplementuju, podle me neni nutny, tim ze to uzivateli pada na
		-- chybe 19, tak podle me spatne zadal geograficke urovne pro vypocet.
		-- proces odstraneni tech vstupnich ZSJ (ktere tvori estimation_cell jen jednou geometrii)
		-- ze vstupni promenne _estimation_cell, pokud _estimation_cell obsahuje jeji vyssi
		-- geografickou jednotku
		--------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------
		-- KONTROLA EXISTENCE v DB --

		-- zde se v databazi kontroluje pritomnost zaznamu, a to
		-- pro i-tou estimation_cell a pro vsechny config_id dane
		-- "konfigurace" => pokud tato podminka je splnena, pak
		-- nastane vyjimka => uzivatel toto uz v aplikaci zadat nesmi

		-- tato kontrola je zde z duvodu, kdy uzivatel zastavi vypocet,
		-- verze extenze je zde nutna a jednotlive kategorie konfigurace
		-- musi odpovidat _ext_version_current
		
		FOR i IN 1..array_length(_estimation_cell,1)
		LOOP
			FOR ii IN 1..array_length(_configs,1)
			LOOP
				SELECT tc.config_query FROM @extschema@.t_config AS tc
				WHERE tc.id = _configs[ii]
				INTO _config_query_ii;
				
				IF _config_query_ii = 500 -- REFERENCE
				THEN
					SELECT tc.categories::integer
					FROM @extschema@.t_config AS tc
					WHERE tc.id = _configs[ii]
					INTO _config_id_reference_ii;

					IF _config_id_reference_ii IS NULL
					THEN
						RAISE EXCEPTION 'Chyba 18: fn_get_gids4aux_total_app: Kontrolovane _config_id = % je reference [config_query = 500] a v konfiguraci [v tabulce t_config] nema ve sloupci categories nakonfigurovanou referenci!',_configs[ii];
					END IF;

					_config_id_base_ii := _config_id_reference_ii;
				ELSE
					_config_id_base_ii := _configs[ii]; -- vstupnim config_id je bud konfigurace ze zakladni skupiny [100,300,400] nebo konfigurace z agregacni skupiny ci navazujici kolekce [200,300,400]
				END IF;

				WITH
				w1 AS	(
					SELECT tat.* FROM @extschema@.t_aux_total AS tat
					WHERE tat.estimation_cell = _estimation_cell[i]
					AND tat.config = _config_id_base_ii
					AND tat.ext_version = _ext_version_current
					)
				SELECT count(*) FROM w1	-- pokud je pocet vetsi jak 0 tak zaznam(y) exisuje(i)
				INTO _check_ii;

				IF _check_ii > 0
				THEN
					IF ii = 1
					THEN
						_check_ii_boolean := array[0];
					ELSE
						_check_ii_boolean := _check_ii_boolean || array[0];
					END IF;
				ELSE
					IF ii = 1
					THEN
						_check_ii_boolean := array[1];
					ELSE
						_check_ii_boolean := _check_ii_boolean || array[1];
					END IF;
				END IF;

			END LOOP;

			IF	(
				SELECT sum(t.check_ii_boolean) = 0
				FROM (SELECT unnest(_check_ii_boolean) AS check_ii_boolean) AS t
				)
			THEN
				RAISE EXCEPTION 'Chyba 19: fn_get_gids4aux_total_app: Vypocet neni mozny => pro zadanou estimation_cell = % jiz v tabulce t_aux_total existuji hodnoty aux_total pro vsechny kategorie dane konfigurace (config_collection = %) ve verzi %! Vstupni seznam estimation_cell: %.',_estimation_cell[i],_config_collection,_ext_version_current_label,_estimation_cell;
			END IF;

		END LOOP;
		--------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------
		-- CONFIG_ID_BASE
		IF _config_query = 500 -- REFERENCE
		THEN
			SELECT tc.categories::integer
			FROM @extschema@.t_config AS tc
			WHERE tc.id = _config_id
			INTO _config_id_reference;

			IF _config_id_reference IS NULL
			THEN
				RAISE EXCEPTION 'Chyba 20: fn_get_gids4aux_total_app: Vstupni _config_id = % je reference [config_query = 500] a v konfiguraci [v tabulce t_config] nema nakonfigurovanou referenci!',_config_id;
			END IF;

			_config_id_base := _config_id_reference;
		ELSE
			_config_id_base := _config_id; -- vstupnim config_id je bud konfigurace ze zakladni skupiny [100,300,400] nebo konfigurace z agregacni skupiny ci navazujici kolekce [200,300,400]
		END IF;
		--------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------
		-- KONTROLA proveditelnosti pro "VSTUPNI" CONFIG_QUERY [200,300,400,500]:
		-- 200,300,400	=> jsou v t_aux_total vypocitany data zakladu v current ext_version
		-- 500		=> je v t_aux_total reference odpovidajici current ext_version => zde probihaji jen kontroly => vystupem je bud vyjimka nebo 0 radku
		IF _config_query = ANY(array[200,300,400,500]) -- vetev pro soucet a doplnky
		THEN
			-- zjisteni potrebnych promennych z konfigurace pro vetev soucet nebo doplnky
			SELECT
				tc.complete,
				tc.categories
			FROM
				@extschema@.t_config AS tc WHERE tc.id = _config_id_base
			INTO
				_complete,
				_categories;

			-- raise notice '_complete: %',_complete;
			-- raise notice '_categories: %',_categories;

			-- spojeni konfiguracnich idecek _complete/_categories
			IF _complete IS NULL
			THEN
				_config_ids_text := concat('array[',_categories,']');
			ELSE
				_config_ids_text := concat('array[',_complete,',',_categories,']');
			END IF;

			-- raise notice '_config_ids_text: %',_config_ids_text;

			-- zjisteni poctu idecek tvorici _complete/_categories
			EXECUTE 'SELECT array_length('||_config_ids_text||',1)'
			INTO _config_ids_length;

			-- raise notice '_config_ids_length: %',_config_ids_length;

			-- zjisteni existujicich konfiguracnich idecek pro (_complete/_categories)
			EXECUTE 'SELECT array_agg(tc.id ORDER BY tc.id) FROM @extschema@.t_config AS tc WHERE tc.id in (select unnest('||_config_ids_text||'))'
			INTO _config_ids;

			-- raise notice '_config_ids: %',_config_ids;

			-- splneni podminky stejneho poctu idecek
			IF _config_ids_length = array_length(_config_ids,1)
			THEN
				-- kontrola zda i obsah je stejny
				EXECUTE 'SELECT array_agg(t.ids ORDER BY t.ids) FROM (SELECT unnest('||_config_ids_text||') AS ids) AS t'
				INTO _config_ids4check;

				IF _config_ids4check = _config_ids
				THEN
					-- cyklus kontroly, zda pro soucty/doplnky a referenci jsou vypocitany vsechny potrebne
					-- kategorie a odpovidaji aktualni verzi ext_version
					-- pozn. zde si uz muzu sahat do tabulky t_aux_total, protoze pro tento typ vypoctu
					-- uz musi existovat zakladni (protinaci) aux_total hodnoty, respektive musely pro
					-- complete nebo categories probehnout _config_query 100 !!!

					-- raise notice '_estimation_cell: %',_estimation_cell;
					
					FOR i IN 1..array_length(_estimation_cell,1)
					LOOP
						FOR y IN 1..array_length(_config_ids,1)
						LOOP
							WITH
							w1 AS	(
								SELECT tat.* FROM @extschema@.t_aux_total AS tat
								WHERE tat.config = _config_ids[y]
								AND tat.estimation_cell = _estimation_cell[i]
								AND tat.ext_version = _ext_version_current
								)
							SELECT count(*) FROM w1
							INTO _check_avc4categories;

							IF _check_avc4categories != 1
							THEN		
								IF _config_query = 500
								THEN
									RAISE EXCEPTION 'Chyba 21: fn_get_gids4aux_total: Vstupni _config_id = % je reference na _config_id = %	a pro estimation_cell = % neexistuje v tabulce t_aux_total vypocitana hodnota aux_total	v aktulani verzi ext_version = %!',_config_id_base,_config_ids[y],_estimation_cell[i],_ext_version_current_label;
								ELSE
									RAISE EXCEPTION 'Chyba 22: fn_get_gids4aux_total: Vstupni _config_id = % je soucet nebo doplnek a pro konfiguraci [z complete/categories] = % a pro estimation_cell = % neexistuje v tabulce t_aux_total vypocitana hodnota aux_total v aktualni verzi ext_version = %!',_config_id_base,_config_ids[y],_estimation_cell[i],_ext_version_current_label;
								END IF;
							END IF;
						END LOOP;
					END LOOP;
				ELSE
					RAISE EXCEPTION 'Chyba 23: fn_get_gids4aux_total: Pro nakonfigurovane complete = % nebo pro nektere nakonfigurovane categories = % neexistuje konfigurace v t_config!',_complete,_categories;
				END IF;
			ELSE
				RAISE EXCEPTION 'Chyba 24: fn_get_gids4aux_total: Pro nakonfigurovane complete = % nebo pro nektere nakonfigurovane categories = % neexistuje konfigurace v t_config!',_complete,_categories;
			END IF;
		END IF;
		--------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------

		IF _config_query = ANY(array[100,200,300,400])
		THEN
			-- zde je implementovan proces ziskani zakladnich stavebnich jednotek (ZSJ)
			---------------------------------------------------------------------------
				
			-- cyklus pro ziskani POCTu
			FOR i IN 1..array_length(_estimation_cell,1)
			LOOP
				-- zjisteni zakladnich stavebnich jednotek (gidu) z tabulky f_a_cell pro i-tou _estimation_cell
				SELECT array_agg(fac.gid ORDER BY fac.gid) FROM @extschema@.f_a_cell AS fac
				WHERE fac.estimation_cell = _estimation_cell[i]
				INTO _gids4estimation_cell_i;

				IF _gids4estimation_cell_i IS NULL
				THEN
					RAISE EXCEPTION 'Chyba 25: fn_get_gids4aux_total_app: Pro _estimation_cell[i] = % nenalezeny zadne zaznamy v tabulce f_a_cell!',_estimation_cell[i];
				END IF;

				-- volani funkce fn_get_lowest_gids [funkce si saha do mapovaci tabulky cm_f_a_cell]
				-- funkce by mela vratit seznam zakladnich stavebnich jednotek (gidu), ktere tvori zadany seznam vyssich gidu
				-- v tomto cyklu ale nejprve pozaduju pocet gidu
				_gids4estimation_cell_pocet := array_length(@extschema@.fn_get_lowest_gids_app(_gids4estimation_cell_i),1);

				IF i = 1
				THEN
					_pocet := array[_gids4estimation_cell_pocet];
				ELSE
					_pocet := _pocet || array[_gids4estimation_cell_pocet];
				END IF;
				
			END LOOP;

			-- zjisteni nejdelsiho pole _gids4estimation_cell
			SELECT max(t.pocet) FROM (SELECT unnest(_pocet) as pocet) AS t
			INTO _max_pocet;

			-- cyklus ziskani GIDu
			FOR i IN 1..array_length(_estimation_cell,1)
			LOOP
				-- zjisteni gidu z tabulky f_a_cell pro i-tou _estimation_cell
				SELECT array_agg(fac.gid ORDER BY fac.gid) FROM @extschema@.f_a_cell AS fac
				WHERE fac.estimation_cell = _estimation_cell[i]
				INTO _gids4estimation_cell_i;

				-- volani funkce fn_get_lowest_gids
				-- funkce by mela vratit seznam gidu nejnizsi urovne, ktere tvori zadany seznam vyssich gidu
				_gids4estimation_cell := @extschema@.fn_get_lowest_gids_app(_gids4estimation_cell_i);

				-- zjisteni poctu prvku(gidu) v poli _gids4estimation_cell
				_pocet_gids4estimation_cell := array_length(_gids4estimation_cell,1);

				-- vytvoreni pole s nulama o poctu _max_pocet - _pocet_gids4estimation_cell
				SELECT array_agg(t.gid_gs) FROM (SELECT 0 AS gid_gs FROM generate_series(1,(_max_pocet - _pocet_gids4estimation_cell))) AS t
				INTO _doplnek;

				-- proces doplneni 0 tak aby pole _gids4estimation_cell melo delku _max_pocet
				_gids4estimation_cell := _gids4estimation_cell || _doplnek;

				IF i = 1
				THEN
					_res_estimation_cell := array[array[_estimation_cell[i]]];
					_res_gids := array[_gids4estimation_cell];
				ELSE
					_res_estimation_cell := _res_estimation_cell || array[array[_estimation_cell[i]]];
					_res_gids := _res_gids || array[_gids4estimation_cell];
				END IF;

			END LOOP;
		END IF;

		--------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------

		-- po sem mam:
			-- _res_estimation_cell => pole poli vstupnich estimation_cell co si uzivatel zvolil [muze
			-- jit o estimation_cell z ruznych geografickych urovni a muzou se zde vyskytovat estimation_cell jako ZSJ]
			-- _res_gids => seznam zakladnich stavebnich jednotek (gidu) pro zadane estimation_cell [pozor gidy se zde mohou duplikovat]

		--------------------------------------------------------------------------------------------
		--raise notice '_res_estimation_cell: %',_res_estimation_cell;
		--raise notice '_res_gids: %',_res_gids;
		--------------------------------------------------------------------------------------------
		--raise notice '_unnest_text:%',_unnest_text;
		--------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------
		-- sestaveni vysledneho textu pro _config_query
		IF _config_query = 100	-- vetev pro zakladni (protinani)
		THEN
			-- kontrola ze pocet prvku v poli poli _res_estimation_cell se rovna poctu prvku v poli poli _res_gids
			IF (array_length(_res_estimation_cell,1) != array_length(_res_gids,1))
			THEN
				RAISE EXCEPTION 'Chyba 26: fn_get_gids4aux_total_app: Pocet prvku v interni promenne _res_estimation_cell neodpovida poctu prvku v interni promenne _res_gids!';
			END IF;

			-- zjisteni poctu prvku v jednotlivych prvcich u _res_gids [u multi array je vsude stejny pocet]
			SELECT array_length(_res_gids,2)
			INTO _res_gids_count_count;		

			-- proces sestaveni _res_estimation_cell_4_with do return query
			FOR i IN 1..array_length(_res_estimation_cell,1)
			LOOP
				WITH
				w AS	(
					SELECT
						unnest(_res_estimation_cell[i:i]) as estimation_cell,
						generate_series(1,_res_gids_count_count)
					)
				SELECT array_agg(w.estimation_cell) FROM w
				INTO _res_estimation_cell_4_with_i;

				IF i = 1
				THEN
					_res_estimation_cell_4_with := array[_res_estimation_cell_4_with_i];
				ELSE
					_res_estimation_cell_4_with := _res_estimation_cell_4_with || array[_res_estimation_cell_4_with_i];
				END IF;
			END LOOP;
			--------------------------------------------------------------------------------------------
			-- proces zjisteni k jednotlivym _res_estimation_cell techto informaci:
			-- je-li estimation_cell zakladni stavebni jednotkou nebo neni
			-- je-li estimation_cell zakladni stavebni jednotkou a je nutna jeste jeji sumarizace,
			WITH
			w1 AS	(
				SELECT unnest(_res_estimation_cell) AS estimation_cell
				),
			w2 AS	(	
				SELECT
					w1.estimation_cell,
					t1.estimation_cell_collection,
					t2.estimation_cell_collection_lowest
				FROM
						w1
				INNER JOIN 	@extschema@.c_estimation_cell 			AS t1	ON w1.estimation_cell = t1.id
				INNER JOIN	@extschema@.cm_estimation_cell_collection	AS t2	ON t1.estimation_cell_collection = t2.estimation_cell_collection
				),
			w3 AS	(
				SELECT
					w2.*,
					CASE
					WHEN w2.estimation_cell_collection = w2.estimation_cell_collection_lowest
					THEN TRUE ELSE FALSE
					END AS estimation_cell_w4
				FROM
					w2
				),
			w4 AS	(
				select
					fac.estimation_cell,
					count(fac.gid) as count_gids
				from @extschema@.f_a_cell AS fac
				where fac.estimation_cell in	(
								SELECT w1.estimation_cell FROM w1
								)
				group by fac.estimation_cell
				),
			w5 AS	(
				SELECT
					w3.*,
					w4.count_gids
				FROM
					w3 LEFT JOIN w4 ON w3.estimation_cell = w4.estimation_cell
				),
			w6 AS	(
				SELECT
					w5.*,
					CASE
						WHEN w5.estimation_cell_w4 = FALSE THEN TRUE
						ELSE
							CASE
							WHEN w5.count_gids > 1 THEN TRUE
							ELSE FALSE
							END
					END AS summarization
				FROM
					w5
				)	
			SELECT
				array_agg(w6.estimation_cell ORDER BY w6.estimation_cell),
				array_agg(w6.summarization ORDER BY w6.estimation_cell)
			FROM
				w6
			INTO
				_res_estimation_cell_summarization,
				_summarization;
			--------------------------------------------------------------------------------------------
			_q := 	'
				WITH
				---------------------------------------------------------------------------
				---------------------------------------------------------------------------
				-- tato cast je plny (i s duplicitama) seznam gidu zakladnich stavebnich jednotek,
				-- ktery tvori jednotlive pozadovane _estimation_cell[]
				w1 AS	(
					SELECT
						$11 AS estimation_cell,	-- jde o pole poli => identifikace estimation_cell ze vstupu teto funkce [muze obsahovat estimation_cell i ZSJ]
						$2 AS gid		-- jde o pole poli => seznam gidu (ZSJ) tvorici danou estimation_cell
					),
				w2 AS 	(
					SELECT
						unnest(estimation_cell) AS estimation_cell,
						unnest(gid) AS gid
					FROM
						w1
					),
				w3 AS	(SELECT * FROM w2 WHERE gid IS DISTINCT FROM 0),	-- zde je roz-unnestovan with w1 a odstraneny nulove radky
				---------------------------------------------------------------------------
				-- pripojeni informace summarization k estimation_cell
				w4 AS	(
					SELECT
						unnest($5) AS estimation_cell_summarization,
						unnest($6) AS summarization
					),
				w5 AS	(
					SELECT
						w3.estimation_cell,
						w3.gid,
						w4.summarization
					FROM
						w3 LEFT JOIN w4 ON w3.estimation_cell = w4.estimation_cell_summarization
					),
				---------------------------------------------------------------------------
				-- tato cast je vyber zakladnich stavebnich jednotek (gidu), ktere jsou
				-- doposud vypocitany v t_aux_total a odpovidaji aktualni verzi _ext_version_current
				-- ty, ktere neodpovidaji se hold prepocitaji i za cenu, ze povysenim verze extenze
				-- nedoslo k upravam ve vypocetni oblasti vyvoje
				w9 AS 	(
					SELECT * FROM @extschema@.t_aux_total
					WHERE config = $3
					AND cell IS NOT NULL					-- ve sloupci cell se ukladaji jen gidy nejnizsi urovne z f_a_cell 
					AND cell IN (SELECT DISTINCT gid FROM w3)
					AND ext_version = $7		
					),
				---------------------------------------------------------------------------
				-- tato cast je pripojeni withu w9 k withu w5
				w10 AS	(
					SELECT
						w5.estimation_cell,
						w5.gid,			-- gid nejnizsi urovne, ktery tvori danou estimation_cell
						w5.summarization,			
						w9.cell			-- gid nejsizsi urovne, ktery je doposud vypocitan v t_aux_total
					FROM
						w5
					LEFT
					JOIN	w9	ON w5.gid = w9.cell
					),
				---------------------------------------------------------------------------
				-- tato cast rika, zda gid nejnizsi urovne je pro danou estimation_cell[i] jiz vypocitan nebo nikoliv
				-- pro danou estimation_cell => gidy s calculated vsude TRUE		=> vsechny pozadovane gidy uz jsou v DB vypocitany
				-- pro danou estimation_cell => gidy s calculated vsude FALSE		=> zadny z pozadovanych gidu jeste neni v DB vypocitan
				-- pro danou estimation_cell => gidy s calculated TRUE nebo FALSE	=> z pozadovanych gidu uz nektere v DB vypocitany jsou a nektere ne
				w11 AS	(
					SELECT
						$3 AS config_id,
						w10.estimation_cell,			-- vstupni hodnota i-te estimation_cell
						w10.gid,				-- gid nejnizsi urovne tvorici danou i-tou estimation_cell
						w10.summarization,

						CASE
						WHEN w10.cell IS NOT NULL THEN TRUE	-- toto plati je-li gid nejnizsi urovne pro danou i-tou estimation_cell je jiz vypocitan
						ELSE FALSE				-- toto plati je-li gid nejnizsi urovne pro danou i-tou estimation_cell neni jeste vypocitan
						END AS calculated
					FROM
						w10
					),
				---------------------------------------------------------------------------
				---------------------------------------------------------------------------
				-- vyber zakladnich stavebnich jednotek pro vypocet
				w12 AS	(SELECT DISTINCT gid FROM w11 WHERE calculated = FALSE ORDER BY gid),
				w13 AS	(
					SELECT
						1 AS step,
						$3 AS config_id,
						fac.estimation_cell,
						w12.gid
					FROM
						w12
					---------------------------------------------------------------------
					LEFT JOIN	$extschema$.f_a_cell AS fac ON w12.gid = fac.gid	-- toto doplnuje zpatky estimation_cell [v aplikaci by se mohlo spojit LABEL a GID]
					---------------------------------------------------------------------
					ORDER
						BY w12.gid
					),
				---------------------------------------------------------------------------
				---------------------------------------------------------------------------
				-- ted reseni SUMARIZACE => bude se provadet vzdy
				---------------------------------------------------------------------------
				w14 AS	(SELECT DISTINCT estimation_cell FROM w11 WHERE summarization = TRUE),
				w15 AS	( -- napojeni na DB
					SELECT
						tat.estimation_cell
					FROM
						@extschema@.t_aux_total AS tat
					WHERE
						tat.config = $3
					AND
						tat.cell IS NULL
					AND
						tat.estimation_cell IN (SELECT w14.estimation_cell FROM w14)
					AND
						tat.ext_version = $7
					),
				w16 AS	(
					SELECT
						w14.estimation_cell,
						CASE
						WHEN w15.estimation_cell IS NOT NULL THEN TRUE	-- estimation_cell, ktera uz je v DB sumarizovana v current verzi
						ELSE FALSE					-- estimation_cell, kterou je nutno do-sumarizovat
						END AS calculated
					FROM
						w14
					LEFT
					JOIN	w15	ON w14.estimation_cell = w15.estimation_cell
					),
				w17 AS	(
					SELECT
						2 AS step,
						$3 AS config_id,
						w16.estimation_cell,
						0::integer AS gid
					FROM
						w16
					WHERE
						w16.calculated = FALSE
					
					ORDER
						BY w16.estimation_cell
					)
				---------------------------------------------------------------------------
				---------------------------------------------------------------------------	
				SELECT * FROM w13	-- => VZDY NUTNY DOPLNEK
				UNION ALL
				SELECT * FROM w17	-- => VZDY NUTNA SUMMARIZACE
				---------------------------------------------------------------------------			
				';
		ELSE
			IF _config_query = 500
			THEN
				_q :=	'
					WITH
					w1 AS	(
						SELECT
							1 AS step,
							$3 AS config_id,
							NULL::integer AS estimation_cell,
							NULL::integer AS gid
						)
					SELECT
						w1.step,
						w1.config_id,
						w1.estimation_cell,
						w1.gid
					FROM
						w1 WHERE w1.step = 0;
					';
			ELSE
				-- vetev pro config_query [200,300,400] => soucet a doplnky

				_q :=	'
					WITH
					w1 AS	(SELECT unnest($2) AS gids),	-- jde o seznam gidu (ZSJ), ktere tvori vstupni estimation_cell [vyskyt duplicitnich gidu]
					w2 AS	(SELECT DISTINCT gids FROM w1),	-- z-unikatneni gidu (ZSJ)
					w3 AS	(				-- vyber zaznamu z f_a_cell pro unikatni gidy (ZSJ)
						SELECT
							fac.gid,
							fac.estimation_cell
						FROM
							@extschema@.f_a_cell AS fac
						WHERE
							fac.gid IN (SELECT gids FROM w2)
						),
					w4 AS	(				-- zjisteni poctu gidu tvorici estimation_cell
						SELECT
							w3.estimation_cell,
							count(w3.gid) AS pocet
						FROM
							w3 GROUP BY w3.estimation_cell
						),
					w5 AS	(				-- vyber ZSJ, ktere tvori estimation_cell jen jednou geometrii
						SELECT w4.estimation_cell FROM w4
						WHERE w4.pocet = 1
						),
					w6 AS	(SELECT unnest($4) AS estimation_cell),	-- roz-unnestovani vstupnich estimation_cell
					w7 AS	(					-- odstraneni vstupnich estimation_cell z w5.estimation_cell
						SELECT w5.estimation_cell FROM w5 EXCEPT
						SELECT w6.estimation_cell FROM w6	-- vysledkem jsou estimation_cell (ZSJ) pro DOPLNENI
						),
					w8 AS	(				-- zjisteni gidu (ZSJ) pres estimation_cell co je opravdu v DB pro danou ext_version_current
						SELECT				-- u (ZSJ) je estimation_cell vzdy vyplneno pokud je estimation_cell tvoreno jednou geometrii
							tat.estimation_cell
						FROM
							@extschema@.t_aux_total AS tat
						WHERE
							config = $3
						AND
							tat.ext_version = $7
						AND
							tat.estimation_cell IN (SELECT w7.estimation_cell FROM w7)
						),
					w9 AS	(
						SELECT
							$3 AS config_id,
							w7.estimation_cell,
						
							CASE
							WHEN w8.estimation_cell IS NOT NULL THEN TRUE	-- toto plati je-li estimation_cell nejnizsi urovne pro danou ext_version_current => jiz vypocitano
							ELSE FALSE					-- toto plati je-li estimation_cell nejnizsi urovne pro danou ext_version_current => neni jeste vypocitano
							END AS calculated
						FROM
							w7 LEFT JOIN w8 ON w7.estimation_cell = w8.estimation_cell					
						),
					w10 AS	(				-- 1. cast do UNIONu
						SELECT
							1 AS step,
							w9.config_id,
							w9.estimation_cell,
							NULL::integer AS gid
						FROM
							w9 WHERE w9.calculated = FALSE
						ORDER
							BY w9.estimation_cell
						),
					-------------------------------------------------
					w11 AS	(
						SELECT
							2 AS step,
							$3 AS config_id,
							$4 AS estimation_cell,
							NULL::integer AS gid
						),
					w12 AS	(	
						SELECT
							w11.step,
							w11.config_id,
							unnest(w11.estimation_cell) AS estimation_cell,
							w11.gid
						FROM
							w11
						),
					w13 AS	( -- napojeni na DB
						SELECT
							tat.estimation_cell
						FROM
							@extschema@.t_aux_total AS tat
						WHERE
							tat.config = $3
						AND
							tat.cell IS NULL
						AND
							tat.estimation_cell IN (SELECT w12.estimation_cell FROM w12)
						AND
							tat.ext_version = $7
						),
					w14 AS	(

						SELECT
							w12.step,
							w12.config_id,
							w12.estimation_cell,
							w12.gid,
							CASE
							WHEN w13.estimation_cell IS NOT NULL THEN TRUE	-- estimation_cell, ktera uz je v DB sumarizovana v current verzi
							ELSE FALSE					-- estimation_cell, kterou je nutno do-sumarizovat
							END AS calculated
						FROM
							w12
						LEFT
						JOIN	w13	ON w12.estimation_cell = w13.estimation_cell
						),						
					w15 AS	(				-- 2. cast do UNIONu
						SELECT
							w14.step,
							w14.config_id,
							w14.estimation_cell,
							w14.gid
						FROM
							w14
						WHERE
							w14.calculated = FALSE
						ORDER
							BY w14.estimation_cell
						)
					-------------------------------------------------------
					SELECT w10.* FROM w10 UNION ALL
					SELECT w15.* FROM w15
					';
			END IF;
		END IF;
	END IF;
	--------------------------------------------------------------------------------------------
	RETURN QUERY EXECUTE ''||_q||''
	USING
		_res_estimation_cell,				-- $1
		_res_gids,					-- $2
		_config_id,					-- $3
		_estimation_cell,				-- $4
		_res_estimation_cell_summarization,		-- $5
		_summarization,					-- $6
		_ext_version_current,				-- $7
		_res_gids_prepocet_4_recount,			-- $8
		_res_estimation_cell_prepocet_4_summarization,	-- $9
		_res_estimation_cell_prepocet_4_check,		-- $10
		_res_estimation_cell_4_with;			-- $11
	--------------------------------------------------------------------------------------------	
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


ALTER FUNCTION @extschema@.fn_get_gids4aux_total_app(integer,integer[],integer,boolean) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_gids4aux_total_app(integer,integer[],integer,boolean) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_gids4aux_total_app(integer,integer[],integer,boolean) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_gids4aux_total_app(integer,integer[],integer,boolean) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_gids4aux_total_app(integer,integer[],integer,boolean) IS
'Funkce vraci seznam vypocetnich udaju pro funkci fn_get_aux_total_app.';

-- </function>


-- <function name="fn_get_gids4under_estimation_cells_app" schema="extschema" src="functions/extschema/fn_get_gids4under_estimation_cells_app.sql">

-- DROP FUNCTION @extschema@.fn_get_gids4under_estimation_cells_app(integer[],integer[]);

CREATE OR REPLACE FUNCTION @extschema@.fn_get_gids4under_estimation_cells_app
(
	_gids4check	integer[],
	_gids		integer[]
)
RETURNS integer[] AS
$BODY$
DECLARE
	_cell_4check		integer[];
	_count_check		integer;
	_gids4next_step		integer[];
	_res_gids		integer[];
	_gids4check_original	integer[];
	_gids4check_except	integer[];
BEGIN
	-- pro vstupni _gids se zjisti jejich podrizene gidy
	SELECT array_agg(cmfac.cell)
	FROM @extschema@.cm_f_a_cell AS cmfac
	WHERE cmfac.cell_sup IN (SELECT unnest(_gids))
	INTO _cell_4check;

	WITH
	w1 AS	(SELECT unnest(_gids4check) AS gids4check),
	w2 AS	(SELECT unnest(_gids) AS gids)
	SELECT count(w1.*) FROM w1
	WHERE w1.gids4check IN (SELECT w2.gids FROM w2)
	INTO _count_check;

	-- pokud se se prichozi gidy z postupne prichozich estimation_cell
	-- stale nevyskytuji v seznamu gidu ZSJ, pak se tato funkce vola
	-- opakovane, stim, ze seznam prichozich gidu se rozsiruje o seznam
	-- gidu prodrizenych, tedy jde o spojeni vstupniho _gids a interniho
	-- seznamu _cell_4check

	-- pokud je promenna _count_check > 0, pak to znamena, ze hledani
	-- podrizenych gidu je ukonceno a do vystupu se vraci seznam gidu
	-- kdy se ze seznamu vstupnich _gids exceptem odstrani seznam gidu
	-- _gids4check

	-- vyjimka zde ale nastava u gidu [ZSJ], ktera sama osobne netvori
	-- celou jeji estimation_cell

	IF _count_check = 0
	THEN
		_gids4next_step := _gids || _cell_4check;
		_res_gids := @extschema@.fn_get_gids4under_estimation_cells_app(_gids4check,_gids4next_step);
	ELSE	
		_gids4check_original := _gids4check;
		_gids4check_except := _gids4check;
		
		-- pokud se v seznamu _gids4check vyskytuji gidy ZSJ, ktere sami o sobe
		-- netvori celou estimation_cell, se pak ze seznamu _gids4check musi odstranit,
		-- aby se tyto gidy ze seznamu _gids pak neodstranili

		FOR i IN 1..array_length(_gids4check,1)
		LOOP
			IF	(
				SELECT count(fac2.gid) > 1
				FROM @extschema@.f_a_cell AS fac2
				WHERE fac2.estimation_cell =
					(
					SELECT fac1.estimation_cell
					FROM @extschema@.f_a_cell AS fac1
					WHERE fac1.gid = _gids4check[i]
					)
				)
			THEN
				-- gid se ze seznamu _gids4check_original musi odstranit
				_gids4check_except := array_remove(_gids4check_except,_gids4check[i]);				
			ELSE
				-- gid se v seznamu _gids4check_original ponecha
				_gids4check_except := _gids4check_except;
			END IF;
		END LOOP;
	
		WITH
		w1 AS	(SELECT unnest(_gids || _gids4check_original) AS gids),
		w2 AS	(SELECT unnest(_gids4check_except) AS gids),
		w3 AS	(
			SELECT w1.gids FROM w1 except
			SELECT w2.gids FROM w2
			)
		SELECT array_agg(w3.gids) FROM w3
		INTO _res_gids;
	END IF;

	RETURN _res_gids;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;

ALTER FUNCTION @extschema@.fn_get_gids4under_estimation_cells_app(integer[],integer[]) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_gids4under_estimation_cells_app(integer[],integer[]) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_gids4under_estimation_cells_app(integer[],integer[]) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_gids4under_estimation_cells_app(integer[],integer[]) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_gids4under_estimation_cells_app(integer[],integer[]) IS
'Funkce pro zadane gidy (druhy vstupni argument funkce _gids), ktere tvori estimation_cell, vraci gidy za vsechny podrizene estimation_cell. Vracene gidy jsou cizim klicem na pole gid do tabulky f_a_cell.';

-- </function>


-- <function name="fn_get_lowest_gids_app" schema="extschema" src="functions/extschema/fn_get_lowest_gids_app.sql">
---------------------------------------------------------------------------------------------------
-- fn_get_lowest_gids_app
---------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION @extschema@.fn_get_lowest_gids_app
(
	_appropriate_gids	integer[]	-- vstupem je gid(gidy) nejake urovne [gid odpovida gid z f_a_cell] a hledaji se gidy az te nejnizsi urovne, ktere tvori zadany vstupni gid 
)
RETURNS integer[] AS
$BODY$
DECLARE
	_res_1	integer[];
	_res	integer[];
BEGIN
	IF _appropriate_gids IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_lowest_gids_app: Vstupni argument _appropriate_gids nesmi byt NULL !';
	END IF;

	SELECT array_agg(cell order by cell) FROM @extschema@.cm_f_a_cell
	WHERE cell_sup IN (SELECT unnest(_appropriate_gids))
	INTO _res_1;

	IF _res_1 IS NULL
	THEN
		_res := _appropriate_gids;
	ELSE
		_res := @extschema@.fn_get_lowest_gids_app(_res_1);
	END IF;

	RETURN _res;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;

ALTER FUNCTION @extschema@.fn_get_lowest_gids_app(integer[]) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_lowest_gids_app(integer[]) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_lowest_gids_app(integer[]) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_lowest_gids_app(integer[]) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_lowest_gids_app(integer[]) IS
'Funkce vraci gidy nejnizsi urovne, a to pro zadany gid (zadany seznam gidu). Vracene gidy jsou cizim klicem na pole gid do tabulky f_a_cell.';
---------------------------------------------------------------------------------------------------
-- </function>


-- <function name="fn_get_r_ndsm_smooth_rids" schema="extschema" src="functions/extschema/fn_get_r_ndsm_smooth_rids.sql">
--drop function @extschema@.fn_get_r_ndsm_smooth_rids(integer[], integer)

CREATE OR REPLACE FUNCTION @extschema@.fn_get_r_ndsm_smooth_rids( 
	_rids integer[], _syear integer
)
RETURNS TABLE(
	rid 	integer
) AS
$BODY$
DECLARE
	_syear_plan		integer;	-- rok snimkovani v planu snimkovani
	_x			integer;	-- pomocne cislo pro ziskani finalni souradnice x
	_q 			text;
BEGIN
	-- volba roku planu setreni, ktery navazuje spojite na rok setreni, ktery se prave hladi (kvuli okrajovym pixelum)
	-- rok 2010 zatim nelze, je uprostred a je potreba u nej vyresit dva okraje (s vychodem a zapadem)
	-- take se zde urci souradnice x a y v okrajovych tilech, pro snimkovani na zapade jsou okrajove souradnice 9 a 10
	-- pro snimkovani na vychode jsou okrajove souradnice 1 a 2
	CASE
		WHEN _syear = 2009 THEN _syear_plan := 2010; _x := 0;	-- snimek je na vychode, souradnice x pak bude napr. abs(0 - 1) = 1
		WHEN _syear = 2011 THEN _syear_plan := 2010; _x := 11;	-- snimek je na zapade, souradnice x pak bude napr. abs(11 - 2) = 9
		WHEN _syear = 2012 THEN _syear_plan := 2013; _x := 0;
		WHEN _syear = 2013 THEN _syear_plan := 2012; _x := 11;
		WHEN _syear = 2014 THEN _syear_plan := 2013; _x := 0;
		WHEN _syear = 2015 THEN _syear_plan := 2014; _x := 11;
		WHEN _syear = 2016 THEN _syear_plan := 2015; _x := 0;
		WHEN _syear = 2017 THEN _syear_plan := 2016; _x := 11;
		WHEN _syear = 2018 THEN _syear_plan := 2017; _x := 0;
		WHEN _syear = 2019 THEN _syear_plan := 2018; _x := 11;
	ELSE 
		RAISE EXCEPTION 'Pro zadaný rok snímkování nelze provést hlazení vrstvy r_ndsm (_syear = %)', _syear;
	END CASE
	;

_q :=
	'WITH
	w_rast AS (
		SELECT 
			rid, syear ,rast, map_section_smo50
		FROM
			gisdata_internal.r_ndsm
		WHERE
			 rid = ANY($2)  --AND syear = $1
		--limit 5
		)
        
    --spojeni se celeho rastru kvuli zjisteni sousednich pixelu
, w_rast_union AS (
	SELECT
		t2.rid, t1.syear, st_union(t1.rast) AS rast_union
	FROM
		gisdata_internal.r_ndsm AS t1
	INNER JOIN
		w_rast AS t2
	ON
		t1.syear = t2.syear 
	AND
		ST_Intersects(t1.rast, t2.rast)
	GROUP BY
		t2.rid, t1.syear
		)
		
	-------------------------------TOUCHES---------------------------------------------------------------------------------------	

	, w_if_touches AS (
		SELECT 
			CASE WHEN t2.syear IS NOT NULL THEN true ELSE false END AS touches,
			t1.rid, t1.rast, t1.syear as raster, t2.geom, t2.syear as plan
		FROM
				w_rast as t1
		LEFT JOIN 
			(SELECT 
				syear, ST_Union(geom) AS geom
			FROM 
				gisdata_external.f_a_cuzk_survey_plan
			WHERE 
				syear = $3
			GROUP BY 
				syear
			) AS t2
		ON
			ST_Touches(st_convexhull(t1.rast),t2.geom)
			)

	, w_points_touches AS (
-- zde si musime zjistit pocet pixelu, ktere jsou za hranici snimkovani, tzn. cast okoli (velkeho tile), ktera je pred hranici snimkovani se bude delit nominalnim poctem bodu (25) minus
-- pocet pixelu za hranici snimkovani
-- nebo si zjistit presne pocet pixelu pred hranici (myslet na to, ze muzou chybet nulove pixely)
-- hodnoty na prvni y souradnici se  budou delit 20, na druhe 15 a zbyle 25
	SELECT
		rid, rast, (ST_PixelAsCentroids(rast,1,false)).* -- geom, val, x, y
	FROM
		w_if_touches
	WHERE
		touches = true
)
, w_neigh_touches AS (
	SELECT
		t1.rid, t2.val, t2.geom, t2.x, t2.y
		,ST_Neighborhood(t1.rast_union, t2.geom, 2, 2) AS neigh_touches
	FROM
		w_rast_union AS t1
	INNER JOIN
		w_points_touches AS t2
	ON
		t1.rid = t2.rid
  	)
    
, w_unnest_touches AS (
	SELECT 
		rid, x, y, unnest (neigh_touches) AS value_touches,
		CASE
		WHEN x = '|| abs(_x - 1) ||' THEN 15.0
		WHEN x = '|| abs(_x - 2) ||' THEN 20.0
		ELSE 25.0
		END AS pixcount
	FROM 
		w_neigh_touches
	)
	
, w_sum_touches AS (	
	SELECT
		rid,
		x,
		y,
		SUM(value_touches)/pixcount AS avg 
	FROM
		w_unnest_touches
	GROUP BY
		rid,
		x, y, pixcount
	)
----------------------------------------NOT TOUCHES------------------------------------------------------------------------------------------------------------
--nedotykaji se hranice snimkovani 2014/2015
-- bude se delit nominalnim poctem bodu, tzn. teoreticky chybejici pixely v okoli nahradime 0 (protoze tily, ktere jsou cele 0 jsou z DB odstraneny)
-- okrajove casti CR budou timto postupem trochu znepresneny, ale protoze je okraj nejblize 103 metru (buffer r_ndsm), nebude toto znepresneni nikdy ve vyberu bodu,
-- ani nebude vstupovat do vypoctu uhrnu v geograficke domene (CR), uhlopricka tile je mensi nez tato vzdalenost

, w_points_not_touches AS (
	SELECT 
		rid, rast, (ST_PixelAsCentroids(rast,1,false)).* -- geom, val, x, y
	FROM 
		w_if_touches
	WHERE
		touches = false
	)

  --sousedi, kteri se nedotykaji hranice
, w_neigh_not_touches AS (
	SELECT
		t1.rid, t2.val, t2.geom, t2.x, t2.y
		,ST_Neighborhood(t1.rast_union, t2.geom, 2, 2) AS neigh_not_touches
	FROM
		w_rast_union AS t1
	INNER JOIN
		w_points_not_touches AS t2
	ON
		t1.rid = t2.rid
  	)

  --rozdeleni pole na tabulkove hodnoty
, w_unnest_not_touches AS (
	SELECT 
		rid, x, y, unnest (neigh_not_touches) AS value_not_touches
	FROM 
		w_neigh_not_touches
	)

  --suma pixelu z hranice se nedotykajicich ridu
, w_sum_not_touches AS (	
	SELECT
		rid,
		x,
		y,
		SUM(value_not_touches)/25.0 AS avg
	FROM
		w_unnest_not_touches
	GROUP BY
		rid,
		x, y
	)
------------------------------------------------------------------------------------------------------------------------------
--vytvoreni prazdneho rastru, ktery ma prazdnou strukturu jako kopii rastru puvodniho
, w_new_raster AS (
SELECT
	rid, syear, map_section_smo50, ST_AddBand(ST_MakeEmptyRaster(rast),1, ''8BUI'', 0,255) AS rast
FROM
	w_rast
	),

  --nove hodnoty pro rastr, spojeni tabulek pro dotykajici se a nedotykajici se ridy
w_new_values_1 AS (
SELECT 
	*, round(avg::numeric,0) as avg_r
FROM 
	w_sum_not_touches
UNION ALL
SELECT 
	* , round(avg::numeric,0) as avg_r
FROM 
	w_sum_touches
)

--nove hodnoty, agregace po radcich
, w_new_values AS (	
	SELECT
		rid,
		array_agg(radek ORDER BY y) AS values
	FROM (
		SELECT 
			rid, y, array_agg(avg_r ORDER BY x) as radek
		FROM 
			w_new_values_1
		GROUP BY 
			rid, y
		) as t1
	GROUP BY
		rid
	)

  --vlozeni hodnot do prazdneho rastru
, w_new AS (
	SELECT
		t1.rid, t1.syear, t1.map_section_smo50, ST_SetValues(t1.rast, 1, 1, 1, t2.values) AS rast
	FROM
		w_new_raster AS t1
	INNER JOIN
		w_new_values AS t2
	ON
		t1.rid = t2.rid
)         
INSERT INTO @extschema@.r_ndsm_smooth (rid, syear,  map_section_smo50, rast)
SELECT 
	rid, syear, map_section_smo50, rast 
FROM
	w_new
RETURNING rid';

--RAISE NOTICE 'rid = %', _rids;
--RAISE NOTICE 'survey_plan = %, %', _syear, _syear_plan;
--RAISE NOTICE ' %', _q;

RETURN QUERY EXECUTE _q
USING _syear, _rids, _syear_plan;

END;
$BODY$
LANGUAGE plpgsql VOLATILE
;	

ALTER FUNCTION @extschema@.fn_get_r_ndsm_smooth_rids(integer[], integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_r_ndsm_smooth_rids(integer[], integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_r_ndsm_smooth_rids(integer[], integer) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_r_ndsm_smooth_rids(integer[], integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_r_ndsm_smooth_rids(integer[], integer) IS 'Funkce pro shlazování rasterů normalizovaného modelu povrchu. 
Funkce přejímá seznam ridů (tileů), které se vyhladí a insertují do tabulky r_ndsm_smooth. Hlazení se provádí v kernelu (oknu) 5 x 5 pixelů. 
Je potřeba jí předat také rok snímkování, ze kterého ridy pochází, aby bylo možné určit, které tiley jsou na okraji snímkování se sousedním rokem.
V těchto případech se okno ořezává na okraj (4x5 nebo 3x5 pixelů) snímkování. Okraj snímkování v případě hranic ČR se neřeší, snímkování je v dostatečné vzdálenosti za hranicí ČR.';



-- </function>


-- <function name="fn_get_reclass_app" schema="extschema" src="functions/extschema/fn_get_reclass_app.sql">
-- DROP FUNCTION @extschema@.fn_get_reclass_app(raster,integer,text,integer)

CREATE OR REPLACE FUNCTION @extschema@.fn_get_reclass_app(_rast raster, _band integer, _pixel_type text, _reclass_value integer)
RETURNS raster AS
$$
DECLARE
	_max_pixel_range_value	integer;
	_reclass_value_minus	integer;
	_reclass_value_plus	integer;
	_reclass_text		text;
BEGIN
	-------------------------------------------------------------------------------------------
	IF _rast IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_reclass_app: Vstupni argument _rast nesmi byt NULL!';
	END IF;

	IF _band IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_reclass_app: Vstupni argument _band nesmi byt NULL!';
	END IF;

	IF _pixel_type IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_get_reclass_app: Vstupni argument _pixel_type nesmi byt NULL!';
	END IF;

	IF _reclass_value IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_get_reclass_app: Vstupni argument _reclass_value nesmi byt NULL!';
	END IF;

	IF _reclass_value = 0
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_get_reclass_app: Hodnota _reclass_value nesmi byt 0!';
	END IF;
	-------------------------------------------------------------------------------------------
	-- nastaveni _range podle typu pixelu
	CASE
	WHEN _pixel_type = '2BUI'	THEN _max_pixel_range_value := 4;
	WHEN _pixel_type = '4BUI'	THEN _max_pixel_range_value := 16;
	WHEN _pixel_type = '8BUI'	THEN _max_pixel_range_value := 255;
	WHEN _pixel_type = '16BUI'	THEN _max_pixel_range_value := 65536;
	ELSE
		RAISE EXCEPTION 'Chyba 06: fn_get_reclass_app: Neznama hodnota _pixel_type pro nastaveni _max_pixel_range_value!';
	END CASE;
	-------------------------------------------------------------------------------------------
	IF _reclass_value >= _max_pixel_range_value
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_get_reclass_app: Hodnota _reclass_value nesmi byt vetsi nebo rovna hodnote _max_pixel_range_value!';
	END IF;
	-------------------------------------------------------------------------------------------
	-- proces sestaveni reklasifikacniho stringu pro funci ST_Reclass
	IF _reclass_value = 1
	THEN
		_reclass_text := concat('[0-0]:0,[1-1]:1,[2-',(_max_pixel_range_value-1),']:0');

	ELSE
		_reclass_value_minus := _reclass_value - 1;
		_reclass_value_plus  := _reclass_value + 1;

		_reclass_text := concat('[0-',_reclass_value_minus,']:0,[',_reclass_value,'-',_reclass_value,']:1,[',_reclass_value_plus,'-',(_max_pixel_range_value-1),']:0');
	END IF;
	-------------------------------------------------------------------------------------------
	RETURN (ST_Reclass(_rast, _band, _reclass_text, _pixel_type, _max_pixel_range_value));
	-------------------------------------------------------------------------------------------
END;
$$
LANGUAGE plpgsql VOLATILE
COST 100;

ALTER FUNCTION @extschema@.fn_get_reclass_app(raster,integer,text,integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_reclass_app(raster,integer,text,integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_reclass_app(raster,integer,text,integer) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_reclass_app(raster,integer,text,integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_reclass_app(raster,integer,text,integer) IS
'Funkce vraci reklasifikovany raster v zavislosti na zadanem bandu, typu pixelu a hodnoty pro reklasifikaci.';

-- </function>


-- <function name="fn_sql_aux_total_raster_app" schema="extschema" src="functions/extschema/fn_sql_aux_total_raster_app.sql">
--------------------------------------------------------------------------------;
--	fn_sql_aux_total_raster_app
--------------------------------------------------------------------------------;

DROP FUNCTION IF EXISTS @extschema@.fn_sql_aux_total_raster_app(
	integer, character varying, character varying, character varying,
	integer, integer, character varying, double precision,
	character varying);

CREATE OR REPLACE FUNCTION @extschema@.fn_sql_aux_total_raster_app
(
	config_id			integer,
	schema_name			character varying,
	table_name			character varying,
	column_name			character varying,
	band				integer,
	reclass				integer,
	condition			character varying,
	unit				double precision,
	gid				character varying
)
RETURNS text
LANGUAGE plpgsql
IMMUTABLE
SECURITY INVOKER
AS
$$
DECLARE
	reclass_text		text;
	result			text;
BEGIN
	-----------------------------------------------------------------------------------
	-- schema_name
	IF ((schema_name IS NULL) OR (trim(schema_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_sql_aux_total_raster_app: Hodnota parametru schema_name nesmí být NULL.';
	END IF;

	-- table_name
	IF ((table_name IS NULL) OR (trim(table_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_sql_aux_total_raster_app: Hodnota parametru table_name nesmí být NULL.';
	END IF;

	-- column_name
	IF ((column_name IS NULL) OR (trim(column_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_sql_aux_total_raster_app: Hodnota parametru column_name nesmí být NULL.';
	END IF;

	-- band
	IF (band IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_sql_aux_total_raster_app: Hodnota parametru band nesmí být NULL.';
	END IF;

	-- condition [povoleno NULL]
	IF ((condition IS NULL) OR (trim(condition) = ''))
	THEN
		condition := NULL;
	END IF;

	-- unit
	IF (unit IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_sql_aux_total_raster_app: Hodnota parametru unit nesmí být NULL.';
	END IF;

	-- gid
	IF (gid IS NULL) OR (trim(gid) = '')
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_sql_aux_total_raster_app: Hodnota parametru gid nesmí být NULL.';
	END IF;
	-----------------------------------------------------------------------------------
	-- sestaveni dotazu pro vypocet uhrnu
	result :=
		'
		WITH 
		---------------------------------------------------------------
		w_rast AS	(
				SELECT
					t1.gid, 
					t1.estimation_cell,
					t1.geom,
					CASE WHEN t2.#COLUMN_NAME# IS NULL THEN false ELSE true END AS ident_null,
					#RECLASS# AS rast
				FROM
					@extschema@.f_a_cell AS t1
				LEFT JOIN
					#SCHEMA_NAME#.#TABLE_NAME# AS t2
				ON
					t1.geom && ST_Convexhull(t2.#COLUMN_NAME#)
				AND
					ST_Intersects(t1.geom, ST_Convexhull(t2.#COLUMN_NAME#))
				AND			 
					#CONDITION#
				WHERE 
					t1.gid = #GID#
				)  
		---------------------------------------------------------------
		,w_covers AS	(
				SELECT
					gid,
					estimation_cell,
					rast,
					geom,
					ST_Covers(geom, ST_Convexhull(rast)) AS covers
				FROM
					w_rast
				WHERE
					ident_null
				)
		---------------------------------------------------------------	
		,w_uhrn AS 	(
				SELECT
					gid,
					estimation_cell,
					(ST_ValueCount(rast, #BAND#, true)) AS val_count,
					(ST_PixelWidth(rast) * ST_PixelHeight(rast))*#UNIT# AS pixarea
				FROM
					w_covers
				WHERE
					covers = true
				)
		---------------------------------------------------------------	
		,w_uhrn_not AS	(
				SELECT
					gid,
					estimation_cell,
					ST_Intersection(geom, rast, #BAND#) AS vector_intersect_record
				FROM
					w_covers
				WHERE
					covers = false
				)
		---------------------------------------------------------------	
		,w_sum AS	(
				SELECT
					SUM(((ST_Area((vector_intersect_record).geom))*#UNIT#)*((vector_intersect_record).val)) AS sum_part
				FROM 
					w_uhrn_not
				---------------------------
				UNION ALL
				---------------------------
				SELECT
					SUM((val_count).value*(val_count).count*pixarea) AS sum_part
				FROM 
					w_uhrn 
				---------------------------
				UNION ALL
				---------------------------
				SELECT
					0.0 AS sum_part
				FROM
					w_rast
				WHERE
					ident_null = false
				)
		---------------------------------------------------------------	
		SELECT
			coalesce(sum(sum_part),0) AS aux_total_#CONFIG_ID#_#GID#
		FROM 
			w_sum;			
		';
	-----------------------------------------------------------------------------------
	-- nastaveni pripadne reklasifikace
	IF (reclass IS NULL)
	THEN
		reclass_text := 't2.#COLUMN_NAME#';
	ELSE
		reclass_text := '@extschema@.fn_get_reclass_app(t2.#COLUMN_NAME#,#BAND#,ST_BandPixelType(t2.#COLUMN_NAME#,#BAND#),#RECLASS_VALUE#)';
	END IF;
	-----------------------------------------------------------------------------------
	-- nahrazeni promennych casti v dotazu pro vypocet uhrnu
	result := replace(result, '#RECLASS#', reclass_text);
	result := replace(result, '#CONFIG_ID#', config_id::character varying);
	result := replace(result, '#SCHEMA_NAME#', schema_name);
	result := replace(result, '#TABLE_NAME#', table_name);
	result := replace(result, '#COLUMN_NAME#', column_name);
	result := replace(result, '#BAND#', band::character varying);
	result := replace(result, '#CONDITION#', coalesce(condition::character varying, 'TRUE'));
	result := replace(result, '#UNIT#', unit::character varying);
	result := replace(result, '#GID#', gid);

	IF (reclass IS NOT NULL)
	THEN
		result := replace(result, '#RECLASS_VALUE#', reclass::character varying);
	END IF;
	-----------------------------------------------------------------------------------
	RETURN result;
	-----------------------------------------------------------------------------------
END;
$$;

--------------------------------------------------------------------------------;

ALTER FUNCTION @extschema@.fn_sql_aux_total_raster_app(
	integer, character varying, character varying, character varying,
	integer, integer, character varying, double precision,
	character varying)
OWNER TO adm_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_raster_app(
	integer, character varying, character varying, character varying,
	integer, integer, character varying, double precision,
	character varying)
TO adm_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_raster_app(
	integer, character varying, character varying, character varying,
	integer, integer, character varying, double precision,
	character varying)
TO app_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_raster_app(
	integer, character varying, character varying, character varying,
	integer, integer, character varying, double precision,
	character varying)
TO public;

COMMENT ON FUNCTION @extschema@.fn_sql_aux_total_raster_app(
	integer, character varying, character varying, character varying,
	integer, integer, character varying, double precision,
	character varying)
IS
'Funkce vrací SQL textový řetězec pro výpočet úhrnu pomocné proměnné z rasterové vrstvy.';

--------------------------------------------------------------------------------;

-- </function>


-- <function name="fn_sql_aux_total_raster_comb_app" schema="extschema" src="functions/extschema/fn_sql_aux_total_raster_comb_app.sql">
--------------------------------------------------------------------------------;
--	fn_sql_aux_total_raster_comb_app
--------------------------------------------------------------------------------;

DROP FUNCTION IF EXISTS @extschema@.fn_sql_aux_total_raster_comb_app(
	integer,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	);

CREATE OR REPLACE FUNCTION @extschema@.fn_sql_aux_total_raster_comb_app
(
	config_id			integer,
	schema_name			character varying,
	table_name			character varying,
	column_name			character varying,
	band				integer,
	reclass				integer,
	condition			character varying,
	unit				double precision,
	schema_name_1			character varying,
	table_name_1			character varying,
	column_name_1			character varying,
	band_1				integer,
	reclass_1			integer,
	condition_1			character varying,
	unit_1				double precision,
	gid				character varying
)
RETURNS text
LANGUAGE plpgsql
IMMUTABLE
SECURITY INVOKER
AS
$$
DECLARE
	reclass_text		text;
	reclass_val_column	text;
	result			text;
BEGIN
	-----------------------------------------------------------------------------------
	-- schema_name
	IF ((schema_name IS NULL) OR (trim(schema_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_sql_aux_total_raster_comb_app: Hodnota parametru schema_name nesmí být NULL.';
	END IF;

	-- table_name
	IF ((table_name IS NULL) OR (trim(table_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_sql_aux_total_raster_comb_app: Hodnota parametru table_name nesmí být NULL.';
	END IF;

	-- column_name
	IF ((column_name IS NULL) OR (trim(column_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_sql_aux_total_raster_comb_app: Hodnota parametru column_name nesmí být NULL.';
	END IF;

	-- band
	IF (band IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_sql_aux_total_raster_comb_app: Hodnota parametru band nesmí být NULL.';
	END IF;

	-- condition [povoleno NULL]
	IF ((condition IS NULL) OR (trim(condition) = ''))
	THEN
		condition := NULL;
	END IF;

	-- unit
	IF (unit IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_sql_aux_total_raster_comb_app: Hodnota parametru unit nesmí být NULL.';
	END IF;

	-- schema_name_1
	IF ((schema_name_1 IS NULL) OR (trim(schema_name_1) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_sql_aux_total_raster_comb_app: Hodnota parametru schema_name_1 nesmí být NULL.';
	END IF;

	-- table_name_1
	IF ((table_name_1 IS NULL) OR (trim(table_name_1) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_sql_aux_total_raster_comb_app: Hodnota parametru table_name_1 nesmí být NULL.';
	END IF;

	-- column_name_1
	IF ((column_name_1 IS NULL) OR (trim(column_name_1) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 08: fn_sql_aux_total_raster_comb_app: Hodnota parametru column_name_1 nesmí být NULL.';
	END IF;

	-- band_1
	IF (band_1 IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 09: fn_sql_aux_total_raster_comb_app: Hodnota parametru band_1 nesmí být NULL.';
	END IF;

	-- condition_1 [povoleno NULL]
	IF ((condition_1 IS NULL) OR (trim(condition_1) = ''))
	THEN
		condition_1 := NULL;
	END IF;

	-- unit_1
	IF (unit_1 IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 10: fn_sql_aux_total_raster_comb_app: Hodnota parametru unit_1 nesmí být NULL.';
	END IF;
	-----------------------------------------------------------------------------------
	-- sestaveni dotazu pro vypocet uhrnu
	result :=
	'
	WITH
	---------------------------------------------------------------
	w_cell AS	(-- vyber uzemi
			SELECT
				ST_SetSRID(geom,5221) AS geom_cell,
				estimation_cell
			FROM
				@extschema@.f_a_cell
			WHERE
				gid = #GID#
			)
	---------------------------------------------------------------
	,w_cell_rast2 AS materialized
			(-- protnuti vybraneho uzemi z rasterem
			SELECT
				t1.geom_cell,
				t1.estimation_cell,
				t2.#COLUMN_NAME# AS rast4dump
			FROM
				w_cell AS t1
			LEFT
			JOIN
				#SCHEMA_NAME#.#TABLE_NAME# AS t2

			ON	t1.geom_cell && st_convexhull(t2.#COLUMN_NAME#)
			
			AND	ST_Intersects(t1.geom_cell, st_convexhull(t2.#COLUMN_NAME#))
			
			AND	#CONDITION#
			)
	---------------------------------------------------------------
	,w_polygons AS materialized
			(-- rozdumpovani rasteru na polygony (nemusi byt pokryt cely katastr, kvuli 0 tileum)
			SELECT
				geom_cell,
				estimation_cell,
				ST_DumpAsPolygons(rast4dump, #BAND#) AS rast_poly
			FROM
				w_cell_rast2
			WHERE
				rast4dump IS NOT NULL
			)
	---------------------------------------------------------------
	,w_dumps AS materialized
			(
			SELECT
				geom_cell,
				estimation_cell,
				(rast_poly).val,
				(rast_poly).geom
			FROM
				w_polygons 
			WHERE
				#RECLASS#
			)
	---------------------------------------------------------------
	,w_intersection AS materialized
			(
			SELECT
				geom_cell,
				estimation_cell,
				#RECLASS_VAL_COLUMN# * #UNIT# AS val_reclass,				
				ST_Intersection (geom_cell, geom) AS geom
			FROM
				w_dumps
			)
	---------------------------------------------------------------
	,w_cell_rast1 as materialized
			(-- protnuti rasteru1 z rozdampovanyma geometriema withu w_intersection
			SELECT
				geom_cell,
				estimation_cell,
				val_reclass,
				ST_Intersection(t1.geom, t2.#COLUMN_NAME_1#) AS res_intersects
			FROM
				w_intersection AS t1
			LEFT JOIN
				#SCHEMA_NAME_1#.#TABLE_NAME_1# AS t2
			
			ON	t1.geom && st_convexhull(t2.#COLUMN_NAME_1#)
			
			AND	ST_Intersects(t1.geom, st_convexhull(t2.#COLUMN_NAME_1#))
			
			AND	#CONDITION_1#
			
			WHERE
				ST_IsEmpty(t1.geom) = false
			)
	---------------------------------------------------------------
	,w_dump_all as materialized
			(-- rozbaleni intersection ndsm na geometrii a hodnotu
			SELECT 
				estimation_cell,
				val_reclass,
				(res_intersects).geom AS res_geom,
				(res_intersects).val AS ndsm_val
			FROM
				w_cell_rast1
			)
	---------------------------------------------------------------
	,w_sum_part AS	(
			select
				sum((ST_Area(res_geom)) * val_reclass * ndsm_val) as sum_part
			FROM
				w_dump_all
			)
	---------------------------------------------------------------
	,w_sum AS 	(
			-- tady se protnul ndsm s ftype
			-- tam kde cast katastru byla pokryta ftype 0 (chybeji tile) se neprovede nic,
			-- tyto pixely jsou odstraneny jiz na zacatku
			
			SELECT 
				sum_part
			FROM 
				w_sum_part
			-------------------------------------
			UNION ALL
			-------------------------------------
			-- pripojeni katastru, ktere maji ftype vsude 0
			
			SELECT
				0.0 AS sum_part
			FROM
				w_cell_rast2
			WHERE
				rast4dump IS NULL
			-------------------------------------
			UNION ALL
			-------------------------------------
			-- pripojeni geometrie, ktere jsou pokryte nejakym tilem, ale vsude je 0

			SELECT
				0.0 AS sum_part
			FROM
				w_polygons
			WHERE
				(rast_poly).val = 0
			)
	---------------------------------------------------------------
	SELECT 
		coalesce(sum(sum_part),0) AS aux_total_#CONFIG_ID#_#GID#
	FROM 
		w_sum;
	';
	-----------------------------------------------------------------------------------
	-- nastaveni pripadne reklasifikace pro raster v prvnim poradi protinani
	IF (reclass IS NULL)
	THEN
		reclass_text := '(rast_poly).val != 0';			-- defaultni nastaveni
		reclass_val_column := 'val';				-- val hodnota se zde nepreklasifikovava [bere se sloupec val]
	ELSE
		reclass_text := '(rast_poly).val = #RECLASS_VALUE#';	-- hodnota val urci s jakymi rozdampovanymi polygony se bude dale pracovat [ty si nesou hodnotu val z prvniho protinani]
		reclass_val_column := '1.0';				-- hodnota val z prvniho protinani se umele preklasifikuje vzdy na hodnotu 1.0
	END IF;
	-----------------------------------------------------------------------------------
	-- nastaveni pripadne reklasifikace pro raster ve druhem poradi protinani
	IF (reclass_1 IS NOT NULL)
	THEN
		RAISE EXCEPTION 'Chyba 11: fn_sql_aux_total_raster_comb_app: Neznama reklasifikace pro druhy raster v protinani!'; -- momentalne jako raster pro druhe protinani je vrstva ndsm u ktere se reklasifikace neprovadi
	END IF;
	-----------------------------------------------------------------------------------
	-- nahrazeni promennych casti v dotazu pro vypocet uhrnu
	result := replace(result, '#RECLASS#', reclass_text);
	result := replace(result, '#RECLASS_VAL_COLUMN#', reclass_val_column);
	result := replace(result, '#CONFIG_ID#', config_id::character varying);
	result := replace(result, '#SCHEMA_NAME#', schema_name);
	result := replace(result, '#TABLE_NAME#', table_name);
	result := replace(result, '#COLUMN_NAME#', column_name);
	result := replace(result, '#BAND#', band::character varying);	
	result := replace(result, '#CONDITION#', coalesce(condition::character varying, 'TRUE'));
	result := replace(result, '#UNIT#', unit::character varying);
	result := replace(result, '#SCHEMA_NAME_1#', schema_name_1);
	result := replace(result, '#TABLE_NAME_1#', table_name_1);
	result := replace(result, '#COLUMN_NAME_1#', column_name_1);
	result := replace(result, '#BAND_1#', band_1::character varying);
	result := replace(result, '#CONDITION_1#', coalesce(condition_1::character varying, 'TRUE'));
	result := replace(result, '#UNIT_1#', unit_1::character varying);
	result := replace(result, '#GID#', gid);

	IF (reclass IS NOT NULL)
	THEN
		result := replace(result, '#RECLASS_VALUE#', reclass::character varying);
	END IF;
	-----------------------------------------------------------------------------------
	RETURN result;
	-----------------------------------------------------------------------------------
END;
$$;

--------------------------------------------------------------------------------;

ALTER FUNCTION @extschema@.fn_sql_aux_total_raster_comb_app
	(
	integer,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	)
OWNER TO adm_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_raster_comb_app
	(
	integer,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	)
TO adm_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_raster_comb_app
	(
	integer,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	)
TO app_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_raster_comb_app
	(
	integer,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	)
TO public;

COMMENT ON FUNCTION @extschema@.fn_sql_aux_total_raster_comb_app
	(
	integer,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	)
IS
'Funkce vrací SQL textový řetězec pro výpočet úhrnu pomocné proměnné z rasterové kategorie v rámci vektorové vrstvy.';

--------------------------------------------------------------------------------;

-- </function>


-- <function name="fn_sql_aux_total_vector_app" schema="extschema" src="functions/extschema/fn_sql_aux_total_vector_app.sql">
--------------------------------------------------------------------------------;
--	fn_sql_aux_total_vector_app
--------------------------------------------------------------------------------;

DROP FUNCTION IF EXISTS @extschema@.fn_sql_aux_total_vector_app(
	integer, character varying, character varying,
	character varying, character varying,
	double precision, character varying);

CREATE OR REPLACE FUNCTION @extschema@.fn_sql_aux_total_vector_app
(
    config_id		integer,
    schema_name		character varying,
    table_name		character varying,
    column_name		character varying,
    condition		character varying,
    unit		double precision,
    gid			character varying
)
  RETURNS text AS
$BODY$
DECLARE
	result	text;
BEGIN
	-----------------------------------------------------------------------------------
	-- schema_name
	IF ((schema_name IS NULL) OR (trim(schema_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_sql_aux_total_vector_app: Hodnota parametru schema_name nesmí být NULL.';
	END IF;

	-- table_name
	IF ((table_name IS NULL) OR (trim(table_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_sql_aux_total_vector_app: Hodnota parametru table_name nesmí být NULL.';
	END IF;

	-- column_name
	IF (column_name IS NULL) OR (trim(column_name) = '')
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_sql_aux_total_vector_app: Hodnota parametru column_name nesmí být NULL.';
	END IF;

	-- condition [povoleno NULL]
	IF (condition IS NULL) OR (trim(condition) = '')
	THEN
		condition := NULL;
	END IF;

	-- unit
	IF (unit IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_sql_aux_total_vector_app: Hodnota parametru unit nesmí být NULL.';
	END IF;

	-- gid
	IF (gid IS NULL) OR (trim(gid) = '')
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_sql_aux_total_vector_app: Hodnota parametru gid nesmí být NULL.';
	END IF;
	-----------------------------------------------------------------------------------
	-- sestaveni dotazu pro vypocet uhrnu
	result :=
		'
		WITH
		---------------------------------------------------------------
		w1 AS	(
			SELECT
				t1.geom as geom_cell,
				t2.geom as geom_layer
			FROM
				@extschema@.f_a_cell AS t1
			LEFT JOIN
				#SCHEMA_NAME#.#TABLE_NAME# AS t2 
			ON
				ST_Intersects(ST_Envelope(t1.geom),t2.#COLUMN_NAME#)
			WHERE
				t1.gid = #GID#
			AND
				#CONDITION#
			)
		---------------------------------------------------------------
		-- vypocet rozlohy pruniku obou geometrii
		,w2 AS	(
			SELECT
				SUM(ST_Area(ST_Intersection (geom_layer,geom_cell))*#UNIT#) AS suma
			FROM
				w1
			)
		---------------------------------------------------------------		
		SELECT
			coalesce(suma,0) AS aux_total_#CONFIG_ID#_#GID#
		FROM 
			w2;
		';
	-----------------------------------------------------------------------------------
	-- nahrazeni promennych casti v dotazu pro vypocet uhrnu
	result := replace(result, '#CONFIG_ID#', config_id::character varying);
	result := replace(result, '#SCHEMA_NAME#', schema_name);
	result := replace(result, '#TABLE_NAME#', table_name);
	result := replace(result, '#COLUMN_NAME#', column_name);
	result := replace(result, '#CONDITION#', coalesce(condition::character varying, 'TRUE'));
	result := replace(result, '#UNIT#', unit::character varying);
	result := replace(result, '#GID#', gid);
	-----------------------------------------------------------------------------------
	RETURN result;
	-----------------------------------------------------------------------------------
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE;

--------------------------------------------------------------------------------;

ALTER FUNCTION @extschema@.fn_sql_aux_total_vector_app(
	integer, character varying, character varying,
	character varying, character varying,
	double precision, character varying)
OWNER TO adm_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_vector_app(
	integer, character varying, character varying,
	character varying, character varying,
	double precision, character varying)
TO adm_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_vector_app(
	integer, character varying, character varying,
	character varying, character varying,
	double precision, character varying)
TO app_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_vector_app(
	integer, character varying, character varying,
	character varying, character varying,
	double precision, character varying)
TO public;

COMMENT ON FUNCTION @extschema@.fn_sql_aux_total_vector_app(
	integer, character varying, character varying,
	character varying, character varying,
	double precision, character varying)
IS
'Funkce vrací SQL textový řetězec pro výpočet úhrnu pomocné proměnné z vektorové vrstvy.';

--------------------------------------------------------------------------------;

-- </function>


-- <function name="fn_sql_aux_total_vector_comb_app" schema="extschema" src="functions/extschema/fn_sql_aux_total_vector_comb_app.sql">
--------------------------------------------------------------------------------;
--	fn_sql_aux_total_vector_comb_app
--------------------------------------------------------------------------------;

DROP FUNCTION IF EXISTS @extschema@.fn_sql_aux_total_vector_comb_app
	(
	integer,
	character varying, character varying, character varying, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	);

CREATE OR REPLACE FUNCTION @extschema@.fn_sql_aux_total_vector_comb_app
(
	config_id			integer,
	schema_name			character varying,
	table_name			character varying,
	column_name			character varying,
	condition			character varying,
	unit				double precision,
	schema_name_1			character varying,
	table_name_1			character varying,
	column_name_1			character varying,
	band_1				integer,
	reclass_1			integer,
	condition_1			character varying,
	unit_1				double precision,
	gid				character varying
)
RETURNS text
LANGUAGE plpgsql
IMMUTABLE
SECURITY INVOKER
AS
$$
DECLARE
	reclass_text		text;
	result			text;
BEGIN
	-----------------------------------------------------------------------------------
	-- schema_name
	IF ((schema_name IS NULL) OR (trim(schema_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_sql_aux_total_vector_comb_app: fn_sql_aux_total_vector_comb_app: Hodnota parametru schema_name nesmí být NULL.';
	END IF;

	-- table_name
	IF ((table_name IS NULL) OR (trim(table_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_sql_aux_total_vector_comb_app: fn_sql_aux_total_vector_comb_app: Hodnota parametru table_name nesmí být NULL.';
	END IF;

	-- column_name
	IF (column_name IS NULL) OR (trim(column_name) = '')
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_sql_aux_total_vector_comb_app: fn_sql_aux_total_vector_comb_app: Hodnota parametru column_name nesmí být NULL.';
	END IF;

	-- condition
	-- povoleno NULL
	IF (condition IS NULL) OR (trim(condition) = '')
	THEN
		condition := NULL;
	END IF;

	-- unit
	IF (unit IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_sql_aux_total_vector_comb_app: Hodnota parametru unit nesmí být NULL.';
	END IF;

	-- schema_name_1
	IF ((schema_name_1 IS NULL) OR (trim(schema_name_1) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_sql_aux_total_vector_comb_app: Hodnota parametru schema_name_1 nesmí být NULL.';
	END IF;

	-- table_name_1
	IF ((table_name_1 IS NULL) OR (trim(table_name_1) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_sql_aux_total_vector_comb_app: Hodnota parametru table_name_1 nesmí být NULL.';
	END IF;

	-- column_name_1
	IF ((column_name_1 IS NULL) OR (trim(column_name_1) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_sql_aux_total_vector_comb_app: Hodnota parametru column_name_1 nesmí být NULL.';
	END IF;

	-- band_1
	IF (band_1 IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 08: fn_sql_aux_total_vector_comb_app: Hodnota parametru band_1 nesmí být NULL.';
	END IF;

	-- condition_1 [povoleno NULL]
	IF ((condition_1 IS NULL) OR (trim(condition_1) = ''))
	THEN
		condition_1 := NULL;
	END IF;

	-- unit_1
	IF (unit_1 IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 09: fn_sql_aux_total_vector_comb_app: Hodnota parametru unit_1 nesmí být NULL.';
	END IF;
	-----------------------------------------------------------------------------------
	-- sestaveni dotazu pro vypocet uhrnu
	result :=
	'
	WITH
	--------------------------------------------------------------- 
	w_ku_olil AS materialized

			(-- propojeni tabulek olilu a katastru na zaklade jejich pruniku, vektorová operace
			SELECT
				t1.gid, 
				t1.estimation_cell,
				t1.geom as geom_cell,
				t2.#COLUMN_NAME#,
				ST_Intersection(t1.geom, t2.#COLUMN_NAME#) as intersect_ku_olil  -- vysledna geometrie protnuti geometrie GIDu a OLILu
			FROM
				@extschema@.f_a_cell AS t1
			LEFT JOIN
				#SCHEMA_NAME#.#TABLE_NAME# AS t2
			ON
				ST_Intersects(t2.#COLUMN_NAME#,t1.geom) 
			AND 
				#CONDITION#
			WHERE 
				t1.gid = #GID#
			)
	---------------------------------------------------------------
	---------------------------------------------------------------
	,w_ndsm_olil_katastr_0 AS	(
					SELECT
						t1.*,
						t2.*,
						#RECLASS_1# AS rast_reclass
					FROM
						w_ku_olil AS t1
					LEFT
					JOIN	#SCHEMA_NAME_1#.#TABLE_NAME_1# AS t2

					ON	t1.intersect_ku_olil && ST_Convexhull(t2.#COLUMN_NAME_1#)

					AND	ST_Intersects(t1.intersect_ku_olil,ST_Convexhull(t2.#COLUMN_NAME_1#))
					)
	,w_ndsm_olil_katastr AS		(
					SELECT
						gid,
						estimation_cell,
						rast_reclass,
						intersect_ku_olil,
						CASE WHEN rast_reclass IS NULL THEN false ELSE true END AS ident_null
					FROM
						w_ndsm_olil_katastr_0 as A
					WHERE
						#CONDITION_1#
					AND
						(
						ST_GeometryType(A.intersect_ku_olil) = ''ST_Polygon'' OR
						ST_GeometryType(A.intersect_ku_olil) = ''ST_MultiPolygon''
						)
					)
	---------------------------------------------------------------
	---------------------------------------------------------------
	,w_covers AS materialized
			(
			SELECT
				gid, 
				estimation_cell, 
				rast_reclass, 
				intersect_ku_olil,
				ST_Covers(intersect_ku_olil, ST_Convexhull(rast_reclass)) AS covers
			FROM
				w_ndsm_olil_katastr
			WHERE
				ident_null
			)
	---------------------------------------------------------------
	,w_uhrn AS	(
			SELECT
				estimation_cell,
				(ST_ValueCount(rast_reclass, #BAND_1#, true)) AS val_count,
				(ST_PixelWidth(rast_reclass) * ST_PixelHeight(rast_reclass))*#UNIT_1# AS pixarea
			FROM
				w_covers
			WHERE
				covers = true
			)
	---------------------------------------------------------------
	,w_uhrn_not AS	(
			SELECT
				estimation_cell,
				ST_Intersection(intersect_ku_olil,rast_reclass) AS vector_intersect_record
			FROM
				w_covers
			WHERE
				covers = false
			)
	---------------------------------------------------------------
	,w_sum AS	(
			SELECT
				SUM(((ST_Area((vector_intersect_record).geom))*#UNIT#)*((vector_intersect_record).val)) AS sum_part
			FROM 
				w_uhrn_not
			-------------------
			UNION ALL
			-------------------
			SELECT 
				SUM((val_count).value*(val_count).count*pixarea) AS sum_part
			FROM 
				w_uhrn 
			-------------------			
			UNION ALL
			-------------------
			SELECT
				0.0 AS sum_part
			FROM
				w_ndsm_olil_katastr
			WHERE
				ident_null = false
			)
	---------------------------------------------------------------
	SELECT 
		sum(coalesce((sum_part),0)) AS aux_total_#CONFIG_ID#_#GID#
	FROM 
		w_sum;
	';
	-----------------------------------------------------------------------------------
	-- nastaveni pripadne reklasifikace
	IF (reclass_1 IS NULL)
	THEN
		reclass_text := 't2.#COLUMN_NAME_1#';
	ELSE
		reclass_text := '@extschema@.fn_get_reclass_app(t2.#COLUMN_NAME_1#,#BAND_1#,ST_BandPixelType(t2.#COLUMN_NAME_1#,#BAND_1#),#RECLASS_VALUE_1#)';
	END IF;
	-----------------------------------------------------------------------------------
	-- nahrazeni promennych casti v dotazu pro vypocet uhrnu
	result := replace(result, '#RECLASS_1#', reclass_text);
	result := replace(result, '#CONFIG_ID#', config_id::character varying);
	result := replace(result, '#SCHEMA_NAME#', schema_name);
	result := replace(result, '#TABLE_NAME#', table_name);
	result := replace(result, '#COLUMN_NAME#', column_name);
	result := replace(result, '#CONDITION#', coalesce(condition::character varying, 'TRUE'));
	result := replace(result, '#UNIT#', unit::character varying);
	result := replace(result, '#SCHEMA_NAME_1#', schema_name_1);
	result := replace(result, '#TABLE_NAME_1#', table_name_1);
	result := replace(result, '#COLUMN_NAME_1#', column_name_1);
	result := replace(result, '#BAND_1#', band_1::character varying);
	result := replace(result, '#CONDITION_1#', coalesce(condition_1::character varying, 'TRUE'));
	result := replace(result, '#UNIT_1#', unit_1::character varying);
	result := replace(result, '#GID#', gid);

	IF (reclass_1 IS NOT NULL)
	THEN
		result := replace(result, '#RECLASS_VALUE_1#', reclass_1::character varying);
	END IF;
	-----------------------------------------------------------------------------------
	RETURN result;
	-----------------------------------------------------------------------------------
END;
$$;

--------------------------------------------------------------------------------;

ALTER FUNCTION @extschema@.fn_sql_aux_total_vector_comb_app
	(
	integer,
	character varying, character varying, character varying, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	)
OWNER TO adm_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_vector_comb_app
	(
	integer,
	character varying, character varying, character varying, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	)
TO adm_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_vector_comb_app
	(
	integer,
	character varying, character varying, character varying, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	)
TO app_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_vector_comb_app
	(
	integer,
	character varying, character varying, character varying, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	)
TO public;

COMMENT ON FUNCTION @extschema@.fn_sql_aux_total_vector_comb_app
	(
	integer,
	character varying, character varying, character varying, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	)
IS
'Funkce vrací SQL textový řetězec pro výpočet úhrnu pomocné proměnné z rasterové kategorie v rámci vektorové vrstvy.';

--------------------------------------------------------------------------------;

-- </function>
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- DUMPs
---------------------------------------------------------------------------------------------------;
-- tables
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_estimation_cell_collection', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_estimation_cell', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_estimation_cell_collection', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_f_a_cell', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.f_a_cell', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.r_ndsm_smooth','');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.r_s2_ftype_smooth','');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.r_s2_species_smooth','');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_config', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_config_collection', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_aux_total', '');
-- sequences
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_estimation_cell_collection_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_f_a_cell_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.f_a_cell_gid_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.r_ndsm_smooth_rid_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.r_s2_ftype_smooth_rid_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.r_s2_species_smooth_rid_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.seq__t_aux_total_id', '');
---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;


-- po sem je to ok, jen se musi prelozit vse do ANGLICTINY !!!




