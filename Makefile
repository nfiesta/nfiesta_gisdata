#nfiesta_gisdata/Makefile

EXTENSION = nfiesta_gisdata # nazev extenze
DATA = nfiesta_gisdata--1.0.0.sql \
	nfiesta_gisdata--1.0.0--1.1.0.sql \
	nfiesta_gisdata--1.1.0--1.1.1.sql \
	nfiesta_gisdata--1.1.1--1.2.0.sql \
	nfiesta_gisdata--1.2.0--1.2.1.sql \
	nfiesta_gisdata--1.2.1--1.2.2.sql \
	nfiesta_gisdata--1.2.2--1.2.3.sql \
	nfiesta_gisdata--1.2.3--1.2.4.sql \
	nfiesta_gisdata--1.2.4--1.2.5.sql \
	nfiesta_gisdata--1.2.5--2.0.0.sql \
	nfiesta_gisdata--2.0.0--2.0.1.sql \
	nfiesta_gisdata--2.0.1--3.0.0.sql \
	nfiesta_gisdata--3.0.0--3.0.1.sql \
	nfiesta_gisdata--3.0.1--3.0.2.sql \
	nfiesta_gisdata--3.0.2--3.0.3.sql \
	nfiesta_gisdata--3.0.3--3.0.4.sql \
    nfiesta_gisdata--3.0.4--3.0.5.sql \
	nfiesta_gisdata--3.0.5--3.0.6.sql \
	nfiesta_gisdata--3.0.6--3.0.7.sql \
	nfiesta_gisdata--3.0.7--3.1.0.sql

DOCS=README.md # readme

REGRESS = 	install;
			
export SRC_DIR := $(shell pwd)

all:

installcheck-all:
	make installcheck REGRESS_OPTS="--dbname=contrib_regression_nfiesta_gisdata"
	make installcheck-data

installcheck-data:
	psql -d postgres -c "drop database if exists contrib_regression_nfiesta_gisdata_data;" -c "create database contrib_regression_nfiesta_gisdata_data template contrib_regression_nfiesta_gisdata;"
	make installcheck REGRESS_OPTS="--use-existing --dbname=contrib_regression_nfiesta_gisdata_data" REGRESS="fst_vector_data fst_data fst_raster_schema fst_raster_data fst_conf fst_total fst_api_fce"

purge-db:
	psql -d postgres -c "drop database if exists contrib_regression_nfiesta_gisdata;"
	psql -d postgres -c "drop database if exists contrib_regression_nfiesta_gisdata_data;"

PG_MAJOR_VERSION := $(shell pg_config --version | sed -E 's/PostgreSQL ([0-9]+).*/\1/')
purge:
	rm -rf /usr/share/postgresql/$(PG_MAJOR_VERSION)/extension/nfiesta_gisdata.control /usr/share/postgresql/$(PG_MAJOR_VERSION)/$(MODULEDIR)

# postgres build
PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)
