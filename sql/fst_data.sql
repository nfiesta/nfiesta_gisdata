--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--

--
-- PostgreSQL database dump
--

--------------------------------------------------------------------------------------------------;
-- BEGIN INSPIRE --
--------------------------------------------------------------------------------------------------;

INSERT INTO gisdata.c_estimation_cell_collection(id,label,description,label_en,description_en) VALUES
(1,	'NUTS1-INSPIRE','Nomenclature of Territorial Units for Statistics, level 1 – state.','NUTS1-INSPIRE','Nomenclature of Territorial Units for Statistics, level 1 – state.'),
(2,	'100km-INSPIRE','A collection of estimation cells corresponding to 100km INSPIRE grid.','100km-INSPIRE','A collection of estimation cells corresponding to 100km INSPIRE grid.'),
(3,	'50km-INSPIRE','A collection of estimation cells corresponding to 50km INSPIRE grid.','50km-INSPIRE','A collection of estimation cells corresponding to 50km INSPIRE grid.'),
(4,	'25km-INSPIRE','A collection of estimation cells corresponding to 25km INSPIRE grid.','25km-INSPIRE','A collection of estimation cells corresponding to 25km INSPIRE grid.'),
(5,	'1km-INSPIRE','A collection of estimation cells corresponding to 1km INSPIRE grid.','1km-INSPIRE','A collection of estimation cells corresponding to 1km INSPIRE grid.');

update gisdata.c_estimation_cell_collection set use4estimates = false where id = 5;

drop table if exists csv.inspire_grid;
create table csv.inspire_grid as
with
w_cr as	(
		select
				1 as new_gid,
				gid,
				'NUTS1-INSPIRE' as cell_collection,
				'CZ0 [INSPIRE]' as label,
				'CZ0 – Česká republika [INSPIRE]' as description,
				geom,
				null::integer as cell_sup from gisdata.nuts1_gen
		)
-----------------
,w_100_cover as 	(
				select
				100000 as scalex, -100000 as  scaley,
				--------------------------------range of CR----------------------------
			    -- more precisely the borders of CR from EUROSTAT with a 10 km buffer
			    ST_GeomFromText('POLYGON((4400000 2800000, 5000000 2800000, 5000000 3200000, 4400000 3200000, 4400000 2800000))', 3035) as extent
				)
,w_100_grid_1 as	(
					SELECT 
						(ST_PixelAsPolygons(ST_AddBand(ST_MakeEmptyRaster(
						((ST_Xmax(w_100_cover.extent) - ST_Xmin(w_100_cover.extent)) / scalex)::int, ((ST_Ymax(w_100_cover.extent) - ST_Ymin(w_100_cover.extent)) / abs(scaley))::int,
						ST_Xmin(w_100_cover.extent),
						ST_Ymax(w_100_cover.extent),
						scalex, scaley, 0, 0, 3035), '8BSI'::text, 1, 0), 1, false)).geom
					from w_100_cover
					)
,w_100_grid_2 as	(
					select
						row_number() over() as gid,
						format('%s-INSPIRE', '100km') as cell_collection,
						concat('100km', 'N', (ST_ymin(geom)/1000)::int::varchar, 'E', (ST_xmin(geom)/1000)::int::varchar) as label,
					    	format('The %s-INSPIRE grid cell, the south-west corner of which is located %s km north and %s km east of the false origin of the ETRS89-extended / LAEA - Europe projection system.',
							'100km', (ST_ymin(geom)/1000)::int::varchar, (ST_xmin(geom)/1000)::int::varchar) as description,
						geom
					from w_100_grid_1
					)
,w_100_grid_all as	(
					select w_100_grid_2.gid, w_100_grid_2.cell_collection, w_100_grid_2.label, w_100_grid_2.description, w_100_grid_2.geom,
					st_centroid(w_100_grid_2.geom) as geom_centroid
					from w_100_grid_2, gisdata.nuts1_gen as t where st_intersects(w_100_grid_2.geom, t.geom)
					)
,w_100_grid as	(
				select
						row_number() over(order by w_100_grid_all.gid) + (select max(new_gid) from w_cr) as new_gid,
						w_100_grid_all.*
				from
						w_100_grid_all
				)
-----------------
,w_50_cover as 	(
				select
				50000 as scalex, -50000 as  scaley,
				--------------------------------range of CR----------------------------
			    -- more precisely the borders of CR from EUROSTAT with a 10 km buffer
			    ST_GeomFromText('POLYGON((4400000 2800000, 5000000 2800000, 5000000 3200000, 4400000 3200000, 4400000 2800000))', 3035) as extent
				)
,w_50_grid_1 as	(
				SELECT 
					(ST_PixelAsPolygons(ST_AddBand(ST_MakeEmptyRaster(
					((ST_Xmax(w_50_cover.extent) - ST_Xmin(w_50_cover.extent)) / scalex)::int, ((ST_Ymax(w_50_cover.extent) - ST_Ymin(w_50_cover.extent)) / abs(scaley))::int,
					ST_Xmin(w_50_cover.extent),
					ST_Ymax(w_50_cover.extent),
					scalex, scaley, 0, 0, 3035), '8BSI'::text, 1, 0), 1, false)).geom
				from w_50_cover
				)
,w_50_grid_2 as	(
				select
					row_number() over() as gid,
					format('%s-INSPIRE', '50km') as cell_collection,
					concat('50km', 'N', (ST_ymin(geom)/1000)::int::varchar, 'E', (ST_xmin(geom)/1000)::int::varchar) as label,
				    	format('The %s-INSPIRE grid cell, the south-west corner of which is located %s km north and %s km east of the false origin of the ETRS89-extended / LAEA - Europe projection system.',
						'50km', (ST_ymin(geom)/1000)::int::varchar, (ST_xmin(geom)/1000)::int::varchar) as description,
					geom
				from w_50_grid_1
				)
,w_50_grid_all as	(
				select w_50_grid_2.gid, w_50_grid_2.cell_collection, w_50_grid_2.label, w_50_grid_2.description, w_50_grid_2.geom,
				st_centroid(w_50_grid_2.geom) as geom_centroid
				from w_50_grid_2, gisdata.nuts1_gen as t where st_intersects(w_50_grid_2.geom, t.geom)
				)
,w_50_grid as	(
				select
						row_number() over(order by w_50_grid_all.gid) + (select max(new_gid) from w_100_grid) as new_gid,
						w_50_grid_all.*
				from w_50_grid_all
				)
-----------------
,w_25_cover as 	(
				select
				25000 as scalex, -25000 as  scaley,
				--------------------------------range of CR----------------------------
			    -- more precisely the borders of CR from EUROSTAT with a 10 km buffer
			    ST_GeomFromText('POLYGON((4400000 2800000, 5000000 2800000, 5000000 3200000, 4400000 3200000, 4400000 2800000))', 3035) as extent
				)
,w_25_grid_1 as	(
				SELECT 
					(ST_PixelAsPolygons(ST_AddBand(ST_MakeEmptyRaster(
					((ST_Xmax(w_25_cover.extent) - ST_Xmin(w_25_cover.extent)) / scalex)::int, ((ST_Ymax(w_25_cover.extent) - ST_Ymin(w_25_cover.extent)) / abs(scaley))::int,
					ST_Xmin(w_25_cover.extent),
					ST_Ymax(w_25_cover.extent),
					scalex, scaley, 0, 0, 3035), '8BSI'::text, 1, 0), 1, false)).geom
				from w_25_cover
				)
,w_25_grid_2 as	(
				select
					row_number() over() as gid,
					format('%s-INSPIRE', '25km') as cell_collection,
					concat('25km', 'N', (ST_ymin(geom)/1000)::int::varchar, 'E', (ST_xmin(geom)/1000)::int::varchar) as label,
				    	format('The %s-INSPIRE grid cell, the south-west corner of which is located %s km north and %s km east of the false origin of the ETRS89-extended / LAEA - Europe projection system.',
						'25km', (ST_ymin(geom)/1000)::int::varchar, (ST_xmin(geom)/1000)::int::varchar) as description,
					geom
				from w_25_grid_1
				)
,w_25_grid_all as	(
				select w_25_grid_2.gid, w_25_grid_2.cell_collection, w_25_grid_2.label, w_25_grid_2.description, w_25_grid_2.geom,
				st_centroid(w_25_grid_2.geom) as geom_centroid
				from w_25_grid_2, gisdata.nuts1_gen as t where st_intersects(w_25_grid_2.geom, t.geom)
				)
,w_25_grid as	(
				select
						row_number() over(order by w_25_grid_all.gid) + (select max(new_gid) from w_50_grid) as new_gid,
						w_25_grid_all.*
				from
						w_25_grid_all
				)
-----------------				
,w_1_cover as 	(
				select
				1000 as scalex, -1000 as  scaley,
				--------------------------------range of CR----------------------------
			    -- more precisely the borders of CR from EUROSTAT with a 10 km buffer
			    ST_GeomFromText('POLYGON((4400000 2800000, 5000000 2800000, 5000000 3200000, 4400000 3200000, 4400000 2800000))', 3035) as extent
				)
,w_1_grid_1 as	(
				SELECT 
					(ST_PixelAsPolygons(ST_AddBand(ST_MakeEmptyRaster(
					((ST_Xmax(w_1_cover.extent) - ST_Xmin(w_1_cover.extent)) / scalex)::int, ((ST_Ymax(w_1_cover.extent) - ST_Ymin(w_1_cover.extent)) / abs(scaley))::int,
					ST_Xmin(w_1_cover.extent),
					ST_Ymax(w_1_cover.extent),
					scalex, scaley, 0, 0, 3035), '8BSI'::text, 1, 0), 1, false)).geom
				from w_1_cover
				)
,w_1_grid_2 as	(
				select
					row_number() over() as gid,
					format('%s-INSPIRE', '1km') as cell_collection,
					concat('1km', 'N', (ST_ymin(geom)/1000)::int::varchar, 'E', (ST_xmin(geom)/1000)::int::varchar) as label,
				    	format('The %s-INSPIRE grid cell, the south-west corner of which is located %s km north and %s km east of the false origin of the ETRS89-extended / LAEA - Europe projection system.',
						'1km', (ST_ymin(geom)/1000)::int::varchar, (ST_xmin(geom)/1000)::int::varchar) as description,
					geom
				from w_1_grid_1
				)
,w_1_grid_all as	(
					select w_1_grid_2.gid, w_1_grid_2.cell_collection, w_1_grid_2.label, w_1_grid_2.description, w_1_grid_2.geom,
					st_centroid(w_1_grid_2.geom) as geom_centroid
					from w_1_grid_2, gisdata.nuts1_gen as t where st_intersects(w_1_grid_2.geom, t.geom)
					)
,w_1_grid as	(
				select
						row_number() over(order by w_1_grid_all.gid) + (select max(new_gid) from w_25_grid) as new_gid,
						w_1_grid_all.*
				from
						w_1_grid_all
				)
-----------------------------------------
-----------------------------------------
-- CELL_SUP --
,w_1_grid_25 as		(select w_1_grid.*, w_25_grid.new_gid as cell_sup from w_1_grid join w_25_grid on st_intersects(w_1_grid.geom_centroid, w_25_grid.geom))
,w_25_grid_50 as	(select w_25_grid.*, w_50_grid.new_gid as cell_sup from w_25_grid join w_50_grid on st_intersects(w_25_grid.geom_centroid, w_50_grid.geom))
,w_50_grid_100 as	(select w_50_grid.*, w_100_grid.new_gid as cell_sup from w_50_grid join w_100_grid on st_intersects(w_50_grid.geom_centroid, w_100_grid.geom))
,w_100_grid_cr as	(select w_100_grid.*, (select max(new_gid) from w_cr) as cell_sup from w_100_grid)
-----------------------------------------
-----------------------------------------
,w_100_grid_ring as	(-- vyber co lezi na hranici CR
					select w_100_grid_cr.* from w_100_grid_cr, gisdata.nuts1_gen as t where st_intersects(w_100_grid_cr.geom, st_exteriorring(t.geom))
					)
,w_100_grid_cut as	(-- orezani a vytvoreni geometrie na hracici CR
					select w_100_grid_ring.new_gid, w_100_grid_ring.cell_collection, w_100_grid_ring.label, w_100_grid_ring.description,
					st_intersection(w_100_grid_ring.geom,t.geom) as geom, w_100_grid_ring.cell_sup
					from w_100_grid_ring, gisdata.nuts1_gen as t
					)
,w_100_grid_cut_type as	(-- prevedeni polygonu na multipolygon
						select new_gid, cell_collection, label, description, st_multi(geom) as geom, cell_sup
						from w_100_grid_cut where st_geometrytype(geom) = 'ST_Polygon'
						union all
						select new_gid, cell_collection, label, description, geom, cell_sup
						from w_100_grid_cut where st_geometrytype(geom) = 'ST_MultiPolygon'
						)
,w_100_res as	(-- kompletace
				select new_gid, cell_collection, label, description, st_multi(geom) as geom, cell_sup from w_100_grid_cr
				where new_gid not in (select new_gid from w_100_grid_cut_type)
				union all
				select new_gid, cell_collection, label, description, geom, cell_sup from w_100_grid_cut_type
				)
-----------------------------------------
,w_50_grid_ring as	(-- vyber co lezi na hranici CR
					select w_50_grid_100.* from w_50_grid_100, gisdata.nuts1_gen as t where st_intersects(w_50_grid_100.geom, st_exteriorring(t.geom))
					)
,w_50_grid_cut as	(-- orezani a vytvoreni geometrie na hracici CR
					select w_50_grid_ring.new_gid, w_50_grid_ring.cell_collection, w_50_grid_ring.label, w_50_grid_ring.description,
					st_intersection(w_50_grid_ring.geom,t.geom) as geom, w_50_grid_ring.cell_sup
					from w_50_grid_ring, gisdata.nuts1_gen as t
					)
,w_50_grid_cut_type as	(-- prevedeni polygonu na multipolygon
						select new_gid, cell_collection, label, description, st_multi(geom) as geom, cell_sup
						from w_50_grid_cut where st_geometrytype(geom) = 'ST_Polygon'
						union all
						select new_gid, cell_collection, label, description, geom, cell_sup
						from w_50_grid_cut where st_geometrytype(geom) = 'ST_MultiPolygon'
						)
,w_50_res as	(-- kompletace
				select new_gid, cell_collection, label, description, st_multi(geom) as geom, cell_sup from w_50_grid_100
				where new_gid not in (select new_gid from w_50_grid_cut_type)
				union all
				select new_gid, cell_collection, label, description, geom, cell_sup from w_50_grid_cut_type
				)
-----------------------------------------
,w_25_grid_ring as	(-- vyber co lezi na hranici CR
					select w_25_grid_50.* from w_25_grid_50, gisdata.nuts1_gen as t where st_intersects(w_25_grid_50.geom, st_exteriorring(t.geom))
					)
,w_25_grid_cut as	(-- orezani a vytvoreni geometrie na hracici CR
					select w_25_grid_ring.new_gid, w_25_grid_ring.cell_collection, w_25_grid_ring.label, w_25_grid_ring.description,
					st_intersection(w_25_grid_ring.geom,t.geom) as geom, w_25_grid_ring.cell_sup
					from w_25_grid_ring, gisdata.nuts1_gen as t
					)
,w_25_grid_cut_type as	(-- prevedeni polygonu na multipolygon
						select new_gid, cell_collection, label, description, st_multi(geom) as geom, cell_sup
						from w_25_grid_cut where st_geometrytype(geom) = 'ST_Polygon'
						union all
						select new_gid, cell_collection, label, description, geom, cell_sup
						from w_25_grid_cut where st_geometrytype(geom) = 'ST_MultiPolygon'
						)
,w_25_res as	(-- kompletace
				select new_gid, cell_collection, label, description, st_multi(geom) as geom, cell_sup from w_25_grid_50
				where new_gid not in (select new_gid from w_25_grid_cut_type)
				union all
				select new_gid, cell_collection, label, description, geom, cell_sup from w_25_grid_cut_type
				)
-----------------------------------------
,w_1_grid_ring as	(-- vyber co lezi na hranici CR
					select w_1_grid_25.* from w_1_grid_25, gisdata.nuts1_gen as t where st_intersects(w_1_grid_25.geom, st_exteriorring(t.geom))
					)
,w_1_grid_cut as	(-- orezani a vytvoreni geometrie na hracici CR
					select w_1_grid_ring.new_gid, w_1_grid_ring.cell_collection, w_1_grid_ring.label, w_1_grid_ring.description,
					st_intersection(w_1_grid_ring.geom,t.geom) as geom, w_1_grid_ring.cell_sup
					from w_1_grid_ring, gisdata.nuts1_gen as t
					)
,w_1_grid_cut_type as	(-- prevedeni polygonu na multipolygon
						select new_gid, cell_collection, label, description, st_multi(geom) as geom, cell_sup
						from w_1_grid_cut where st_geometrytype(geom) = 'ST_Polygon'
						union all
						select new_gid, cell_collection, label, description, geom, cell_sup
						from w_1_grid_cut where st_geometrytype(geom) = 'ST_MultiPolygon'
						)
,w_1_res as	(-- kompletace
				select new_gid, cell_collection, label, description, st_multi(geom) as geom, cell_sup from w_1_grid_25
				where new_gid not in (select new_gid from w_1_grid_cut_type)
				union all
				select new_gid, cell_collection, label, description, geom, cell_sup from w_1_grid_cut_type
				)
-----------------------------------------
-----------------------------------------
,w_res as	(
			select new_gid as gid, cell_collection, label, description, st_multi(geom) as geom, cell_sup from w_cr union all
			select new_gid as gid, cell_collection, label, label as description, geom, cell_sup from w_100_res union all
			select new_gid as gid, cell_collection, label, label as description, geom, cell_sup from w_50_res union all
			select new_gid as gid, cell_collection, label, label as description, geom, cell_sup from w_25_res union all
			select new_gid as gid, cell_collection, label, label as description, geom, cell_sup from w_1_res
			)
select gid, cell_collection, label, description, geom, cell_sup from w_res order by gid;

ALTER TABLE csv.inspire_grid ADD PRIMARY KEY (gid);
CREATE INDEX idx_inspire_grid_geom ON csv.inspire_grid USING gist (geom);
alter table csv.inspire_grid owner to adm_nfiesta_gisdata;
grant select on table csv.inspire_grid to public;



insert into gisdata.c_estimation_cell (id, label, description, label_en, description_en)
select
		gid,
		label,
		description,
		label,
		description
from
		csv.inspire_grid order by gid;

analyze gisdata.c_estimation_cell;


INSERT INTO gisdata.cm_estimation_cell_collection(id,estimation_cell_collection,estimation_cell_collection_lowest,estimation_cell_collection_highest)
VALUES
(1,1,5,1), -- nuts1
(2,2,5,1), -- inspire 100km
(3,3,5,1), -- inspire 50km
(4,4,5,1), -- inspire 25km
(5,5,5,1); -- inspire 1km


with
w as	(
		select
				cec.id as gid,
				geom,
				cec.id as estimation_cell 
		from
				gisdata.c_estimation_cell as cec
		inner join
				csv.inspire_grid as t
		on
				cec.label = t.label
		)
insert into gisdata.f_a_cell(gid,geom,estimation_cell)
select w.gid, w.geom, w.estimation_cell from w order by w.gid;


analyze gisdata.f_a_cell;


with
w1 as	(
		select
				ig.gid,
				ig.label,
				ig.cell_collection,
				cec.id as estimation_cell,
				cecc.id as estimation_cell_collection
		from
				csv.inspire_grid as ig
				inner join gisdata.c_estimation_cell as cec on ig.label = cec.label
				inner join gisdata.c_estimation_cell_collection as cecc on ig.cell_collection = cecc.label
		)
insert into gisdata.cm_estimation_cell(estimation_cell, estimation_cell_collection)
select w1.estimation_cell, w1.estimation_cell_collection from w1 order by gid;


analyze gisdata.cm_estimation_cell;


insert into gisdata.t_estimation_cell_hierarchy(estimation_cell, estimation_cell_superior)
select gid, cell_sup from csv.inspire_grid order by gid;


analyze gisdata.t_estimation_cell_hierarchy;


drop table if exists csv.inspire_grid;


--ALTER TABLE gisdata.f_a_cell ALTER COLUMN geom TYPE geometry(MultiPolygon, 3035) USING ST_SetSRID(geom, 3035);


--SELECT pg_catalog.setval('gisdata.c_estimation_cell_collection_id_seq', (select max(id) from gisdata.c_estimation_cell_collection), true);
--SELECT pg_catalog.setval('gisdata.c_estimation_cell_id_seq', (select max(id) from gisdata.c_estimation_cell), true);
SELECT pg_catalog.setval('gisdata.cm_estimation_cell_collection_id_seq', (select max(id) from gisdata.cm_estimation_cell_collection), true);
SELECT pg_catalog.setval('gisdata.f_a_cell_gid_seq', (select max(gid) from gisdata.f_a_cell), true);
SELECT pg_catalog.setval('gisdata.cm_estimation_cell_id_seq', (select max(id) from gisdata.cm_estimation_cell), true);
--------------------------------------------------------------------------------------------------;
-- END INSPIRE
--------------------------------------------------------------------------------------------------;


/*
--------------------------------------------------------------------------------------------------;
-- BEGIN NUTS GEN
--------------------------------------------------------------------------------------------------;
\set srcdir `echo $SRC_DIR`
--------------------------------------------------------------------------------------------------;
-- NUTS2_GEN --
--------------------------------------------------------------------------------------------------;
\set afile :srcdir '/csv/nuts2_gen.csv'
CREATE FOREIGN TABLE csv.nuts2_gen
(
	gid integer not null,
	nuts_geometry text not null,
	label text not null,
	description text not null,
	nuts1 integer not null
)
SERVER csv_files
OPTIONS ( header 'true', format 'csv', filename :'afile');

CREATE TABLE gisdata.nuts2_gen 
(
	gid integer NOT NULL,
	geom geometry(POLYGON, 3035) NOT NULL,
	label text not null,
	description text not null,
	nuts1 integer not null
);

ALTER TABLE gisdata.nuts2_gen OWNER TO adm_nfiesta_gisdata;

CREATE SEQUENCE gisdata.nuts2_gen_gid_seq
	AS integer
	START WITH 1
	INCREMENT BY 1
	NO MINVALUE
	NO MAXVALUE
	CACHE 1;

ALTER TABLE gisdata.nuts2_gen_gid_seq OWNER TO adm_nfiesta_gisdata;

ALTER SEQUENCE gisdata.nuts2_gen_gid_seq OWNED BY gisdata.nuts2_gen.gid;

ALTER TABLE ONLY gisdata.nuts2_gen ALTER COLUMN gid SET DEFAULT nextval('gisdata.nuts2_gen_gid_seq'::regclass);

ALTER TABLE ONLY gisdata.nuts2_gen ADD CONSTRAINT nuts2_gen_pkey PRIMARY KEY (gid);

ALTER TABLE gisdata.nuts2_gen ADD CONSTRAINT enforce_dims_geom CHECK (st_ndims(geom) = 2);

ALTER TABLE gisdata.nuts2_gen ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);

ALTER TABLE gisdata.nuts2_gen ADD CONSTRAINT enforce_srid_geom CHECK (st_srid(geom) = 3035);

CREATE INDEX spidx__nuts2_gen__geom ON gisdata.nuts2_gen USING gist (geom);
CREATE INDEX idx__nuts2_gen__label ON gisdata.nuts2_gen USING btree(label);
CREATE INDEX idx__nuts2_gen__nuts1 ON gisdata.nuts2_gen USING btree(nuts1);

INSERT INTO gisdata.nuts2_gen(gid,geom,label,description,nuts1)
SELECT
	gid,
	ST_GeomFromEWKT(nuts_geometry) AS geom,
	label,
	description,
	nuts1
FROM
	csv.nuts2_gen order by gid;
--------------------------------------------------------------------------------------------------;
--------------------------------------------------------------------------------------------------;
--------------------------------------------------------------------------------------------------;
-- NUTS3_GEN --
--------------------------------------------------------------------------------------------------;
\set afile :srcdir '/csv/nuts3_gen.csv'
CREATE FOREIGN TABLE csv.nuts3_gen
(
	gid integer not null,
	nuts_geometry text not null,
	label text not null,
	description text not null,
	nuts2 integer not null
)
SERVER csv_files
OPTIONS ( header 'true', format 'csv', filename :'afile');

CREATE TABLE gisdata.nuts3_gen 
(
	gid integer NOT NULL,
	geom geometry(POLYGON, 3035) NOT NULL,
	label text not null,
	description text not null,
	nuts2 integer not null
);

ALTER TABLE gisdata.nuts3_gen OWNER TO adm_nfiesta_gisdata;

CREATE SEQUENCE gisdata.nuts3_gen_gid_seq
	AS integer
	START WITH 1
	INCREMENT BY 1
	NO MINVALUE
	NO MAXVALUE
	CACHE 1;

ALTER TABLE gisdata.nuts3_gen_gid_seq OWNER TO adm_nfiesta_gisdata;

ALTER SEQUENCE gisdata.nuts3_gen_gid_seq OWNED BY gisdata.nuts3_gen.gid;

ALTER TABLE ONLY gisdata.nuts3_gen ALTER COLUMN gid SET DEFAULT nextval('gisdata.nuts3_gen_gid_seq'::regclass);

ALTER TABLE ONLY gisdata.nuts3_gen ADD CONSTRAINT nuts3_gen_pkey PRIMARY KEY (gid);

ALTER TABLE gisdata.nuts3_gen ADD CONSTRAINT enforce_dims_geom CHECK (st_ndims(geom) = 2);

ALTER TABLE gisdata.nuts3_gen ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);

ALTER TABLE gisdata.nuts3_gen ADD CONSTRAINT enforce_srid_geom CHECK (st_srid(geom) = 3035);

CREATE INDEX spidx__nuts3_gen__geom ON gisdata.nuts3_gen USING gist (geom);
CREATE INDEX idx__nuts3_gen__label ON gisdata.nuts3_gen USING btree(label);
CREATE INDEX idx__nuts3_gen__nuts2 ON gisdata.nuts3_gen USING btree(nuts2);

INSERT INTO gisdata.nuts3_gen(gid,geom,label,description,nuts2)
SELECT
	gid,
	ST_GeomFromEWKT(nuts_geometry) AS geom,
	label,
	description,
	nuts2
FROM
	csv.nuts3_gen order by gid;
--------------------------------------------------------------------------------------------------;
--------------------------------------------------------------------------------------------------;
--------------------------------------------------------------------------------------------------;
-- NUTS4_GEN --
--------------------------------------------------------------------------------------------------;
\set afile :srcdir '/csv/nuts4_gen.csv'
CREATE FOREIGN TABLE csv.nuts4_gen
(
	gid integer not null,
	nuts_geometry text not null,
	label text not null,
	description text not null,
	nuts3 integer not null
)
SERVER csv_files
OPTIONS ( header 'true', format 'csv', filename :'afile');

CREATE TABLE gisdata.nuts4_gen 
(
	gid integer NOT NULL,
	geom geometry(POLYGON, 3035) NOT NULL,
	label text not null,
	description text not null,
	nuts3 integer not null
);

ALTER TABLE gisdata.nuts4_gen OWNER TO adm_nfiesta_gisdata;

CREATE SEQUENCE gisdata.nuts4_gen_gid_seq
	AS integer
	START WITH 1
	INCREMENT BY 1
	NO MINVALUE
	NO MAXVALUE
	CACHE 1;

ALTER TABLE gisdata.nuts4_gen_gid_seq OWNER TO adm_nfiesta_gisdata;

ALTER SEQUENCE gisdata.nuts4_gen_gid_seq OWNED BY gisdata.nuts4_gen.gid;

ALTER TABLE ONLY gisdata.nuts4_gen ALTER COLUMN gid SET DEFAULT nextval('gisdata.nuts4_gen_gid_seq'::regclass);

ALTER TABLE ONLY gisdata.nuts4_gen ADD CONSTRAINT nuts4_gen_pkey PRIMARY KEY (gid);

ALTER TABLE gisdata.nuts4_gen ADD CONSTRAINT enforce_dims_geom CHECK (st_ndims(geom) = 2);

ALTER TABLE gisdata.nuts4_gen ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);

ALTER TABLE gisdata.nuts4_gen ADD CONSTRAINT enforce_srid_geom CHECK (st_srid(geom) = 3035);

CREATE INDEX spidx__nuts4_gen__geom ON gisdata.nuts4_gen USING gist (geom);
CREATE INDEX idx__nuts4_gen__label ON gisdata.nuts4_gen USING btree(label);
CREATE INDEX idx__nuts4_gen__nuts3 ON gisdata.nuts4_gen USING btree(nuts3);

INSERT INTO gisdata.nuts4_gen(gid,geom,label,description,nuts3)
SELECT
	gid,
	ST_GeomFromEWKT(nuts_geometry) AS geom,
	label,
	description,
	nuts3
FROM
	csv.nuts4_gen order by gid;
--------------------------------------------------------------------------------------------------;
--------------------------------------------------------------------------------------------------;
--------------------------------------------------------------------------------------------------;
-- NUTS2, NUTS3, NUTS4 and NUTS4-1km-INSPIRE
--------------------------------------------------------------------------------------------------;
INSERT INTO gisdata.c_estimation_cell_collection(id,label,description,label_en,description_en) VALUES
(6,	'NUTS1','Nomenclature of Territorial Units for Statistics, level 1 – state.','NUTS1','Nomenclature of Territorial Units for Statistics, level 1 – state.'),
(7,	'NUTS2','Nomenklatura územních statistických jednotek 2. úroveň – region.','NUTS2','Nomenclature of Territorial Units for Statistics, level 2 – area.'),
(8,	'NUTS3','Nomenklatura územních statistických jednotek 3. úroveň – kraj.','NUTS3','Nomenclature of Territorial Units for Statistics, level 3 – region.'),
(9,	'NUTS4','Nomenklatura územních statistických jednotek 4. úroveň – okres.','NUTS4','Nomenclature of Territorial Units for Statistics, level 4 – district.'),
(10,'NUTS4-1km-INSPIRE','A collection of estimation cells corresponding to 1km INSPIRE grid and nomenclature of Territorial Units for Statistics, level 4 – okres.','NUTS4-1km-INSPIRE','A collection of estimation cells corresponding to 1km INSPIRE grid and nomenclature of Territorial Units for Statistics, level 4 – okres.');
---------------------------------------
update gisdata.c_estimation_cell_collection set use4estimates = false where id = 10;
---------------------------------------
INSERT INTO gisdata.cm_estimation_cell_collection(id,estimation_cell_collection,estimation_cell_collection_lowest,estimation_cell_collection_highest)
VALUES
(6,6,10,6),  -- nuts1
(7,7,10,6), -- nuts2
(8,8,10,6), -- nuts3
(9,9,10,6), -- nuts4
(10,10,10,6); -- nuts4 inspire 1km
---------------------------------------
insert into gisdata.c_estimation_cell(id,label,description,label_en,description_en)
select
		gid + (select max(id) from gisdata.c_estimation_cell) as id,
		label,
		description,
		label,
		description
from
		gisdata.nuts1_gen order by gid;
---------------------------------------
insert into gisdata.c_estimation_cell(id,label,description,label_en,description_en)
select
		gid + (select max(id) from gisdata.c_estimation_cell) as id,
		label,
		description,
		label,
		description
from
		gisdata.nuts2_gen order by gid;
---------------------------------------
insert into gisdata.c_estimation_cell(id,label,description,label_en,description_en)
select
		gid + (select max(id) from gisdata.c_estimation_cell) as id,
		label,
		description,
		label,
		description	
from
		gisdata.nuts3_gen order by gid;	
---------------------------------------
insert into gisdata.c_estimation_cell(id,label,description,label_en,description_en)
select
		gid + (select max(id) from gisdata.c_estimation_cell) as id,
		label,
		description,
		label,
		description	
from
		gisdata.nuts4_gen order by gid;
---------------------------------------
---------------------------------------
with
w1 as	(
		select
				t1.id as estimation_cell,
				t2.geom
		from
			gisdata.c_estimation_cell as t1
		inner
		join
			(
			select geom, label from gisdata.nuts1_gen union all
			select geom, label from gisdata.nuts2_gen union all
			select geom, label from gisdata.nuts3_gen union all
			select geom, label from gisdata.nuts4_gen
			) as t2
		on
			t1.label = t2.label order by t1.id
		)
,w2 as	(
		select row_number() over(order by estimation_cell) as new_gid, estimation_cell, geom from w1
		)		
insert into gisdata.f_a_cell(gid,geom,estimation_cell)
select (w2.new_gid + (select max(gid) from gisdata.f_a_cell)) as gid, geom, estimation_cell from w2 order by new_gid;
---------------------------------------
---------------------------------------
analyze gisdata.c_estimation_cell;
analyze gisdata.f_a_cell;
---------------------------------------
---------------------------------------
with
w1 as	(
		select
				t1.id as estimation_cell,
				t2.estimation_cell_collection
		from
			gisdata.c_estimation_cell as t1
		inner
		join
			(
			select (select id from gisdata.c_estimation_cell_collection where label = 'NUTS1') as estimation_cell_collection, label from gisdata.nuts1_gen union all
			select (select id from gisdata.c_estimation_cell_collection where label = 'NUTS2') as estimation_cell_collection, label from gisdata.nuts2_gen union all
			select (select id from gisdata.c_estimation_cell_collection where label = 'NUTS3') as estimation_cell_collection, label from gisdata.nuts3_gen union all
			select (select id from gisdata.c_estimation_cell_collection where label = 'NUTS4') as estimation_cell_collection, label from gisdata.nuts4_gen
			) as t2
		on
			t1.label = t2.label order by t1.id
		)
insert into gisdata.cm_estimation_cell(estimation_cell, estimation_cell_collection)
select estimation_cell, estimation_cell_collection from w1 order by estimation_cell;
---------------------------------------
---------------------------------------
---------------------------------------
---------------------------------------
with
w_nuts1 as	(
			select estimation_cell, null::integer as estimation_cell_superior
			from gisdata.cm_estimation_cell where estimation_cell_collection =
			(select id from gisdata.c_estimation_cell_collection where label = 'NUTS1')
			)
,w_nuts2 as	(
			select
					t1.estimation_cell,
					--t2.label,
					--t3.nuts1,
					--t4.label as label_nuts1,
					t5.id as estimation_cell_superior
			from
						(select * from gisdata.cm_estimation_cell where estimation_cell_collection = (select id from gisdata.c_estimation_cell_collection where label = 'NUTS2')) as t1
			inner join	gisdata.c_estimation_cell as t2 on t1.estimation_cell = t2.id
			inner join	gisdata.nuts2_gen as t3 on t2.label = t3.label
			inner join	gisdata.nuts1_gen as t4 on t3.nuts1 = t4.gid
			inner join	gisdata.c_estimation_cell as t5 on t4.label = t5.label		
			)
,w_nuts3 as	(
			select
					t1.estimation_cell,
					--t2.label,
					--t3.nuts1,
					--t4.label as label_nuts1,
					t5.id as estimation_cell_superior
			from
						(select * from gisdata.cm_estimation_cell where estimation_cell_collection = (select id from gisdata.c_estimation_cell_collection where label = 'NUTS3')) as t1
			inner join	gisdata.c_estimation_cell as t2 on t1.estimation_cell = t2.id
			inner join	gisdata.nuts3_gen as t3 on t2.label = t3.label
			inner join	gisdata.nuts2_gen as t4 on t3.nuts2 = t4.gid
			inner join	gisdata.c_estimation_cell as t5 on t4.label = t5.label		
			)
,w_nuts4 as	(
			select
					t1.estimation_cell,
					--t2.label,
					--t3.nuts1,
					--t4.label as label_nuts1,
					t5.id as estimation_cell_superior
			from
						(select * from gisdata.cm_estimation_cell where estimation_cell_collection = (select id from gisdata.c_estimation_cell_collection where label = 'NUTS4')) as t1
			inner join	gisdata.c_estimation_cell as t2 on t1.estimation_cell = t2.id
			inner join	gisdata.nuts4_gen as t3 on t2.label = t3.label
			inner join	gisdata.nuts3_gen as t4 on t3.nuts3 = t4.gid
			inner join	gisdata.c_estimation_cell as t5 on t4.label = t5.label		
			)
,w_res as	(
			select * from w_nuts1 union all
			select * from w_nuts2 union all
			select * from w_nuts3 union all
			select * from w_nuts4
			)
insert into gisdata.t_estimation_cell_hierarchy(estimation_cell,estimation_cell_superior)
select estimation_cell, estimation_cell_superior from w_res order by estimation_cell;
---------------------------------------
---------------------------------------
analyze gisdata.t_estimation_cell_hierarchy;
---------------------------------------
---------------------------------------
--------------------------------------------------------------------------------------------------;
-- NUTS4_GEN_1km_INSPIRE --
--------------------------------------------------------------------------------------------------;
\set afile :srcdir '/csv/nuts4_gen_1km_inspire.csv'
CREATE FOREIGN TABLE csv.nuts4_gen_1km_inspire
(
	gid integer not null,
	cell_collection text not null,
	label text not null,
	label_nuts4 text not null,
	nuts_geometry text not null
)
SERVER csv_files
OPTIONS ( header 'true', format 'csv', filename :'afile');

CREATE TABLE gisdata.nuts4_gen_1km_inspire 
(
	gid integer NOT NULL,
	cell_collection text not null,
	label text not null,
	label_nuts4 text not null,
	geom geometry(MULTIPOLYGON, 3035) NOT NULL
);

ALTER TABLE gisdata.nuts4_gen_1km_inspire OWNER TO adm_nfiesta_gisdata;

CREATE SEQUENCE gisdata.nuts4_gen_1km_inspire_gid_seq
	AS integer
	START WITH 1
	INCREMENT BY 1
	NO MINVALUE
	NO MAXVALUE
	CACHE 1;

ALTER TABLE gisdata.nuts4_gen_1km_inspire_gid_seq OWNER TO adm_nfiesta_gisdata;

ALTER SEQUENCE gisdata.nuts4_gen_1km_inspire_gid_seq OWNED BY gisdata.nuts4_gen_1km_inspire.gid;

ALTER TABLE ONLY gisdata.nuts4_gen_1km_inspire ALTER COLUMN gid SET DEFAULT nextval('gisdata.nuts4_gen_1km_inspire_gid_seq'::regclass);

ALTER TABLE ONLY gisdata.nuts4_gen_1km_inspire ADD CONSTRAINT nuts4_gen_1km_inspire_pkey PRIMARY KEY (gid);

ALTER TABLE gisdata.nuts4_gen_1km_inspire ADD CONSTRAINT enforce_dims_geom CHECK (st_ndims(geom) = 2);

ALTER TABLE gisdata.nuts4_gen_1km_inspire ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'MULTIPOLYGON'::text);

ALTER TABLE gisdata.nuts4_gen_1km_inspire ADD CONSTRAINT enforce_srid_geom CHECK (st_srid(geom) = 3035);

CREATE INDEX spidx__nuts4_gen_1km_inspire__geom ON gisdata.nuts4_gen_1km_inspire USING gist (geom);
CREATE INDEX idx__nuts4_gen_1km_inspire__label ON gisdata.nuts4_gen_1km_inspire USING btree(label);
CREATE INDEX idx__nuts4_gen_1km_inspire__label_nuts4 ON gisdata.nuts4_gen_1km_inspire USING btree(label_nuts4);

INSERT INTO gisdata.nuts4_gen_1km_inspire(gid,cell_collection,label,label_nuts4,geom)
SELECT
	gid,
	cell_collection,
	label,
	label_nuts4,
	ST_GeomFromEWKT(nuts_geometry) AS geom
FROM
	csv.nuts4_gen_1km_inspire order by gid;
--------------------------------------------------------------------------------------------------;
--------------------------------------------------------------------------------------------------;
---------------------------------------
analyze gisdata.nuts4_gen_1km_inspire;
---------------------------------------
--------------------------------------------------------------------------------------------------;
-- END NUTS GEN
--------------------------------------------------------------------------------------------------;
*/

--------------------------------------------------------------------------------------------------;
-- BEGIN NUTS GEN APPROXIMATE
--------------------------------------------------------------------------------------------------;
\set srcdir `echo $SRC_DIR`
--------------------------------------------------------------------------------------------------;
-- NUTS1_GEN_APPROXIMATE --
--------------------------------------------------------------------------------------------------;
\set afile :srcdir '/csv/nuts1_gen_approximate.csv'
CREATE FOREIGN TABLE csv.nuts1_gen_approximate
(
	gid integer not null,
	nuts_geometry text not null,
	label text not null,
	description text not null,
	nuts1 integer not null
)
SERVER csv_files
OPTIONS ( header 'true', format 'csv', filename :'afile');

CREATE TABLE gisdata.nuts1_gen_approximate 
(
	gid integer NOT NULL,
	geom geometry(MULTIPOLYGON, 3035) NOT NULL,
	label text not null,
	description text not null,
	nuts1 integer not null
);

ALTER TABLE gisdata.nuts1_gen_approximate OWNER TO adm_nfiesta_gisdata;

CREATE SEQUENCE gisdata.nuts1_gen_approximate_gid_seq
	AS integer
	START WITH 1
	INCREMENT BY 1
	NO MINVALUE
	NO MAXVALUE
	CACHE 1;

ALTER TABLE gisdata.nuts1_gen_approximate_gid_seq OWNER TO adm_nfiesta_gisdata;

ALTER SEQUENCE gisdata.nuts1_gen_approximate_gid_seq OWNED BY gisdata.nuts1_gen_approximate.gid;

ALTER TABLE ONLY gisdata.nuts1_gen_approximate ALTER COLUMN gid SET DEFAULT nextval('gisdata.nuts1_gen_approximate_gid_seq'::regclass);

ALTER TABLE ONLY gisdata.nuts1_gen_approximate ADD CONSTRAINT nuts1_gen_approximate_pkey PRIMARY KEY (gid);

ALTER TABLE gisdata.nuts1_gen_approximate ADD CONSTRAINT enforce_dims_geom CHECK (st_ndims(geom) = 2);

ALTER TABLE gisdata.nuts1_gen_approximate ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'MULTIPOLYGON'::text);

ALTER TABLE gisdata.nuts1_gen_approximate ADD CONSTRAINT enforce_srid_geom CHECK (st_srid(geom) = 3035);

CREATE INDEX spidx__nuts1_gen_approximate__geom ON gisdata.nuts1_gen_approximate USING gist (geom);
CREATE INDEX idx__nuts1_gen_approximate__label ON gisdata.nuts1_gen_approximate USING btree(label);
CREATE INDEX idx__nuts1_gen_approximate__nuts1 ON gisdata.nuts1_gen_approximate USING btree(nuts1);

INSERT INTO gisdata.nuts1_gen_approximate(gid,geom,label,description,nuts1)
SELECT
	gid,
	ST_GeomFromEWKT(nuts_geometry) AS geom,
	label,
	description,
	nuts1
FROM
	csv.nuts1_gen_approximate order by gid;
--------------------------------------------------------------------------------------------------;
--------------------------------------------------------------------------------------------------;


--------------------------------------------------------------------------------------------------;
-- NUTS2_GEN_APPROXIMATE --
--------------------------------------------------------------------------------------------------;
\set afile :srcdir '/csv/nuts2_gen_approximate.csv'
CREATE FOREIGN TABLE csv.nuts2_gen_approximate
(
	gid integer not null,
	nuts_geometry text not null,
	label text not null,
	description text not null,
	nuts1 integer not null
)
SERVER csv_files
OPTIONS ( header 'true', format 'csv', filename :'afile');

CREATE TABLE gisdata.nuts2_gen_approximate 
(
	gid integer NOT NULL,
	geom geometry(MULTIPOLYGON, 3035) NOT NULL,
	label text not null,
	description text not null,
	nuts1 integer not null
);

ALTER TABLE gisdata.nuts2_gen_approximate OWNER TO adm_nfiesta_gisdata;

CREATE SEQUENCE gisdata.nuts2_gen_approximate_gid_seq
	AS integer
	START WITH 1
	INCREMENT BY 1
	NO MINVALUE
	NO MAXVALUE
	CACHE 1;

ALTER TABLE gisdata.nuts2_gen_approximate_gid_seq OWNER TO adm_nfiesta_gisdata;

ALTER SEQUENCE gisdata.nuts2_gen_approximate_gid_seq OWNED BY gisdata.nuts2_gen_approximate.gid;

ALTER TABLE ONLY gisdata.nuts2_gen_approximate ALTER COLUMN gid SET DEFAULT nextval('gisdata.nuts2_gen_approximate_gid_seq'::regclass);

ALTER TABLE ONLY gisdata.nuts2_gen_approximate ADD CONSTRAINT nuts2_gen_approximate_pkey PRIMARY KEY (gid);

ALTER TABLE gisdata.nuts2_gen_approximate ADD CONSTRAINT enforce_dims_geom CHECK (st_ndims(geom) = 2);

ALTER TABLE gisdata.nuts2_gen_approximate ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'MULTIPOLYGON'::text);

ALTER TABLE gisdata.nuts2_gen_approximate ADD CONSTRAINT enforce_srid_geom CHECK (st_srid(geom) = 3035);

CREATE INDEX spidx__nuts2_gen_approximate__geom ON gisdata.nuts2_gen_approximate USING gist (geom);
CREATE INDEX idx__nuts2_gen_approximate__label ON gisdata.nuts2_gen_approximate USING btree(label);
CREATE INDEX idx__nuts2_gen_approximate__nuts1 ON gisdata.nuts2_gen_approximate USING btree(nuts1);

INSERT INTO gisdata.nuts2_gen_approximate(gid,geom,label,description,nuts1)
SELECT
	gid,
	ST_GeomFromEWKT(nuts_geometry) AS geom,
	label,
	description,
	nuts1
FROM
	csv.nuts2_gen_approximate order by gid;
--------------------------------------------------------------------------------------------------;
--------------------------------------------------------------------------------------------------;


--------------------------------------------------------------------------------------------------;
-- NUTS3_GEN_APPROXIMATE --
--------------------------------------------------------------------------------------------------;
\set afile :srcdir '/csv/nuts3_gen_approximate.csv'
CREATE FOREIGN TABLE csv.nuts3_gen_approximate
(
	gid integer not null,
	nuts_geometry text not null,
	label text not null,
	description text not null,
	nuts2 integer not null
)
SERVER csv_files
OPTIONS ( header 'true', format 'csv', filename :'afile');

CREATE TABLE gisdata.nuts3_gen_approximate 
(
	gid integer NOT NULL,
	geom geometry(MULTIPOLYGON, 3035) NOT NULL,
	label text not null,
	description text not null,
	nuts2 integer not null
);

ALTER TABLE gisdata.nuts3_gen_approximate OWNER TO adm_nfiesta_gisdata;

CREATE SEQUENCE gisdata.nuts3_gen_approximate_gid_seq
	AS integer
	START WITH 1
	INCREMENT BY 1
	NO MINVALUE
	NO MAXVALUE
	CACHE 1;

ALTER TABLE gisdata.nuts3_gen_approximate_gid_seq OWNER TO adm_nfiesta_gisdata;

ALTER SEQUENCE gisdata.nuts3_gen_approximate_gid_seq OWNED BY gisdata.nuts3_gen_approximate.gid;

ALTER TABLE ONLY gisdata.nuts3_gen_approximate ALTER COLUMN gid SET DEFAULT nextval('gisdata.nuts3_gen_approximate_gid_seq'::regclass);

ALTER TABLE ONLY gisdata.nuts3_gen_approximate ADD CONSTRAINT nuts3_gen_approximate_pkey PRIMARY KEY (gid);

ALTER TABLE gisdata.nuts3_gen_approximate ADD CONSTRAINT enforce_dims_geom CHECK (st_ndims(geom) = 2);

ALTER TABLE gisdata.nuts3_gen_approximate ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'MULTIPOLYGON'::text);

ALTER TABLE gisdata.nuts3_gen_approximate ADD CONSTRAINT enforce_srid_geom CHECK (st_srid(geom) = 3035);

CREATE INDEX spidx__nuts3_gen_approximate__geom ON gisdata.nuts3_gen_approximate USING gist (geom);
CREATE INDEX idx__nuts3_gen_approximate__label ON gisdata.nuts3_gen_approximate USING btree(label);
CREATE INDEX idx__nuts3_gen_approximate__nuts2 ON gisdata.nuts3_gen_approximate USING btree(nuts2);

INSERT INTO gisdata.nuts3_gen_approximate(gid,geom,label,description,nuts2)
SELECT
	gid,
	ST_GeomFromEWKT(nuts_geometry) AS geom,
	label,
	description,
	nuts2
FROM
	csv.nuts3_gen_approximate order by gid;
--------------------------------------------------------------------------------------------------;
--------------------------------------------------------------------------------------------------;


--------------------------------------------------------------------------------------------------;
-- NUTS4_GEN_APPROXIMATE --
--------------------------------------------------------------------------------------------------;
\set afile :srcdir '/csv/nuts4_gen_approximate.csv'
CREATE FOREIGN TABLE csv.nuts4_gen_approximate
(
	gid integer not null,
	nuts_geometry text not null,
	label text not null,
	description text not null,
	nuts3 integer not null
)
SERVER csv_files
OPTIONS ( header 'true', format 'csv', filename :'afile');

CREATE TABLE gisdata.nuts4_gen_approximate 
(
	gid integer NOT NULL,
	geom geometry(MULTIPOLYGON, 3035) NOT NULL,
	label text not null,
	description text not null,
	nuts3 integer not null
);

ALTER TABLE gisdata.nuts4_gen_approximate OWNER TO adm_nfiesta_gisdata;

CREATE SEQUENCE gisdata.nuts4_gen_approximate_gid_seq
	AS integer
	START WITH 1
	INCREMENT BY 1
	NO MINVALUE
	NO MAXVALUE
	CACHE 1;

ALTER TABLE gisdata.nuts4_gen_approximate_gid_seq OWNER TO adm_nfiesta_gisdata;

ALTER SEQUENCE gisdata.nuts4_gen_approximate_gid_seq OWNED BY gisdata.nuts4_gen_approximate.gid;

ALTER TABLE ONLY gisdata.nuts4_gen_approximate ALTER COLUMN gid SET DEFAULT nextval('gisdata.nuts4_gen_approximate_gid_seq'::regclass);

ALTER TABLE ONLY gisdata.nuts4_gen_approximate ADD CONSTRAINT nuts4_gen_approximate_pkey PRIMARY KEY (gid);

ALTER TABLE gisdata.nuts4_gen_approximate ADD CONSTRAINT enforce_dims_geom CHECK (st_ndims(geom) = 2);

ALTER TABLE gisdata.nuts4_gen_approximate ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'MULTIPOLYGON'::text);

ALTER TABLE gisdata.nuts4_gen_approximate ADD CONSTRAINT enforce_srid_geom CHECK (st_srid(geom) = 3035);

CREATE INDEX spidx__nuts4_gen_approximate__geom ON gisdata.nuts4_gen_approximate USING gist (geom);
CREATE INDEX idx__nuts4_gen_approximate__label ON gisdata.nuts4_gen_approximate USING btree(label);
CREATE INDEX idx__nuts4_gen_approximate__nuts3 ON gisdata.nuts4_gen_approximate USING btree(nuts3);

INSERT INTO gisdata.nuts4_gen_approximate(gid,geom,label,description,nuts3)
SELECT
	gid,
	ST_GeomFromEWKT(nuts_geometry) AS geom,
	label,
	description,
	nuts3
FROM
	csv.nuts4_gen_approximate order by gid;
--------------------------------------------------------------------------------------------------;
--------------------------------------------------------------------------------------------------;
--------------------------------------------------------------------------------------------------;
-- NUTS1, NUTS2, NUTS3, NUTS4 and NUTS4-1km-INSPIRE => APPROXIMATE
--------------------------------------------------------------------------------------------------;
INSERT INTO gisdata.c_estimation_cell_collection(id,label,description,label_en,description_en) VALUES
(11,	'NUTS1-APPROXIMATE','Nomenclature of Territorial Units for Statistics, level 1 – state.','NUTS1-APPROXIMATE','Nomenclature of Territorial Units for Statistics, level 1 – state.'),
(12,	'NUTS2-APPROXIMATE','Nomenklatura územních statistických jednotek 2. úroveň – region.','NUTS2-APPROXIMATE','Nomenclature of Territorial Units for Statistics, level 2 – area.'),
(13,	'NUTS3-APPROXIMATE','Nomenklatura územních statistických jednotek 3. úroveň – kraj.','NUTS3-APPROXIMATE','Nomenclature of Territorial Units for Statistics, level 3 – region.'),
(14,	'NUTS4-APPROXIMATE','Nomenklatura územních statistických jednotek 4. úroveň – okres.','NUTS4-APPROXIMATE','Nomenclature of Territorial Units for Statistics, level 4 – district.'),
(15,	'NUTS4-1km-inspire-APPROXIMATE','A collection of estimation cells corresponding to 1km INSPIRE grid and nomenclature of Territorial Units for Statistics, level 4 – okres.','NUTS4-1km-inspire-APPROXIMATE','A collection of estimation cells corresponding to 1km INSPIRE grid and nomenclature of Territorial Units for Statistics, level 4 – okres.');
---------------------------------------	
update gisdata.c_estimation_cell_collection set use4estimates = false where id = 15;
---------------------------------------
INSERT INTO gisdata.cm_estimation_cell_collection(id,estimation_cell_collection,estimation_cell_collection_lowest,estimation_cell_collection_highest)
VALUES
(11,11,15,11), -- nuts1-approximate
(12,12,15,11), -- nuts2-approximate
(13,13,15,11), -- nuts3-approximate
(14,14,15,11), -- nuts4-approximate
(15,15,15,11); -- nuts4-1km-inspire-approximate
---------------------------------------
insert into gisdata.c_estimation_cell(id,label,description,label_en,description_en)
select
		gid + (select max(id) from gisdata.c_estimation_cell) as id,
		'CZ0 [APPROXIMATE]'::varchar as label,
		'CZ0 – Česká republika [APPROXIMATE]'::text as description,
		'CZ0 [APPROXIMATE]'::varchar as label_en,
		'CZ0 – Česká republika [APPROXIMATE]'::text as description_en
from
		gisdata.nuts1_gen_approximate order by gid;
---------------------------------------
insert into gisdata.c_estimation_cell(id,label,description,label_en,description_en)
select
		gid + (select max(id) from gisdata.c_estimation_cell) as id,
		label,
		description,
		label,
		description
from
		gisdata.nuts2_gen_approximate order by gid;
---------------------------------------
insert into gisdata.c_estimation_cell(id,label,description,label_en,description_en)
select
		gid + (select max(id) from gisdata.c_estimation_cell) as id,
		label,
		description,
		label,
		description	
from
		gisdata.nuts3_gen_approximate order by gid;
---------------------------------------
insert into gisdata.c_estimation_cell(id,label,description,label_en,description_en)
select
		gid + (select max(id) from gisdata.c_estimation_cell) as id,
		label,
		description,
		label,
		description	
from
		gisdata.nuts4_gen_approximate order by gid;
---------------------------------------
---------------------------------------	
with
w1 as	(
		select
				t1.id as estimation_cell,
				t2.geom
		from
			gisdata.c_estimation_cell as t1
		inner
		join
			(
			select geom, label || ' [APPROXIMATE]' as label from gisdata.nuts1_gen_approximate union all
			select geom, label from gisdata.nuts2_gen_approximate union all
			select geom, label from gisdata.nuts3_gen_approximate union all
			select geom, label from gisdata.nuts4_gen_approximate
			) as t2
		on
			t1.label = t2.label order by t1.id
		)
,w2 as	(
		select row_number() over(order by estimation_cell) as new_gid, estimation_cell, geom from w1
		)		
insert into gisdata.f_a_cell(gid,geom,estimation_cell)
select (w2.new_gid + (select max(gid) from gisdata.f_a_cell)) as gid, geom, estimation_cell from w2 order by new_gid;
---------------------------------------
---------------------------------------
analyze gisdata.c_estimation_cell;
analyze gisdata.f_a_cell;
---------------------------------------
---------------------------------------
with
w1 as	(
		select
				t1.id as estimation_cell,
				t2.estimation_cell_collection
		from
			gisdata.c_estimation_cell as t1
		inner
		join
			(
			select (select id from gisdata.c_estimation_cell_collection where label = 'NUTS1-APPROXIMATE') as estimation_cell_collection, label || ' [APPROXIMATE]' as label from gisdata.nuts1_gen_approximate union all
			select (select id from gisdata.c_estimation_cell_collection where label = 'NUTS2-APPROXIMATE') as estimation_cell_collection, label from gisdata.nuts2_gen_approximate union all
			select (select id from gisdata.c_estimation_cell_collection where label = 'NUTS3-APPROXIMATE') as estimation_cell_collection, label from gisdata.nuts3_gen_approximate union all
			select (select id from gisdata.c_estimation_cell_collection where label = 'NUTS4-APPROXIMATE') as estimation_cell_collection, label from gisdata.nuts4_gen_approximate
			) as t2
		on
			t1.label = t2.label order by t1.id
		)
insert into gisdata.cm_estimation_cell(estimation_cell, estimation_cell_collection)
select estimation_cell, estimation_cell_collection from w1 order by estimation_cell;
---------------------------------------
---------------------------------------
with
w_nuts1 as	(
			select estimation_cell, null::integer as estimation_cell_superior
			from gisdata.cm_estimation_cell where estimation_cell_collection =
			(select id from gisdata.c_estimation_cell_collection where label = 'NUTS1-APPROXIMATE')
			)
,w_nuts2 as	(
			select
					t1.estimation_cell,
					--t2.label,
					--t3.nuts1,
					--t4.label as label_nuts1,
					t5.id as estimation_cell_superior
			from
						(select * from gisdata.cm_estimation_cell where estimation_cell_collection = (select id from gisdata.c_estimation_cell_collection where label = 'NUTS2-APPROXIMATE')) as t1
			inner join	gisdata.c_estimation_cell as t2 on t1.estimation_cell = t2.id
			inner join	gisdata.nuts2_gen_approximate as t3 on t2.label = t3.label
			inner join	gisdata.nuts1_gen_approximate as t4 on t3.nuts1 = t4.gid
			inner join	gisdata.c_estimation_cell as t5 on t4.label || ' [APPROXIMATE]' = t5.label		
			)
,w_nuts3 as	(
			select
					t1.estimation_cell,
					--t2.label,
					--t3.nuts1,
					--t4.label as label_nuts1,
					t5.id as estimation_cell_superior
			from
						(select * from gisdata.cm_estimation_cell where estimation_cell_collection = (select id from gisdata.c_estimation_cell_collection where label = 'NUTS3-APPROXIMATE')) as t1
			inner join	gisdata.c_estimation_cell as t2 on t1.estimation_cell = t2.id
			inner join	gisdata.nuts3_gen_approximate as t3 on t2.label = t3.label
			inner join	gisdata.nuts2_gen_approximate as t4 on t3.nuts2 = t4.gid
			inner join	gisdata.c_estimation_cell as t5 on t4.label = t5.label		
			)
,w_nuts4 as	(
			select
					t1.estimation_cell,
					--t2.label,
					--t3.nuts1,
					--t4.label as label_nuts1,
					t5.id as estimation_cell_superior
			from
						(select * from gisdata.cm_estimation_cell where estimation_cell_collection = (select id from gisdata.c_estimation_cell_collection where label = 'NUTS4-APPROXIMATE')) as t1
			inner join	gisdata.c_estimation_cell as t2 on t1.estimation_cell = t2.id
			inner join	gisdata.nuts4_gen_approximate as t3 on t2.label = t3.label
			inner join	gisdata.nuts3_gen_approximate as t4 on t3.nuts3 = t4.gid
			inner join	gisdata.c_estimation_cell as t5 on t4.label = t5.label		
			)
,w_res as	(
			select * from w_nuts1 union all
			select * from w_nuts2 union all
			select * from w_nuts3 union all
			select * from w_nuts4
			)
insert into gisdata.t_estimation_cell_hierarchy(estimation_cell,estimation_cell_superior)
select estimation_cell, estimation_cell_superior from w_res order by estimation_cell;
---------------------------------------
---------------------------------------
analyze gisdata.t_estimation_cell_hierarchy;
---------------------------------------
---------------------------------------
--------------------------------------------------------------------------------------------------;
-- NUTS4_GEN_1km_INSPIRE_APPROXIMATE --
--------------------------------------------------------------------------------------------------;
\set afile :srcdir '/csv/nuts4_gen_1km_inspire_approximate.csv'
CREATE FOREIGN TABLE csv.nuts4_gen_1km_inspire_approximate
(
	gid integer not null,
	cell_collection text not null,
	label text not null,
	nuts_geometry text not null,
	nuts4_label text not null,
	nuts3_label text not null,
	nuts2_label text not null,
	nuts1_label text not null
)
SERVER csv_files
OPTIONS ( header 'true', format 'csv', filename :'afile');

CREATE TABLE gisdata.nuts4_gen_1km_inspire_approximate 
(
	gid integer NOT NULL,
	cell_collection text not null,
	label text not null,
	geom geometry(MULTIPOLYGON, 3035) NOT NULL,
	nuts4_label text not null,
	nuts3_label text not null,
	nuts2_label text not null,
	nuts1_label text not null
);

ALTER TABLE gisdata.nuts4_gen_1km_inspire_approximate OWNER TO adm_nfiesta_gisdata;

CREATE SEQUENCE gisdata.nuts4_gen_1km_inspire_approximate_gid_seq
	AS integer
	START WITH 1
	INCREMENT BY 1
	NO MINVALUE
	NO MAXVALUE
	CACHE 1;

ALTER TABLE gisdata.nuts4_gen_1km_inspire_approximate_gid_seq OWNER TO adm_nfiesta_gisdata;

ALTER SEQUENCE gisdata.nuts4_gen_1km_inspire_approximate_gid_seq OWNED BY gisdata.nuts4_gen_1km_inspire_approximate.gid;

ALTER TABLE ONLY gisdata.nuts4_gen_1km_inspire_approximate ALTER COLUMN gid SET DEFAULT nextval('gisdata.nuts4_gen_1km_inspire_approximate_gid_seq'::regclass);

ALTER TABLE ONLY gisdata.nuts4_gen_1km_inspire_approximate ADD CONSTRAINT nuts4_gen_1km_inspire_approximate_pkey PRIMARY KEY (gid);

ALTER TABLE gisdata.nuts4_gen_1km_inspire_approximate ADD CONSTRAINT enforce_dims_geom CHECK (st_ndims(geom) = 2);

ALTER TABLE gisdata.nuts4_gen_1km_inspire_approximate ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'MULTIPOLYGON'::text);

ALTER TABLE gisdata.nuts4_gen_1km_inspire_approximate ADD CONSTRAINT enforce_srid_geom CHECK (st_srid(geom) = 3035);

CREATE INDEX spidx__nuts4_gen_1km_inspire_approximate__geom ON gisdata.nuts4_gen_1km_inspire_approximate USING gist (geom);
CREATE INDEX idx__nuts4_gen_1km_inspire_approximate__cell_collection ON gisdata.nuts4_gen_1km_inspire_approximate USING btree(cell_collection);
CREATE INDEX idx__nuts4_gen_1km_inspire_approximate__label ON gisdata.nuts4_gen_1km_inspire_approximate USING btree(label);
CREATE INDEX idx__nuts4_gen_1km_inspire_approximate__nuts4_label ON gisdata.nuts4_gen_1km_inspire_approximate USING btree(nuts4_label);
CREATE INDEX idx__nuts4_gen_1km_inspire_approximate__nuts3_label ON gisdata.nuts4_gen_1km_inspire_approximate USING btree(nuts3_label);
CREATE INDEX idx__nuts4_gen_1km_inspire_approximate__nuts2_label ON gisdata.nuts4_gen_1km_inspire_approximate USING btree(nuts2_label);
CREATE INDEX idx__nuts4_gen_1km_inspire_approximate__nuts1_label ON gisdata.nuts4_gen_1km_inspire_approximate USING btree(nuts1_label);
---------------------------------------
INSERT INTO gisdata.nuts4_gen_1km_inspire_approximate(gid,cell_collection,label,geom,nuts4_label,nuts3_label,nuts2_label,nuts1_label)
SELECT
	gid,
	cell_collection,
	label,
	ST_GeomFromEWKT(nuts_geometry) AS geom,
	nuts4_label,
	nuts3_label,
	nuts2_label,
	nuts1_label
FROM
	csv.nuts4_gen_1km_inspire_approximate order by gid;
---------------------------------------
analyze gisdata.nuts4_gen_1km_inspire_approximate;
---------------------------------------
with
w1 as	(
		select label from gisdata.nuts4_gen_1km_inspire_approximate
		)
,w2 as	(
		select id, label from gisdata.c_estimation_cell
		where id in	(
					select estimation_cell from gisdata.cm_estimation_cell
					where estimation_cell_collection = (select id from gisdata.c_estimation_cell_collection where label = '1km-INSPIRE')
					)
		)
,w3 as	(
		select
				w1.label,
				w2.id as estimation_cell,
				(select id from gisdata.c_estimation_cell_collection where label = 'NUTS4-1km-inspire-APPROXIMATE') as estimation_cell_collection
		from
				w1 inner join w2 on w1.label = w2.label
		)	
insert into gisdata.cm_estimation_cell(estimation_cell,estimation_cell_collection)
select estimation_cell, estimation_cell_collection from w3 order by estimation_cell;
---------------------------------------
with
w1 as	(
		select label, nuts4_label from gisdata.nuts4_gen_1km_inspire_approximate
		)
,w2 as	(
		select id, label from gisdata.c_estimation_cell
		where id in	(
					select estimation_cell from gisdata.cm_estimation_cell
					where estimation_cell_collection = (select id from gisdata.c_estimation_cell_collection where label = '1km-INSPIRE')
					)
		)
,w3 as	(
		select
				w1.label,
				w1.nuts4_label,
				w2.id as estimation_cell,
				t1.id as estimation_cell_superior
		from
				w1
				inner join w2 on w1.label = w2.label
				inner join	(
							select * from gisdata.c_estimation_cell
							where id in (
										select estimation_cell from gisdata.cm_estimation_cell
										where estimation_cell_collection = (select id from gisdata.c_estimation_cell_collection where label = 'NUTS4-APPROXIMATE')
										)
							) as t1
							on w1.nuts4_label = t1.label
		)	
insert into gisdata.t_estimation_cell_hierarchy(estimation_cell,estimation_cell_superior)
select estimation_cell, estimation_cell_superior from w3 order by estimation_cell;
---------------------------------------
analyze gisdata.cm_estimation_cell;
analyze gisdata.t_estimation_cell_hierarchy;
---------------------------------------
--------------------------------------------------------------------------------------------------;
-- END NUTS GEN APPROXIMATE
--------------------------------------------------------------------------------------------------;
--------------------------------------------------------------------------------------------------;
-- aux_total --
--------------------------------------------------------------------------------------------------;
\set afile :srcdir '/csv/aux_total.csv'
CREATE FOREIGN TABLE csv.aux_total
(
	id int4 NOT NULL,
	config int4 NOT NULL,
	estimation_cell int4 NULL,
	cell int4 NULL,
	aux_total float8 NOT NULL,
	est_date timestamp DEFAULT now() NOT NULL,
	ext_version int4 NOT NULL,
	gui_version int4 NOT NULL
)
SERVER csv_files
OPTIONS ( header 'true', format 'csv', filename :'afile');
--------------------------------------------------------------------------------------------------;

