--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--


\set srcdir `echo $SRC_DIR`


CREATE SCHEMA csv;


--------------------------------------------------------------------------------------------------;
-- OLIL --
--------------------------------------------------------------------------------------------------;
----------------------------------------------------------;
-- c_olil_2019
----------------------------------------------------------;
DROP TABLE IF EXISTS gisdata.c_olil_2019 CASCADE;

CREATE TABLE gisdata.c_olil_2019
(
	id integer NOT NULL,
	label character varying(100) NOT NULL,
	description	text,
	CONSTRAINT pkey__c_olil_2019 PRIMARY KEY (id)
);

COMMENT ON TABLE  gisdata.c_olil_2019 IS 'Dial bands in raster';
COMMENT ON COLUMN gisdata.c_olil_2019.id IS 'Code of category';
COMMENT ON COLUMN gisdata.c_olil_2019.label IS 'Label of category';
COMMENT ON COLUMN gisdata.c_olil_2019.description IS 'Description of category';
COMMENT ON CONSTRAINT pkey__c_olil_2019 ON gisdata.c_olil_2019 IS 'Primay key of the table';

ALTER TABLE gisdata.c_olil_2019 OWNER TO adm_nfiesta_gisdata;
GRANT ALL ON TABLE gisdata.c_olil_2019 TO adm_nfiesta_gisdata;
GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE gisdata.c_olil_2019 TO app_nfiesta_gisdata;
GRANT SELECT ON TABLE gisdata.c_olil_2019 TO public;

INSERT INTO gisdata.c_olil_2019 (id, label, description)
VALUES
(100, 'forest', 'Forest.'),
(200, 'OLWTC', 'Other Land With Tree Cover.'),
(300, 'Linear stands of woody plants', 'Linear stands of woody plants.'),
(400, 'Small patches of woody plants', 'Small patches of woody plants.'),
(500, 'Solitary trees and clumps of woody plants', 'Solitary trees and clumps of woody plants.');
--------------------------------------------------------------------------------------------------;
--------------------------------------------------------------------------------------------------;


\set afile :srcdir '/csv/olil_2019_all_clip_50x50.csv'
CREATE FOREIGN TABLE csv.olil_2019
(
	gid integer not null,
	olil_2019 integer not null,
	geom text not null
)
SERVER csv_files
OPTIONS ( header 'true', format 'csv', filename :'afile');

CREATE TABLE gisdata.olil_2019 
(
	gid integer NOT NULL,
	olil_2019 integer not null,
	geom geometry(MULTIPOLYGON, 3035) NOT NULL
);

ALTER TABLE gisdata.olil_2019 OWNER TO adm_nfiesta_gisdata;

CREATE SEQUENCE gisdata.olil_2019_gid_seq
	AS integer
	START WITH 1
	INCREMENT BY 1
	NO MINVALUE
	NO MAXVALUE
	CACHE 1;

ALTER TABLE gisdata.olil_2019_gid_seq OWNER TO adm_nfiesta_gisdata;

ALTER SEQUENCE gisdata.olil_2019_gid_seq OWNED BY gisdata.olil_2019.gid;

ALTER TABLE ONLY gisdata.olil_2019 ALTER COLUMN gid SET DEFAULT nextval('gisdata.olil_2019_gid_seq'::regclass);

ALTER TABLE ONLY gisdata.olil_2019 ADD CONSTRAINT olil_2019_pkey PRIMARY KEY (gid);

ALTER TABLE gisdata.olil_2019 ADD CONSTRAINT enforce_dims_geom CHECK (st_ndims(geom) = 2);

ALTER TABLE gisdata.olil_2019 ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'MULTIPOLYGON'::text);

ALTER TABLE gisdata.olil_2019 ADD CONSTRAINT enforce_srid_geom CHECK (st_srid(geom) = 3035);

CREATE INDEX spidx__olil_2019__geom ON gisdata.olil_2019 USING gist (geom);

ALTER TABLE gisdata.olil_2019
  ADD CONSTRAINT fkey__olil_2019__c_olil_2019 FOREIGN KEY (olil_2019)
      REFERENCES gisdata.c_olil_2019 (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__olil_2019__c_olil_2019 ON gisdata.olil_2019 IS 'Fkey on column olil_2019.';


INSERT INTO gisdata.olil_2019(olil_2019, geom)
SELECT
	olil_2019,
	st_multi(ST_GeomFromEWKT(geom)) AS geom
FROM
	csv.olil_2019;

analyze gisdata.olil_2019;
--------------------------------------------------------------------------------------------------;
-- NUTS1_GEN --
--------------------------------------------------------------------------------------------------;
\set afile :srcdir '/csv/nuts1_gen.csv'
CREATE FOREIGN TABLE csv.nuts1_gen
(
	gid integer not null,
	nuts_geometry text not null,
	label text not null,
	description text not null,
	nuts1 integer not null
)
SERVER csv_files
OPTIONS ( header 'true', format 'csv', filename :'afile');

CREATE TABLE gisdata.nuts1_gen 
(
	gid integer NOT NULL,
	geom geometry(POLYGON, 3035) NOT NULL,
	label text not null,
	description text not null,
	nuts1 integer not null
);

ALTER TABLE gisdata.nuts1_gen OWNER TO adm_nfiesta_gisdata;

CREATE SEQUENCE gisdata.nuts1_gen_gid_seq
	AS integer
	START WITH 1
	INCREMENT BY 1
	NO MINVALUE
	NO MAXVALUE
	CACHE 1;

ALTER TABLE gisdata.nuts1_gen_gid_seq OWNER TO adm_nfiesta_gisdata;

ALTER SEQUENCE gisdata.nuts1_gen_gid_seq OWNED BY gisdata.nuts1_gen.gid;

ALTER TABLE ONLY gisdata.nuts1_gen ALTER COLUMN gid SET DEFAULT nextval('gisdata.nuts1_gen_gid_seq'::regclass);

ALTER TABLE ONLY gisdata.nuts1_gen ADD CONSTRAINT nuts1_gen_pkey PRIMARY KEY (gid);

ALTER TABLE gisdata.nuts1_gen ADD CONSTRAINT enforce_dims_geom CHECK (st_ndims(geom) = 2);

ALTER TABLE gisdata.nuts1_gen ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);

ALTER TABLE gisdata.nuts1_gen ADD CONSTRAINT enforce_srid_geom CHECK (st_srid(geom) = 3035);

CREATE INDEX spidx__nuts1_gen__geom ON gisdata.nuts1_gen USING gist (geom);

INSERT INTO gisdata.nuts1_gen(gid, geom, label, description, nuts1)
SELECT
	gid,
	ST_GeomFromEWKT(nuts_geometry) AS geom,
	label,
	description,
	nuts1
FROM
	csv.nuts1_gen;
--------------------------------------------------------------------------------------------------;
--------------------------------------------------------------------------------------------------;