--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--

--
-- Data for Name: t_config_collection; Type: TABLE DATA; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

insert into gisdata.t_config_collection(id,config_function,label,aggregated,catalog_name,schema_name,table_name,column_ident,column_name,unit,description,closed,edit_date,label_en,description_en) values
(1,100,'olil_2019',false,'contrib_regression_nfiesta_gisdata_data','gisdata','olil_2019','gid','geom',0.0001,'Olil_2019.',true,now(),'olil_2019','Olil_2019.'),
(3,100,'olil_2019_agg',true,null::varchar,null::varchar,null::varchar,null::varchar,null::integer,null::float,'Olil_2019_agg.',true,now(),'olil_2019_agg','Olil_2019_agg.'),
(6,200,'tcd',false,'contrib_regression_nfiesta_gisdata_data','gisdata','r_tcd_10_100m_cr','rid','rast',0.000001,'Copernicus Tree Cover Density (TCD), year 2018. The plot value is determined as the weighted statistical mode.',true,now(),'tcd','Copernicus Tree Cover Density (TCD), year 2018. The plot value is determined as the weighted statistical mode.'),
(9,200,'fty',false,'contrib_regression_nfiesta_gisdata_data','gisdata','r_fty_10_100m_cr','rid','rast',0.0001,'Copernicus Forest Type (FTY), year 2018. The plot membership with respect to all categories (dummy coded) of FTY is determined as the weighted statistical mode.',true,now(),'fty','Copernicus Forest Type (FTY), year 2018. The plot membership with respect to all categories (dummy coded) of FTY is determined as the weighted statistical mode.'),
(10,200,'gfc',false,'contrib_regression_nfiesta_gisdata_data','gisdata','r_gfch_10_100m_cr','rid','rast',0.0001,'Forest Cover Loss year according to Global Forest Change 2000-2021 data. The plot membership with respect to all categories (dummy coded) of FCL is determined as the weighted statistical mode.',true,now(),'gfc','Forest Cover Loss year according to Global Forest Change 2000-2021 data. The plot membership with respect to all categories (dummy coded) of FCL is determined as the weighted statistical mode.');

--
-- Data for Name: t_config; Type: TABLE DATA; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

insert into gisdata.t_config(id,config_collection,config_query,label,complete,categories,condition,description,edit_date,label_en,description_en) values
(1,1,100,'forest',								null::varchar,null::varchar,	'(olil_2019 = 100)','Forest according to the OLIL map version 2019.',now(),'forest','Forest according to the OLIL map version 2019.'),
(2,1,100,'OLwTC',								null::varchar,null::varchar,	'(olil_2019 = 200)','Other land with tree cover category according to OLIL version 2019.',now(),'OLwTC','Other land with tree cover category according to OLIL version 2019.'),
(4,1,100,'small patches of trees or shrubs',	null::varchar,null::varchar,	'(olil_2019 = 400)','Small patches of trees or shrubs according to OLIL map version 2019.',now(),'small patches of trees or shrubs','Small patches of trees or shrubs according to OLIL map version 2019.'),
(5,1,100,'solitery trees and clumps',			null::varchar,null::varchar,	'(olil_2019 = 500)','Solitery trees and clumps of woody vegetation according to OLIL map version 2019.',now(),'solitery trees and clumps','Solitery trees and clumps of woody vegetation according to OLIL map version 2019.'),
(3,1,100,'linear formations',					null::varchar,null::varchar,	'(olil_2019 = 300)','Linear formations of trees according to OLIL map version 2019.',now(),'linear formations','Linear formations of trees according to OLIL map version 2019.'),
(6,1,300,'other lands',							null::varchar,'1,2,3,4,5',		null::varchar,		'Complement to the whole area.',now(),'other lands','Complement to the whole area.');

insert into gisdata.t_config(id,config_collection,config_query,label,complete,categories,description,edit_date,label_en,description_en) values
(11,3,500,'forest',null::varchar,'1','Forest according to the OLIL map version 2019.',now(),'forest','Forest according to the OLIL map version 2019.'),
(12,3,500,'OLwTC',null::varchar,'2','Other land with tree cover category according to OLIL version 2019.',now(),'OLwTC','Other land with tree cover category according to OLIL version 2019.'),
(13,3,200,'ToL',null::varchar,'3,4,5','Trees on Other Land, an aggregation of linear formations, small patches and solitery trees.',now(),'ToL','Trees on Other Land, an aggregation of linear formations, small patches and solitery trees.'),
(14,3,500,'other lands',null::varchar,'6','Complement to the whole area.',now(),'other lands','Complement to the whole area.');

insert into gisdata.t_config(id,config_collection,config_query,label,band,description,edit_date,label_en,description_en) values
(20,6,100,'val_w.mean',1,'Value of TCD layer.',now(),'val_w.mean','Value of TCD layer.');

insert into gisdata.t_config(id,config_collection,config_query,label,categories,band,reclass,description,edit_date,label_en,description_en) values
(45,9,100,'coniferous_forest',null::varchar,1,2,'Coniferous forest.',now(),'coniferous_forest','Coniferous forest.'),
(44,9,100,'broadleaved_forest',null::varchar,1,1,'Broadleaved forest.',now(),'broadleaved_forest','Broadleaved forest.'),
(46,9,300,'non_forest','44,45',null::int,null::int,'Non forest land.',now(),'non_forest','Non forest land.');

insert into gisdata.t_config(id,config_collection,config_query,label,categories,band,reclass,description,edit_date,label_en,description_en) values
(47,10,100,'y2011',null::varchar,1,11,'Forest cover loss which occurred in 2011.',now(),'y2011','Forest cover loss which occurred in 2011.'),
(48,10,100,'y2012',null::varchar,1,12,'Forest cover loss which occurred in 2012.',now(),'y2012','Forest cover loss which occurred in 2012.'),
(49,10,100,'y2013',null::varchar,1,13,'Forest cover loss which occurred in 2013.',now(),'y2013','Forest cover loss which occurred in 2013.'),
(50,10,100,'y2014',null::varchar,1,14,'Forest cover loss which occurred in 2014.',now(),'y2014','Forest cover loss which occurred in 2014.'),
(51,10,100,'y2015',null::varchar,1,15,'Forest cover loss which occurred in 2015.',now(),'y2015','Forest cover loss which occurred in 2015.'),
(52,10,100,'y2016',null::varchar,1,16,'Forest cover loss which occurred in 2016.',now(),'y2016','Forest cover loss which occurred in 2016.'),
(53,10,100,'y2017',null::varchar,1,17,'Forest cover loss which occurred in 2017.',now(),'y2017','Forest cover loss which occurred in 2017.'),
(54,10,100,'y2018',null::varchar,1,18,'Forest cover loss which occurred in 2018.',now(),'y2018','Forest cover loss which occurred in 2018.'),
(55,10,100,'y2019',null::varchar,1,19,'Forest cover loss which occurred in 2019.',now(),'y2019','Forest cover loss which occurred in 2019.'),
(56,10,100,'y2020',null::varchar,1,20,'Forest cover loss which occurred in 2020.',now(),'y2020','Forest cover loss which occurred in 2020.'),
(57,10,300,'no_change','47,48,49,50,51,52,53,54,55,56',null::int,null::int,'No forest cover loss between 2000 and 2021.',now(),'no_change','No forest cover loss between 2000 and 2021.');

