-- OLIL
-- config = 1, step = 1
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(1,array[184],900) where step = 1)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 1, step = 2
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(1,array[184],900) where step = 2)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 2, step = 1
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(2,array[184],900) where step = 1)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 2, step = 2
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(2,array[184],900) where step = 2)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 3, step = 1
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(3,array[184],900) where step = 1)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 3, step = 2
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(3,array[184],900) where step = 2)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 4, step = 1
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(4,array[184],900) where step = 1)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 4, step = 2
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(4,array[184],900) where step = 2)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 5, step = 1
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(5,array[184],900) where step = 1)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 5, step = 2
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(5,array[184],900) where step = 2)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 6, step = 1
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(6,array[184],900) where step = 1)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 6, step = 2
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(6,array[184],900) where step = 2)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;

-- TCD
-- config = 20, step = 1
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(20,array[184],900) where step = 1)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 20, step = 2
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(20,array[184],900) where step = 2)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;

-- FTY
-- config = 44, step = 1
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(44,array[184],900) where step = 1)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 44, step = 2
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(44,array[184],900) where step = 2)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 45, step = 1
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(45,array[184],900) where step = 1)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 45, step = 2
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(45,array[184],900) where step = 2)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 46, step = 1
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(46,array[184],900) where step = 1)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 46, step = 2
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(46,array[184],900) where step = 2)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;

-- GFC
-- config = 47, step = 1
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(47,array[184],900) where step = 1)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 47, step = 2
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(47,array[184],900) where step = 2)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 48, step = 1
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(48,array[184],900) where step = 1)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 48, step = 2
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(48,array[184],900) where step = 2)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 49, step = 1
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(49,array[184],900) where step = 1)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 49, step = 2
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(49,array[184],900) where step = 2)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 50, step = 1
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(50,array[184],900) where step = 1)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 50, step = 2
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(50,array[184],900) where step = 2)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 51, step = 1
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(51,array[184],900) where step = 1)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 51, step = 2
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(51,array[184],900) where step = 2)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 52, step = 1
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(52,array[184],900) where step = 1)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 52, step = 2
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(52,array[184],900) where step = 2)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 53, step = 1
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(53,array[184],900) where step = 1)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 53, step = 2
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(53,array[184],900) where step = 2)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 54, step = 1
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(54,array[184],900) where step = 1)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 54, step = 2
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(54,array[184],900) where step = 2)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 55, step = 1
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(55,array[184],900) where step = 1)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 55, step = 2
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(55,array[184],900) where step = 2)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 56, step = 1
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(56,array[184],900) where step = 1)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 56, step = 2
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(56,array[184],900) where step = 2)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 57, step = 1
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(57,array[184],900) where step = 1)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;
-- config = 57, step = 2
with w1 as (select * from gisdata.fn_get_gids4aux_total_app(57,array[184],900) where step = 2)
select case when (gisdata.fn_api_make_aux_total(config_id,estimation_cell,gid,900)) is null then 1 else 0 end as res from w1;

-- check aux totals between csv.aux_total and gisdata.t_aux_total table
with
w1 as	(
		select
				t1.config,
				t1.estimation_cell,
				t1.aux_total as aux_total_t1,
				t2.aux_total as aux_total_t2
		from
				csv.aux_total as t1
				inner join gisdata.t_aux_total as t2
		on
				t1.config = t2.config and t1.estimation_cell = t2.estimation_cell
		)
,w2 as	(
		select
				w1.*,
				abs(w1.aux_total_t1 - w1.aux_total_t1) as rozdil
		from
				w1
		)
select count(*) from w2 where rozdil > 0.0000;