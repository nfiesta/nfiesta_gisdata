-- regression test for some API functions ------------------------------------
select * from gisdata.fn_check_cell_sup_app();

with
w1 as	(select fn_get_lowest_gids_app as gids from gisdata.fn_get_lowest_gids_app(array[1]))
,w2 as	(select unnest(w1.gids) as gids from w1)
select w2.gids from w2 order by w2.gids;

with
w1 as	(select fn_get_lowest_gids_app as gids from gisdata.fn_get_lowest_gids_app(array[80379]))
,w2 as	(select unnest(w1.gids) as gids from w1)
select w2.gids from w2 order by w2.gids;

select * from gisdata.fn_api_get_estimation_cell_hierarchy() order by id;

select * from gisdata.fn_api_get_estimation_cell_hierarchy_stage(1) order by id;